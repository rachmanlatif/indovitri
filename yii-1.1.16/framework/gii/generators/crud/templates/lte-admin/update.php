<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>

<div>
	<h1 class="left">Update <?php echo $this->modelClass; ?></h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo "<?php echo \$this->createUrl('index'); ?>"; ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo "<?php echo \$this->createUrl('add'); ?>"; ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo "<?php echo \$this->createUrl('view', array('id'=>\$model->". $this->tableSchema->primaryKey .")); ?>"; ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo "<?php echo \$this->renderPartial('_form', array(
    'model'=>\$model,
)); ?>";
?>