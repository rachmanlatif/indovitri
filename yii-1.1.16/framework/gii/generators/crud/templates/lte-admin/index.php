<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php $grid_id= $this->class2id($this->modelClass).'-grid'; ?>
<?php
echo "<?php\n";
?>

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('<?php echo $this->class2id($this->modelClass); ?>-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<div>
	<h1 class="left">Manage <?php echo $this->pluralize($this->class2name($this->modelClass)); ?></h1>	
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo "<?php echo \$this->createUrl('add'); ?>"; ?>">Add</a>
		<?php echo "<?php echo CHtml::ajaxLink(\"Delete\", 
				\$this->createUrl('deleteSelected'), 
				array(
					'type' => 'post',
					'data' => 'js:{ajax:true, ids:$.fn.yiiGridView.getSelection(\'". $this->class2id($this->modelClass) ."-grid\')}',
					'success' => 'function(data) {
						$.fn.yiiGridView.update(\'". $this->class2id($this->modelClass) ."-grid\');
					}',
				),
				array(
					'class' => 'form-button btn btn-primary',
					'confirm' => 'Are you sure?',
				)
			);
		?>"; ?>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo "<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>"; ?>

<div class="search-form" style="display:none">
<?php echo "<?php \$this->renderPartial('_search',array(
	'model'=>\$model,
)); ?>\n"; ?>
</div><!-- search-form -->

<?php echo "<?php"; ?> $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'<?php echo $grid_id; ?>',
    'itemsCssClass'=>'table table-striped table-bordered table-hover',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'selectableRows'=>2,
	'columns'=>array(
		array(			
			'class'=>'CCheckBoxColumn',
			'id'=>'ids',
		),
<?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if(++$count==7)
		echo "\t\t/*\n";
	echo "\t\t'".$column->name."',\n";
}
if($count>=7)
	echo "\t\t*/\n";
?>
		//CGridViewHelper::getCreatedBy(),
		//CGridViewHelper::getCreatedDate(),
		//CGridViewHelper::getModifiedBy(),
		//CGridViewHelper::getModifiedDate(),
		array(
			'class'=>'CButtonColumn',
            'template'=>'{view} {update} {delete}',
            'buttons'=>array(
                'view'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-search fa-fw"></i> View',
                    'options'=>array(
                        'class'=>'btn btn-info btn-xs',
                        'title'=>'View Detail',
                    ),
                ),
                'update'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-edit fa-fw"></i> Update',
                    'options'=>array(
                        'class'=>'btn btn-warning btn-xs',
                        'title'=>'Update',
                    ),
                ),
                'delete'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-trash-o fa-fw"></i> Delete',
                    'options'=>array(
                        'class'=>'btn btn-danger btn-xs',
                        'title'=>'Delete',
                    ),
                ),
            ),
		),
	),
)); ?>
