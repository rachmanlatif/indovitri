<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<div class="form">

<?php echo "<?php \$form=\$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl(\$this->route),
	'method'=>'get',
)); ?>\n"; ?>

<?php foreach($this->tableSchema->columns as $column): ?>
<?php
	$field=$this->generateInputField($this->modelClass,$column);
	if(strpos($field,'password')!==false)
		continue;
?>
    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo "<?php echo \$form->label(\$model,'{$column->name}'); ?>\n"; ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo "<?php echo ".$this->generateActiveField($this->modelClass,$column)."; ?>\n"; ?>
                </div>
            </div>

        </div>
    </div>

<?php endforeach; ?>
	<div class="row buttons">
        <div class="col-xs-12">
            <?php echo "<button type=\"submit\" class=\"btn btn-primary\"><i class=\"fa fa-search\"></i> Search</button>"; ?>
    	</div>
	</div>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>

</div><!-- search-form -->