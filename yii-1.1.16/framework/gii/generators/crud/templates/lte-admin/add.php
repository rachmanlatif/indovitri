<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>

<div>
	<h1 class="left">Create <?php echo $this->modelClass; ?></h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo "<?php echo \$this->createUrl('index'); ?>"; ?>">List</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo "<?php echo \$this->renderPartial('_form', array(
    'model'=>\$model,
)); ?>";
?>
