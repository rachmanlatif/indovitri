<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta charset="utf-8" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <meta name="description" content="login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl;?>/frontend/images/p5logo.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/font-awesome.min.css" rel="stylesheet" />

    <!--Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

    <!--Beyond styles-->
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/beyond.min.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/demo.min.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/animate.min.css" rel="stylesheet" />
    <link id="skin-link" href="" rel="stylesheet" type="text/css" />

    <!-- Jquery -->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/jquery-2.0.3.min.js"></script>

    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/skins.min.js"></script>
</head>

<body>
<!-- Loading Container -->
<div class="loading-container">
    <div class="loading-progress">
        <div class="rotator">
            <div class="rotator">
                <div class="rotator colored">
                    <div class="rotator">
                        <div class="rotator colored">
                            <div class="rotator colored"></div>
                            <div class="rotator"></div>
                        </div>
                        <div class="rotator colored"></div>
                    </div>
                    <div class="rotator"></div>
                </div>
                <div class="rotator"></div>
            </div>
            <div class="rotator"></div>
        </div>
        <div class="rotator"></div>
    </div>
</div>
<!--  /Loading Container -->
<!-- Navbar -->
<div class="navbar">
    <div class="navbar-inner">
        <div class="navbar-container">
            <!-- Navbar Barnd -->
            <div class="navbar-header">
                <a href="#" class="navbar-brand">
                        <img width="90" height="50" src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/images/p5logo.png" alt="" />
                </a>
            </div>
            <!-- /Navbar Barnd -->

            <!-- Sidebar Collapse -->
            <div class="sidebar-collapse" id="sidebar-collapse">
                <i class="collapse-icon fa fa-bars"></i>
            </div>
            <!-- /Sidebar Collapse -->
            <!-- Account Area and Settings --->
            <div class="navbar-header pull-right">
                <div class="navbar-account">
                    <ul class="account-area">
                        <li>
                            <a class="login-area dropdown-toggle" data-toggle="dropdown">
                                <div class="avatar" title="View your public profile">
                                    <img src="<?php echo Yii::app()->theme->baseUrl;?>/backend/img/avatars/adam-jansen.jpg">
                                </div>
                                <section>
                                    <?php
                                    $username = '';
                                    $nama = '';
                                    $user = MyHelper::getUser();
                                    if($user->nama != ''){
                                        $username = $user->username;
                                        $nama = $user->nama;
                                    }
                                    ?>
                                    <h2><span class="profile"><span><?php echo ucfirst($nama); ?></span></span></h2>
                                </section>
                            </a>
                            <!--Login Area Dropdown-->
                            <ul class="pull-right dropdown-menu dropdown-arrow dropdown-login-area">
                                <li class="username"><a><?php echo $username; ?></a></li>
                                <li class="email"><a><?php echo $username; ?></a></li>
                                <!--Avatar Area-->
                                <li>
                                    <div class="avatar-area">
                                        <img src="<?php echo Yii::app()->theme->baseUrl;?>/backend/img/avatars/adam-jansen.jpg" class="avatar">
                                        <span class="caption">Change Photo</span>
                                    </div>
                                </li>
                                <!--Avatar Area-->
                                <li class="edit">
                                    <a href="<?php echo Yii::app()->baseUrl;?>/administrator/profile" class="pull-left">Profile</a>
                                </li>
                                <li class="dropdown-footer">
                                    <a href="<?php echo Yii::app()->baseUrl;?>/administrator/logout">
                                        Sign out
                                    </a>
                                </li>
                            </ul>
                            <!--/Login Area Dropdown-->
                        </li>
                        <!-- /Account Area -->
                        <!--Note: notice that setting div must start right after account area list.
                        no space must be between these elements-->
                        <!-- Settings -->
                    </ul><div class="setting">
                        <a id="btn-setting" title="Setting" href="#">
                            <i class="icon glyphicon glyphicon-cog"></i>
                        </a>
                    </div><div class="setting-container">
                        <label>
                            <input type="checkbox" id="checkbox_fixednavbar">
                            <span class="text">Fixed Navbar</span>
                        </label>
                        <label>
                            <input type="checkbox" id="checkbox_fixedsidebar">
                            <span class="text">Fixed SideBar</span>
                        </label>
                        <label>
                            <input type="checkbox" id="checkbox_fixedbreadcrumbs">
                            <span class="text">Fixed BreadCrumbs</span>
                        </label>
                        <label>
                            <input type="checkbox" id="checkbox_fixedheader">
                            <span class="text">Fixed Header</span>
                        </label>
                    </div>
                    <!-- Settings -->
                </div>
            </div>
            <!-- /Account Area and Settings -->
        </div>
    </div>
</div>
<!-- /Navbar -->
<!-- Main Container -->
<div class="main-container container-fluid">
    <!-- Page Container -->
    <div class="page-container">
        <!-- Page Sidebar -->
        <div class="page-sidebar" id="sidebar">
            <!-- Page Sidebar Header-->
            <div class="sidebar-header-wrapper">
                <input type="text" class="searchinput" />
                <i class="searchicon fa fa-search"></i>
                <div class="searchhelper">Search Reports, Charts, Emails or Notifications</div>
            </div>
            <!-- /Page Sidebar Header -->
            <!-- Sidebar Menu -->
            <ul class="nav sidebar-menu">
                <!--Dashboard-->
                <li class="<?php echo $this->id == 'dashboard' ? 'active' : '' ?>">
                    <a href="<?php echo Yii::app()->baseUrl.'/administrator/dashboard'?>">
                        <i class="menu-icon glyphicon glyphicon-home"></i>
                        <span class="menu-text"> Dashboard </span>
                    </a>
                </li>

                <?php $master = array('user','counter','country','state','city','bankList','bankAccount','consigne','deliveryAgent','shipper','language') ?>

                <li class="<?php echo in_array($this->id, $master) ? 'open' : '' ?>">
                    <a href="#" class="menu-dropdown">
                        <i class="menu-icon fa fa-tasks"></i>
                        <span class="menu-text"> Master </span>
                        <i class="menu-expand"></i>
                    </a>

                    <ul class="submenu">
                        <li class="<?php echo $this->id == 'user' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/user'?>">
                                <span class="menu-text"> User Login</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'counter' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/counter'?>">
                                <span class="menu-text"> Counter</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'country' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/country'?>">
                                <span class="menu-text"> Country</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'state' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/state'?>">
                                <span class="menu-text"> State</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'city' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/city'?>">
                                <span class="menu-text"> City</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'bankList' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/bankList'?>">
                                <span class="menu-text"> Bank List</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'bankAccount' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/bankAccount'?>">
                                <span class="menu-text"> Bank Account</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'consigne' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/consigne'?>">
                                <span class="menu-text"> Consigne</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'deliveryAgent' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/deliveryAgent'?>">
                                <span class="menu-text"> Delivery Agent</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'shipper' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/shipper'?>">
                                <span class="menu-text"> Shipper</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'language' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/language'?>">
                                <span class="menu-text"> Language</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <?php $data = array('listKoneksi','serverList') ?>

                <!-- <li class="<?php echo in_array($this->id, $data) ? 'open' : '' ?>">
                    <a href="#" class="menu-dropdown">
                        <i class="menu-icon fa fa-desktop"></i>
                        <span class="menu-text"> Config </span>
                        <i class="menu-expand"></i>
                    </a>

                    <ul class="submenu">
                        <li class="<?php echo $this->id == 'listKoneksi' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/listKoneksi'?>">
                                <span class="menu-text"> Connection List</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'serverList' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/serverList'?>">
                                <span class="menu-text"> Server List</span>
                            </a>
                        </li>
                    </ul>
                </li> -->

                <?php $front = array('member','register','spekList', 'masterKolom', 'productCategory','currency','kontener') ?>

                <li class="<?php echo in_array($this->id, $front) ? 'open' : '' ?>">
                    <a href="#" class="menu-dropdown">
                        <i class="menu-icon fa fa-tags"></i>
                        <span class="menu-text"> Frontend </span>
                        <i class="menu-expand"></i>
                    </a>

                    <ul class="submenu">
                        <li class="<?php echo $this->id == 'member' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/member'?>">
                                <span class="menu-text"> Member List</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'register' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/register'?>">
                                <span class="menu-text"> Register</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'spekList' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/spekList'?>">
                                <span class="menu-text"> Specification List</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'masterKolom' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/masterKolom'?>">
                                <span class="menu-text"> Master Coloumn Import</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'productCategory' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/productCategory'?>">
                                <span class="menu-text"> Product Category</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'currency' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/currency'?>">
                                <span class="menu-text"> Currency</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'kontener' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/administrator/kontener'?>">
                                <span class="menu-text"> Container</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="<?php echo $this->id == 'order' ? 'active' : '' ?>">
                    <a href="<?php echo Yii::app()->baseUrl.'/administrator/order'?>">
                        <i class="menu-icon glyphicon glyphicon-folder-close"></i>
                        <span class="menu-text"> Order </span>
                    </a>
                </li>

            </ul>
            <!-- /Sidebar Menu -->
        </div>
        <!-- /Page Sidebar -->
        <!-- Page Content -->
        <div class="page-content">
            <!-- Page Breadcrumb -->
            <div class="page-breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="<?php echo Yii::app()->baseUrl;?>/administrator">Home</a>
                    </li>
                    <li class="active">
                        <a href="<?php echo Yii::app()->baseUrl.'/'.$this->id;?>"><?php echo ucfirst($this->id); ?></a>
                    </li>
                </ul>
            </div>
            <!-- /Page Breadcrumb -->
            <!-- Page Header -->
            <div class="page-header position-relative">
                <!--Header Buttons-->
                <div class="header-buttons">
                    <a class="sidebar-toggler" href="#">
                        <i class="fa fa-arrows-h"></i>
                    </a>
                    <a class="refresh" id="refresh-toggler" href="">
                        <i class="glyphicon glyphicon-refresh"></i>
                    </a>
                    <a class="fullscreen" id="fullscreen-toggler" href="#">
                        <i class="glyphicon glyphicon-fullscreen"></i>
                    </a>
                </div>
                <!--Header Buttons End-->
            </div>
            <!-- /Page Header -->
            <!-- Page Body -->
            <div class="page-body">
                <?php echo $content?>
                <!-- Your Content Goes Here -->
            </div>
            <!-- /Page Body -->
        </div>
        <!-- /Page Content -->
    </div>
    <!-- /Page Container -->
    <!-- Main Container -->
</div>

<!--Basic Scripts-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/beyond.min.js"></script>

<!--Page Related Scripts-->
<!--Sparkline Charts Needed Scripts-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/sparkline/jquery.sparkline.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/sparkline/sparkline-init.js"></script>

<!--Easy Pie Charts Needed Scripts-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/easypiechart/jquery.easypiechart.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/easypiechart/easypiechart-init.js"></script>

<!--Flot Charts Needed Scripts-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/flot/jquery.flot.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/flot/jquery.flot.resize.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/flot/jquery.flot.pie.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/flot/jquery.flot.tooltip.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/flot/jquery.flot.orderBars.js"></script>

<!--Bootstrap Date Picker-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/datetime/bootstrap-datepicker.js"></script>

<!--Jquery Select2-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/select2/select2.js"></script>

<script type="text/javascript">
$(".select2").select2();

//--Bootstrap Date Picker--
$('.date-picker').datepicker();

// If you want to draw your charts with Theme colors you must run initiating charts after that current skin is loaded
$(window).bind("load", function () {

    /*Sets Themed Colors Based on Themes*/
    themeprimary = getThemeColorFromCss('themeprimary');
    themesecondary = getThemeColorFromCss('themesecondary');
    themethirdcolor = getThemeColorFromCss('themethirdcolor');
    themefourthcolor = getThemeColorFromCss('themefourthcolor');
    themefifthcolor = getThemeColorFromCss('themefifthcolor');

    //Sets The Hidden Chart Width
    $('#dashboard-bandwidth-chart')
        .data('width', $('.box-tabbs')
            .width() - 20);

    //-------------------------Visitor Sources Pie Chart----------------------------------------//
    var data = [
        {
            data: [[1, 21]],
            color: '#fb6e52'
        },
        {
            data: [[1, 12]],
            color: '#e75b8d'
        },
        {
            data: [[1, 11]],
            color: '#a0d468'
        },
        {
            data: [[1, 10]],
            color: '#ffce55'
        },
        {
            data: [[1, 46]],
            color: '#5db2ff'
        }
    ];
    var placeholder = $("#dashboard-pie-chart-sources");
    placeholder.unbind();

    $.plot(placeholder, data, {
        series: {
            pie: {
                innerRadius: 0.45,
                show: true,
                stroke: {
                    width: 4
                }
            }
        }
    });

    //------------------------------Visit Chart------------------------------------------------//
    var data2 = [{
        color: themesecondary,
        label: "Direct Visits",
        data: [[3, 2], [4, 5], [5, 4], [6, 11], [7, 12], [8, 11], [9, 8], [10, 14], [11, 12], [12, 16], [13, 9],
            [14, 10], [15, 14], [16, 15], [17, 9]],

        lines: {
            show: true,
            fill: true,
            lineWidth: .1,
            fillColor: {
                colors: [{
                    opacity: 0
                }, {
                    opacity: 0.4
                }]
            }
        },
        points: {
            show: false
        },
        shadowSize: 0
    },
        {
            color: themeprimary,
            label: "Referral Visits",
            data: [[3, 10], [4, 13], [5, 12], [6, 16], [7, 19], [8, 19], [9, 24], [10, 19], [11, 18], [12, 21], [13, 17],
                [14, 14], [15, 12], [16, 14], [17, 15]],
            bars: {
                order: 1,
                show: true,
                borderWidth: 0,
                barWidth: 0.4,
                lineWidth: .5,
                fillColor: {
                    colors: [{
                        opacity: 0.4
                    }, {
                        opacity: 1
                    }]
                }
            }
        },
        {
            color: themethirdcolor,
            label: "Search Engines",
            data: [[3, 14], [4, 11], [5, 10], [6, 9], [7, 5], [8, 8], [9, 5], [10, 6], [11, 4], [12, 7], [13, 4],
                [14, 3], [15, 4], [16, 6], [17, 4]],
            lines: {
                show: true,
                fill: false,
                fillColor: {
                    colors: [{
                        opacity: 0.3
                    }, {
                        opacity: 0
                    }]
                }
            },
            points: {
                show: true
            }
        }
    ];
    var options = {
        legend: {
            show: false
        },
        xaxis: {
            tickDecimals: 0,
            color: '#f3f3f3'
        },
        yaxis: {
            min: 0,
            color: '#f3f3f3',
            tickFormatter: function (val, axis) {
                return "";
            }
        },
        grid: {
            hoverable: true,
            clickable: false,
            borderWidth: 0,
            aboveData: false,
            color: '#fbfbfb'

        },
        tooltip: true,
        tooltipOpts: {
            defaultTheme: false,
            content: " <b>%x May</b> , <b>%s</b> : <span>%y</span>"
        }
    };
    var placeholder = $("#dashboard-chart-visits");
    var plot = $.plot(placeholder, data2, options);

    //------------------------------Real-Time Chart-------------------------------------------//
    var data = [],
        totalPoints = 300;

    function getRandomData() {

        if (data.length > 0)
            data = data.slice(1);

        // Do a random walk

        while (data.length < totalPoints) {

            var prev = data.length > 0 ? data[data.length - 1] : 50,
                y = prev + Math.random() * 10 - 5;

            if (y < 0) {
                y = 0;
            } else if (y > 100) {
                y = 100;
            }

            data.push(y);
        }

        // Zip the generated y values with the x values

        var res = [];
        for (var i = 0; i < data.length; ++i) {
            res.push([i, data[i]]);
        }

        return res;
    }
    // Set up the control widget
    var updateInterval = 100;
    var plot = $.plot("#dashboard-chart-realtime", [getRandomData()], {
        yaxis: {
            color: '#f3f3f3',
            min: 0,
            max: 100,
            tickFormatter: function (val, axis) {
                return "";
            }
        },
        xaxis: {
            color: '#f3f3f3',
            min: 0,
            max: 100,
            tickFormatter: function (val, axis) {
                return "";
            }
        },
        colors: [themeprimary],
        series: {
            lines: {
                lineWidth: 0,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.5
                    }, {
                        opacity: 0
                    }]
                },
                steps: false
            },
            shadowSize: 0
        },
        grid: {
            hoverable: true,
            clickable: false,
            borderWidth: 0,
            aboveData: false
        }
    });

    function update() {

        plot.setData([getRandomData()]);

        plot.draw();
        setTimeout(update, updateInterval);
    }
    update();


    //-------------------------Initiates Easy Pie Chart instances in page--------------------//
    InitiateEasyPieChart.init();

    //-------------------------Initiates Sparkline Chart instances in page------------------//
    InitiateSparklineCharts.init();
});

</script>

</body>
</html>