<?php

Yii::import('application.controllers.UserController');

class CGridViewHelper
{
    public static function getStatus()
    {
        return array(
            'name'=>'status',
            'type'=>'raw',
            'value'=>'EnumStatus::getLabel($data->status)',
        );
    }

    public static function getCreatedBy()
    {
    	return self::getUser('dibuatOleh', 'userCreate');
    }

    public static function getModifiedBy()
    {
    	return self::getUser('diubahOleh', 'userModify');
    }

    public static function getCreatedDate()
    {
    	return self::getDate('tglDibuat');
    }

    public static function getModifiedDate()
    {
    	return self::getDate('tglDiubah');
    }
    
    public static function getUser($attr_name, $relation) {
		return array(
            'name'=>$attr_name,
            'type'=>'raw',
            'value'=>'$data->'. $attr_name .' === "0" ? "System" : ($data->'. $relation .' != NULL ? $data->'. $relation .'->userLogin->username : NULL)',            
        );
    }

    public static function getDate($attr_name)
    {
        return array(
            'name'=>$attr_name,
            'type'=>'raw',
            'value'=>'($data->'.$attr_name.' != NULL)?date("d-M-Y H:i:s", strtotime($data->'.$attr_name.')):NULL',
        	//'filter' => false,
        );
    }
    
    public static function getLink($label, $url)
    {
    	return array(
			'header'=>'',
			'type'=>'raw',
			'value'=>'CHtml::link("'. $label .'", '. $url .', array(
				\'class\'=>\'grid-view-link\'
			))',
		);
    }

	public static function orderUp($action, $grid_id)
	{
		return array(
			'label'=>'Up',
			'imageUrl'=>Yii::app()->params['orderUpImage'],
			'url'=>$action,
			'click'=>'function() {
				$.fn.yiiGridView.update("'. $grid_id .'", {
					type:"POST",
					url:$(this).attr("href"),
					success:function() {
						$.fn.yiiGridView.update("'. $grid_id .'");
					}
				});
				return false;
			}
			',
		);
	}

	public static function orderDown($action, $grid_id)
	{
		return array(
			'label'=>'Down',
			'imageUrl'=>Yii::app()->params['orderDownImage'],
			'url'=>$action,
			'click'=>'function() {
				$.fn.yiiGridView.update("'. $grid_id .'", {
					type:"POST",
					url:$(this).attr("href"),
					success:function() {
						$.fn.yiiGridView.update("'. $grid_id .'");
					}
				});
				return false;
			}
			',
		);
	}

	public static function published($attribute_name, $action, $grid_id)
	{
		return array(
			'visible'=>'($data->'. $attribute_name .' == EnumPublishedStatus::PUBLISHED)',
			'label'=>'Unpublish',
			'imageUrl'=>Yii::app()->params["publishedImage"],
			'url'=>$action,
			'click'=>'function() {
				$.fn.yiiGridView.update("'. $grid_id .'", {
					type:"POST",
					url:$(this).attr("href"),
					success:function() {
						$.fn.yiiGridView.update("'. $grid_id .'");
					}
				});
				return false;
			}
			',
		);
	}

	public static function unpublished($attribute_name, $action, $grid_id)
	{
		return array(
			'visible'=>'$data->'. $attribute_name .' == EnumPublishedStatus::UNPUBLISHED',
			'label'=>'Publish',
			'imageUrl'=>Yii::app()->params["unpublishedImage"],
			'url'=>$action,
			'click'=>'function() {
				$.fn.yiiGridView.update("'. $grid_id .'", {
					type:"POST",
					url:$(this).attr("href"),
					success:function() {
						$.fn.yiiGridView.update("'. $grid_id .'");
					}
				});
				return false;
			}
			',
		);
	}
}

?>
