<div class="row">
    <div class="col-sm-12">
        <h3 class="left">Order List</h3>
        <div class="clear"></div>
        <hr />

        <div id="status-message">
            <?php if(Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success">
                    <?php echo Yii::app()->user->getFlash('success') ?>
                </div>
            <?php endif ?>

            <?php if(Yii::app()->user->hasFlash('info')): ?>
                <div class="alert alert-info">
                    <?php echo Yii::app()->user->getFlash('info') ?>
                </div>
            <?php endif ?>

            <?php if(Yii::app()->user->hasFlash('danger')): ?>
                <div class="alert alert-danger">
                    <?php echo Yii::app()->user->getFlash('danger') ?>
                </div>
            <?php endif ?>
        </div>

        <?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
        <div class="search-form" style="display:none">
            <?php $this->renderPartial('_search',array(
                'model'=>$model,
            )); ?>
        </div><!-- search-form -->

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'list-order-grid',
            'itemsCssClass'=>'table table-striped table-bordered table-hover',
            'dataProvider'=>$model->search(),
            'columns'=>array(
                'orderID',
                'tanggal',
                array(
                    'name'=>'kodeMember',
                    'value'=>'($data->member != "" ? $data->member->nama : "")',
                ),
                array(
                    'name'=>'kodeKontener',
                    'value'=>'($data->kontener != "" ? $data->kontener->nama : "")',
                ),
                array(
                    'name'=>'volume',
                    'value'=>'($data->kontener != "" ? $data->kontener->volume : "")',
                ),
                array(
                    'name'=>'status',
                    'value'=>'EnumOrder::getLabel($data->status)',
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{view}',
                    'buttons'=>array(
                        'view'=>array(
                            'imageUrl'=>false,
                            'label'=>'<i class="fa fa-search fa-fw"></i> View',
                            'options'=>array(
                                'class'=>'btn btn-info btn-xs',
                                'title'=>'View Detail',
                            ),
                        ),
                    ),
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{send} {pickup}',
                    'buttons'=>array(
                        'send'=>array(
                            'imageUrl'=>false,
                            'label'=>'<i class="fa fa-edit fa-fw"></i> Send To Factory',
                            'options'=>array(
                                'class'=>'btn btn-primary btn-xs',
                                'title'=>'Send To Factory',
                            ),
                            'visible'=>'$data->status == 2',
                            'url'=>'Yii::app()->createUrl("sell/sendOrder", array("id"=>$data->orderID))',
                        ),
                        'pickup'=>array(
                            'imageUrl'=>false,
                            'label'=>'<i class="fa fa-truck fa-fw"></i> Pickup Order',
                            'options'=>array(
                                'class'=>'btn btn-primary btn-xs',
                                'title'=>'Pickup Order',
                            ),
                            'visible'=>'$data->status == 8',
                            'url'=>'Yii::app()->createUrl("sell/pickup", array("id"=>$data->orderID))',
                        ),
                    ),
                ),
            ),
        )); ?>

    </div>
</div>
