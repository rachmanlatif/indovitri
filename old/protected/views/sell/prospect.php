<div class="row">
    <div class="col-sm-12">
        <h3>Prospects</h3>
        <div class="clear"></div>
        <hr />

        <?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
        <div class="search-form" style="display:none">
            <?php $this->renderPartial('_search',array(
                'model'=>$model,
            )); ?>
        </div><!-- search-form -->

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'list-order-grid',
            'itemsCssClass'=>'table table-striped table-bordered table-hover',
            'dataProvider'=>$model->search(),
            'columns'=>array(
                'orderID',
                'tanggal',
                array(
                    'name'=>'kodeMember',
                    'value'=>'($data->member != "" ? $data->member->nama : "")',
                ),
                array(
                    'name'=>'kodeKontener',
                    'value'=>'($data->kontener != "" ? $data->kontener->nama : "")',
                ),
                array(
                    'name'=>'volume',
                    'value'=>'($data->kontener != "" ? $data->kontener->volume : "")',
                ),
                array(
                    'name'=>'status',
                    'value'=>'EnumOrder::getLabel($data->status)',
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{view}',
                    'buttons'=>array(
                        'view'=>array(
                            'imageUrl'=>false,
                            'label'=>'<i class="fa fa-search fa-fw"></i> View',
                            'options'=>array(
                                'class'=>'btn btn-info btn-xs',
                                'title'=>'View Detail',
                            ),
                        ),
                    ),
                ),
            ),
        )); ?>
    </div>
</div>
