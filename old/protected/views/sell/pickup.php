<div>
    <h1 class="left">Pickup Order</h1>
    <div class="form-button-container">
        <a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
    </div>
</div>
<div class="clear"></div>
<hr />

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'list-order-form',
    'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'nomor'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'nomor', array('size'=>50)); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'nomor'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'tanggalPickup'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'tanggalPickup', array('class'=>'date-picker')); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'tanggalPickup'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'tanggalEstimation'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'tanggalEstimation', array('class'=>'date-picker')); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'tanggalEstimation'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'kodeConsigne'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->dropDownList($model,'kodeConsigne',
                    CHtml::listData(Consigne::model()->findAllByAttributes(array('status'=>EnumStatus::ACTIVE)), 'kodeConsigne', 'nama'),
                    array('empty'=>'- Choose -', 'class'=>'form-control')); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'kodeConsigne'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'kodeDeliveryAgent'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->dropDownList($model,'kodeDeliveryAgent',
                    CHtml::listData(DeliveryAgent::model()->findAllByAttributes(array('status'=>EnumStatus::ACTIVE)), 'kodeDeliveryAgent', 'nama'),
                    array('empty'=>'- Choose -', 'class'=>'form-control')); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'kodeDeliveryAgent'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'kodeShipper'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->dropDownList($model,'kodeShipper',
                    CHtml::listData(Shipper::model()->findAllByAttributes(array('status'=>EnumStatus::ACTIVE)), 'kodeShipper', 'nama'),
                    array('empty'=>'- Choose -', 'class'=>'form-control')); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'kodeShipper'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row buttons">
    <div class="col-xs-12">
        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
    </div>
</div>

<?php $this->endWidget(); ?>