<div>
    <h1 class="left">Pay Order To Factory</h1>
    <div class="form-button-container">
        <a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
    </div>
</div>
<div class="clear"></div>
<hr />

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'list-order-form',
    'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'tanggalBayar'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'tanggalBayar', array('class'=>'date-picker')); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'tanggalBayar'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'jumlahBayar'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'jumlahBayar'); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'jumlahBayar'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'note'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textArea($model,'note'); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'note'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row buttons">
    <div class="col-xs-12">
        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
    </div>
</div>

<?php $this->endWidget(); ?>