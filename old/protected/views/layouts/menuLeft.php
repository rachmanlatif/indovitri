<?php if(Yii::app()->user->id != null){ ?>
    <div class="col-sm-12">
        <div class="row">
            <h4>Profile</h4>
            <div id="index-member">

            </div>

            <h4>Inbox</h4>
            <table class="table table-bordered">
                <tr>
                    <td>Chat</td>
                </tr>
                <tr>
                    <td>Discussion</td>
                </tr>
                <tr>
                    <td>Review</td>
                </tr>
            </table>

            <h4>Buy</h4>
            <table class="table table-bordered">
                <tr>
                    <td><a href="<?php echo Yii::app()->baseUrl; ?>/order">Order Status</a></td>
                </tr>
                <tr>
                    <td><a href="<?php echo Yii::app()->baseUrl; ?>/order/confirmation">Confirmation</a></td>
                </tr>
                <tr>
                    <td><a href="<?php echo Yii::app()->baseUrl; ?>/order/cancel">Canceled</a></td>
                </tr>
            </table>

            <?php if(MyHelper::getToko() != null){ ?>

                <h4>Sell</h4>
                <table class="table table-bordered">
                    <tr>
                        <td><a href="<?php echo Yii::app()->baseUrl; ?>/sell/prospect">Prospects</a></td>
                    </tr>
                    <tr>
                        <td><a href="<?php echo Yii::app()->baseUrl; ?>/sell">Order List</a></td>
                    </tr>
                    <tr>
                        <td><a href="<?php echo Yii::app()->baseUrl; ?>/sell/confirmation">Confirmation</a></td>
                    </tr>
                </table>

                <h4>Product</h4>
                <table class="table table-bordered">
                    <tr>
                        <td><a href="<?php echo Yii::app()->baseUrl; ?>/product/add">Add New</a></td>
                    </tr>
                    <tr>
                        <td><a href="<?php echo Yii::app()->baseUrl; ?>/product">Product List</a></td>
                    </tr>
                    <tr>
                        <td><a href="<?php echo Yii::app()->baseUrl; ?>/product/import">Import Data</a></td>
                    </tr>
                </table>

                <hr>

                <?php
                $toko = MyHelper::getToko();
                if($toko != null){ ?>
                    <table class="table table-bordered">
                        <tr>
                            <td><a href="<?php echo Yii::app()->baseUrl.'/store/view/id/'.$toko[0]['domain']; ?>" title="Go to store"><b><i class="fa fa-home"></i> <?php echo $toko[0]['namaToko']; ?></b></a></td>
                        </tr>
                    </table>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>
