<div class="footer-middle">
    <div class="wrap">
        <div class="section group">
            <div class="col_1_of_middle span_1_of_middle">
            </div>
            <div class="col_1_of_middle span_1_of_middle">

            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="footer-bottom">
    <div class="wrap">
        <div class="section group">
            <div class="col_1_of_5 span_1_of_5">
                <h4>Category</h4>
                <ul>
                    <li><a href="<?php echo Yii::app()->baseUrl; ?>">Home</a></li>
                    <?php foreach($category as $c){ ?>
                        <li><a href="<?php echo $this->createUrl('cat', array('m'=>strtolower($c['nama']))); ?>"><?php echo $c['nama']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col_1_of_1 span_1_of_1">
                <?php foreach($models as $model){ ?>
                    <a href="<?php echo $this->createUrl('view', array('id'=>$model->kodeBarang)); ?>" style="float: left;width: 150px;">
                        <div class="top_box" style="border: 1px solid #000;">
                            <div class="grid_img">
                                <div class="css3"><img height="120" src="<?php if($model->image != null){ echo $model->image->imagePath;} ?>" alt=""/></div>
                            </div>
                        </div>
                    </a>
                <?php } ?>
                <div class="clear"> </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
