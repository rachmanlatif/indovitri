<div class="row">
    <div class="col-sm-12">
        <h3 class="left">View ListOrder</h3>
        <a href="<?php echo $this->createUrl('index'); ?>"><input type="button" class="btn btn-sm btn-info" value="List"></a>
        <?php if($model->status == 1){ ?>
            <a href="<?php echo $this->createUrl('delete', array('id'=>$model->orderID)); ?>"><input type="button" class="btn btn-sm btn-warning" value="Cancel Order"></a>
            <a href="<?php echo $this->createUrl('checkout', array('id'=>$model->orderID)); ?>"><input type="button" class="btn btn-sm btn-default" value="Checkout Order"></a>
        <?php } ?>
        <?php if($model->status == 3){ ?>
            <a href="<?php echo $this->createUrl('approve', array('id'=>$model->orderID)); ?>"><input type="button" class="btn btn-sm btn-primary" value="Approve Order"></a>
        <?php } ?>
        <div class="clear"></div>
        <hr />

        <?php $this->widget('zii.widgets.CDetailView', array(
            'htmlOptions'=>array(
                'class'=>'detail-view table table-striped table-bordered table-hover'
            ),
            'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
            'data'=>$model,
            'attributes'=>array(
                'orderID',
                'tanggal',
                'ip',
                'kodeMember',
                'kodeToko',
                'kodeKontener',
                array(
                    'name'=>'status',
                    'value'=>EnumOrder::getLabel($model->status),
                ),
            ),
        )); ?>

        <hr>

        <div class="tabbable">
            <ul class="nav nav-tabs tabs-flat" id="myTab11">
                <li class="active">
                    <a data-toggle="tab" href="#home11">
                        Item Order
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#profile11">
                        Item Ready
                    </a>
                </li>
            </ul>
            <div class="tab-content tabs-flat bg-white">
                <div id="home11" class="tab-pane in active">
                    <?php if($model->details != null){ ?>
                        <table class="table table-bordered table-striped table-hover">
                            <tr>
                                <th>Product Code</th>
                                <th>Price</th>
                                <th>Volume</th>
                                <th>Qty</th>
                                <th>Total</th>
                            </tr>
                            <?php foreach($model->details as $detail){ ?>
                                <tr>
                                    <td>
                                        <?php echo $detail->kodeBarang; ?>
                                    </td>
                                    <td><?php echo number_format($detail->price); ?></td>
                                    <td><?php echo $detail->volume; ?></td>
                                    <td><?php echo number_format($detail->qty); ?></td>
                                    <td><?php echo number_format($detail->total); ?></td>
                                </tr>
                            <?php } ?>
                        </table>
                    <?php } ?>
                </div>

                <div id="profile11" class="tab-pane">
                    <p>Click confirm button above to confirmation this order</p>

                    <?php if($model->confirms != null){ ?>
                        <table class="table table-bordered table-striped table-hover">
                            <tr>
                                <th>Product Code</th>
                                <th>Price</th>
                                <th>Volume</th>
                                <th>Qty</th>
                                <th>Total</th>
                            </tr>
                            <?php foreach($model->confirms as $detail){ ?>
                                <tr>
                                    <td>
                                        <?php echo $detail->kodeBarang; ?>
                                    </td>
                                    <td><?php echo number_format($detail->price); ?></td>
                                    <td><?php echo $detail->volume; ?></td>
                                    <td><?php echo number_format($detail->qty); ?></td>
                                    <td><?php echo number_format($detail->total); ?></td>
                                </tr>
                            <?php } ?>
                        </table>
                    <?php } ?>
                </div>
            </div>
        </div>

    </div>
</div>
