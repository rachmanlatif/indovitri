<div class="row">
    <div class="col-sm-12">
        <h3 class="left">Confirm Order</h3>
        <a href="<?php echo $this->createUrl('index'); ?>"><input type="button" class="btn btn-sm btn-info" value="List"></a>
        <div class="clear"></div>
        <hr />

        <div class="form">

            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'confirm-form',
                'enableAjaxValidation'=>false,
            )); ?>

            <div class="row form-row form-group">
                <div class="col-xs-2">
                    <?php echo $form->labelEx($model,'tanggalTerima'); ?>
                </div>
                <div class="col-xs-10">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php echo $form->textField($model,'tanggalTerima',array('class'=>'form-control date-picker','size'=>60,'maxlength'=>255)); ?>
                        </div>
                        <div class="col-xs-6">
                            <?php echo $form->error($model,'tanggalTerima'); ?>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row form-row form-group">
                <div class="col-xs-2">
                    <?php echo $form->labelEx($model,'kodeBankPenerima'); ?>
                </div>
                <div class="col-xs-10">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php echo $form->dropDownList($model,"kodeBankPenerima", $list,
                                array('class'=>'form-control', 'empty'=>'- Choose -')); ?>
                        </div>
                        <div class="col-xs-6">
                            <?php echo $form->error($model,'kodeBankPenerima'); ?>
                        </div>
                    </div>

                </div>
            </div>

            <hr>

            <div class="row form-row form-group">
                <div class="col-xs-2">
                    <?php echo $form->labelEx($model,'kodeBank'); ?>
                </div>
                <div class="col-xs-10">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php echo $form->dropDownList($model,'kodeBank',
                                CHtml::listData(BankList::model()->findAllByAttributes(array('status'=>EnumStatus::ACTIVE)), 'kodeBank', 'nama'),
                                array('empty'=>'- Choose -', 'class'=>'form-control')); ?>
                        </div>
                        <div class="col-xs-6">
                            <?php echo $form->error($model,'kodeBank'); ?>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row form-row form-group">
                <div class="col-xs-2">
                    <?php echo $form->labelEx($model,'norekPengirim'); ?>
                </div>
                <div class="col-xs-10">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php echo $form->textField($model,'norekPengirim',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
                        </div>
                        <div class="col-xs-6">
                            <?php echo $form->error($model,'norekPengirim'); ?>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row form-row form-group">
                <div class="col-xs-2">
                    <?php echo $form->labelEx($model,'namaPengirim'); ?>
                </div>
                <div class="col-xs-10">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php echo $form->textField($model,'namaPengirim',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
                        </div>
                        <div class="col-xs-6">
                            <?php echo $form->error($model,'namaPengirim'); ?>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row form-row form-group">
                <div class="col-xs-2">
                    <?php echo $form->labelEx($model,'tanggal'); ?>
                </div>
                <div class="col-xs-10">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php echo $form->textField($model,'tanggal',array('class'=>'form-control date-picker','size'=>60,'maxlength'=>255)); ?>
                        </div>
                        <div class="col-xs-6">
                            <?php echo $form->error($model,'tanggal'); ?>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row form-row form-group">
                <div class="col-xs-2">
                    <?php echo $form->labelEx($model,'total'); ?>
                </div>
                <div class="col-xs-10">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php echo $form->textField($model,'total',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
                        </div>
                        <div class="col-xs-6">
                            <?php echo $form->error($model,'total'); ?>
                        </div>
                    </div>

                </div>
            </div>

            <input type="submit" class="btn btn-lg btn-primary" value="Save">

            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
