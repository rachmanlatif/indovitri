<div class="row">
    <div class="col-sm-12">
        <h3 class="left">Order List</h3>
        <div class="clear"></div>
        <hr />

        <div id="status-message">
            <?php if(Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success">
                    <?php echo Yii::app()->user->getFlash('success') ?>
                </div>
            <?php endif ?>

            <?php if(Yii::app()->user->hasFlash('info')): ?>
                <div class="alert alert-info">
                    <?php echo Yii::app()->user->getFlash('info') ?>
                </div>
            <?php endif ?>

            <?php if(Yii::app()->user->hasFlash('danger')): ?>
                <div class="alert alert-danger">
                    <?php echo Yii::app()->user->getFlash('danger') ?>
                </div>
            <?php endif ?>
        </div>

        <?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
        <div class="search-form" style="display:none">
            <?php $this->renderPartial('_search',array(
                'model'=>$model,
            )); ?>
        </div><!-- search-form -->

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'list-order-grid',
            'itemsCssClass'=>'table table-striped table-bordered table-hover',
            'dataProvider'=>$model->search(),
            'columns'=>array(
                'orderID',
                'tanggal',
                array(
                    'name'=>'kodeToko',
                    'value'=>'($data->toko != "" ? $data->toko->namaToko : "")',
                ),
                array(
                    'name'=>'kodeKontener',
                    'value'=>'($data->kontener != "" ? $data->kontener->nama : "")',
                ),
                array(
                    'name'=>'volume',
                    'value'=>'($data->kontener != "" ? $data->kontener->volume : "")',
                ),
                array(
                    'name'=>'status',
                    'value'=>'EnumOrder::getLabel($data->status)',
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{view}',
                    'buttons'=>array(
                        'view'=>array(
                            'imageUrl'=>false,
                            'label'=>'<i class="fa fa-search fa-fw"></i> View',
                            'options'=>array(
                                'class'=>'btn btn-info btn-xs',
                                'title'=>'View Detail',
                            ),
                        ),
                    ),
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{cancel} {checkout} {approve}',
                    'buttons'=>array(
                        'cancel'=>array(
                            'imageUrl'=>false,
                            'label'=>'<i class="fa fa-times fa-fw"></i> Cancel Order',
                            'options'=>array(
                                'class'=>'btn btn-warning btn-xs',
                                'title'=>'Cancel Order',
                            ),
                            'visible'=>'$data->status == 1',
                            'url'=>'Yii::app()->createUrl("order/delete", array("id"=>$data->orderID))',
                        ),
                        'checkout'=>array(
                            'imageUrl'=>false,
                            'label'=>'<i class="fa fa-search fa-fw"></i> Check Out',
                            'options'=>array(
                                'class'=>'btn btn-default btn-xs',
                                'title'=>'Check Out',
                            ),
                            'visible'=>'$data->status == 1',
                            'url'=>'Yii::app()->createUrl("order/checkout", array("id"=>$data->orderID))',
                        ),
                        'approve'=>array(
                            'imageUrl'=>false,
                            'label'=>'<i class="fa fa-check fa-fw"></i> Approve Order',
                            'options'=>array(
                                'class'=>'btn btn-primary btn-xs',
                                'title'=>'Approve Order',
                            ),
                            'visible'=>'$data->status == 3',
                            'url'=>'Yii::app()->createUrl("order/approve", array("id"=>$data->orderID))',
                        ),
                    ),
                ),
            ),
        )); ?>

    </div>
</div>
