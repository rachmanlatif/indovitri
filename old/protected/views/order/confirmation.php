<div class="row">
    <div class="col-sm-12">
        <h3 class="left">Confirmation Order</h3>
        <div class="clear"></div>
        <hr />

        <div id="status-message">
            <?php if(Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success">
                    <?php echo Yii::app()->user->getFlash('success') ?>
                </div>
            <?php endif ?>

            <?php if(Yii::app()->user->hasFlash('info')): ?>
                <div class="alert alert-info">
                    <?php echo Yii::app()->user->getFlash('info') ?>
                </div>
            <?php endif ?>

            <?php if(Yii::app()->user->hasFlash('danger')): ?>
                <div class="alert alert-danger">
                    <?php echo Yii::app()->user->getFlash('danger') ?>
                </div>
            <?php endif ?>
        </div>

        <?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
        <div class="search-form" style="display:none">
            <?php $this->renderPartial('_search',array(
                'model'=>$model,
            )); ?>
        </div><!-- search-form -->

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'list-order-grid',
            'itemsCssClass'=>'table table-striped table-bordered table-hover',
            'dataProvider'=>$model->search(),
            'columns'=>array(
                'orderID',
                'tanggal',
                array(
                    'name'=>'kodeToko',
                    'value'=>'($data->toko != "" ? $data->toko->namaToko : "")',
                ),
                array(
                    'name'=>'kodeKontener',
                    'value'=>'($data->kontener != "" ? $data->kontener->nama : "")',
                ),
                array(
                    'name'=>'volume',
                    'value'=>'($data->kontener != "" ? $data->kontener->volume : "")',
                ),
                array(
                    'name'=>'status',
                    'value'=>'EnumOrder::getLabel($data->status)',
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{view} {delete}',
                    'buttons'=>array(
                        'view'=>array(
                            'imageUrl'=>false,
                            'label'=>'<i class="fa fa-search fa-fw"></i> View',
                            'options'=>array(
                                'class'=>'btn btn-info btn-xs',
                                'title'=>'View Detail',
                            ),
                        ),
                        'delete'=>array(
                            'imageUrl'=>false,
                            'label'=>'<i class="fa fa-trash-o fa-fw"></i> Delete',
                            'options'=>array(
                                'class'=>'btn btn-danger btn-xs',
                                'title'=>'Delete',
                            ),
                            'visible'=>'$data->status == 1',
                        ),
                    ),
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{payDP} {pay}',
                    'buttons'=>array(
                        'payDP'=>array(
                            'imageUrl'=>false,
                            'label'=>'<i class="fa fa-edit fa-fw"></i> Confirm Payment Order',
                            'options'=>array(
                                'class'=>'btn btn-default btn-xs',
                                'title'=>'Confirm Payment Order',
                            ),
                            'visible'=>'$data->status == 6',
                            'url'=>'Yii::app()->createUrl("order/pay", array("id"=>$data->orderID))',
                        ),
                        'pay'=>array(
                            'imageUrl'=>false,
                            'label'=>'<i class="fa fa-money fa-fw"></i> Last Payment',
                            'options'=>array(
                                'class'=>'btn btn-default btn-xs',
                                'title'=>'Last Payment',
                            ),
                            'visible'=>'$data->status == 10',
                            'url'=>'Yii::app()->createUrl("order/payLast", array("id"=>$data->orderID))',
                        ),
                    ),
                ),
            ),
        )); ?>

    </div>
</div>
