<?php
$cs = Yii::app()->clientScript;

$cs->registerScript('form-specific', "
	$(document).ready(function(){
		$('#image-container').on('click','.delete-image', function(){
			if (confirm('Are you sure?')) {
				$.ajax({
					type: 'post',
					url: '". Yii::app()->request->baseUrl ."/profile/deleteFile',
					data: {filename: $('.Profile_filename').val()},
					success: function(response) {
						$('#image-container').html('');
					}
				});
			}
			return false;
		});

	});
");
?>

<h4>Profile</h4>
<hr>

<?php if($status == 0){ ?>
    <div id="status-message">
        <div class="alert alert-info">
            You must complete your profile
        </div>
    </div>
<?php } ?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'account-form',
    'enableAjaxValidation'=>false,
)); ?>

        <?php $this->renderPartial('account', array(
            'model'=>$modelProfile,
            'form'=>$form,
        )); ?>
        <hr>
        <?php $this->renderPartial('basic', array(
            'model'=>$modelProfileAddress,
            'form'=>$form,
        )); ?>
<hr>
<div class="row buttons">
    <div class="col-xs-12">
        <button type="submit" class="btn btn-lg btn-blue"><i class="fa fa-save"></i> Save</button>
    </div>
</div>

<?php $this->endWidget(); ?>
