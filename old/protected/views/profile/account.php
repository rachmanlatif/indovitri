<div class="form">

    <?php echo $form->errorSummary($model); ?>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'email'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'email', array('class'=>'form-control', 'readonly'=>true)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'email'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'nama'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'nama', array('class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'nama'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'hp'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'hp', array('class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'hp'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'filename'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <div id="image-container" class="uploaded-image">
                        <div id="image-delete">

                        </div>
                    </div>
                    <div class="clear"></div>
                    <?php $this->widget('common.extensions.widgets.qqFileUploader.QQFileUploader', array(
                        'id'=>'qq-file-uploader',
                        'action'=>Yii::app()->request->baseUrl . '/profile/upload',
                        'options'=>array(
                            'allowedExtensions'=>Yii::app()->params['imageExtension'],
                        ),
                        'onComplete'=>"
                                var content = '<input type=\"hidden\" class=\"ProfileMember_delete\" id=\"ProfileMember_filename\" name=\"ProfileMember[filename]\" value=\"'+ responseJSON.newFileName +'\" />';
                                content += '<a href=\"javascript:void(0)\" class=\"delete-image\" id=\"delete-image\"><i class=\"fa fa-file-image-o \"/> Delete file</a><br>';
                                content += '<img src=\"".ProfileMember::getFileUrl()."'+responseJSON.newFileName +' \" width=\"200\" height=\"150\"><br>';

                                $('#image-container').append(content);
                            ",
                        'onSubmit' => "$('.qq-upload-list').html('');",
                    ));?>
                    Choose for add or change photo profile
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'filename'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'tempatLahir'); ?>
        </div>
        <div class="col-xs-5">
            <div class="row">
                <div class="col-xs-12">
                    <?php echo $form->textField($model,'tempatLahir', array('class'=>'form-control')); ?>
                    <?php echo $form->error($model,'tempatLahir'); ?>
                </div>
            </div>
        </div>
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'tanggalLahir'); ?>
        </div>
        <div class="col-xs-3">
            <div class="row">
                <div class="col-xs-12">
                    <?php echo $form->textField($model,'tanggalLahir', array('class'=>'form-control date-picker', 'data-date-format'=>'yyyy-mm-dd')); ?>
                    <?php echo $form->error($model,'tanggalLahir'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'jenisKelamin'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php
                    $a = '';
                    $b = '';
                    if($model->jenisKelamin == EnumJenisKelamin::MAN){
                        $a = 'checked';
                        $b = '';
                    }
                    else{
                        $a = '';
                        $b = 'checked';
                    }
                    ?>
                    <div class="radio row">
                        <label>
                            <input name="ProfileMember[jenisKelamin]" value="<?php echo EnumJenisKelamin::MAN; ?>" type="radio" class="inverted" <?php echo $a; ?>>
                            <span class="text"><?php echo EnumJenisKelamin::getLabel(EnumJenisKelamin::MAN); ?></span>
                        </label>
                    </div>
                    <div class="radio row">
                        <label>
                            <input name="ProfileMember[jenisKelamin]" value="<?php echo EnumJenisKelamin::WOMAN; ?>" type="radio" class="inverted" <?php echo $b; ?>>
                            <span class="text"><?php echo EnumJenisKelamin::getLabel(EnumJenisKelamin::WOMAN); ?></span>
                        </label>
                    </div>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'jenisKelamin'); ?>
                </div>
            </div>
        </div>
    </div>

</div><!-- form -->