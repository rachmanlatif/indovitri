<div class="form">

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->hiddenField($model,'HP', array('readonly'=>true)); ?>
    <?php echo $form->hiddenField($model,'penerima', array('readonly'=>true)); ?>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'address'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textArea($model,'address', array('class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'address'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'kodeCountry'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'kodeCountry',
                        CHtml::listData(Country::model()->findAllByAttributes(array('status'=>EnumStatus::ACTIVE)), 'kodeCountry', 'nama'),
                        array('empty'=>'- Choose -', 'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'kodeCountry'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'kodeState'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'kodeState',
                        CHtml::listData(State::model()->findAllByAttributes(array('status'=>EnumStatus::ACTIVE)), 'kodeState', 'nama'),
                        array('empty'=>'- Choose -', 'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'kodeState'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'kodeCity'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'kodeCity',
                        CHtml::listData(City::model()->findAllByAttributes(array('status'=>EnumStatus::ACTIVE)), 'kodeCity', 'nama'),
                        array('empty'=>'- Choose -', 'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'kodeCity'); ?>
                </div>
            </div>
        </div>
    </div>

</div><!-- form -->