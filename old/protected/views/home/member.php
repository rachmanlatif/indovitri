<table class="table table-bordered">
    <tr>
        <td>
            <?php if($profileMember != null and file_exists(ProfileMember::getFilePath().$profileMember->filename)){ ?>
                <img src="<?php echo $profileMember->pathFoto; ?>" class="img-responsive">
            <?php } else{ ?>
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/frontend/images/user.png" class="img-responsive">
            <?php } ?>
            <?php echo $model->email; ?>
        </td>
    </tr>
    <tr>
        <td class="text-center">
            <?php echo $model->nama; ?>
        </td>
    </tr>
    <tr>
        <td class="text-center"><a href="<?php echo Yii::app()->baseUrl; ?>/profile/change">Change profile</a></td>
    </tr>
</table>