<?php
$no = 1;
foreach($models as $model){ ?>
    <div class="col-sm-3">
        <div class="plan" style="height: 400px;">
            <div class="header"><?php echo $model['kodeBarang']; ?></div>
            <div class="price">$<?php echo number_format($model['priceMaster']); ?></div>
            <div class="monthly"><?php echo $model['namaBarang']; ?></div>
            <ul>
                <li id="image<?php echo $no; ?>">

                </li>
            </ul>
            <a class="signup bg-blue" href="<?php echo Yii::app()->homeUrl.'store/view/id/'.$model['domain']; ?>">Go To Seller</a>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {

            $.ajax({
                type: 'post',
                url: '<?php echo Yii::app()->request->baseUrl ."/home/getImageProduct"; ?>',
                data: {kodeBarang: '<?php echo $model['kodeBarang']; ?>', kodeToko: '<?php echo $model['kodeToko']; ?>'},
                success: function(response) {
                    $('#image<?php echo $no; ?>').html(response);
                }
            });
        });
    </script>

    <?php $no++; } ?>