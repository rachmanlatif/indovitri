<div class="row">
    <div class="col-sm-12">
        <h4>Category "<?php echo $nama; ?>", find other product in store</h4>
        <?php
        foreach($toko as $t){ ?>
            <a href="<?php echo Yii::app()->baseUrl.'/store/view/id/'.$t['domain'];?>">
                <?php if(file_exists(Yii::app()->baseUrl.'/store/view/id/'.$t['imagePath'])){ ?>
                    <div class="col-sm-2 col-xs-6">
                        <div class="databox databox-shadowed bg-white radius-bordered padding-5">
                            <img class="img-responsive" src="<?php echo $t['imagePath']; ?>">
                        </div>
                    </div>
                <?php } else { ?>
                    <h3><?php echo $t['namaToko']; ?></h3>
                <?php } ?>
            </a>
        <?php } ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $.ajax({
            type: 'post',
            url: '<?php echo Yii::app()->request->baseUrl ."/home/loadProductCategory"; ?>',
            data: {cat:'<?php echo $cat; ?>'},
            success: function(response) {
                $('#index-productCategory').html(response);
            }
        });
    });
</script>

<div class="row pricing-container">
    <div class="col-sm-12">
        <h4>Similar Product with category "<?php echo $nama; ?>"</h4>

        <div id="index-productCategory">

        </div>
    </div>
</div>