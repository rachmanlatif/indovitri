<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div id="status-message">
            <?php if(Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success">
                    <?php echo Yii::app()->user->getFlash('success') ?>
                </div>
            <?php endif ?>

            <?php if(Yii::app()->user->hasFlash('info')): ?>
                <div class="alert alert-info">
                    <?php echo Yii::app()->user->getFlash('info') ?>
                </div>
            <?php endif ?>

            <?php if(Yii::app()->user->hasFlash('danger')): ?>
                <div class="alert alert-danger">
                    <?php echo Yii::app()->user->getFlash('danger') ?>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-xs-12">
        <h4>Find product in store</h4>
        <?php foreach($toko as $t){ ?>
            <a href="<?php echo Yii::app()->baseUrl.'/store/view/id/'.$t['domain'];?>">
                <?php if(file_exists(Yii::app()->baseUrl.'/store/view/id/'.$t['imagePath'])){ ?>
                    <div class="col-sm-2 col-xs-6">
                        <div class="databox databox-shadowed bg-white radius-bordered padding-5">
                            <img class="img-responsive" src="<?php echo $t['imagePath']; ?>">
                        </div>
                    </div>
                <?php } else { ?>
                    <h3><?php echo $t['namaToko']; ?></h3>
                <?php } ?>
            </a>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-xs-12">
        <h4>Categories</h4>
    </div>
    <?php foreach($category as $c){ ?>
        <a href="<?php echo Yii::app()->baseUrl.'/home/cat/m/'.strtolower($c['nama']); ?>">
            <div class="col-sm-3 col-xs-6" style="text-align: center;">
                <div class="databox radius-bordered databox-shadowed databox-graded">
                    <div class="databox-right">
                        <h3><?php echo $c['nama']; ?></h3>
                    </div>
                </div>
            </div>
        </a>
    <?php } ?>
</div>