<div class="row form-row form-group">
    <div class="col-xs-3">
        Choose your container
    </div>
    <div class="col-xs-9">
        <div class="row">
            <div class="col-xs-6">
                <?php echo CHtml::dropDownList('ListOrder[kodeKontener]', '',
                    $listKontener,
                    array('class'=>'form-control select', 'empty'=>'- Choose -')) ?>
            </div>

            <button type="submit" class="btn btn-lg btn-info">Add To Container</button>

        </div>
    </div>
</div>

<?php echo CHtml::hiddenField('ListOrder[kodeToko]', $toko->kodeToko, array('readonly'=>true)) ?>

<table class="table table-bordered table-striped table-hover">
    <tr>
        <th>Image</th>
        <th>Product Code</th>
        <th>Product Name</th>
        <th>Price</th>
        <th>Diameter</th>
        <th>Lenght</th>
        <th>Height</th>
        <th>Weight</th>
        <th>CBM</th>
        <th>Qty</th>
        <th>Stock</th>
    </tr>
    <?php
    $no = 0;
    foreach($models as $model){
        $stock = MyHelper::getStock($model['stockCode']);
        ?>
        <tr>
            <td>
                <div id="index-image<?php echo $no; ?>">

                </div>
            </td>
            <td>
                <?php echo $model['kodeBarang']; ?>
                <?php echo CHtml::hiddenField('KodeBarang['.$no.']', $model['kodeBarang'], array('readonly'=>true)) ?>
            </td>
            <td><?php echo $model['namaBarang']; ?></td>
            <td>
                <?php echo number_format($model['priceMaster']); ?>
                <?php echo CHtml::hiddenField('Price['.$no.']', $model['priceMaster'], array('readonly'=>true)) ?>
            </td>
            <td><?php echo number_format($model['diameter']); ?></td>
            <td><?php echo number_format($model['length']); ?></td>
            <td><?php echo number_format($model['height']); ?></td>
            <td><?php echo number_format($model['weight'],3); ?></td>
            <td><?php echo number_format($model['cbm']); ?></td>
            <td>
                <?php echo CHtml::numberField('Qty['.$no.']', '', array('class'=>'form-control')) ?>
            </td>
            <td><?php echo ($stock != null ? $stock[0]['stockName'] : '-'); ?></td>
        </tr>

        <script type="text/javascript">
        $(document).ready(function() {

            $.ajax({
                type: 'post',
                url: '<?php echo Yii::app()->request->baseUrl ."/store/loadProductImage"; ?>',
                data: {product : '<?php echo $model['kodeBarang']; ?>'},
                success: function(response) {
                    $('#index-image<?php echo $no; ?>').html(response);
                }
            });
        });
        </script>
        <?php $no++; } ?>
</table>
