<?php if(Yii::app()->user->id != null){ ?>

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'product-form',
        'enableAjaxValidation'=>false,
    )); ?>

    <div class="row">
        <div class="profile-container">
            <div class="profile-header row bg-white">
                <div class="col-lg-2 col-md-4 col-sm-12 text-center">
                    <?php if(file_exists(Yii::app()->baseUrl.'/store/view/id/'.$toko->imagePath)){ ?>
                        <img src="<?php echo $t['imagePath']; ?>" alt="" class="header-avatar">
                    <?php } else { ?>
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/frontend/images/store.jpg" alt="" class="header-avatar">
                    <?php } ?>
                </div>
                <div class="col-lg-5 col-md-8 col-sm-12 profile-info">
                    <div class="header-fullname"><?php echo $toko->namaToko; ?></div>
                    <?php if(MyHelper::getToko() != null){ ?>
                        <a href="<?php echo Yii::app()->baseUrl.'/'; ?>page/store/change" class="btn btn-default btn-sm btn-follow">
                            <i class="fa fa-check"></i>
                            Change Profile
                        </a>
                    <?php } ?>
                    <div class="header-information">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent orci dui, cursus ut arcu et, dignissim tincidunt urna.
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 profile-stats">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 stats-col">
                            <div class="stats-value pink">284</div>
                            <div class="stats-title">SUCCESS ORDER</div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 stats-col">
                            <div class="stats-value pink">803</div>
                            <div class="stats-title">PRODUCTS</div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 stats-col">
                            <div class="stats-value pink">1207</div>
                            <div class="stats-title">CATEGORY</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 inlinestats-col">
                            <i class="glyphicon glyphicon-map-marker"></i> Indonesia
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 inlinestats-col">
                            Currency: <strong>USD/IDR</strong>
                        </div>
                    </div>
                </div>
            </div>
            <div class="profile-body">
                <div class="col-lg-12">
                    <div class="tabbable">
                        <ul class="nav nav-tabs tabs-flat  nav-justified" id="myTab11">
                            <li class="active">
                                <a data-toggle="tab" href="#products">
                                    Products
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#review">
                                    Review
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" id="contacttab" href="#chat">
                                    Chat
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content tabs-flat bg-white">
                            <div id="status-message">
                                <?php if(Yii::app()->user->hasFlash('success')): ?>
                                    <div class="alert alert-success">
                                        <?php echo Yii::app()->user->getFlash('success') ?>
                                    </div>
                                <?php endif ?>

                                <?php if(Yii::app()->user->hasFlash('info')): ?>
                                    <div class="alert alert-info">
                                        <?php echo Yii::app()->user->getFlash('info') ?>
                                    </div>
                                <?php endif ?>

                                <?php if(Yii::app()->user->hasFlash('danger')): ?>
                                    <div class="alert alert-danger">
                                        <?php echo Yii::app()->user->getFlash('danger') ?>
                                    </div>
                                <?php endif ?>
                            </div>

                            <div id="products" class="tab-pane active">
                                <?php
                                if($toko->kodeMember == Yii::app()->user->id){
                                    $this->renderPartial('productview', array(
                                        'toko'=>$toko,
                                        'models'=>$models,
                                        'listKontener'=>$listKontener,
                                    ));
                                }
                                else{
                                    $this->renderPartial('products', array(
                                        'toko'=>$toko,
                                        'models'=>$models,
                                        'listKontener'=>$listKontener,
                                    ));
                                }
                                ?>
                                <div class="clear"></div>
                            </div>
                            <div id="review" class="tab-pane">
                                <b>Review</b>
                                <div class="clear"></div>
                            </div>
                            <div id="chat" class="tab-pane">
                                <b>Chat</b>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>

<?php } ?>
