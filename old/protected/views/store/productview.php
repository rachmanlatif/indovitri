<table class="table table-bordered table-striped table-hover">
    <tr>
        <th>Image</th>
        <th>Product Code</th>
        <th>Product Name</th>
        <th>Price</th>
        <th>Diameter</th>
        <th>Lenght</th>
        <th>Height</th>
        <th>Weight</th>
        <th>CBM</th>
        <th>Stock</th>
    </tr>
    <?php
    $no = 0;
    foreach($models as $model){ ?>
        <tr>
            <td>
                <div class="box1">
                    <img src="<?php echo ($model->image != null ? $model->image->imagePath : ''); ?>" title="Click for detail" width="60" height="60" src="" alt=""/>
                </div>
            </td>
            <td>
                <?php echo $model->kodeBarang; ?>
            </td>
            <td><?php echo $model->namaBarang; ?></td>
            <td>
                <?php echo number_format($model->priceMaster); ?>
            </td>
            <td><?php echo number_format($model->diameter,3); ?></td>
            <td><?php echo number_format($model->length,3); ?></td>
            <td><?php echo number_format($model->height,3); ?></td>
            <td><?php echo number_format($model->weight,3); ?></td>
            <td><?php echo number_format($model->cbm,3); ?></td>
            <td><?php echo ($model->stock != null ? $model->stock->stockName : ''); ?></td>
        </tr>
        <?php $no++; } ?>
</table>
