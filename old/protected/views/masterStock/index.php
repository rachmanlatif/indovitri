<div class="row">
    <div class="col-sm-12">

<div>
	<h3 class="left">Manage Master Unit</h3>
	<div class="button1">
		<a href="<?php echo $this->createUrl('add'); ?>"><input class="btn btn-sm" type="button" value="Add"></a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'master-stock-grid',
    'itemsCssClass'=>'table table-striped table-bordered table-hover',
	'dataProvider'=>$model->search(),
	'selectableRows'=>2,
	'columns'=>array(
		'stockName',
		'note',
        array(
            'name'=>'status',
            'value'=>'EnumStatus::getLabel($data->status)',
        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

    </div>
</div>
