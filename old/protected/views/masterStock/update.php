<div class="row">
    <div class="col-sm-12">

<div>
	<h1 class="left">Update Unit</h1>
	<div class="button1">
        <a href="<?php echo $this->createUrl('index'); ?>"><input type="button" class="btn btn-sm" value="List"></a>
        <a href="<?php echo $this->createUrl('add'); ?>"><input type="button" class="btn btn-sm" value="Add"></a>
        <a href="<?php echo $this->createUrl('update', array('id'=>$model->stockCode)); ?>"><input type="button" class="btn btn-sm" value="Update"></a>
    </div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>


    </div>
</div>
