<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('stockCode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->stockCode), array('view', 'id'=>$data->stockCode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stockName')); ?>:</b>
	<?php echo CHtml::encode($data->stockName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('note')); ?>:</b>
	<?php echo CHtml::encode($data->note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>