<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'master-stock-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'stockName'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'stockName',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'stockName'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'note'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'note',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'note'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'status'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'status',
                        EnumStatus::getList()); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'status'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row buttons">
        <div class="button1">
            <button type="submit" class="btn btn-sm"><i class="fa fa-save"></i> Save</button>
        </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->