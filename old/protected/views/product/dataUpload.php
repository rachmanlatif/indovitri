<div class="row">
    <div class="col-sm-12">

            <h3 class="left">Import Review</h3>
            <div class="form-button-container">
                <div class="button1">
                    <a href="<?php echo $this->createUrl('index'); ?>"><input type="button" value="Product List" class="btn btn-sm"></a>
                    <a href="<?php echo $this->createUrl('add'); ?>"><input type="button" value="Add New Product" class="btn btn-sm"></a>
                    <a href="<?php echo $this->createUrl('delete'); ?>"><input type="button" value="Delete Data" class="btn btn-sm"></a>
                </div>
                <div class="clear"></div>
            </div>
            <hr>

            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'product-form',
                'enableAjaxValidation'=>false,
            )); ?>

            <div class="row form-row form-group">
                <div class="col-xs-2">
                    <?php echo $form->labelEx($model,'kodeProductCategory'); ?>
                </div>
                <div class="col-xs-10">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php echo $form->dropDownList($model,"kodeProductCategory", $list2,
                                array('class'=>'form-control', 'empty'=>'- Choose -')); ?>
                        </div>
                        <div class="col-xs-6">
                            <?php echo $form->error($model,'kodeProductCategory'); ?>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row form-row form-group">
                <div class="col-xs-2">
                    <?php echo $form->labelEx($model,'stockCode'); ?>
                </div>
                <div class="col-xs-10">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php echo $form->dropDownList($model,'stockCode',
                                CHtml::listData(MasterStock::model()->findAllByAttributes(array('status'=>EnumStatus::ACTIVE)), 'stockCode', 'stockName'),
                                array('empty'=>'- Choose -', 'class'=>'form-control')); ?>
                        </div>
                        <div class="col-xs-6">
                            <div class="button1">
                                <a href="<?php echo Yii::app()->homeUrl.'masterStock/add'; ?>"><input type="button" value="Add Unit" class="btn btn-sm"></a>
                            </div>
                            <?php echo $form->error($model,'stockCode'); ?>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row form-row form-group">
                <div class="col-xs-2">
                    <?php echo $form->labelEx($model,'status'); ?>
                </div>
                <div class="col-xs-10">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php echo $form->dropDownList($model,'status',
                                EnumStatusProduct::getList(), array('class'=>'form-control')); ?>
                        </div>
                        <div class="col-xs-6">
                            <?php echo $form->error($model,'status'); ?>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row form-row form-group">
                <div class="col-xs-2">
                    <?php echo $form->labelEx($model,'sellerID'); ?>
                </div>
                <div class="col-xs-10">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php echo $form->dropDownList($model,'sellerID',
                                CHtml::listData(Seller::model()->findAllByAttributes(array('status'=>EnumStatus::ACTIVE)), 'sellerID', 'sellerName'),
                                array('empty'=>'- Choose -', 'class'=>'form-control')); ?>
                        </div>
                        <div class="col-xs-6">
                            <div class="button1">
                                <a href="<?php echo Yii::app()->homeUrl.'seller/add'; ?>"><input type="button" value="Add Seller" class="btn btn-sm"></a>
                            </div>
                            <?php echo $form->error($model,'sellerID'); ?>
                        </div>
                    </div>

                </div>
            </div>

            <hr>

            <b>Mapping your data</b>
            <div class="table-responsive">
                <?php echo $form->hiddenField($model,"filename",array('value'=>$filename)); ?>

                <?php
                $row = 1;
                $a = 0;
                foreach($data[0] as $d){ ?>
                    <div class="col-sm-3">
                        <table class="table table-bordered table-triped table-hover">
                            <tr>
                                <th>
                                    <?php echo $d; ?>
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <b>Mapping to:</b>
                                    <br>
                                    <?php echo $form->dropDownList($model,"column[$row]", $list,
                                        array('class'=>'form-control', 'empty'=>'- Choose -')); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <?php
                    $row++;
                    $a++;
                } ?>
            </div>
            <div class="clear"></div>
            <div class="button1">
                <input type="submit" class="btn btn-sm" value="Save">
            </div>

            <?php $this->endWidget(); ?>

    </div>
</div>
