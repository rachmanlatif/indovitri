<div class="row">
    <div class="col-sm-12">
        <h3 class="left">Add New Product</h3>
        <div class="form-button-container">
            <div class="button1">
                <a href="<?php echo $this->createUrl('index'); ?>"><input type="button" value="List" class="btn btn-sm"></a>
                <a href="<?php echo $this->createUrl('import'); ?>"><input type="button" value="Import Product" class="pull-right btn btn-sm"></a>
            </div>
            <div class="clear"></div>
        </div>
        <hr>
        <?php echo $this->renderPartial('_form', array(
            'model'=>$model,
            'modelImage'=>$modelImage,
            'list'=>$list,
        )); ?>

    </div>
</div>