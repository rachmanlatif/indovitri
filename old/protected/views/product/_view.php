<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeBarang')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kodeBarang), array('view', 'id'=>$data->kodeBarang)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('namaBarang')); ?>:</b>
	<?php echo CHtml::encode($data->namaBarang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('desc')); ?>:</b>
	<?php echo CHtml::encode($data->desc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qty')); ?>:</b>
	<?php echo CHtml::encode($data->qty); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stockCode')); ?>:</b>
	<?php echo CHtml::encode($data->stockCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('oz')); ?>:</b>
	<?php echo CHtml::encode($data->oz); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('diameter')); ?>:</b>
	<?php echo CHtml::encode($data->diameter); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('weight')); ?>:</b>
	<?php echo CHtml::encode($data->weight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('volumeml')); ?>:</b>
	<?php echo CHtml::encode($data->volumeml); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('volumeOz')); ?>:</b>
	<?php echo CHtml::encode($data->volumeOz); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('length')); ?>:</b>
	<?php echo CHtml::encode($data->length); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('height')); ?>:</b>
	<?php echo CHtml::encode($data->height); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cbm')); ?>:</b>
	<?php echo CHtml::encode($data->cbm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('priceFOB')); ?>:</b>
	<?php echo CHtml::encode($data->priceFOB); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('priceMaster')); ?>:</b>
	<?php echo CHtml::encode($data->priceMaster); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inner')); ?>:</b>
	<?php echo CHtml::encode($data->inner); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('master')); ?>:</b>
	<?php echo CHtml::encode($data->master); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('moq')); ?>:</b>
	<?php echo CHtml::encode($data->moq); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('artCapasity')); ?>:</b>
	<?php echo CHtml::encode($data->artCapasity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nettWeight')); ?>:</b>
	<?php echo CHtml::encode($data->nettWeight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grossWeight')); ?>:</b>
	<?php echo CHtml::encode($data->grossWeight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qtyContainer1')); ?>:</b>
	<?php echo CHtml::encode($data->qtyContainer1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qtyContainer2')); ?>:</b>
	<?php echo CHtml::encode($data->qtyContainer2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qtyContainer3')); ?>:</b>
	<?php echo CHtml::encode($data->qtyContainer3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('obLength')); ?>:</b>
	<?php echo CHtml::encode($data->obLength); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('obWidth')); ?>:</b>
	<?php echo CHtml::encode($data->obWidth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('obHeight')); ?>:</b>
	<?php echo CHtml::encode($data->obHeight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalCTN')); ?>:</b>
	<?php echo CHtml::encode($data->totalCTN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalGW')); ?>:</b>
	<?php echo CHtml::encode($data->totalGW); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalCBM')); ?>:</b>
	<?php echo CHtml::encode($data->totalCBM); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalUSD')); ?>:</b>
	<?php echo CHtml::encode($data->totalUSD); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	*/ ?>

</div>