<?php
$cs = Yii::app()->clientScript;

$cs->registerScript('form-specific', "
	$(document).ready(function(){
		$('#image-container').on('click','.delete-image', function(){
			if (confirm('Are you sure?')) {
				$.ajax({
					type: 'post',
					url: '". Yii::app()->request->baseUrl ."/product/deleteFile',
					data: {filename: $('.ProductImage_delete').val()},
					success: function(response) {
						$('#image-container').html('');
					}
				});
			}
			return false;
		});

	});
");
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'product-form',
    'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($modelImage,'imagePath'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php foreach($model->medias as $media){ ?>
                    <img src='<?php echo $media->imagePath; ?>' width="100" height="100">
                <?php } ?>
                <div id="image-container" class="uploaded-image">
                    <div id="image-delete">

                    </div>
                </div>
                <div class="clear"></div>
                <?php $this->widget('common.extensions.widgets.qqFileUploader.QQFileUploader', array(
                    'id'=>'qq-file-uploader',
                    'action'=>Yii::app()->request->baseUrl . '/product/upload',
                    'options'=>array(
                        'allowedExtensions'=>Yii::app()->params['imageExtension'],
                    ),
                    'onComplete'=>"
                            var content = '<input type=\"hidden\" class=\"ProductImage_delete\" id=\"ProductImage_imagePath\" name=\"ProductImage[imagePath][]\" value=\"'+ responseJSON.newFileName +'\" />';
                            content += '<img src=\"".ProductImage::getFileUrl()."'+responseJSON.newFileName +' \" width=\"100\" height=\"100\">';
                            var content2 = '<a href=\"javascript:void(0)\" class=\"delete-image\" id=\"delete-image\"><i class=\"fa fa-file-image-o \"/> Delete all file</a><br>';

                            $('#image-container').append(content);
                            $('#image-delete').html(content2);
                        ",
                    'onSubmit' => "$('.qq-upload-list').html('');",
                ));?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($modelImage,'imagePath'); ?>
            </div>
        </div>

    </div>
</div>

<hr>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'kodeProductCategory'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->dropDownList($model,"kodeProductCategory", $list,
                    array('class'=>'form-control', 'empty'=>'- Choose -')); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'kodeProductCategory'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'sellerID'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->dropDownList($model,'sellerID',
                    CHtml::listData(Seller::model()->findAllByAttributes(array('status'=>EnumStatus::ACTIVE)), 'sellerID', 'sellerName'),
                    array('empty'=>'- Choose -', 'class'=>'form-control')); ?>
            </div>
            <div class="col-xs-6">
                <div class="button1">
                    <a href="<?php echo Yii::app()->homeUrl.'seller/add'; ?>"><input type="button" value="Add Seller" class="btn btn-sm"></a>
                </div>
                <?php echo $form->error($model,'sellerID'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'namaBarang'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'namaBarang',array('size'=>60,'maxlength'=>255)); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'namaBarang'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'desc'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'desc',array('size'=>60,'maxlength'=>255)); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'desc'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'stockCode'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->dropDownList($model,'stockCode',
                    CHtml::listData(MasterStock::model()->findAllByAttributes(array('status'=>EnumStatus::ACTIVE)), 'stockCode', 'stockName'),
                    array('empty'=>'- Choose -', 'class'=>'form-control')); ?>
            </div>
            <div class="col-xs-6">
                <div class="button1">
                    <a href="<?php echo Yii::app()->homeUrl.'masterStock/add'; ?>"><input type="button" value="Add Unit" class="btn btn-sm"></a>
                </div>
                <?php echo $form->error($model,'stockCode'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'status'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->dropDownList($model,'status',
                    EnumStatusProduct::getList(), array('class'=>'form-control')); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'status'); ?>
            </div>
        </div>

    </div>
</div>

<hr>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'qty'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'qty'); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'qty'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'oz'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'oz'); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'oz'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'diameter'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'diameter'); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'diameter'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'weight'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'weight'); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'weight'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-6">
        <table class="table table-bordered">
            <tr>
                <th colspan="2">Volume</th>
            </tr>
            <tr>
                <td>
                    <?php echo $form->labelEx($model,'volumeml'); ?>
                </td>
                <td>
                    <?php echo $form->labelEx($model,'volumeOz'); ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo $form->textField($model,'volumeml'); ?>
                    <?php echo $form->error($model,'volumeml'); ?>
                </td>
                <td>
                    <?php echo $form->textField($model,'volumeOz'); ?>
                    <?php echo $form->error($model,'volumeOz'); ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="col-xs-6"></div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'length'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'length'); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'length'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'height'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'height'); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'height'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'cbm'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'cbm'); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'cbm'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'priceFOB'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'priceFOB',array('size'=>19,'maxlength'=>19)); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'priceFOB'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'priceMaster'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'priceMaster',array('size'=>19,'maxlength'=>19)); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'priceMaster'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-6">
        <table class="table table-bordered">
            <tr>
                <th colspan="2">Packing</th>
            </tr>
            <tr>
                <td>
                    <?php echo $form->labelEx($model,'inner'); ?>
                </td>
                <td>
                    <?php echo $form->labelEx($model,'master'); ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo $form->textField($model,'inner'); ?>
                    <?php echo $form->error($model,'inner'); ?>
                </td>
                <td>
                    <?php echo $form->textField($model,'master'); ?>
                    <?php echo $form->error($model,'master'); ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="col-xs-6"></div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'moq'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'moq',array('size'=>60,'maxlength'=>255)); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'moq'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'artCapasity'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'artCapasity'); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'artCapasity'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'nettWeight'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'nettWeight'); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'nettWeight'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'grossWeight'); ?>
    </div>
    <div class="col-xs-10">
        <div class="row">
            <div class="col-xs-6">
                <?php echo $form->textField($model,'grossWeight'); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->error($model,'grossWeight'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-10">
        <table class="table table-bordered">
            <tr>
                <th colspan="3">OB Measurement (mm)</th>
            </tr>
            <tr>
                <td>
                    <?php echo $form->labelEx($model,'obLength'); ?>
                </td>
                <td>
                    <?php echo $form->labelEx($model,'obWidth'); ?>
                </td>
                <td>
                    <?php echo $form->labelEx($model,'obHeight'); ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo $form->textField($model,'obLength'); ?>
                    <?php echo $form->error($model,'obLength'); ?>
                </td>
                <td>
                    <?php echo $form->textField($model,'obWidth'); ?>
                    <?php echo $form->error($model,'obWidth'); ?>
                </td>
                <td>
                    <?php echo $form->textField($model,'obHeight'); ?>
                    <?php echo $form->error($model,'obHeight'); ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="col-xs-2"></div>
</div>

<div class="button1">
    <input type="submit" class="btn btn-sm" value="Save">
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->