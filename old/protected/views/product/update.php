<div class="row">
    <div class="col-sm-12">

        <div>
            <h1 class="left">Update Product</h1>
            <div class="form-button-container">
                <div class="button1">
                    <a href="<?php echo $this->createUrl('index'); ?>"><input type="button" value="List" class="btn btn-sm"></a>
                    <a href="<?php echo $this->createUrl('add'); ?>"><input type="button" value="Add" class="btn btn-sm"></a>
                    <a href="<?php echo $this->createUrl('view', array('id'=>$model->kodeBarang)); ?>"><input type="button" value="View" class="btn btn-sm"></a>
                    <a href="<?php echo $this->createUrl('import'); ?>"><input type="button" value="Import Product" class="pull-right btn btn-sm"></a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="clear"></div>
        <hr />

        <?php echo $this->renderPartial('_form', array(
            'model'=>$model,
            'modelImage'=>$modelImage,
            'list'=>$list,
        )); ?>

    </div>
</div>