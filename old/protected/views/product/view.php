<div class="row">
    <div class="col-sm-12">

<div>
	<h3 class="left">View Product</h3>
	<div class="button1">
		<a href="<?php echo $this->createUrl('index'); ?>"><input type="button" class="btn btn-sm" value="List"></a>
		<a href="<?php echo $this->createUrl('add'); ?>"><input type="button" class="btn btn-sm" value="Add"></a>
		<a href="<?php echo $this->createUrl('update', array('id'=>$model->kodeBarang)); ?>"><input type="button" class="btn btn-sm" value="Update"></a>
		<?php echo CHtml::linkButton('Delete',  
				array(
					'class' => 'btn btn-sm',
					'submit' => array('delete','id'=>$model->kodeBarang),
					'confirm' => 'Are you sure you want to delete this item?',					
				)
			);
		?>
    </div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'kodeBarang',
		'namaBarang',
		'desc',
        array(
            'name'=>'status',
            'value'=>EnumStatusProduct::getLabel($model->status),
        ),
        'qty',
		'stockCode',
		'oz',
		'diameter',
		'weight',
		'volumeml',
		'volumeOz',
		'length',
		'height',
		'cbm',
		'priceFOB',
		'priceMaster',
		'inner',
		'master',
		'moq',
		'artCapasity',
		'nettWeight',
		'grossWeight',
		'obLength',
		'obWidth',
		'obHeight',
		'totalCTN',
		'totalGW',
		'totalCBM',
		'totalUSD',
    ),
)); ?>
    </div>
</div>