<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <table class="table">
        <tr>
            <td>
                <?php echo $form->textField($model,'kodeBarang',array('class'=>'form-control','placeholder'=>'Nama Barang')); ?>
            </td>
            <td>
                <div class="button1">
                    <?php echo CHtml::submitButton('Search', array('class'=>'btn btn-sm')); ?>
                </div>
            </td>
        </tr>
    </table>

<?php $this->endWidget(); ?>

</div><!-- search-form -->