<div class="row">
    <div class="col-sm-12">

        <h3 class="left">Import Product</h3>
        <div class="form-button-container">
            <div class="button1">
                <a href="<?php echo $this->createUrl('index'); ?>"><input type="button" value="Product List" class="btn btn-sm"></a>
                <a href="<?php echo $this->createUrl('add'); ?>"><input type="button" value="Add New Product" class="btn btn-sm"></a>
            </div>
            <div class="clear"></div>
        </div>
        <hr>

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'product-form',
            'enableAjaxValidation'=>false,
        )); ?>

        <?php echo $form->errorSummary($model); ?>

        <div class="row form-row form-group">
            <div class="col-xs-2">
                <?php echo $form->labelEx($model,'filename'); ?>
            </div>
            <div class="col-xs-10">
                <div class="row">
                    <div class="col-xs-6">
                        <div id="image-container" class="uploaded-image">

                        </div>
                        <div class="clear"></div>
                        <?php $this->widget('common.extensions.widgets.qqFileUploader.QQFileUploader', array(
                            'id'=>'qq-file-uploader',
                            'action'=>Yii::app()->request->baseUrl . '/product/uploadExcel',
                            'options'=>array(
                                'allowedExtensions'=>Yii::app()->params['excelExtension'],
                            ),
                            'onComplete'=>"
                            var content = '<input type=\"hidden\" id=\"UploadForm_filename\" name=\"UploadForm[filename]\" value=\"'+ responseJSON.newFileName +'\" />';

                            $('#image-container').html(content);
                        ",
                            'onSubmit' => "$('.qq-upload-list').html('');",
                        ));?>
                    </div>
                    <div class="col-xs-6">
                        <?php echo $form->error($model,'filename'); ?>
                    </div>
                </div>

            </div>
        </div>

        <div class="button1">
            <input type="submit" class="btn btn-sm" value="Mapping Data">
        </div>

        <?php $this->endWidget(); ?>

    </div>
</div>