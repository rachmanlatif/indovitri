<div class="row">
    <div class="col-sm-12">

        <h3 class="left">Product List</h3>
        <div class="button1">
            <a href="<?php echo $this->createUrl('add'); ?>"><input type="button" value="Add" class="btn btn-sm"></a>
            <a href="<?php echo Yii::app()->homeUrl.'masterStock'; ?>"><input type="button" value="List Unit" class="btn btn-sm"></a>
        </div>
        <div class="clear"></div>
        <br>

        <div id="status-message">
            <?php if(Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success">
                    <?php echo Yii::app()->user->getFlash('success') ?>
                </div>
            <?php endif ?>

            <?php if(Yii::app()->user->hasFlash('info')): ?>
                <div class="alert alert-info">
                    <?php echo Yii::app()->user->getFlash('info') ?>
                </div>
            <?php endif ?>

            <?php if(Yii::app()->user->hasFlash('danger')): ?>
                <div class="alert alert-danger">
                    <?php echo Yii::app()->user->getFlash('danger') ?>
                </div>
            <?php endif ?>
        </div>

        <?php $this->renderPartial('_search',array(
            'model'=>$model,
        )); ?>


        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'product-grid',
            'itemsCssClass'=>'table table-striped table-bordered table-hover',
            'dataProvider'=>$model->search(),
            'selectableRows'=>2,
            'columns'=>array(
                'namaBarang',
                'desc',
                'oz',
                'diameter',
                'weight',
                'length',
                'height',
                'cbm',
                array(
                    'name'=>'status',
                    'value'=>'EnumStatusProduct::getLabel($data->status)',
                ),
                array(
                    'class'=>'CButtonColumn',
                ),
            ),
        )); ?>

    </div>
</div>
