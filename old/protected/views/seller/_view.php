<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('sellerID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->sellerID), array('view', 'id'=>$data->sellerID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sellerName')); ?>:</b>
	<?php echo CHtml::encode($data->sellerName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telp')); ?>:</b>
	<?php echo CHtml::encode($data->telp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('HP')); ?>:</b>
	<?php echo CHtml::encode($data->HP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contactPerson')); ?>:</b>
	<?php echo CHtml::encode($data->contactPerson); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeNegara')); ?>:</b>
	<?php echo CHtml::encode($data->kodeNegara); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeProvinsi')); ?>:</b>
	<?php echo CHtml::encode($data->kodeProvinsi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeKota')); ?>:</b>
	<?php echo CHtml::encode($data->kodeKota); ?>
	<br />

	*/ ?>

</div>