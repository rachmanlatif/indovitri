<div class="row">
    <div class="col-sm-12">

        <div>
	<h3 class="left">Create Seller</h3>
	<div class="button1">
		<a href="<?php echo $this->createUrl('index'); ?>"><input class="btn btn-sm" type="button" value="List"></a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

    </div>
</div>