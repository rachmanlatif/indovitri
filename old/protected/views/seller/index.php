<div class="row">
    <div class="col-sm-12">

<div>
	<h3 class="left">Manage Sellers</h3>
	<div class="button1">
		<a href="<?php echo $this->createUrl('add'); ?>"><input class="btn btn-sm" type="button" value="Add"></a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'seller-grid',
    'itemsCssClass'=>'table table-striped table-bordered table-hover',
	'dataProvider'=>$model->search(),
	'selectableRows'=>2,
	'columns'=>array(
		'sellerName',
		'address',
		'telp',
		'status',
		'HP',
		'contactPerson',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

    </div>
</div>
