<div class="row">
    <div class="col-sm-12">

<div>
	<h3 class="left">View Seller</h3>
	<div class="button1">
		<a href="<?php echo $this->createUrl('index'); ?>"><input class="btn btn-sm" type="button" value="List"></a>
		<a href="<?php echo $this->createUrl('add'); ?>"><input class="btn btn-sm" type="button" value="Add"></a>
		<a href="<?php echo $this->createUrl('update', array('id'=>$model->sellerID)); ?>"><input class="btn btn-sm" type="button" value="Update"></a>
		<?php echo CHtml::linkButton('Delete',  
				array(
					'class' => 'button1 btn btn-sm',
					'submit' => array('delete','id'=>$model->sellerID),
					'confirm' => 'Are you sure you want to delete this item?',					
				)
			);
		?>	</div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'sellerID',
		'sellerName',
		'address',
		'telp',
		'status',
		'HP',
		'contactPerson',
		'kodeNegara',
		'kodeProvinsi',
		'kodeKota',
	),
)); ?>

    </div>
</div>
