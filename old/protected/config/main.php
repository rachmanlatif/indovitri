<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'P5TRADE',

    'theme'=>'default',
    'defaultController'=>'home',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
        'application.components.*',
		'application.models.*',
		'application.models.forms.*',
        'common.components.valums.*',
        'common.models.enumerations.*',
        'common.models.forms.*',
	),

	'modules'=>array(
        // uncomment the following to enable the Gii tool
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'admin123',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1'),
        ),
        'administrator'=>array(
            'defaultController'=>'login',
        ),
        'page'=>array(
            'defaultController'=>'login',
        ),
    ),

	// application components
	'components'=>array(
		'bootstrap' => array(
			'class' => 'common.extensions.bootstrap.components.Bootstrap',
			'responsiveCss' => true,
		),
        'user'=>array(
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
        ),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
            'showScriptName'=>false,
			'urlFormat'=>'path',
			'rules'=>array(
                '<controller:\w+>/id/<id:\w+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/id/<id:\w+>'=>'<controller>/<action>',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'error/index',
        ),
        'authManager'=>array(
	        'class'=>'common.components.MyAuthManager',
            //'class'=>'application.modules.srbac.components.SDbAuthManager',
            'connectionID'=>'db',
	        'schemaName'=>'auth',
            'itemTable'=>'auth.items',
            'assignmentTable'=>'auth.assignments',
            'itemChildTable'=>'auth.itemchildren',
        ),
        'swSource'=>array(
            'class'=>'common.components.simpleWorkflow.SWPhpWorkflowSource',
        ),
		'curl' => array(
			'class' => 'common.extensions.curl.Curl'
		),
	),

	// application-level parameters that can be accessed
	'params'=>array(
		// this is used in contact page
		'adminEmail' => 'system@email.co.id'
	),
);
