<?php
$this->breadcrumbs=array(
	'Tokos'=>array('index'),
	$model->kodeToko=>array('view','id'=>$model->kodeToko),
	'Update',
);

$this->menu=array(
	array('label'=>'List Toko', 'url'=>array('index')),
	array('label'=>'Create Toko', 'url'=>array('create')),
	array('label'=>'View Toko', 'url'=>array('view', 'id'=>$model->kodeToko)),
	array('label'=>'Manage Toko', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Update Stores</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->kodeToko)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>