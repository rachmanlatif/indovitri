<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'toko-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'kodePengguna'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'kodePengguna',array('size'=>15,'maxlength'=>15)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'kodePengguna'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'namaToko'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'namaToko',array('size'=>50,'maxlength'=>50)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'namaToko'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'domain'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'domain',array('size'=>20,'maxlength'=>20)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'domain'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'status'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'status'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'status'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'tglDibuat'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'tglDibuat'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'tglDibuat'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>        </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->