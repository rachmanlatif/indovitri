<?php
$this->breadcrumbs=array(
	'Tokos'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Toko', 'url'=>array('index')),
	array('label'=>'Create Toko', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('toko-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div>
	<h1 class="left">Manage Stores</h1>	
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<?php echo CHtml::ajaxLink("Delete", 
				$this->createUrl('deleteSelected'), 
				array(
					'type' => 'post',
					'data' => 'js:{ajax:true, ids:$.fn.yiiGridView.getSelection(\'toko-grid\')}',
					'success' => 'function(data) {
						$.fn.yiiGridView.update(\'toko-grid\');
					}',
				),
				array(
					'class' => 'form-button btn btn-primary',
					'confirm' => 'Are you sure?',
				)
			);
		?>	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'toko-grid',
    'itemsCssClass'=>'table table-striped table-bordered table-hover',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'selectableRows'=>2,
	'columns'=>array(
		array(			
			'class'=>'CCheckBoxColumn',
			'id'=>'ids',
		),
		'kodeToko',
		'kodePengguna',
		'namaToko',
		'domain',
		'status',
		'tglDibuat',
		CGridViewHelper::getCreatedBy(),
		CGridViewHelper::getCreatedDate(),
		CGridViewHelper::getModifiedBy(),
		CGridViewHelper::getModifiedDate(),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
