<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeToko')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kodeToko), array('view', 'id'=>$data->kodeToko)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodePengguna')); ?>:</b>
	<?php echo CHtml::encode($data->kodePengguna); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('namaToko')); ?>:</b>
	<?php echo CHtml::encode($data->namaToko); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('domain')); ?>:</b>
	<?php echo CHtml::encode($data->domain); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglDibuat')); ?>:</b>
	<?php echo CHtml::encode($data->tglDibuat); ?>
	<br />


</div>