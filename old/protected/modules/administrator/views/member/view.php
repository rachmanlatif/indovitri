<?php
$this->breadcrumbs=array(
	'Members'=>array('index'),
	$model->kodeMember,
);

$this->menu=array(
	array('label'=>'List Member', 'url'=>array('index')),
	array('label'=>'Create Member', 'url'=>array('create')),
	array('label'=>'Update Member', 'url'=>array('update', 'id'=>$model->kodeMember)),
	array('label'=>'Delete Member', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->kodeMember),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Member', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">View Member</h1>	
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('update', array('id'=>$model->kodeMember)); ?>">Update</a>
		<?php echo CHtml::linkButton('Delete',  
				array(
					'class' => 'form-button btn btn-primary',
					'submit' => array('delete','id'=>$model->kodeMember),
					'confirm' => 'Are you sure you want to delete this item?',					
				)
			);
		?>	</div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'kodeMember',
		'email',
		'hp',
		'nama',
        array(
            'name'=>'status',
            'value'=>EnumStatus::getLabel($model->status),
        ),
		'lastLoggedIn',
		'sessionKey',
		'sessionExpired',
		'tglDibuat',
	),
)); ?>
