<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeMember')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kodeMember), array('view', 'id'=>$data->kodeMember)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hp')); ?>:</b>
	<?php echo CHtml::encode($data->hp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastLoggedIn')); ?>:</b>
	<?php echo CHtml::encode($data->lastLoggedIn); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('sessionKey')); ?>:</b>
	<?php echo CHtml::encode($data->sessionKey); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sessionExpired')); ?>:</b>
	<?php echo CHtml::encode($data->sessionExpired); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglDibuat')); ?>:</b>
	<?php echo CHtml::encode($data->tglDibuat); ?>
	<br />

	*/ ?>

</div>