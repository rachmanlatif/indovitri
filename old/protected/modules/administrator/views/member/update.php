<?php
$this->breadcrumbs=array(
	'Members'=>array('index'),
	$model->kodeMember=>array('view','id'=>$model->kodeMember),
	'Update',
);

$this->menu=array(
	array('label'=>'List Member', 'url'=>array('index')),
	array('label'=>'Create Member', 'url'=>array('create')),
	array('label'=>'View Member', 'url'=>array('view', 'id'=>$model->kodeMember)),
	array('label'=>'Manage Member', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Update Member</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->kodeMember)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>