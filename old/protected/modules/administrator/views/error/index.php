<div class="error-page">
    <h1 class="headline text-yellow"><?php echo $code; ?></h1>

    <div class="error-content">
        <h2><i class="fa fa-warning text-yellow"></i> Oops! Page is not found.</h2>

        <p>
            <?php echo CHtml::encode($message); ?>
            <br>
            <br>
            <button onclick="window.history.back()" class="btn btn-success btn-sm">Back</button>
        </p>
    </div>
    <!-- /.error-content -->
</div>