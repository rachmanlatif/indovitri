<?php
$this->breadcrumbs=array(
	'Spek Lists'=>array('index'),
	$model->kodeSpek=>array('view','id'=>$model->kodeSpek),
	'Update',
);

$this->menu=array(
	array('label'=>'List SpekList', 'url'=>array('index')),
	array('label'=>'Create SpekList', 'url'=>array('create')),
	array('label'=>'View SpekList', 'url'=>array('view', 'id'=>$model->kodeSpek)),
	array('label'=>'Manage SpekList', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Update Specification Lists</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->kodeSpek)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>