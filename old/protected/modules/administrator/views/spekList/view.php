<?php
$this->breadcrumbs=array(
	'Spek Lists'=>array('index'),
	$model->kodeSpek,
);

$this->menu=array(
	array('label'=>'List SpekList', 'url'=>array('index')),
	array('label'=>'Create SpekList', 'url'=>array('create')),
	array('label'=>'Update SpekList', 'url'=>array('update', 'id'=>$model->kodeSpek)),
	array('label'=>'Delete SpekList', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->kodeSpek),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SpekList', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">View Specification Lists</h1>	
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('update', array('id'=>$model->kodeSpek)); ?>">Update</a>
		<?php echo CHtml::linkButton('Delete',  
				array(
					'class' => 'form-button btn btn-primary',
					'submit' => array('delete','id'=>$model->kodeSpek),
					'confirm' => 'Are you sure you want to delete this item?',					
				)
			);
		?>	</div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'kodeSpek',
		'namaSpek',
		'note',
        array(
            'name'=>'status',
            'value'=>EnumStatus::getLabel($model->status),
        ),
	),
)); ?>
