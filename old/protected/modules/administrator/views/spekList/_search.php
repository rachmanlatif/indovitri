<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <table class="table table-bordered table-striped table-hover">
        <tr>
            <th><?php echo $form->label($model,'kodeSpek'); ?></th>
            <th><?php echo $form->label($model,'namaSpek'); ?></th>
            <th><?php echo $form->label($model,'note'); ?></th>
            <th></th>
        </tr>
        <tr>
            <td>
                <?php echo $form->textField($model,'kodeSpek'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'namaSpek'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'note'); ?>
            </td>
            <td>
                <?php echo CHtml::submitButton('Search', array('class'=>'btn btn-info')); ?>
            </td>
        </tr>
    </table>

<?php $this->endWidget(); ?>

</div><!-- search-form -->