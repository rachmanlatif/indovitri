<?php
$this->breadcrumbs=array(
	'Spek Lists'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SpekList', 'url'=>array('index')),
	array('label'=>'Manage SpekList', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Create Specification Lists</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>