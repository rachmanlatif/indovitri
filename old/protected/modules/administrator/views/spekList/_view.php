<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeSpek')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kodeSpek), array('view', 'id'=>$data->kodeSpek)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('namaSpek')); ?>:</b>
	<?php echo CHtml::encode($data->namaSpek); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('note')); ?>:</b>
	<?php echo CHtml::encode($data->note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>