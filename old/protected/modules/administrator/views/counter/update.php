<?php
$this->breadcrumbs=array(
	'Counters'=>array('index'),
	$model->keyword=>array('view','id'=>$model->keyword),
	'Update',
);

$this->menu=array(
	array('label'=>'List Counter', 'url'=>array('index')),
	array('label'=>'Create Counter', 'url'=>array('create')),
	array('label'=>'View Counter', 'url'=>array('view', 'id'=>$model->keyword)),
	array('label'=>'Manage Counter', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Update Counter</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->keyword)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>