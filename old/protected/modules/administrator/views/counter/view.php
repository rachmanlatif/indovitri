<?php
$this->breadcrumbs=array(
	'Counters'=>array('index'),
	$model->keyword,
);

$this->menu=array(
	array('label'=>'List Counter', 'url'=>array('index')),
	array('label'=>'Create Counter', 'url'=>array('create')),
	array('label'=>'Update Counter', 'url'=>array('update', 'id'=>$model->keyword)),
	array('label'=>'Delete Counter', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->keyword),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Counter', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">View Counter</h1>	
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('update', array('id'=>$model->keyword)); ?>">Update</a>
		<?php echo CHtml::linkButton('Delete',  
				array(
					'class' => 'form-button btn btn-primary',
					'submit' => array('delete','id'=>$model->keyword),
					'confirm' => 'Are you sure you want to delete this item?',					
				)
			);
		?>	</div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'keyword',
		'value',
	),
)); ?>
