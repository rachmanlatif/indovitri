<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeBankAccount')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kodeBankAccount), array('view', 'id'=>$data->kodeBankAccount)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeBank')); ?>:</b>
	<?php echo CHtml::encode($data->kodeBank); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('norek')); ?>:</b>
	<?php echo CHtml::encode($data->norek); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>