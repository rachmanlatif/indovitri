<?php
$this->breadcrumbs=array(
	'Bank Accounts'=>array('index'),
	$model->kodeBankAccount=>array('view','id'=>$model->kodeBankAccount),
	'Update',
);

$this->menu=array(
	array('label'=>'List BankAccount', 'url'=>array('index')),
	array('label'=>'Create BankAccount', 'url'=>array('create')),
	array('label'=>'View BankAccount', 'url'=>array('view', 'id'=>$model->kodeBankAccount)),
	array('label'=>'Manage BankAccount', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Update BankAccount</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->kodeBankAccount)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>