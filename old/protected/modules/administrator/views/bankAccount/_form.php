<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bank-account-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'kodeBank'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'kodeBank',
                        CHtml::listData(BankList::model()->findAllByAttributes(array('status'=>EnumStatus::ACTIVE)), 'kodeBank', 'nama'),
                        array('empty'=>'- Choose -', 'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'kodeBank'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'norek'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'norek',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'norek'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'nama'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'nama',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'nama'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'status'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'status',
                        EnumStatus::getList()); ?>
                    <?php echo $form->error($model,'status'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'status'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>        </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->