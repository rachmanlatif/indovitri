<?php
$this->breadcrumbs=array(
	'Bank Accounts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BankAccount', 'url'=>array('index')),
	array('label'=>'Manage BankAccount', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Create BankAccount</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>