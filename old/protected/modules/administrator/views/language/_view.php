<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeLanguage')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kodeLanguage), array('view', 'id'=>$data->kodeLanguage)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('frasa')); ?>:</b>
	<?php echo CHtml::encode($data->frasa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('value')); ?>:</b>
	<?php echo CHtml::encode($data->value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>