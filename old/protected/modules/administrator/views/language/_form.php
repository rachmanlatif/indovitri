<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'language-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'kodeCountry'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'kodeCountry',
                        CHtml::listData(Country::model()->findAllByAttributes(array('status'=>EnumStatus::ACTIVE)), 'kodeCountry', 'nama'),
                        array('empty'=>'- Choose -', 'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'kodeState'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'frasa'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'frasa',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'frasa'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'value'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'value',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'value'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'status'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'status',
                        EnumStatus::getList()); ?>
                    <?php echo $form->error($model,'status'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'status'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>        </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->