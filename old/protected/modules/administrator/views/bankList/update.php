<?php
$this->breadcrumbs=array(
	'Bank Lists'=>array('index'),
	$model->kodeBank=>array('view','id'=>$model->kodeBank),
	'Update',
);

$this->menu=array(
	array('label'=>'List BankList', 'url'=>array('index')),
	array('label'=>'Create BankList', 'url'=>array('create')),
	array('label'=>'View BankList', 'url'=>array('view', 'id'=>$model->kodeBank)),
	array('label'=>'Manage BankList', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Update BankList</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->kodeBank)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>