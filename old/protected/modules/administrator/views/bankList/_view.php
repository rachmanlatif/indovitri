<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeBank')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kodeBank), array('view', 'id'=>$data->kodeBank)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('swiftCode')); ?>:</b>
	<?php echo CHtml::encode($data->swiftCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>