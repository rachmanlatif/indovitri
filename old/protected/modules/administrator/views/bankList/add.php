<?php
$this->breadcrumbs=array(
	'Bank Lists'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BankList', 'url'=>array('index')),
	array('label'=>'Manage BankList', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Create BankList</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>