<?php
$this->breadcrumbs=array(
	'Bank Lists'=>array('index'),
	$model->kodeBank,
);

$this->menu=array(
	array('label'=>'List BankList', 'url'=>array('index')),
	array('label'=>'Create BankList', 'url'=>array('create')),
	array('label'=>'Update BankList', 'url'=>array('update', 'id'=>$model->kodeBank)),
	array('label'=>'Delete BankList', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->kodeBank),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BankList', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">View BankList</h1>	
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('update', array('id'=>$model->kodeBank)); ?>">Update</a>
		<?php echo CHtml::linkButton('Delete',  
				array(
					'class' => 'form-button btn btn-primary',
					'submit' => array('delete','id'=>$model->kodeBank),
					'confirm' => 'Are you sure you want to delete this item?',					
				)
			);
		?>	</div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'kodeBank',
		'nama',
		'swiftCode',
		'keterangan',
        array(
            'name'=>'status',
            'value'=>EnumStatus::getLabel($model->status),
        ),
	),
)); ?>
