<?php
$this->breadcrumbs=array(
	'Master Koloms'=>array('index'),
	$model->kodeKolom=>array('view','id'=>$model->kodeKolom),
	'Update',
);

$this->menu=array(
	array('label'=>'List MasterKolom', 'url'=>array('index')),
	array('label'=>'Create MasterKolom', 'url'=>array('create')),
	array('label'=>'View MasterKolom', 'url'=>array('view', 'id'=>$model->kodeKolom)),
	array('label'=>'Manage MasterKolom', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Update Master Column</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->kodeKolom)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>