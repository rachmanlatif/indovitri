<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeKolom')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kodeKolom), array('view', 'id'=>$data->kodeKolom)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('namaKolom')); ?>:</b>
	<?php echo CHtml::encode($data->namaKolom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>