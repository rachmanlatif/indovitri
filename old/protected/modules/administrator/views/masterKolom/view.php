<?php
$this->breadcrumbs=array(
	'Master Koloms'=>array('index'),
	$model->kodeKolom,
);

$this->menu=array(
	array('label'=>'List MasterKolom', 'url'=>array('index')),
	array('label'=>'Create MasterKolom', 'url'=>array('create')),
	array('label'=>'Update MasterKolom', 'url'=>array('update', 'id'=>$model->kodeKolom)),
	array('label'=>'Delete MasterKolom', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->kodeKolom),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MasterKolom', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">View Master Column</h1>	
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('update', array('id'=>$model->kodeKolom)); ?>">Update</a>
		<?php echo CHtml::linkButton('Delete',  
				array(
					'class' => 'form-button btn btn-primary',
					'submit' => array('delete','id'=>$model->kodeKolom),
					'confirm' => 'Are you sure you want to delete this item?',					
				)
			);
		?>	</div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'kodeKolom',
		'namaKolom',
        array(
            'name'=>'status',
            'value'=>EnumStatus::getLabel($model->status),
        ),
	),
)); ?>
