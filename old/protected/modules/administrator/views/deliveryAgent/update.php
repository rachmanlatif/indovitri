<?php
$this->breadcrumbs=array(
	'Delivery Agents'=>array('index'),
	$model->KodeDeliveryAgent=>array('view','id'=>$model->KodeDeliveryAgent),
	'Update',
);

$this->menu=array(
	array('label'=>'List DeliveryAgent', 'url'=>array('index')),
	array('label'=>'Create DeliveryAgent', 'url'=>array('create')),
	array('label'=>'View DeliveryAgent', 'url'=>array('view', 'id'=>$model->KodeDeliveryAgent)),
	array('label'=>'Manage DeliveryAgent', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Update DeliveryAgent</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->KodeDeliveryAgent)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>