<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'KodeDeliveryAgent'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'KodeDeliveryAgent',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'nama'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'nama',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'address'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'status'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'status'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'phone'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'email'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'contactPerson'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'contactPerson',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'kodeCity'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'kodeCity',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'kodeState'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'kodeState',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'kodeCountry'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'kodeCountry',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'zipcode'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'zipcode',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

	<div class="row buttons">
        <div class="col-xs-12">
            <?php echo CHtml::submitButton('Search'); ?>
    	</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->