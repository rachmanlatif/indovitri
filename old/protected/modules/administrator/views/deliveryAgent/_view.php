<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeDeliveryAgent')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->KodeDeliveryAgent), array('view', 'id'=>$data->KodeDeliveryAgent)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contactPerson')); ?>:</b>
	<?php echo CHtml::encode($data->contactPerson); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeCity')); ?>:</b>
	<?php echo CHtml::encode($data->kodeCity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeState')); ?>:</b>
	<?php echo CHtml::encode($data->kodeState); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeCountry')); ?>:</b>
	<?php echo CHtml::encode($data->kodeCountry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zipcode')); ?>:</b>
	<?php echo CHtml::encode($data->zipcode); ?>
	<br />

	*/ ?>

</div>