<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeCity')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kodeCity), array('view', 'id'=>$data->kodeCity)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeState')); ?>:</b>
	<?php echo CHtml::encode($data->kodeState); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>