<div>
	<h1 class="left">Manage States</h1>	
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'state-grid',
    'itemsCssClass'=>'table table-striped table-bordered table-hover',
	'dataProvider'=>$model->search(),
	'selectableRows'=>2,
	'columns'=>array(
		'kodeState',
        array(
            'name'=>'kodeCountry',
            'value'=>'$data->country->nama',
        ),
		'nama',
		'keterangan',
        array(
            'name'=>'status',
            'value'=>'EnumStatus::getLabel($data->status)',
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{view} {update} {delete}',
            'buttons'=>array(
                'view'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-search fa-fw"></i> View',
                    'options'=>array(
                        'class'=>'btn btn-info btn-xs',
                        'title'=>'View Detail',
                    ),
                ),
                'update'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-edit fa-fw"></i> Update',
                    'options'=>array(
                        'class'=>'btn btn-warning btn-xs',
                        'title'=>'Update',
                    ),
                ),
                'delete'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-trash-o fa-fw"></i> Delete',
                    'options'=>array(
                        'class'=>'btn btn-danger btn-xs',
                        'title'=>'Delete',
                    ),
                ),
            ),
        ),
	),
)); ?>
