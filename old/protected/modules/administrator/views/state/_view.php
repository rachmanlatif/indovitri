<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeState')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kodeState), array('view', 'id'=>$data->kodeState)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeCountry')); ?>:</b>
	<?php echo CHtml::encode($data->kodeCountry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>