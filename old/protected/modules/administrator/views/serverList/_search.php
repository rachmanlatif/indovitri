<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'kodeServer'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'kodeServer',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'namaServer'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'namaServer',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'username'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'status'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'status'); ?>
                </div>
            </div>

        </div>
    </div>

	<div class="row buttons">
        <div class="col-xs-12">
            <?php echo CHtml::submitButton('Search'); ?>
    	</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->