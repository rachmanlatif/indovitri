<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeServer')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kodeServer), array('view', 'id'=>$data->kodeServer)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('namaServer')); ?>:</b>
	<?php echo CHtml::encode($data->namaServer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>