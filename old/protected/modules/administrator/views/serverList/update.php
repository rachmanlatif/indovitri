<?php
$this->breadcrumbs=array(
	'Server Lists'=>array('index'),
	$model->kodeServer=>array('view','id'=>$model->kodeServer),
	'Update',
);

$this->menu=array(
	array('label'=>'List ServerList', 'url'=>array('index')),
	array('label'=>'Create ServerList', 'url'=>array('create')),
	array('label'=>'View ServerList', 'url'=>array('view', 'id'=>$model->kodeServer)),
	array('label'=>'Manage ServerList', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Update Server Lists</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->kodeServer)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>