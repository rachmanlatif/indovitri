<?php
$this->breadcrumbs=array(
	'Consignes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Consigne', 'url'=>array('index')),
	array('label'=>'Manage Consigne', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Create Consigne</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>