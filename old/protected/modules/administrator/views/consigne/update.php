<?php
$this->breadcrumbs=array(
	'Consignes'=>array('index'),
	$model->KodeConsigne=>array('view','id'=>$model->KodeConsigne),
	'Update',
);

$this->menu=array(
	array('label'=>'List Consigne', 'url'=>array('index')),
	array('label'=>'Create Consigne', 'url'=>array('create')),
	array('label'=>'View Consigne', 'url'=>array('view', 'id'=>$model->KodeConsigne)),
	array('label'=>'Manage Consigne', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Update Consigne</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->KodeConsigne)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>