<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeCurrency')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kodeCurrency), array('view', 'id'=>$data->kodeCurrency)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>