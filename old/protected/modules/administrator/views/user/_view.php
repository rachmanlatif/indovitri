<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastLoggedIn')); ?>:</b>
	<?php echo CHtml::encode($data->lastLoggedIn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sessionKey')); ?>:</b>
	<?php echo CHtml::encode($data->sessionKey); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('sessionExpired')); ?>:</b>
	<?php echo CHtml::encode($data->sessionExpired); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglDibuat')); ?>:</b>
	<?php echo CHtml::encode($data->tglDibuat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dibuatOleh')); ?>:</b>
	<?php echo CHtml::encode($data->dibuatOleh); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglDiubah')); ?>:</b>
	<?php echo CHtml::encode($data->tglDiubah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('diubahOleh')); ?>:</b>
	<?php echo CHtml::encode($data->diubahOleh); ?>
	<br />

	*/ ?>

</div>