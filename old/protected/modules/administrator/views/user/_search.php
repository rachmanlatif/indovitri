<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <table class="table table-bordered table-hover table-striped">
        <tr>
            <th><?php echo $form->label($model,'username'); ?></th>
            <th><?php echo $form->label($model,'nama'); ?></th>
            <th><?php echo $form->label($model,'status'); ?></th>
            <th></th>
        </tr>
        <tr>
            <td>
                <?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>20)); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'nama'); ?>
            </td>
            <td>
                <?php echo $form->dropDownList($model,'status',
                    EnumStatus::getList(), array('class'=>'form-control')); ?>
            </td>
            <td>
                <?php echo CHtml::submitButton('Search', array('class'=>'btn btn-info')); ?>
            </td>
        </tr>
    </table>

<?php $this->endWidget(); ?>

</div><!-- search-form -->