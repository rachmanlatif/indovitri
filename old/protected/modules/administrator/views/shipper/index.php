<?php
$this->breadcrumbs=array(
	'Shippers'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Shipper', 'url'=>array('index')),
	array('label'=>'Create Shipper', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('shipper-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div>
	<h1 class="left">Manage Shippers</h1>	
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'shipper-grid',
    'itemsCssClass'=>'table table-striped table-bordered table-hover',
	'dataProvider'=>$model->search(),
	'selectableRows'=>2,
	'columns'=>array(
		'nama',
		'address',
        array(
            'name'=>'status',
            'value'=>'EnumStatus::getLabel($data->status)',
        ),
		'keterangan',
        array(
            'name'=>'kodeCity',
            'value'=>'$data->city->nama',
        ),
        array(
            'name'=>'kodeState',
            'value'=>'$data->state->nama',
        ),
        array(
            'name'=>'kodeCountry',
            'value'=>'$data->country->nama',
        ),
		'zipcode',
        array(
            'class'=>'CButtonColumn',
            'template'=>'{view} {update} {delete}',
            'buttons'=>array(
                'view'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-search fa-fw"></i> View',
                    'options'=>array(
                        'class'=>'btn btn-info btn-xs',
                        'title'=>'View Detail',
                    ),
                ),
                'update'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-edit fa-fw"></i> Update',
                    'options'=>array(
                        'class'=>'btn btn-warning btn-xs',
                        'title'=>'Update',
                    ),
                ),
                'delete'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-trash-o fa-fw"></i> Delete',
                    'options'=>array(
                        'class'=>'btn btn-danger btn-xs',
                        'title'=>'Delete',
                    ),
                ),
            ),
        ),
	),
)); ?>
