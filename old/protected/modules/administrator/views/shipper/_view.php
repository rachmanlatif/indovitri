<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('KodeShipper')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->KodeShipper), array('view', 'id'=>$data->KodeShipper)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeCity')); ?>:</b>
	<?php echo CHtml::encode($data->kodeCity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeState')); ?>:</b>
	<?php echo CHtml::encode($data->kodeState); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeCountry')); ?>:</b>
	<?php echo CHtml::encode($data->kodeCountry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zipcode')); ?>:</b>
	<?php echo CHtml::encode($data->zipcode); ?>
	<br />

	*/ ?>

</div>