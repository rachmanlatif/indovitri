<?php
$this->breadcrumbs=array(
	'Shippers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Shipper', 'url'=>array('index')),
	array('label'=>'Manage Shipper', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Create Shipper</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>