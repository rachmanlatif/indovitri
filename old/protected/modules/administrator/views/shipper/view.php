<div>
	<h1 class="left">View Shipper</h1>	
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('update', array('id'=>$model->KodeShipper)); ?>">Update</a>
		<?php echo CHtml::linkButton('Delete',  
				array(
					'class' => 'form-button btn btn-primary',
					'submit' => array('delete','id'=>$model->KodeShipper),
					'confirm' => 'Are you sure you want to delete this item?',					
				)
			);
		?>	</div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'KodeShipper',
		'nama',
		'address',
        array(
            'name'=>'status',
            'value'=>EnumStatus::getLabel($model->status),
        ),
		'keterangan',
        array(
            'name'=>'kodeState',
            'value'=>$model->state->nama,
        ),
        array(
            'name'=>'kodeCity',
            'value'=>$model->city->nama,
        ),
        array(
            'name'=>'kodeCountry',
            'value'=>$model->country->nama,
        ),
		'zipcode',
	),
)); ?>
