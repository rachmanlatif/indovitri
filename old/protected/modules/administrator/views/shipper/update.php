<?php
$this->breadcrumbs=array(
	'Shippers'=>array('index'),
	$model->KodeShipper=>array('view','id'=>$model->KodeShipper),
	'Update',
);

$this->menu=array(
	array('label'=>'List Shipper', 'url'=>array('index')),
	array('label'=>'Create Shipper', 'url'=>array('create')),
	array('label'=>'View Shipper', 'url'=>array('view', 'id'=>$model->KodeShipper)),
	array('label'=>'Manage Shipper', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Update Shipper</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->KodeShipper)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>