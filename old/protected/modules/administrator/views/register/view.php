<?php
$this->breadcrumbs=array(
	'Registers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Register', 'url'=>array('index')),
	array('label'=>'Create Register', 'url'=>array('create')),
	array('label'=>'Update Register', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Register', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Register', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">View Register</h1>	
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
    </div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nama',
		'hp',
        array(
            'name'=>'status',
            'value'=>EnumStatus::getLabel($model->status),
        ),
		'email',
		'tglDibuat',
	),
)); ?>
