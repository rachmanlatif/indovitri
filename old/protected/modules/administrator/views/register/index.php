<?php
$this->breadcrumbs=array(
	'Registers'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Register', 'url'=>array('index')),
	array('label'=>'Create Register', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('register-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div>
	<h1 class="left">Manage Registers</h1>	
</div>
<div class="clear"></div>
<hr />

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'register-grid',
    'itemsCssClass'=>'table table-striped table-bordered table-hover',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'nama',
		'hp',
        array(
            'name'=>'status',
            'value'=>'EnumStatus::getLabel($data->status)',
        ),
		'email',
		CGridViewHelper::getCreatedDate(),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{view}',
            'buttons'=>array(
                'view'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-search fa-fw"></i> View',
                    'options'=>array(
                        'class'=>'btn btn-info btn-xs',
                        'title'=>'View Detail',
                    ),
                ),
            ),
        ),
	),
)); ?>
