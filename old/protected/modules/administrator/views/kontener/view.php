<?php
$this->breadcrumbs=array(
	'Konteners'=>array('index'),
	$model->kodeKontener,
);

$this->menu=array(
	array('label'=>'List Kontener', 'url'=>array('index')),
	array('label'=>'Create Kontener', 'url'=>array('create')),
	array('label'=>'Update Kontener', 'url'=>array('update', 'id'=>$model->kodeKontener)),
	array('label'=>'Delete Kontener', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->kodeKontener),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Kontener', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">View Container</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('update', array('id'=>$model->kodeKontener)); ?>">Update</a>
		<?php echo CHtml::linkButton('Delete',  
				array(
					'class' => 'form-button btn btn-primary',
					'submit' => array('delete','id'=>$model->kodeKontener),
					'confirm' => 'Are you sure you want to delete this item?',					
				)
			);
		?>	</div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'kodeKontener',
		'nama',
		'dimensiLuarPanjang',
		'dimensiLuarLebar',
		'dimensiLuarTinggi',
        array(
            'type'=>'raw',
            'name'=>'Image',
            'value'=>'<img width="200" height="150" src="'.$model->pathPhoto.'" />',
        ),
		'dimensiDalamPanjang',
		'dimensiDalamLebar',
		'dimensiDalamTinggi',
		'bukaPintuLebar',
		'bukaPintuTinggi',
		'volume',
		'beratKotor',
		'beratKosong',
		'muatanBersih',
        array(
            'name'=>'status',
            'value'=>EnumStatus::getLabel($model->status),
        ),
	),
)); ?>
