<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeKontener')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kodeKontener), array('view', 'id'=>$data->kodeKontener)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dimensiLuarPanjang')); ?>:</b>
	<?php echo CHtml::encode($data->dimensiLuarPanjang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dimensiLuarLebar')); ?>:</b>
	<?php echo CHtml::encode($data->dimensiLuarLebar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dimensiLuarTinggi')); ?>:</b>
	<?php echo CHtml::encode($data->dimensiLuarTinggi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('photo')); ?>:</b>
	<?php echo CHtml::encode($data->photo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pathPhoto')); ?>:</b>
	<?php echo CHtml::encode($data->pathPhoto); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('dimensiDalamPanjang')); ?>:</b>
	<?php echo CHtml::encode($data->dimensiDalamPanjang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dimensiDalamLebar')); ?>:</b>
	<?php echo CHtml::encode($data->dimensiDalamLebar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dimensiDalamTinggi')); ?>:</b>
	<?php echo CHtml::encode($data->dimensiDalamTinggi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bukaPintuLebar')); ?>:</b>
	<?php echo CHtml::encode($data->bukaPintuLebar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bukaPintuTinggi')); ?>:</b>
	<?php echo CHtml::encode($data->bukaPintuTinggi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('volume')); ?>:</b>
	<?php echo CHtml::encode($data->volume); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('beratKotor')); ?>:</b>
	<?php echo CHtml::encode($data->beratKotor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('beratKosong')); ?>:</b>
	<?php echo CHtml::encode($data->beratKosong); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('muatanBersih')); ?>:</b>
	<?php echo CHtml::encode($data->muatanBersih); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	*/ ?>

</div>