<?php
$this->breadcrumbs=array(
	'Konteners'=>array('index'),
	$model->kodeKontener=>array('view','id'=>$model->kodeKontener),
	'Update',
);

$this->menu=array(
	array('label'=>'List Kontener', 'url'=>array('index')),
	array('label'=>'Create Kontener', 'url'=>array('create')),
	array('label'=>'View Kontener', 'url'=>array('view', 'id'=>$model->kodeKontener)),
	array('label'=>'Manage Kontener', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Update Container</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->kodeKontener)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>