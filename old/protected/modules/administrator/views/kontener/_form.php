<?php
$cs = Yii::app()->clientScript;

$cs->registerScript('form-specific', "
	$(document).ready(function(){
		$('#image-container').on('click','.delete-image', function(){
			if (confirm('Are you sure?')) {
				$.ajax({
					type: 'post',
					url: '". Yii::app()->request->baseUrl ."/administrator/kontener/deleteFile',
					data: {photo: $('.Kontener_delete').val()},
					success: function(response) {
						$('#image-container').html('');
					}
				});
			}
			return false;
		});

	});
");
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'kontener-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'photo'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php if($model->pathPhoto != ''){ ?>
                        <img src='<?php echo $model->pathPhoto; ?>' width="200" height="150">
                    <?php } ?>
                    <div id="image-container" class="uploaded-image">
                        <div id="image-delete">

                        </div>
                    </div>
                    <div class="clear"></div>
                    <?php $this->widget('common.extensions.widgets.qqFileUploader.QQFileUploader', array(
                        'id'=>'qq-file-uploader',
                        'action'=>Yii::app()->request->baseUrl . '/administrator/kontener/upload',
                        'options'=>array(
                            'allowedExtensions'=>Yii::app()->params['imageExtension'],
                        ),
                        'onComplete'=>"
                                var content = '<input type=\"hidden\" class=\"Kontener_delete\" id=\"Kontener_pathPhoto\" name=\"Kontener[photo]\" value=\"'+ responseJSON.newFileName +'\" />';
                                content += '<a href=\"javascript:void(0)\" class=\"delete-image\" id=\"delete-image\"><i class=\"fa fa-file-image-o \"/> Delete file</a><br>';
                                content += '<img src=\"".Kontener::getFileUrl()."'+responseJSON.newFileName +' \" width=\"200\" height=\"150\"><br>';

                                $('#image-container').append(content);
                            ",
                        'onSubmit' => "$('.qq-upload-list').html('');",
                    ));?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'photo'); ?>
                </div>
            </div>

        </div>
    </div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'nama'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'nama',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'nama'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'dimensiLuarPanjang'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'dimensiLuarPanjang'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'dimensiLuarPanjang'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'dimensiLuarLebar'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'dimensiLuarLebar'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'dimensiLuarLebar'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'dimensiLuarTinggi'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'dimensiLuarTinggi'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'dimensiLuarTinggi'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'dimensiDalamPanjang'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'dimensiDalamPanjang'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'dimensiDalamPanjang'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'dimensiDalamLebar'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'dimensiDalamLebar'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'dimensiDalamLebar'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'dimensiDalamTinggi'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'dimensiDalamTinggi'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'dimensiDalamTinggi'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'bukaPintuLebar'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'bukaPintuLebar'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'bukaPintuLebar'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'bukaPintuTinggi'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'bukaPintuTinggi'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'bukaPintuTinggi'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'volume'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'volume'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'volume'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'beratKotor'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'beratKotor'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'beratKotor'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'beratKosong'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'beratKosong'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'beratKosong'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'muatanBersih'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'muatanBersih'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'muatanBersih'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'status'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'status',
                        EnumStatus::getList()); ?>
                    <?php echo $form->error($model,'status'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'status'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>        </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->