<?php
$this->breadcrumbs=array(
	'List Koneksis'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ListKoneksi', 'url'=>array('index')),
	array('label'=>'Create ListKoneksi', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('list-koneksi-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div>
	<h1 class="left">Manage Connection List</h1>	
</div>
<div class="clear"></div>
<hr />

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'list-koneksi-grid',
    'itemsCssClass'=>'table table-striped table-bordered table-hover',
	'dataProvider'=>$model->search(),
	'selectableRows'=>2,
	'columns'=>array(
		'kodeToko',
		'db',
		'username',
		'password',
		'server',
		'status',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
