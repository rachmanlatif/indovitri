<?php
$this->breadcrumbs=array(
	'List Koneksis'=>array('index'),
	$model->kodeToko,
);

$this->menu=array(
	array('label'=>'List ListKoneksi', 'url'=>array('index')),
	array('label'=>'Create ListKoneksi', 'url'=>array('create')),
	array('label'=>'Update ListKoneksi', 'url'=>array('update', 'id'=>$model->kodeToko)),
	array('label'=>'Delete ListKoneksi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->kodeToko),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ListKoneksi', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">View Connection List</h1>	
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('update', array('id'=>$model->kodeToko)); ?>">Update</a>
		<?php echo CHtml::linkButton('Delete',  
				array(
					'class' => 'form-button btn btn-primary',
					'submit' => array('delete','id'=>$model->kodeToko),
					'confirm' => 'Are you sure you want to delete this item?',					
				)
			);
		?>	</div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'kodeToko',
		'db',
		'username',
		'password',
		'server',
		'status',
		CDetailViewHelper::getCreatedBy($model),
		CDetailViewHelper::getCreatedDate($model),
		CDetailViewHelper::getModifiedBy($model),
		CDetailViewHelper::getModifiedDate($model),
	),
)); ?>
