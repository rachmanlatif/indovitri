<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeToko')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kodeToko), array('view', 'id'=>$data->kodeToko)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('db')); ?>:</b>
	<?php echo CHtml::encode($data->db); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('server')); ?>:</b>
	<?php echo CHtml::encode($data->server); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>