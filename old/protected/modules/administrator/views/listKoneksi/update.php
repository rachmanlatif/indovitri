<?php
$this->breadcrumbs=array(
	'List Koneksis'=>array('index'),
	$model->kodeToko=>array('view','id'=>$model->kodeToko),
	'Update',
);

$this->menu=array(
	array('label'=>'List ListKoneksi', 'url'=>array('index')),
	array('label'=>'Create ListKoneksi', 'url'=>array('create')),
	array('label'=>'View ListKoneksi', 'url'=>array('view', 'id'=>$model->kodeToko)),
	array('label'=>'Manage ListKoneksi', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Update Connection List</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->kodeToko)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>