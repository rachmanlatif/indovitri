<div>
	<h1 class="left">View ListOrder</h1>	
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
    </div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'orderID',
		'tanggal',
		'ip',
		'kodeMember',
		'kodeToko',
		'kodeKontener',
        array(
            'name'=>'status',
            'value'=>EnumOrder::getLabel($model->status),
        ),
        'IDFactory',
	),
)); ?>
<hr>
<?php if($model->details != null){ ?>
    <table class="table table-bordered table-striped table-hover">
        <tr>
            <th>Product Code</th>
            <th>Price</th>
            <th>Volume</th>
            <th>Qty</th>
            <th>Total</th>
        </tr>
        <?php foreach($model->details as $detail){ ?>
            <tr>
                <td>
                    <?php echo $detail->kodeBarang; ?>
                </td>
                <td><?php echo number_format($detail->price); ?></td>
                <td><?php echo $detail->volume; ?></td>
                <td><?php echo number_format($detail->qty); ?></td>
                <td><?php echo number_format($detail->total); ?></td>
            </tr>
        <?php } ?>
    </table>
<?php } ?>
