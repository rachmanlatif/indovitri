<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'list-order-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'tanggal'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'tanggal'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'tanggal'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'ip'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'ip',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'ip'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'kodeMember'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'kodeMember',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'kodeMember'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'kodeToko'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'kodeToko',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'kodeToko'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'kodeKontener'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'kodeKontener',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'kodeKontener'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'status'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'status'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'status'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'IDFactory'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'IDFactory',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'IDFactory'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>        </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->