<?php
$this->breadcrumbs=array(
	'List Orders'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ListOrder', 'url'=>array('index')),
	array('label'=>'Create ListOrder', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('list-order-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div>
	<h1 class="left">Manage List Orders</h1>
</div>
<div class="clear"></div>
<hr />

<div id="status-message">
    <?php if(Yii::app()->user->hasFlash('success')): ?>
        <div class="alert alert-success">
            <?php echo Yii::app()->user->getFlash('success') ?>
        </div>
    <?php endif ?>

    <?php if(Yii::app()->user->hasFlash('info')): ?>
        <div class="alert alert-info">
            <?php echo Yii::app()->user->getFlash('info') ?>
        </div>
    <?php endif ?>

    <?php if(Yii::app()->user->hasFlash('danger')): ?>
        <div class="alert alert-danger">
            <?php echo Yii::app()->user->getFlash('danger') ?>
        </div>
    <?php endif ?>
</div>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'list-order-grid',
    'itemsCssClass'=>'table table-striped table-bordered table-hover',
	'dataProvider'=>$model->search(),
	'selectableRows'=>2,
	'columns'=>array(
		'orderID',
		'tanggal',
		'kodeMember',
		'kodeToko',
		'kodeKontener',
		'IDFactory',
        array(
            'name'=>'status',
            'value'=>'EnumOrder::getLabel($data->status)',
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{view}',
            'buttons'=>array(
                'view'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-search fa-fw"></i> View',
                    'options'=>array(
                        'class'=>'btn btn-info btn-xs',
                        'title'=>'View Detail',
                    ),
                ),
            ),
        ),
	),
)); ?>
