<?php
$this->breadcrumbs=array(
	'List Orders'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ListOrder', 'url'=>array('index')),
	array('label'=>'Manage ListOrder', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Create ListOrder</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">New</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>