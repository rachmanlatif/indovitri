<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('orderID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->orderID), array('view', 'id'=>$data->orderID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ip')); ?>:</b>
	<?php echo CHtml::encode($data->ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeMember')); ?>:</b>
	<?php echo CHtml::encode($data->kodeMember); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeToko')); ?>:</b>
	<?php echo CHtml::encode($data->kodeToko); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeKontener')); ?>:</b>
	<?php echo CHtml::encode($data->kodeKontener); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('IDFactory')); ?>:</b>
	<?php echo CHtml::encode($data->IDFactory); ?>
	<br />

	*/ ?>

</div>