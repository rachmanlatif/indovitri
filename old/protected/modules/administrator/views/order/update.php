<?php
$this->breadcrumbs=array(
	'List Orders'=>array('index'),
	$model->orderID=>array('view','id'=>$model->orderID),
	'Update',
);

$this->menu=array(
	array('label'=>'List ListOrder', 'url'=>array('index')),
	array('label'=>'Create ListOrder', 'url'=>array('create')),
	array('label'=>'View ListOrder', 'url'=>array('view', 'id'=>$model->orderID)),
	array('label'=>'Manage ListOrder', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Update ListOrder</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->orderID)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>