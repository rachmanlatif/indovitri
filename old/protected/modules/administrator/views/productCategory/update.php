<?php
$this->breadcrumbs=array(
	'Product Categories'=>array('index'),
	$model->kodeProductCategory=>array('view','id'=>$model->kodeProductCategory),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProductCategory', 'url'=>array('index')),
	array('label'=>'Create ProductCategory', 'url'=>array('create')),
	array('label'=>'View ProductCategory', 'url'=>array('view', 'id'=>$model->kodeProductCategory)),
	array('label'=>'Manage ProductCategory', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">Update Product Categories</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->kodeProductCategory)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>