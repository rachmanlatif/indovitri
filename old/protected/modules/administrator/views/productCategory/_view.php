<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodeProductCategory')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kodeProductCategory), array('view', 'id'=>$data->kodeProductCategory)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>