<?php

class KontenerController extends AdminController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array();
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array();
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd()
	{
		$model=new Kontener;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Kontener']))
		{
			$model->attributes=$_POST['Kontener'];
            $model->kodeKontener = IDGenerator::generate('container');
            $model->pathPhoto = Kontener::getFileUrl().$_POST['Kontener']['photo'];
            $model->photo = $_POST['Kontener']['photo'];

			if($model->save())
				$this->redirect(array('view','id'=>$model->kodeKontener));
		}

		$this->render('add',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Kontener']))
		{
			$model->attributes=$_POST['Kontener'];
            if($model->photo != ''){
                $model->pathPhoto = Kontener::getFileUrl().$_POST['Kontener']['photo'];
                $model->photo = $_POST['Kontener']['photo'];
            }

			if($model->save())
				$this->redirect(array('view','id'=>$model->kodeKontener));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	/**
	 * Deletes a list of model.	 
	 */
	public function actionDeleteSelected()
	{
		if(Yii::app()->request->isPostRequest && isset($_POST['ids']))
		{
			// delete
			foreach ($_POST['ids'] as $id) {
				$this->loadModel($id)->delete();
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Kontener('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Kontener']))
			$model->attributes=$_GET['Kontener'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Kontener::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='kontener-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionUpload()
    {
        $allowedExtensions = Yii::app()->params['imageExtension'];
        $sizeLimit = 10 * 1024 * 1024; // 10mb

        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $replaceOldFile = TRUE;
        $result = $uploader->handleUpload(Kontener::getFilePath(), $replaceOldFile, '');

        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
    }

    public function actionDeleteFile() {
        if (isset($_POST['photo'])) {
            self::deleteFile($_POST['photo']);
        }
    }

    public static function deleteFile($file) {
        @unlink(Kontener::getFilePath() . $file);
    }

    public static function deleteUnlinkedFile()
    {

        $photo = array();
        $models = Kontener::model()->findAll();
        foreach ($models as $model) {
            array_push($photo, $model['pathPhoto']);
        }

        RFile::deleteUnexcludedFile(Kontener::getFilePath(), $photo);

    }
}
