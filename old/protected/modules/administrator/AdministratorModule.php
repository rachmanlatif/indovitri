<?php

class AdministratorModule extends CWebModule
{
    /**
     * Initialize Module
     */
    public function init() {
        // import module level models and components
        $this->setImport(array(
            'administrator.models.*',
            'administrator.components.*',
        ));
    }
}