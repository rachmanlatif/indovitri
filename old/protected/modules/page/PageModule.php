<?php

class PageModule extends CWebModule
{
    /**
     * Initialize Module
     */
    public function init() {
        // import module level models and components
        $this->setImport(array(
            'page.models.*',
            'page.components.*',
        ));
    }
}