<?php

class LoginController extends Controller
{

    public function init()
    {
        parent::init();

        // authorize user login
        if (MyHelper::getUser() != null) {
            $this->redirect(Yii::app()->homeUrl);
        }

    }

    public function actionIndex(){

        $model = new UserForm();
        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        // collect user input data
        if(isset($_POST['UserForm']))
        {
            $model->attributes=$_POST['UserForm'];

            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login()) {
                // redirect to select module page
                // sql query for calling the procedure
                $toko = MyHelper::getToko();
                if($toko != null)
                {
                    $sql = "select* from dbo.ListKoneksi where kodeToko = '".$toko[0]['kodeToko']."'";
                    // execute the sql command
                    $conn =  Yii::app()->db->createCommand($sql)->queryAll();
                    if($conn != null){
                        $_SESSION['db'] = $conn[0]['db'];
                    }
                }

                $status = 0;
                $profile = ProfileMember::model()->findByAttributes(array('kodeMember'=>Yii::app()->user->id));
                if($profile != null){
                    $address = ProfileAddress::model()->findByAttributes(array('kodeMember'=>Yii::app()->user->id));
                    if($address != null){
                        $status = 1;
                    }
                }

                if($status == 0){
                    $this->redirect(Yii::app()->homeUrl.'profile/change');
                }
                else{
                    $this->redirect(Yii::app()->homeUrl.'home');
                }
            }
        }

        $this->render('index',array(
            'model'=>$model,
            'category'=>$category,
        ));
    }
}
?>