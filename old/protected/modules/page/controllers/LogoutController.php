<?php

class LogoutController extends UserController
{

    public function actionIndex(){
        // change status_login
        $user = MyHelper::getUser();
        if ($user != null) {
            $user->scenario = 'logout';
            $user->save();
        }

        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}
?>