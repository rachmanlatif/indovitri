<?php

class ConfirmController extends Controller
{

    public $layout='//layouts/empty';

    public function actionRejectOrder($id){
        $model = ListOrder::model()->findByPk($id);
        if($model != null){
            $model->status = 5;
            if($model->save()){
                EmailHelper::sendRejectFactory($model->orderID);

                Yii::app()->user->setFlash('info','Success reject this order');
                $this->redirect(Yii::app()->homeUrl);
            }
            else{
                Yii::app()->user->setFlash('danger','Failed reject this order');
                $this->redirect(Yii::app()->homeUrl);
            }
        }
        else{
            throw new CHttpException(404,'The requested page does not exist.');
        }
    }

    public function actionOrder($id){
        $model = ListOrder::model()->findByPk($id);
        if($model != null){
            $confirm = KonfirmasiOrderDetail::model()->findByAttributes(array('orderID'=>$model->orderID));
            if($confirm == null){
                if(isset($_POST['Confirmation'])){
                    $tempDetail = array();
                    foreach ($_POST['Confirmation'] as $d) {
                        $detail = new KonfirmasiOrderDetail();
                        $detail->attributes = $d;

                        array_push($tempDetail, $detail);
                    }
                    $model->confirms = $tempDetail;

                    $model->status = 3;//approve
                    if($model->save()){
                        EmailHelper::sendApprovalFactory($model->orderID);

                        foreach($model->confirms as $d){
                            $d->total = $d->qty * $d->price;
                            if(!$d->save()){
                                $message = '';
                                $errors = $d->getErrors();
                                foreach ($errors as $error){
                                    foreach ($error as $e){
                                        $message.= $e.'<br />';
                                    }
                                }
                                throw new Exception('Error when saving detail drug '.$message);
                            }

                        }
                    }

                    Yii::app()->user->setFlash('success','Success confirm this order');
                    $this->redirect(Yii::app()->homeUrl);
                }

                $this->render('index', array(
                    'model'=>$model,
                ));
            }
            else{
                Yii::app()->user->setFlash('info','You already confirm this order');
                $this->redirect(Yii::app()->homeUrl);
            }
        }
        else{
            throw new CHttpException(404,'The requested page does not exist.');
        }
    }

    public function actionEmail($e){

        $model = Register::model()->findByPk($e);
        if($model != null){
            $model->status = Member::STATUS_ACTIVE;
            $model->save();

            $member = new Member();
            $member->kodeMember = IDGenerator::generate('member');
            $member->email = $model->email;
            $member->nama = $model->nama;
            $member->hp = $model->hp;
            $member->password = $model->password;
            $member->idRegister = $model->id;
            if(!$member->save()){
                $message = '';
                $errors = $member->getErrors();
                foreach ($errors as $error){
                    foreach ($error as $e){
                        $message.= $e.'<br />';
                    }
                }
                throw new Exception('Error when confirm email'.$message);
            }

            Yii::app()->user->setFlash('success','Confirmation success!');
            $this->redirect(Yii::app()->homeUrl.'page/register');
        }
        else{
            Yii::app()->user->setFlash('danger','Failed confirmation email!');
            $this->redirect(Yii::app()->homeUrl.'page/register');
        }
    }

    public function actionToko($t){

        $model = Toko::model()->findByAttributes(array('kodeToko'=>$t, 'status'=>0));
        if($model != null){
            $sql = "exec dbo.ProsesVerifikasiToko '".$model->kodeToko."'";
            // execute the sql command
            $exec =  Yii::app()->db->createCommand($sql);
            if($exec->execute()){
                Yii::app()->user->setFlash('success','Confirmation success! Please re-login');
                $this->redirect(Yii::app()->homeUrl.'page/login');
            }
            else{
                Yii::app()->user->setFlash('info','Failed confirmation! Please try again');
                $this->redirect(Yii::app()->homeUrl.'page/login');
            }
        }
        else{
            Yii::app()->user->setFlash('danger','Not found!');
            $this->redirect(Yii::app()->homeUrl);
        }
    }
}

?>
