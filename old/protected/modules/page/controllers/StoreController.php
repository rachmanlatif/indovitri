<?php

class StoreController extends UserController
{
		/**
		 * Creates a new model.
		 * If creation is successful, the browser will be redirected to the 'view' page.
		 */
		public function actionIndex()
		{
				$model=new Toko;

				// Uncomment the following line if AJAX validation is needed
				// $this->performAjaxValidation($model);

				if(isset($_POST['Toko']))
				{
            $model->attributes = $_POST['Toko'];

						$toko = Toko::model()->findByAttributes(array('kodeMember'=>Yii::app()->user->id));
						if($toko == null){
								$id = IDGenerator::generate('store');
								$sql = "exec dbo.ProsesToko '".$id."','".Yii::app()->user->id."','".$_POST['Toko']['namaToko']."','".$_POST['Toko']['domain']."',0";
								// execute the sql command
								$exec =  Yii::app()->db->createCommand($sql);
								if($exec->execute()){
										$email = EmailHelper::sendRegisterStore($model->kodeToko);
										if($email){
												Yii::app()->user->setFlash('success','Register store success! You must confirm your email.');
										}
										else{
												Yii::app()->user->setFlash('success','Register store success! Failed send confirmation email.');
										}

										$this->redirect(Yii::app()->homeUrl.'page/store');
								}
						}
						else{
								Yii::app()->user->setFlash('info','Register store failed! You already have store.');
								$this->redirect(Yii::app()->homeUrl.'page/store');
						}
				}

				$this->render('index',array(
					'model'=>$model,
				));
		}

    public function actionChange(){
        $model = MyHelper::getToko();

        $this->render('change',array(
            'model'=>$model,
        ));
    }
}
