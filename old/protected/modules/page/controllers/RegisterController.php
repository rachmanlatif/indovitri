<?php

class RegisterController extends Controller
{

    /**
     * Initialize
     */
    public function init()
    {
        parent::init();

        // authorize user login
        if (MyHelper::getUser() != null) {
            $this->redirect(Yii::app()->homeUrl);
        }

    }

    public function actionIndex(){

        $model=new Register;
        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Register']))
        {
            $model->attributes=$_POST['Register'];

            if($model->save()){
                $email = MyHelper::sendRegister($model->id);
                if($email){
                    Yii::app()->user->setFlash('success','Register success! You must confirm your email.');
                }
                else{
                    Yii::app()->user->setFlash('success','Register success! Failed send confirmation email.');
                }

                $this->redirect(Yii::app()->homeUrl.'page/register');

            }
        }

        $this->render('index',array(
            'model'=>$model,
            'category'=>$category,
        ));
    }
}
?>
