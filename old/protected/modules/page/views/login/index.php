<div class="login">
    <div class="row">
        <div class="col-sm-6">
            <h4 class="title">New Customers</h4>
            <p>Register your account here to start shopping from P5trade!</p>
            <a href="<?php echo Yii::app()->baseUrl; ?>/page/register"><button type="button" class="grey">Register</button></a>
        </div>
        <div class="col-sm-6">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'enableAjaxValidation' => false,
            ));
            ?>
            <div class="login-title">
                <h4 class="title">Registered Customers</h4>
                <div id="status-message">
                    <?php if(Yii::app()->user->hasFlash('success')): ?>
                        <div class="alert alert-success">
                            <?php echo Yii::app()->user->getFlash('success') ?>
                        </div>
                    <?php endif ?>

                    <?php if(Yii::app()->user->hasFlash('info')): ?>
                        <div class="alert alert-info">
                            <?php echo Yii::app()->user->getFlash('info') ?>
                        </div>
                    <?php endif ?>

                    <?php if(Yii::app()->user->hasFlash('danger')): ?>
                        <div class="alert alert-danger">
                            <?php echo Yii::app()->user->getFlash('danger') ?>
                        </div>
                    <?php endif ?>
                </div>

                <div>
                    <p>
                        <label>Email</label>
                        <span>*</span>
                        <?php echo $form->textField($model, 'email',array('class'=>'form-control', 'placeholder'=>'Email')); ?>
                        <?php echo $form->error($model, 'email'); ?>
                    </p>
                    <p>
                        <label>Password</label>
                        <span>*</span>
                        <?php echo $form->passwordField($model, 'password',array('class'=>'form-control', 'placeholder'=>'Password')); ?>
                        <?php echo $form->error($model, 'password'); ?>
                    </p>
                    <p class="pull-left">
                        <label><a href="#">Forget Your Password ? </a></label>
                    </p>
                    <p class="pull-right">
                        <button type="submit" class="grey">Login</button>
                    </p>
                </div>
            </div>

            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>