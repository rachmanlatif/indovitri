<h3>Order ID <?php echo $model->orderID; ?></h3>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'product-form',
    'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>

<a href="<?php echo $this->createUrl('rejectOrder', array('id'=>$model->orderID)); ?>"><input type="button" class="btn btn-sm btn-warning" value="Reject All"></a>
<hr>

<table class="table table-bordered table-striped table-hover">
    <tr>
        <th>Product Code</th>
        <th>Product Name</th>
        <th>Diameter</th>
        <th>Weight</th>
        <th>Lenght</th>
        <th>Height</th>
        <th>CBM</th>
        <th>Quantity Request</th>
        <th>Quantity Approve</th>
        <th></th>
    </tr>
    <?php
    if($model->details != null){
        $index = 0;
        $qty = 0;
        foreach($model->details as $detail){
            $barang = MyHelper::getProduct($detail->kodeBarang);
            if($barang != null){
                $qty = number_format($detail->qty);
            ?>
                <?php echo CHtml::hiddenField("Confirmation[".$index."][orderID]",$model->orderID,array('readonly'=>true)) ?>
                <?php echo CHtml::hiddenField("Confirmation[".$index."][kodeBarang]",$barang[0]['kodeBarang'],array('readonly'=>true)) ?>
                <?php echo CHtml::hiddenField("Confirmation[".$index."][price]",$detail->price,array('readonly'=>true)) ?>
                <?php echo CHtml::hiddenField("Confirmation[".$index."][disc]",$detail->disc,array('readonly'=>true)) ?>
                <?php echo CHtml::hiddenField("Confirmation[".$index."][discMoney]",$detail->discMoney,array('readonly'=>true)) ?>
                <?php echo CHtml::hiddenField("Confirmation[".$index."][vat]",$detail->vat,array('readonly'=>true)) ?>
                <?php echo CHtml::hiddenField("Confirmation[".$index."][vatMoney]",$detail->vatMoney,array('readonly'=>true)) ?>
                <?php echo CHtml::hiddenField("Confirmation[".$index."][volume]",$detail->volume,array('readonly'=>true)) ?>

                <tr>
                    <td><?php echo $barang[0]['kodeBarang']; ?></td>
                    <td><?php echo $barang[0]['namaBarang']; ?></td>
                    <td><?php echo number_format($barang[0]['diameter'],3); ?></td>
                    <td><?php echo number_format($barang[0]['weight'],3); ?></td>
                    <td><?php echo number_format($barang[0]['length'],3); ?></td>
                    <td><?php echo number_format($barang[0]['height'],3); ?></td>
                    <td><?php echo number_format($barang[0]['cbm'],3); ?></td>
                    <td><?php echo number_format($detail->qty); ?></td>
                    <td><?php echo CHtml::textField("Confirmation[".$index."][qty]","$qty",array('class'=>'form-control')) ?></td>
                    <td>
                        <?php echo CHtml::link('Reject', 'javascript:void(0)',
                            array('class'=>'btn btn-danger btn-sm delete')) ?>
                    </td>
                </tr>

                <script>
                    $('.delete').click(function() {
                        $(this).parent().parent().remove();
                    });
                </script>
    <?php   $index++; }
        }
    } ?>
</table>
<hr>
<input type="submit" class="btn btn-lg btn-info" value="Save">

<?php $this->endWidget(); ?>
