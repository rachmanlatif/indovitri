<div class="register_account">
    <div class="row">
        <div class="col-sm-6">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'toko-form',
            'enableAjaxValidation'=>false,
        )); ?>
            <h4 class="title">Register New Store</h4>
            <div id="status-message">
                <?php if(Yii::app()->user->hasFlash('success')): ?>
                    <div class="alert alert-success">
                        <?php echo Yii::app()->user->getFlash('success') ?>
                    </div>
                <?php endif ?>

                <?php if(Yii::app()->user->hasFlash('info')): ?>
                    <div class="alert alert-info">
                        <?php echo Yii::app()->user->getFlash('info') ?>
                    </div>
                <?php endif ?>

                <?php if(Yii::app()->user->hasFlash('danger')): ?>
                    <div class="alert alert-danger">
                        <?php echo Yii::app()->user->getFlash('danger') ?>
                    </div>
                <?php endif ?>
            </div>

            <div>
                <label>Store Name</label>
                <span>*</span>
                <?php echo $form->textField($model,'namaToko',array('class'=>'form-control', 'placeholder'=>'Store Name')); ?>
                <?php echo $form->error($model,'namaToko'); ?>
            </div>

            <div>
                <label>Domain</label>
                <span>*</span>
                <?php echo $form->textField($model,'domain',array('class'=>'form-control', 'placeholder'=>'Domain')); ?>
                <?php echo $form->error($model,'domain'); ?>
            </div>

            <div class="button1">
                <button type="submit" class="btn btn-sm"><i class="fa fa-save"></i> Save</button>
            </div>

            <?php $this->endWidget(); ?>

        </div><!-- col -->
    </div><!-- form -->
</div><!-- form -->