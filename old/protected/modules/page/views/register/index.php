<div class="register_account">
    <div class="row">
        <div class="col-sm-6">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'register-form',
                'enableAjaxValidation'=>false,
            )); ?>

            <h4 class="title">Create an Account</h4>
            <div id="status-message">
                <?php if(Yii::app()->user->hasFlash('success')): ?>
                    <div class="alert alert-success">
                        <?php echo Yii::app()->user->getFlash('success') ?>
                    </div>
                <?php endif ?>

                <?php if(Yii::app()->user->hasFlash('info')): ?>
                    <div class="alert alert-info">
                        <?php echo Yii::app()->user->getFlash('info') ?>
                    </div>
                <?php endif ?>

                <?php if(Yii::app()->user->hasFlash('danger')): ?>
                    <div class="alert alert-danger">
                        <?php echo Yii::app()->user->getFlash('danger') ?>
                    </div>
                <?php endif ?>
            </div>

            <div>
                <?php echo $form->textField($model,'nama',array('class'=>'form-control', 'placeholder'=>'Nama')); ?>
                <?php echo $form->error($model,'nama'); ?>
            </div>
            <div>
                <?php echo $form->textField($model,'hp',array('class'=>'form-control', 'placeholder'=>'HP')); ?>
                <?php echo $form->error($model,'hp'); ?>
            </div>
            <div>
                <?php echo $form->textField($model,'email',array('class'=>'form-control', 'placeholder'=>'Email')); ?>
                <?php echo $form->error($model,'email'); ?>
            </div>
            <div>
                <?php echo $form->passwordField($model,'password',array('class'=>'form-control', 'placeholder'=>'Password')); ?>
                <?php echo $form->error($model,'password'); ?>
            </div>
            <div>
                <button type="submit" class="grey">Submit</button>
                <p class="terms">By clicking 'Create Account' you agree to the <a href="#">Terms &amp; Conditions</a>.</p>
            </div>

            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>