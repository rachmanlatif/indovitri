<?php

class HomeController extends Controller
{
    public function actionIndex()
    {
        // sql query for calling the procedure
        $sql2 = "select* from dbo.TokoView where status = 1";
        // execute the sql command
        $toko =  Yii::app()->db->createCommand($sql2)->queryAll();

        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        $this->render('index', array(
            'toko'=>$toko,
            'category'=>$category,
        ));
    }

    public function actionLoadCategory(){
        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        $this->renderPartial('category', array(
            'category'=>$category,
        ));
    }

    public function actionLoadMember(){
        $id = Yii::app()->user->id;
        if($id != null){
            $model = $this->loadModelMember($id);

            $member = array();
            $profileMember = ProfileMember::model()->findByAttributes(array('kodeMember'=>$model->kodeMember));
            if($profileMember != null){
                $member = $profileMember;
            }
            $this->renderPartial('member', array(
                'model'=>$model,
                'profileMember'=>$member,
            ));
        }
    }

    public function actionLoadProductCategory(){
        if(isset($_POST['cat'])){

            // sql query for calling the procedure
            $sql = "select* from KoneksiIndo.dbo.ListKoneksi";
            // execute the sql command
            $conns =  Yii::app()->db->createCommand($sql)->queryAll();

            $models = array();
            foreach($conns as $conn){
                $s = "select * from dbo.TokoView where kodeToko = '".$conn['kodeToko']."'";
                // execute the sql command
                $m =  Yii::app()->db->createCommand($s)->queryAll();

                $sql2 = "select TOP 4 *,'".$m[0]['domain']."' as domain,'".$m[0]['kodeToko']."' as kodeToko from ".$conn['db'].".dbo.Product where kodeProductCategory = '".$_POST['cat']."'";
                // execute the sql command
                $models =  Yii::app()->db->createCommand($sql2)->queryAll();
            }

            $this->renderPartial('productCategory', array(
                'models'=>$models,
            ));
        }
    }

    public function actionGetImageProduct(){
        if(isset($_POST['kodeToko']) and isset($_POST['kodeBarang'])){

            // sql query for calling the procedure
            $sql = "select* from KoneksiIndo.dbo.ListKoneksi where kodeToko = '".$_POST['kodeToko']."'";
            // execute the sql command
            $conn =  Yii::app()->db->createCommand($sql)->queryAll();
            $model = array();
            if($conn != null){
                $sql2 = "select * from ".$conn[0]['db'].".dbo.ProductImage where kodeBarang = '".$_POST['kodeBarang']."'";
                // execute the sql command
                $model =  Yii::app()->db->createCommand($sql2)->queryAll();
            }

            $this->renderPartial('imageProduct', array(
                'model'=>$model,
            ));
        }
    }

    public function loadModelMember($id)
    {
        $model=Member::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionCat($m){

        // sql query for calling the procedure
        $sql = "select * from dbo.ProductCategoryView where LOWER(nama) = '".$m."'";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();
        $cat = $category[0]['kodeProductCategory'];

        // sql query for calling the procedure
        $sql2 = "select* from dbo.TokoView where status = 1";
        // execute the sql command
        $toko =  Yii::app()->db->createCommand($sql2)->queryAll();

        $this->render('cat', array(
            'toko'=>$toko,
            'nama'=>$m,
            'cat'=>$cat,
        ));
    }

    public function actionView($id){

        $model = Product::model()->findByPk($id);
        $modelsBottom = Product::model()->findAllByAttributes(array(),array('limit'=>9));

        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        // sql query for calling the procedure
        $sql2 = "select* from dbo.TokoView where status = 1";
        // execute the sql command
        $toko =  Yii::app()->db->createCommand($sql2)->queryAll();

        $this->render('after', array(
            'model'=>$model,
            'modelsBottom'=>$modelsBottom,
            'category'=>$category,
            'toko'=>$toko,
        ));
    }
}

?>