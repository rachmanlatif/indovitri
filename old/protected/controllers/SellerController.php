<?php

class SellerController extends UserController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array();
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array();
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        $this->render('view',array(
			'model'=>$this->loadModel($id),
            'category'=>$category,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd()
	{
		$model=new Seller;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Seller']))
		{
            $model->attributes=$_POST['Seller'];
            $model->sellerID = KodeGenerator::generate('seller');

            if($model->save()){
                $this->redirect(array('view','id'=>$model->sellerID));
            }
		}

        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        $this->render('add',array(
			'model'=>$model,
			'category'=>$category,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Seller']))
		{
			$model->attributes=$_POST['Seller'];

			if($model->save())
				$this->redirect(array('view','id'=>$model->sellerID));
		}

        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        $this->render('update',array(
			'model'=>$model,
			'category'=>$category,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	/**
	 * Deletes a list of model.	 
	 */
	public function actionDeleteSelected()
	{
		if(Yii::app()->request->isPostRequest && isset($_POST['ids']))
		{
			// delete
			foreach ($_POST['ids'] as $id) {
				$this->loadModel($id)->delete();
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Seller('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Seller']))
			$model->attributes=$_GET['Seller'];

        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        $this->render('index',array(
			'model'=>$model,
			'category'=>$category,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Seller::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='seller-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
