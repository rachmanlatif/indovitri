<?php
class ProfileController extends UserController{

    public function actionChange(){
        $id = Yii::app()->user->id;
        $model = $this->loadModel($id);

        $status = 0;
        $profile = array();
        $address = array();
        $profile = ProfileMember::model()->findByAttributes(array('kodeMember'=>$id));
        if($profile != null){
            $address = ProfileAddress::model()->findByAttributes(array('kodeMember'=>$id));
            if($address != null){
                $status = 1;
            }
        }

        $profileMember = new ProfileMember();
        $profileMember->nama = $model->nama;
        $profileMember->email = $model->email;
        $profileMember->hp = $model->hp;

        $profileAddress = new ProfileAddress();
        $profileAddress->HP = $model->hp;
        $profileAddress->penerima = $model->nama;

        if($status == 1){
            $profileMember->jenisKelamin = $profile->jenisKelamin;
            $profileMember->tempatLahir = $profile->tempatLahir;
            $profileMember->tanggalLahir = date('Y-m-d', strtotime($profile->tanggalLahir));

            $profileAddress->address = $address->address;
            $profileAddress->kodeCountry = $address->kodeCountry;
            $profileAddress->kodeState = $address->kodeState;
            $profileAddress->kodeCity = $address->kodeCity;
        }

        if(isset($_POST['ProfileMember']) or isset($_POST['ProfileAddress'])){
            $kode = IDGenerator::generate('profileMember');
            $profileMember->attributes=$_POST['ProfileMember'];
            $profileMember->kodeProfileMember = $kode;
            $profileMember->kodeMember = $model->kodeMember;
            $profileMember->pathFoto = ProfileMember::getFileUrl().$_POST['ProfileMember']['filename'];

            $profileAddress->attributes=$_POST['ProfileAddress'];
            $profileAddress->kodeMember = $model->kodeMember;
            $profileAddress->kodeProfileMember = $kode;

            if($profileAddress->save()){
                if($profileMember->save()){
                    $this->redirect(Yii::app()->homeUrl.'profile/change');
                }
            }
        }

        $this->render('change', array(
            'model'=>$model,
            'status'=>$status,
            'modelProfile'=>$profileMember,
            'modelProfileAddress'=>$profileAddress,
        ));
    }

    public function loadModel($id)
    {
        $model=Member::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionUpload()
    {
        $allowedExtensions = Yii::app()->params['imageExtension'];
        $sizeLimit = 10 * 1024 * 1024; // 10mb

        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $replaceOldFile = TRUE;
        $result = $uploader->handleUpload(ProfileMember::getFilePath(), $replaceOldFile, '');

        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
    }

    public function actionDeleteFile() {
        if (isset($_POST['filename'])) {
            self::deleteFile($_POST['filename']);
        }
    }

    public static function deleteFile($file) {
        @unlink(ProfileMember::getFilePath() . $file);
    }

    public static function deleteUnlinkedFile()
    {

        $photo = array();
        $models = ProfileMember::model()->findAll();
        foreach ($models as $model) {
            array_push($photo, $model['pathFoto']);
        }

        RFile::deleteUnexcludedFile(ProfileMember::getFilePath(), $photo);

    }
} 