<?php

class ProductController extends UserController
{

    /**
	 * @return array action filters
	 */
	public function filters()
	{
		return array();
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array();
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        $this->render('view',array(
			'model'=>$this->loadModel($id),
            'category'=>$category,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd()
	{
		$model=new Product();
		$modelImage=new ProductImage();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $c =  Yii::app()->db->createCommand($sql)->queryAll();

        $list = array();
        foreach($c as $col){
            $list[$col['kodeProductCategory']] = $col['nama'];
        }

        if(isset($_POST['Product']))
		{
			$model->attributes=$_POST['Product'];
            $model->kodeBarang = KodeGenerator::generate('product');

			if($model->save()){
                if(isset($_POST['ProductImage'])){
                    foreach($_POST['ProductImage']['imagePath'] as $p){
                        $image = new ProductImage();
                        $image->kodeBarang = $model->kodeBarang;
                        $image->imagePath = ProductImage::getFileUrl().$p;
                        $image->filename = $p;
                        $image->status = EnumStatus::ACTIVE;
                        $image->save();
                    }
                }

                $this->redirect(array('view','id'=>$model->kodeBarang));
            }
		}

        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        $this->render('add',array(
			'model'=>$model,
			'modelImage'=>$modelImage,
			'list'=>$list,
			'category'=>$category,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $modelImage=new ProductImage();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Product']))
		{
			$model->attributes=$_POST['Product'];

			if($model->save()){
                if(isset($_POST['ProductImage'])){
                    foreach($_POST['ProductImage']['imagePath'] as $p){
                        $image = new ProductImage();
                        $image->kodeBarang = $model->kodeBarang;
                        $image->imagePath = ProductImage::getFileUrl().$p;
                        $image->filename = $p;
                        $image->status = EnumStatus::ACTIVE;
                        $image->save();
                    }
                }

                $this->redirect(array('view','id'=>$model->kodeBarang));
            }
		}

        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $c =  Yii::app()->db->createCommand($sql)->queryAll();

        $list = array();
        foreach($c as $col){
            $list[$col['kodeProductCategory']] = $col['nama'];
        }

        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        $this->render('update',array(
			'model'=>$model,
			'modelImage'=>$modelImage,
			'list'=>$list,
			'category'=>$category,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
        if(file_exists(UploadForm::getFilePath().'product.xls')){
            $filename = 'product.xls';
        }
        else{
            $filename = 'product.xlsx';
        }

        @unlink(UploadForm::getFilePath().$filename);
        Yii::app()->user->setFlash('success','Delete success!');
        $this->redirect('import');
    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Product('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Product']))
			$model->attributes=$_GET['Product'];

        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        $this->render('index',array(
			'model'=>$model,
			'category'=>$category,
		));
	}

    public function actionImport(){
        $model = new UploadForm();

        if(isset($_POST['UploadForm']))
        {
            $this->redirect('reviewUpload');
        }

        if(file_exists(UploadForm::getFilePath().'product.xls') || file_exists(UploadForm::getFilePath().'product.xlsx')){
            $this->redirect('reviewUpload');
        }
        else{
            // sql query for calling the procedure
            $sql = "select* from dbo.ProductCategoryView where status = 1";
            // execute the sql command
            $category =  Yii::app()->db->createCommand($sql)->queryAll();

            $this->render('import',array(
                'model'=>$model,
                'category'=>$category,
            ));
        }
    }

    public function actionReviewUpload(){
        if(file_exists(UploadForm::getFilePath().'product.xls') || file_exists(UploadForm::getFilePath().'product.xlsx')){
            $model = new UploadForm();
            $data = $this->getExcel();

            if(file_exists(UploadForm::getFilePath().'product.xls')){
                $filename = 'product.xls';
            }
            else{
                $filename = 'product.xlsx';
            }

            // sql query for calling the procedure
            $sql = "select* from dbo.MasterKolomView where status = 1";
            // execute the sql command
            $c =  Yii::app()->db->createCommand($sql)->queryAll();

            $list = array();
            foreach($c as $col){
                $list[$col['kolom']] = $col['namaKolom'];
            }

            // sql query for calling the procedure
            $sql2 = "select* from dbo.ProductCategoryView where status = 1";
            // execute the sql command
            $c2 =  Yii::app()->db->createCommand($sql2)->queryAll();

            $list2 = array();
            foreach($c2 as $col){
                $list2[$col['kodeProductCategory']] = $col['nama'];
            }

            if(isset($_POST['UploadForm'])){
                $model->setAttributes($_POST['UploadForm']);
                $this->uploadFile();
            }

            // sql query for calling the procedure
            $sql = "select* from dbo.ProductCategoryView where status = 1";
            // execute the sql command
            $category =  Yii::app()->db->createCommand($sql)->queryAll();

            $this->render('dataUpload',array(
                'data'=>$data,
                'model'=>$model,
                'filename'=>$filename,
                'list'=>$list,
                'list2'=>$list2,
                'category'=>$category,
            ));
        }
        else{
            $this->redirect('import');
        }
    }

    public function uploadFile(){
        $phpExcelPath = Yii::getPathOfAlias('site.vendors.phpexcel.Classes');
        include_once($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        require_once $phpExcelPath. '/PHPExcel/IOFactory.php';

        if(file_exists(UploadForm::getFilePath().'product.xlsx')){
            $path = UploadForm::getFilePath().'product.xlsx';
        }
        else{
            $path = UploadForm::getFilePath().'product.xls';
        }

        try{
            $inputFileType = PHPExcel_IOFactory::identify($path);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($path);
            $objPHPExcel->setActiveSheetIndex(0);
            $data = $objPHPExcel->getActiveSheet();
        } catch(Exception $e) {
            throw new CHttpException(404,'Error read excel: '.$e->getMessage());
        }

        $highestRow = $data->getHighestRow();
        $highestColumn = $data->getHighestColumn();
        if(!($highestRow -1) >0)
        {
            throw new CHttpException('No data found in Sheet 0 or Sheet not available.');
        }

        try {
            // datanya dimulai dari row 2
            for($a=2; $a<=$highestRow; ++$a) {
                $rowData = $data->rangeToArray('A'.$a.':'.$highestColumn.$a,NULL,TRUE,FALSE);

                $aa = $rowData[0][0];

                if($aa != null or $aa != ''){
                    $product = new Product();
                    $product->kodeBarang = KodeGenerator::generate('product');
                    $product->sellerID = $_POST['UploadForm']['sellerID'];
                    $product->kodeProductCategory = $_POST['UploadForm']['kodeProductCategory'];
                    $product->status = $_POST['UploadForm']['status'];
                    $product->stockCode = $_POST['UploadForm']['stockCode'];

                    $arr = array_filter($_POST['UploadForm']['column']);
                    foreach($arr as $index=>$aa){
                        $column = $aa;

                        // sql query for calling the procedure
                        $sql = "select* from KoneksiIndo.dbo.MasterKolomView where kolom = '".$column."'";
                        // execute the sql command
                        $c =  Yii::app()->db->createCommand($sql)->queryAll();
                        if($c != null){
                            $string = $rowData[0][$index];
                            $product->$column = $string;
                        }
                    }
                    $product->save();
                }
            }

            @unlink(UploadForm::getFilePath().$_POST['UploadForm']['filename']);
            Yii::app()->user->setFlash('success','Upload success!');
            $this->redirect('index');
        } catch(Exception $e) {
            throw new CHttpException(404,'Failed reading excel: '.$e->getMessage());
        }
    }

    function getExcel(){
        $phpExcelPath = Yii::getPathOfAlias('site.vendors.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        require_once $phpExcelPath. '/PHPExcel/IOFactory.php';

        if(file_exists(UploadForm::getFilePath().'product.xlsx')){
            $path = UploadForm::getFilePath().'product.xlsx';
        }
        else{
            $path = UploadForm::getFilePath().'product.xls';
        }

        try{
            $inputFileType = PHPExcel_IOFactory::identify($path);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($path);
            $objPHPExcel->setActiveSheetIndex(0);
            $data = $objPHPExcel->getActiveSheet();
        } catch(Exception $e) {
            throw new CHttpException(404,'Error read excel: '.$e->getMessage());
        }

        $highestRow = $data->getHighestRow();
        $highestColumn = $data->getHighestColumn();
        if(!($highestRow -1) >0)
        {
            throw new CHttpException('No data found in Sheet 0 or Sheet not available.');
        }

        try {

            $row = 1;
            $lastColumn = $highestColumn;
            $lastColumn++;
            for ($column = 'A'; $column != $lastColumn; $column++) {
                $rowData = $data->rangeToArray($column.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);

                return $rowData;
            }
        } catch(Exception $e) {
            throw new CHttpException(404,'Failed reading excel: '.$e->getMessage());
        }
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Product::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionUpload()
    {
        $allowedExtensions = Yii::app()->params['imageExtension'];
        $sizeLimit = 10 * 1024 * 1024; // 10mb

        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $replaceOldFile = TRUE;
        $result = $uploader->handleUpload(ProductImage::getFilePath(), $replaceOldFile, '');

        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
    }

    public function actionUploadExcel()
    {
        $allowedExtensions = Yii::app()->params['excelExtension'];
        $sizeLimit = 10 * 1024 * 1024; // 10mb

        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $replaceOldFile = TRUE;
        $newFileName = 'product';
        $result = $uploader->handleUpload(UploadForm::getFilePath(), $replaceOldFile, $newFileName);

        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
    }

    public function actionDeleteFile() {
        if (isset($_POST['filename'])) {
            self::deleteFile($_POST['filename']);
        }
    }

    public static function deleteFile($file) {
        @unlink(ProductImage::getFilePath() . $file);
    }

    public static function deleteUnlinkedFile()
    {

        $photo = array();
        $models = ProductImage::model()->findAll();
        foreach ($models as $model) {
            array_push($photo, $model['imagePath']);
        }

        RFile::deleteUnexcludedFile(ProductImage::getFilePath(), $photo);

    }
}
