<?php

class StoreController extends UserController
{

    public function actionLoadProductImage(){
        if(isset($_POST['product'])){
            $image = MyHelper::getProductImage($_POST['product']);

            $this->renderPartial('image', array(
                'image'=>$image,
            ));
        }
    }

    public function actionView($id){

        $toko = Toko::model()->findByAttributes(array('domain'=>$id));
        $models = array();
        if($toko != null){
            $koneksi = ListKoneksi::model()->findByAttributes(array('kodeToko'=>$toko->kodeToko));
            if($koneksi != null){
                $_SESSION['db'] = $koneksi->db;
                if(isset($_SESSION['db'])){
                    $models = MyHelper::getAllProduct();
                }
            }
        }

        $listKontener = array();
        $kontener = Kontener::model()->findAllByAttributes(array('status'=>1));
        if($kontener != null){
            foreach ($kontener as $value) {
                $listKontener[$value->kodeKontener] = $value->nama;
            }
        }

        if(isset($_POST['ListOrder'])){
            $kodeMember = Yii::app()->user->id;
            $kodeToko = $_POST['ListOrder']['kodeToko'];
            $kodeKontener = $_POST['ListOrder']['kodeKontener'];
            $arr = array_filter($_POST['Qty']);

            if($kodeKontener == ''){
                Yii::app()->user->setFlash('info','You must choose container');
            }
            else if($arr == null){
                Yii::app()->user->setFlash('info','You must choose product');
            }
            else{
                foreach($_POST['KodeBarang'] as $index=>$detail){
                    $kodeBarang = $_POST['KodeBarang'][$index];
                    $price = $_POST['Price'][$index];
                    $qty = $_POST['Qty'][$index];
                    if($qty != '' || $qty <> 0){
                        $sql = "exec dbo.ProsesOrder '".$_SERVER['REMOTE_ADDR']."','".$kodeMember."','".$kodeToko."','".$kodeKontener."','".$kodeBarang."',".$qty.",0,0";
                        // execute the sql command
                        $exec =  Yii::app()->db->createCommand($sql);
                        if($exec->execute()){
                            $order = ListOrder::model()->findByAttributes(array('ip'=>$_SERVER['REMOTE_ADDR'], 'kodeMember'=>$kodeMember, 'kodeToko'=>$kodeToko, 'kodeKontener'=>$kodeKontener, 'status'=>1));
                            if($order != null){
                                $email = EmailHelper::sendProspect($order->orderID);
                                if($email){
                                    Yii::app()->user->setFlash('success','Success add to container and email');
                                }
                                else{
                                    Yii::app()->user->setFlash('success','Success add to container');
                                }
                            }

                            $this->redirect(Yii::app()->homeUrl.'store/view/id/'.$toko->domain);
                        }
                        else{
                            Yii::app()->user->setFlash('danger','Failed add to container');
                            $this->redirect(Yii::app()->homeUrl.'store/view/id/'.$toko->domain);
                        }
                    }
                }
            }
        }
        else{
            Yii::app()->user->setFlash('warning','You must choose an item');
        }

        $this->render('view', array(
            'models'=>$models,
            'toko'=>$toko,
            'listKontener'=>$listKontener,
        ));
    }
}

?>
