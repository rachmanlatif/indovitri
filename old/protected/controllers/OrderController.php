<?php

class OrderController extends Controller
{

    public function actionPayLast($id)
    {
        $model = new ConfirmationForm();

        $order = $this->loadModel($id);
        if($order->status <> 10){
            throw new CHttpException(404,'The requested page does not exist.');
        }

        $bankAcc = BankAccount::model()->findAllByAttributes(array('status'=>EnumStatus::ACTIVE));
        if($bankAcc != null){
            $list = array();
            foreach($bankAcc as $acc){
                $list[$acc->kodeBankAccount] = '( '.$acc->bank->nama.' )'.$acc->norek.' - '.$acc->nama;
            }
        }

        if(isset($_POST['ConfirmationForm'])){
            $model->attributes = $_POST['ConfirmationForm'];

            $norek = '';
            $nama = '';
            $ba = BankAccount::model()->findByPk($model->kodeBankPenerima);
            if($ba != null){
                $norek = $ba->norek;
                $nama = $ba->nama;
            }

            if($model->validate()){
                $sql = "exec dbo.ProsesKonfirmasiPembayaranSisa '".$order->orderID."','".$model->total."','".$model->tanggal."','".$model->tanggalTerima."','".$model->kodeBank."','".$model->norekPengirim."','".$model->namaPengirim."','".$model->kodeBankPenerima."','".$norek."','".$nama."'";
                // execute the sql command
                $exec =  Yii::app()->db->createCommand($sql);
                if($exec->execute()){
                    Yii::app()->user->setFlash('success','Success confirm last payment.');
                    $this->redirect(Yii::app()->homeUrl.'order');
                }
                else{
                    Yii::app()->user->setFlash('warning','Failed confirm last payment.');
                    $this->redirect(Yii::app()->homeUrl.'order');
                }
            }
        }

        $this->render('payLast', array(
            'model'=>$model,
            'list'=>$list,
        ));
    }

    public function actionConfirmation()
    {
        $model = new ListOrder();
        $model->status == 6 or $model->status == 10;
        $model->kodeMember = Yii::app()->user->id;

        $this->render('confirmation', array(
            'model'=>$model,
        ));
    }

    public function actionPay($id)
    {
        $model = new ConfirmationForm();

        $order = $this->loadModel($id);
        if($order->status <> 6){
            throw new CHttpException(404,'The requested page does not exist.');
        }

        $bankAcc = BankAccount::model()->findAllByAttributes(array('status'=>EnumStatus::ACTIVE));
        if($bankAcc != null){
            $list = array();
            foreach($bankAcc as $acc){
                $list[$acc->kodeBankAccount] = '( '.$acc->bank->nama.' )'.$acc->norek.' - '.$acc->nama;
            }
        }

        if(isset($_POST['ConfirmationForm'])){
            $model->attributes = $_POST['ConfirmationForm'];

            $norek = '';
            $nama = '';
            $ba = BankAccount::model()->findByPk($model->kodeBankPenerima);
            if($ba != null){
                $norek = $ba->norek;
                $nama = $ba->nama;
            }

            if($model->validate()){
                $sql = "exec dbo.ProsesKonfirmasiBill '".$order->orderID."','".$model->tanggal."','".Yii::app()->user->id."','".$model->total."','".$model->kodeBank."','".$model->norekPengirim."','".$model->namaPengirim."','".$model->kodeBankPenerima."','".$norek."','".$nama."'";
                // execute the sql command
                $exec =  Yii::app()->db->createCommand($sql);
                if($exec->execute()){
                    Yii::app()->user->setFlash('success','Success confirm payment.');
                    $this->redirect(Yii::app()->homeUrl.'order');
                }
                else{
                    Yii::app()->user->setFlash('warning','Failed confirm payment.');
                    $this->redirect(Yii::app()->homeUrl.'order');
                }
            }
        }

        $this->render('pay', array(
            'model'=>$model,
            'list'=>$list,
        ));
    }

    public function actionApprove($id){
        $model = $this->loadModel($id);

        $sql = "exec dbo.ProsesKonfirmasiOrder '".$model->orderID."','".$model->kodeToko."'";
        // execute the sql command
        $exec =  Yii::app()->db->createCommand($sql);
        if($exec->execute()){
            Yii::app()->user->setFlash('success','Success confirm order. Next step, pay downpayment bill.');
            $this->redirect(Yii::app()->homeUrl.'order');
        }
        else{
            Yii::app()->user->setFlash('warning','Failed confirm order.');
            $this->redirect(Yii::app()->homeUrl.'order');
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        $this->render('view',array(
            'category'=>$category,
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model=ListOrder::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        $model->status = 0;
        if($model->save()){
            EmailHelper::sendCancel($model->orderID);

            Yii::app()->user->setFlash('success','Success cancel order');
        }
        else{
            Yii::app()->user->setFlash('danger','Failed cancel order');
        }

        $this->redirect(Yii::app()->homeUrl.'order');
    }

    public function actionIndex(){

        $model = new ListOrder();
        $model->kodeMember = Yii::app()->user->id;

        $this->render('index', array(
            'model'=>$model,
        ));
    }

    public function actionCheckout($id){
        $model = ListOrder::model()->findByPk(array('orderID'=>$id, 'kodeMember'=>Yii::app()->user->id));
        if($model == null){
            Yii::app()->user->setFlash('info','Failed checkout. Not found!');
        }
        else{
            if($model->status <> 1){
                Yii::app()->user->setFlash('info','Failed checkout. Order invalid!');
            }
            else{
                $sql = "exec dbo.ProsesCheckout '".$model->orderID."'";
                // execute the sql command
                $exec =  Yii::app()->db->createCommand($sql);
                if($exec->execute()){
                    EmailHelper::sendToFactory($model->orderID);
                    EmailHelper::sendCheckout($model->orderID);

                    Yii::app()->user->setFlash('success','Success checkout');
                }
                else{
                    Yii::app()->user->setFlash('danger','Failed checkout');
                }
            }
        }

        $this->redirect(Yii::app()->homeUrl.'order');
    }

    public function actionCancel(){

        $model = new ListOrder();
        $model->status = 0;

        $this->render('cancel', array(
            'model'=>$model,
        ));
    }
}
