<?php

class SellController extends Controller
{


    public function actionLastPay($id){

        $order = $this->loadModel($id);

        $model = new PayForm();

        if(isset($_POST['PayForm'])){
            $model->attributes = $_POST['PayForm'];

            $tanggal = date('Y-m-d', strtotime($model->tanggalBayar));
            $sql = "exec dbo.ProsesSellerBayarFactoryAkhir '".$order->orderID."','".Yii::app()->user->id."','".$tanggal."','".$model->jumlahBayar."','".$model->note."'";
            // execute the sql command
            $exec =  Yii::app()->db->createCommand($sql);
            if($exec->execute()){
                Yii::app()->user->setFlash('success','Success save last payment factory.');
                $this->redirect(Yii::app()->homeUrl.'sell');
            }
            else{
                Yii::app()->user->setFlash('warning','Failed save last payment factory.');
                $this->redirect(Yii::app()->homeUrl.'sell');
            }
        }
        $this->render('payLast', array(
            'model'=>$model,
        ));
    }

    public function actionPickup($id){

        $order = $this->loadModel($id);

        $model = new PickupForm();

        if(isset($_POST['PickupForm'])){
            $model->attributes = $_POST['PickupForm'];

            $sql = "exec dbo.ProsesPickupOrder '".$order->orderID."','".$model->kodeShipper."','".$model->kodeConsigne."','".$model->kodeDeliveryAgent."','".$model->tanggalPickup."','".$model->tanggalEstimation."','".$model->nomor."'";
            // execute the sql command
            $exec =  Yii::app()->db->createCommand($sql);
            if($exec->execute()){
                Yii::app()->user->setFlash('success','Success save pickup order.');
                $this->redirect(Yii::app()->homeUrl.'sell');
            }
            else{
                Yii::app()->user->setFlash('warning','Failed save pickup order.');
                $this->redirect(Yii::app()->homeUrl.'sell');
            }
        }
        $this->render('pickup', array(
            'model'=>$model,
        ));
    }

    public function actionPay($id){

        $order = $this->loadModel($id);

        $model = new PayForm();

        if(isset($_POST['PayForm'])){
            $model->attributes = $_POST['PayForm'];

            $sql = "exec dbo.ProsesBayarFactory '".$order->orderID."','".$model->tanggalBayar."','".$model->jumlahBayar."','".Yii::app()->user->id."'";
            // execute the sql command
            $exec =  Yii::app()->db->createCommand($sql);
            if($exec->execute()){
                Yii::app()->user->setFlash('success','Success save payment factory.');
                $this->redirect(Yii::app()->homeUrl.'sell');
            }
            else{
                Yii::app()->user->setFlash('warning','Failed save payment factory.');
                $this->redirect(Yii::app()->homeUrl.'sell');
            }
        }
        $this->render('pay', array(
            'model'=>$model,
        ));
    }

    public function actionSendOrder($id)
    {
        $email = EmailHelper::sendFactory($id);
        if($email){
            Yii::app()->user->setFlash('success','Success send email to factory.');
            $this->redirect(Yii::app()->homeUrl.'sell');
        }
        else{
            Yii::app()->user->setFlash('info','failed send email to factory.');
            $this->redirect(Yii::app()->homeUrl.'sell');
        }
    }

    public function actionConfirmation(){

        $toko = MyHelper::getToko();

        $model = new ListOrder();
        $model->kodeToko = $toko[0]['kodeToko'];

        $this->render('confirmation', array(
            'model'=>$model,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        $this->render('view',array(
            'category'=>$category,
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model=ListOrder::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionIndex(){

        // sql query for calling the procedure
        $sql = "select* from dbo.ProductCategoryView where status = 1";
        // execute the sql command
        $category =  Yii::app()->db->createCommand($sql)->queryAll();

        $toko = MyHelper::getToko();

        $model = new ListOrder();
        $model->status != 1;
        $model->kodeToko = $toko[0]['kodeToko'];

        $this->render('index', array(
            'category'=>$category,
            'model'=>$model,
        ));
    }

    public function actionProspect(){

        $toko = MyHelper::getToko();

        $model = new ListOrder();
        $model->status = 1;
        $model->kodeToko = $toko[0]['kodeToko'];

        $this->render('prospect', array(
            'model'=>$model,
        ));
    }
}
