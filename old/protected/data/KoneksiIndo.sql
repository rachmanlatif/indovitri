USE [KoneksiIndo]
GO
/****** Object:  Table [dbo].[BankList]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BankList](
	[kodeBank] [varchar](255) NULL,
	[nama] [varchar](255) NULL,
	[swiftCode] [varchar](255) NULL,
	[keterangan] [varchar](255) NULL,
	[status] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[City]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[City](
	[kodeCity] [varchar](255) NOT NULL,
	[nama] [varchar](255) NULL,
	[kodeState] [varchar](255) NULL,
	[keterangan] [varchar](255) NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[kodeCity] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Counter]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Counter](
	[keyword] [varchar](50) NOT NULL,
	[value] [varchar](50) NULL,
 CONSTRAINT [PK_Counter] PRIMARY KEY CLUSTERED 
(
	[keyword] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Country]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country](
	[kodeCountry] [varchar](255) NOT NULL,
	[nama] [varchar](255) NULL,
	[keterangan] [varchar](255) NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[kodeCountry] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Currency](
	[kodeCurrency] [varchar](255) NULL,
	[nama] [varchar](255) NULL,
	[keterangan] [varchar](255) NULL,
	[status] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Kontener]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Kontener](
	[kodeKontener] [varchar](255) NOT NULL,
	[nama] [varchar](255) NULL,
	[dimensiLuarPanjang] [float] NULL,
	[dimensiLuarLebar] [float] NULL,
	[dimensiLuarTinggi] [float] NULL,
	[photo] [varchar](255) NULL,
	[pathPhoto] [varchar](255) NULL,
	[dimensiDalamPanjang] [float] NULL,
	[dimensiDalamLebar] [float] NULL,
	[dimensiDalamTinggi] [float] NULL,
	[bukaPintuLebar] [float] NULL,
	[bukaPintuTinggi] [float] NULL,
	[volume] [float] NULL,
	[beratKotor] [float] NULL,
	[beratKosong] [float] NULL,
	[muatanBersih] [float] NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[kodeKontener] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Language]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Language](
	[kodeCountry] [varchar](255) NULL,
	[frasa] [varchar](255) NULL,
	[value] [varchar](255) NULL,
	[status] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ListKoneksi]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ListKoneksi](
	[kodeToko] [varchar](255) NOT NULL,
	[db] [varchar](255) NULL,
	[username] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[server] [varchar](255) NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[kodeToko] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Login]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Login](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](20) NULL,
	[nama] [varchar](50) NULL,
	[password] [varchar](100) NULL,
	[status] [int] NULL,
	[lastLoggedIn] [datetime] NULL,
	[sessionKey] [varchar](50) NULL,
	[sessionExpired] [datetime] NULL,
	[tglDibuat] [datetime] NULL,
	[dibuatOleh] [int] NULL,
	[tglDiubah] [datetime] NULL,
	[diubahOleh] [int] NULL,
 CONSTRAINT [PK_Login] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterKolom]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterKolom](
	[kodeKolom] [varchar](255) NOT NULL,
	[namaKolom] [varchar](255) NULL,
	[status] [int] NULL,
	[kolom] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[kodeKolom] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Member]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Member](
	[kodeMember] [varchar](15) NOT NULL,
	[email] [varchar](50) NULL,
	[hp] [varchar](15) NULL,
	[nama] [varchar](50) NULL,
	[password] [varchar](100) NULL,
	[status] [int] NULL,
	[lastLoggedIn] [datetime] NULL,
	[sessionKey] [varchar](50) NULL,
	[sessionExpired] [datetime] NULL,
	[tglDibuat] [datetime] NULL,
	[idRegister] [int] NULL,
 CONSTRAINT [PK_Member] PRIMARY KEY CLUSTERED 
(
	[kodeMember] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductCategory](
	[kodeProductCategory] [varchar](15) NOT NULL,
	[nama] [varchar](50) NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_ProductCategory] PRIMARY KEY CLUSTERED 
(
	[kodeProductCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProfileAddress]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProfileAddress](
	[kodeMember] [varchar](255) NULL,
	[address] [varchar](255) NULL,
	[status] [int] NULL,
	[longitude] [float] NULL,
	[magnitide] [float] NULL,
	[map] [varchar](255) NULL,
	[kodeCountry] [varchar](255) NULL,
	[kodeState] [varchar](255) NULL,
	[kodeCity] [varchar](255) NULL,
	[HP] [varchar](255) NULL,
	[penerima] [varchar](255) NULL,
	[IDprofileMember] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_ProfileAddress] PRIMARY KEY CLUSTERED 
(
	[IDprofileMember] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProfileMember]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProfileMember](
	[kodeProfileMember] [varchar](255) NOT NULL,
	[kodeMember] [varchar](255) NOT NULL,
	[filename] [varchar](255) NULL,
	[pathFoto] [varchar](255) NULL,
	[tempatLahir] [varchar](255) NULL,
	[tanggalLahir] [datetime] NULL,
	[jenisKelamin] [int] NULL,
	[point] [float] NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[kodeProfileMember] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProfilePayment]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProfilePayment](
	[IDprofilePayment] [int] IDENTITY(1,1) NOT NULL,
	[kodeMember] [varchar](255) NOT NULL,
	[kodeBank] [varchar](255) NOT NULL,
	[rek] [varchar](255) NULL,
	[atasNama] [varchar](255) NULL,
	[isExpireddate] [int] NULL,
	[validdate] [datetime] NULL,
	[saldo] [money] NULL,
	[keterangan] [varchar](255) NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IDprofilePayment] ASC,
	[kodeMember] ASC,
	[kodeBank] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Register]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Register](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nama] [varchar](50) NULL,
	[hp] [varchar](15) NULL,
	[status] [int] NULL,
	[email] [varchar](50) NULL,
	[password] [varchar](100) NULL,
	[tglDibuat] [datetime] NULL,
 CONSTRAINT [PK_RegisTemp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ServerList]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ServerList](
	[kodeServer] [varchar](255) NOT NULL,
	[namaServer] [varchar](255) NULL,
	[username] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[kodeServer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SpekList]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SpekList](
	[kodeSpek] [varchar](255) NOT NULL,
	[namaSpek] [varchar](255) NULL,
	[note] [varchar](255) NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[kodeSpek] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[State]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[State](
	[kodeState] [varchar](255) NOT NULL,
	[nama] [varchar](255) NULL,
	[keterangan] [varchar](255) NULL,
	[kodeCountry] [varchar](255) NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[kodeState] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Toko]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Toko](
	[kodeToko] [varchar](15) NOT NULL,
	[kodeMember] [varchar](15) NULL,
	[namaToko] [varchar](50) NULL,
	[domain] [varchar](20) NULL,
	[status] [int] NULL,
	[tglDibuat] [datetime] NULL,
	[image] [varchar](50) NULL,
	[imagePath] [varchar](100) NULL,
 CONSTRAINT [PK_Toko] PRIMARY KEY CLUSTERED 
(
	[kodeToko] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TokoCurrency]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TokoCurrency](
	[kodeToko] [varchar](255) NULL,
	[kodeCurrency] [varchar](255) NULL,
	[status] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[MasterKolomView]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MasterKolomView]
AS
SELECT        dbo.MasterKolom.*
FROM            dbo.MasterKolom

GO
/****** Object:  View [dbo].[ProductCategoryView]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ProductCategoryView]
AS
SELECT        dbo.ProductCategory.*
FROM            dbo.ProductCategory

GO
/****** Object:  View [dbo].[TokoView]    Script Date: 12/04/2018 09:27:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TokoView]
AS
SELECT        dbo.Toko.*
FROM            dbo.Toko

GO
INSERT [dbo].[Counter] ([keyword], [value]) VALUES (N'counter_masterkolom', N'8')
GO
INSERT [dbo].[Counter] ([keyword], [value]) VALUES (N'counter_member', N'5')
GO
INSERT [dbo].[Counter] ([keyword], [value]) VALUES (N'counter_productCategory', N'3')
GO
INSERT [dbo].[Counter] ([keyword], [value]) VALUES (N'counter_speklist', N'1')
GO
INSERT [dbo].[Counter] ([keyword], [value]) VALUES (N'counter_store', N'0')
GO
INSERT [dbo].[Counter] ([keyword], [value]) VALUES (N'format_masterkolom', N'MC{xxx}')
GO
INSERT [dbo].[Counter] ([keyword], [value]) VALUES (N'format_member', N'M{YY}{MM}{DD}{xxxx}')
GO
INSERT [dbo].[Counter] ([keyword], [value]) VALUES (N'format_productCategory', N'PC{xxxx}')
GO
INSERT [dbo].[Counter] ([keyword], [value]) VALUES (N'format_speklist', N'SP{YY}{MM}{DD}{xx}')
GO
INSERT [dbo].[Counter] ([keyword], [value]) VALUES (N'format_store', N'S{YY}{MM}{DD}{xxx}')
GO
INSERT [dbo].[ListKoneksi] ([kodeToko], [db], [username], [password], [server], [status]) VALUES (N'T1803290001', N'Indovitri', N'sa', N'C0b@t3b@k', N'103.23.235.226', 1)
GO
SET IDENTITY_INSERT [dbo].[Login] ON 

GO
INSERT [dbo].[Login] ([id], [username], [nama], [password], [status], [lastLoggedIn], [sessionKey], [sessionExpired], [tglDibuat], [dibuatOleh], [tglDiubah], [diubahOleh]) VALUES (1, N'admin', N'Administrator', N'21232f297a57a5a743894a0e4a801fc3', 1, CAST(0x0000A8BA0002A300 AS DateTime), N'0b5ad7da1c05500ec354e73f330cd06e', CAST(0x0000A8BA0006C1B0 AS DateTime), NULL, NULL, CAST(0x0000A8BA0002AA08 AS DateTime), 1)
GO
INSERT [dbo].[Login] ([id], [username], [nama], [password], [status], [lastLoggedIn], [sessionKey], [sessionExpired], [tglDibuat], [dibuatOleh], [tglDiubah], [diubahOleh]) VALUES (2, N'admin2', N'Administrator 2', N'21232f297a57a5a743894a0e4a801fc3', 1, CAST(0x0000A8A800C6E5F8 AS DateTime), N'2b4cb9fcd94d90ca2f81ae747f488f42', CAST(0x0000A8A800CB04A8 AS DateTime), CAST(0x0000A8A800C6D464 AS DateTime), 1, CAST(0x0000A8A800C6E97C AS DateTime), 2)
GO
SET IDENTITY_INSERT [dbo].[Login] OFF
GO
INSERT [dbo].[MasterKolom] ([kodeKolom], [namaKolom], [status], [kolom]) VALUES (N'MC001', N'Product Name', 1, N'namaBarang')
GO
INSERT [dbo].[MasterKolom] ([kodeKolom], [namaKolom], [status], [kolom]) VALUES (N'MC002', N'Description', 1, N'desc')
GO
INSERT [dbo].[MasterKolom] ([kodeKolom], [namaKolom], [status], [kolom]) VALUES (N'MC003', N'Quantity', 1, N'qty')
GO
INSERT [dbo].[MasterKolom] ([kodeKolom], [namaKolom], [status], [kolom]) VALUES (N'MC004', N'Diameter', 1, N'diameter')
GO
INSERT [dbo].[MasterKolom] ([kodeKolom], [namaKolom], [status], [kolom]) VALUES (N'MC005', N'Weight', 1, N'weight')
GO
INSERT [dbo].[MasterKolom] ([kodeKolom], [namaKolom], [status], [kolom]) VALUES (N'MC006', N'Length', 1, N'length')
GO
INSERT [dbo].[MasterKolom] ([kodeKolom], [namaKolom], [status], [kolom]) VALUES (N'MC007', N'Height', 1, N'height')
GO
INSERT [dbo].[MasterKolom] ([kodeKolom], [namaKolom], [status], [kolom]) VALUES (N'MC008', N'Image', 1, N'image')
GO
INSERT [dbo].[Member] ([kodeMember], [email], [hp], [nama], [password], [status], [lastLoggedIn], [sessionKey], [sessionExpired], [tglDibuat], [idRegister]) VALUES (N'M1803260005', N'rachman.latif@gmail.com', N'081291590104', N'Rachman Latif', N'21232f297a57a5a743894a0e4a801fc3', 1, CAST(0x0000A8BA0002B494 AS DateTime), N'd073f3424b99f35156333d476bb6403e', CAST(0x0000A8BA0006D344 AS DateTime), CAST(0x0000A8AF0167130C AS DateTime), 8)
GO
INSERT [dbo].[ProductCategory] ([kodeProductCategory], [nama], [status]) VALUES (N'PC0001', N'Glassware', 1)
GO
INSERT [dbo].[ProductCategory] ([kodeProductCategory], [nama], [status]) VALUES (N'PC0002', N'Acrilic', 1)
GO
INSERT [dbo].[ProductCategory] ([kodeProductCategory], [nama], [status]) VALUES (N'PC0003', N'Bowl', 1)
GO
SET IDENTITY_INSERT [dbo].[Register] ON 

GO
INSERT [dbo].[Register] ([id], [nama], [hp], [status], [email], [password], [tglDibuat]) VALUES (8, N'Rachman Latif', N'081291590104', 1, N'rachman.latif@gmail.com', N'21232f297a57a5a743894a0e4a801fc3', CAST(0x0000A8AF0167130C AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Register] OFF
GO
INSERT [dbo].[ServerList] ([kodeServer], [namaServer], [username], [password], [status]) VALUES (N'IV', N'103.23.235.226,25678', N'sa', N'C0b@t3b@k', 1)
GO
INSERT [dbo].[SpekList] ([kodeSpek], [namaSpek], [note], [status]) VALUES (N'SP18032801', N'Bahan', N'', 1)
GO
INSERT [dbo].[Toko] ([kodeToko], [kodeMember], [namaToko], [domain], [status], [tglDibuat], [image], [imagePath]) VALUES (N'T1803290001', N'M1803260005', N'Indovitri', N'indovitri', 1, CAST(0x0000A8B200000000 AS DateTime), N'iv.png', N'/indovitri/dev/uploads/toko/M1803260005/iv.png')
GO
ALTER TABLE [dbo].[Login] ADD  CONSTRAINT [DF_Login_status]  DEFAULT ((0)) FOR [status]
GO
ALTER TABLE [dbo].[Register] ADD  CONSTRAINT [DF_RegisTemp_status]  DEFAULT ((0)) FOR [status]
GO
ALTER TABLE [dbo].[Register] ADD  CONSTRAINT [DF_RegisTemp_tgl_dibuat]  DEFAULT (getdate()) FOR [tglDibuat]
GO
ALTER TABLE [dbo].[Toko] ADD  CONSTRAINT [DF_Toko_status]  DEFAULT ((0)) FOR [status]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MasterKolom"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MasterKolomView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MasterKolomView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProductCategory"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 119
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ProductCategoryView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ProductCategoryView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Toko"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'TokoView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'TokoView'
GO
