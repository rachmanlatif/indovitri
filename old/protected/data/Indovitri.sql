USE [Indovitri]
GO
/****** Object:  Table [dbo].[BarangSpek]    Script Date: 12/04/2018 09:26:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BarangSpek](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[kodeBarang] [varchar](255) NOT NULL,
	[kodeSpek] [varchar](255) NOT NULL,
	[value] [varchar](255) NULL,
	[note] [varchar](255) NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_BarangSpek] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CounterNum]    Script Date: 12/04/2018 09:26:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CounterNum](
	[keyword] [varchar](50) NOT NULL,
	[value] [varchar](50) NULL,
 CONSTRAINT [PK_CounterNum] PRIMARY KEY CLUSTERED 
(
	[keyword] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterStock]    Script Date: 12/04/2018 09:26:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterStock](
	[stockCode] [varchar](255) NOT NULL,
	[stockName] [varchar](255) NULL,
	[note] [varchar](255) NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[stockCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Product]    Script Date: 12/04/2018 09:26:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[kodeBarang] [varchar](255) NOT NULL,
	[namaBarang] [varchar](255) NULL,
	[desc] [varchar](255) NULL,
	[qty] [float] NULL,
	[stockCode] [varchar](255) NULL,
	[oz] [float] NULL,
	[diameter] [float] NULL,
	[weight] [float] NULL,
	[volumeml] [float] NULL,
	[volumeOz] [float] NULL,
	[length] [float] NULL,
	[height] [float] NULL,
	[cbm] [float] NULL,
	[priceFOB] [money] NULL,
	[priceMaster] [money] NULL,
	[inner] [float] NULL,
	[master] [float] NULL,
	[moq] [varchar](255) NULL,
	[artCapasity] [float] NULL,
	[nettWeight] [float] NULL,
	[grossWeight] [float] NULL,
	[obLength] [float] NULL,
	[obWidth] [float] NULL,
	[obHeight] [float] NULL,
	[totalCTN] [float] NULL,
	[totalGW] [float] NULL,
	[totalCBM] [float] NULL,
	[totalUSD] [float] NULL,
	[status] [int] NULL,
	[kodeProductCategory] [varchar](15) NULL,
	[sellerID] [varchar](15) NULL,
	[productID] [varchar](15) NULL,
PRIMARY KEY CLUSTERED 
(
	[kodeBarang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductImage]    Script Date: 12/04/2018 09:26:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductImage](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[kodeBarang] [varchar](255) NOT NULL,
	[imagePath] [varchar](255) NOT NULL,
	[status] [int] NULL,
	[filename] [varchar](255) NULL,
 CONSTRAINT [PK_ProductImage] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Seller]    Script Date: 12/04/2018 09:26:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Seller](
	[sellerID] [varchar](255) NOT NULL,
	[sellerName] [varchar](255) NULL,
	[address] [varchar](255) NULL,
	[telp] [varchar](255) NULL,
	[status] [int] NULL,
	[HP] [varchar](255) NULL,
	[contactPerson] [varchar](255) NULL,
	[kodeNegara] [varchar](255) NULL,
	[kodeProvinsi] [varchar](255) NULL,
	[kodeKota] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[sellerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SellerPayment]    Script Date: 12/04/2018 09:26:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SellerPayment](
	[kodeSeller] [varchar](255) NOT NULL,
	[kodeBank] [varchar](255) NOT NULL,
	[rek] [varchar](255) NULL,
	[atasNama] [varchar](255) NULL,
	[isExpireddate] [int] NULL,
	[validdate] [datetime] NULL,
	[saldo] [money] NULL,
	[keterangan] [varchar](255) NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[kodeSeller] ASC,
	[kodeBank] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[CounterNum] ([keyword], [value]) VALUES (N'counter_product', N'410')
GO
INSERT [dbo].[CounterNum] ([keyword], [value]) VALUES (N'counter_seller', N'1')
GO
INSERT [dbo].[CounterNum] ([keyword], [value]) VALUES (N'counter_unit', N'2')
GO
INSERT [dbo].[CounterNum] ([keyword], [value]) VALUES (N'format_product', N'P{YY}{MM}{xxxx}')
GO
INSERT [dbo].[CounterNum] ([keyword], [value]) VALUES (N'format_seller', N'S{xxxx}')
GO
INSERT [dbo].[CounterNum] ([keyword], [value]) VALUES (N'format_unit', N'U{YY}{MM}{DD}{xxx}')
GO
INSERT [dbo].[MasterStock] ([stockCode], [stockName], [note], [status]) VALUES (N'U180330001', N'Pcs', N'', 1)
GO
INSERT [dbo].[MasterStock] ([stockCode], [stockName], [note], [status]) VALUES (N'U180404002', N'Doz', N'', 1)
GO
INSERT [dbo].[Product] ([kodeBarang], [namaBarang], [desc], [qty], [stockCode], [oz], [diameter], [weight], [volumeml], [volumeOz], [length], [height], [cbm], [priceFOB], [priceMaster], [inner], [master], [moq], [artCapasity], [nettWeight], [grossWeight], [obLength], [obWidth], [obHeight], [totalCTN], [totalGW], [totalCBM], [totalUSD], [status], [kodeProductCategory], [sellerID], [productID]) VALUES (N'P18040024', N'AS-423 F/6DZ ', N'', NULL, N'U180330001', NULL, NULL, 0.035865774, NULL, NULL, NULL, NULL, NULL, 0.0000, 0.0000, NULL, NULL, N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'PC0001', N'S0001', NULL)
GO
INSERT [dbo].[Product] ([kodeBarang], [namaBarang], [desc], [qty], [stockCode], [oz], [diameter], [weight], [volumeml], [volumeOz], [length], [height], [cbm], [priceFOB], [priceMaster], [inner], [master], [moq], [artCapasity], [nettWeight], [grossWeight], [obLength], [obWidth], [obHeight], [totalCTN], [totalGW], [totalCBM], [totalUSD], [status], [kodeProductCategory], [sellerID], [productID]) VALUES (N'P18040025', N'B10-14AF/4DZ ', N'', 14, N'U180330001', NULL, NULL, 0.053959194, NULL, NULL, NULL, NULL, NULL, 0.0000, 0.0000, NULL, NULL, N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'PC0001', N'S0001', NULL)
GO
INSERT [dbo].[Product] ([kodeBarang], [namaBarang], [desc], [qty], [stockCode], [oz], [diameter], [weight], [volumeml], [volumeOz], [length], [height], [cbm], [priceFOB], [priceMaster], [inner], [master], [moq], [artCapasity], [nettWeight], [grossWeight], [obLength], [obWidth], [obHeight], [totalCTN], [totalGW], [totalCBM], [totalUSD], [status], [kodeProductCategory], [sellerID], [productID]) VALUES (N'P18040026', N'B10-14AF/4DZ FNG ', N'', 14, N'U180330001', NULL, NULL, 0.053959194, NULL, NULL, NULL, NULL, NULL, 0.0000, 0.0000, NULL, NULL, N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'PC0001', N'S0001', NULL)
GO
INSERT [dbo].[Product] ([kodeBarang], [namaBarang], [desc], [qty], [stockCode], [oz], [diameter], [weight], [volumeml], [volumeOz], [length], [height], [cbm], [priceFOB], [priceMaster], [inner], [master], [moq], [artCapasity], [nettWeight], [grossWeight], [obLength], [obWidth], [obHeight], [totalCTN], [totalGW], [totalCBM], [totalUSD], [status], [kodeProductCategory], [sellerID], [productID]) VALUES (N'P18040027', N'B10-14BF/4DZ', N'', 14, N'U180330001', NULL, NULL, 0.053959194, NULL, NULL, NULL, NULL, NULL, 0.0000, 0.0000, NULL, NULL, N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'PC0001', N'S0001', NULL)
GO
INSERT [dbo].[Product] ([kodeBarang], [namaBarang], [desc], [qty], [stockCode], [oz], [diameter], [weight], [volumeml], [volumeOz], [length], [height], [cbm], [priceFOB], [priceMaster], [inner], [master], [moq], [artCapasity], [nettWeight], [grossWeight], [obLength], [obWidth], [obHeight], [totalCTN], [totalGW], [totalCBM], [totalUSD], [status], [kodeProductCategory], [sellerID], [productID]) VALUES (N'P18040028', N'B1-10CF/4DZ FNG ', N'', 10, N'U180330001', NULL, NULL, 0.029332055, NULL, NULL, NULL, NULL, NULL, 0.0000, 0.0000, NULL, NULL, N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'PC0001', N'S0001', NULL)
GO
INSERT [dbo].[Product] ([kodeBarang], [namaBarang], [desc], [qty], [stockCode], [oz], [diameter], [weight], [volumeml], [volumeOz], [length], [height], [cbm], [priceFOB], [priceMaster], [inner], [master], [moq], [artCapasity], [nettWeight], [grossWeight], [obLength], [obWidth], [obHeight], [totalCTN], [totalGW], [totalCBM], [totalUSD], [status], [kodeProductCategory], [sellerID], [productID]) VALUES (N'P18040029', N'B1-11BBF/48PCS PART FNG ', N'', 11, N'U180330001', NULL, NULL, 0.033554304, NULL, NULL, NULL, NULL, NULL, 0.0000, 0.0000, NULL, NULL, N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'PC0001', N'S0001', NULL)
GO
SET IDENTITY_INSERT [dbo].[ProductImage] ON 

GO
INSERT [dbo].[ProductImage] ([id], [kodeBarang], [imagePath], [status], [filename]) VALUES (3, N'P18030007', N'/indovitri/dev/uploads/product/M1803260005/es teh manis.jpg', 1, N'es teh manis.jpg')
GO
INSERT [dbo].[ProductImage] ([id], [kodeBarang], [imagePath], [status], [filename]) VALUES (4, N'P18040024', N'/indovitri/dev/uploads/product/M1803260005/1.png', 1, N'1.png')
GO
INSERT [dbo].[ProductImage] ([id], [kodeBarang], [imagePath], [status], [filename]) VALUES (5, N'P18040025', N'/indovitri/dev/uploads/product/M1803260005/2.png', 1, N'2.png')
GO
INSERT [dbo].[ProductImage] ([id], [kodeBarang], [imagePath], [status], [filename]) VALUES (6, N'P18040026', N'/indovitri/dev/uploads/product/M1803260005/3.png', 1, N'3.png')
GO
INSERT [dbo].[ProductImage] ([id], [kodeBarang], [imagePath], [status], [filename]) VALUES (7, N'P18040027', N'/indovitri/dev/uploads/product/M1803260005/4.png', 1, N'4.png')
GO
INSERT [dbo].[ProductImage] ([id], [kodeBarang], [imagePath], [status], [filename]) VALUES (8, N'P18040028', N'/indovitri/dev/uploads/product/M1803260005/5.png', 1, N'5.png')
GO
INSERT [dbo].[ProductImage] ([id], [kodeBarang], [imagePath], [status], [filename]) VALUES (9, N'P18040029', N'/indovitri/dev/uploads/product/M1803260005/6.png', 1, N'6.png')
GO
SET IDENTITY_INSERT [dbo].[ProductImage] OFF
GO
INSERT [dbo].[Seller] ([sellerID], [sellerName], [address], [telp], [status], [HP], [contactPerson], [kodeNegara], [kodeProvinsi], [kodeKota]) VALUES (N'S0001', N'Umbra', N'', N'', 1, N'', N'', N'', N'', N'')
GO
