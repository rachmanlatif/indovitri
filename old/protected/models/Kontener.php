<?php

/**
 * This is the model class for table "Kontener".
 *
 * The followings are the available columns in table 'Kontener':
 * @property string $kodeKontener
 * @property string $nama
 * @property double $dimensiLuarPanjang
 * @property double $dimensiLuarLebar
 * @property double $dimensiLuarTinggi
 * @property string $photo
 * @property string $pathPhoto
 * @property double $dimensiDalamPanjang
 * @property double $dimensiDalamLebar
 * @property double $dimensiDalamTinggi
 * @property double $bukaPintuLebar
 * @property double $bukaPintuTinggi
 * @property double $volume
 * @property double $beratKotor
 * @property double $beratKosong
 * @property double $muatanBersih
 * @property integer $status
 */
class Kontener extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Kontener';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kodeKontener', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('dimensiLuarPanjang, dimensiLuarLebar, dimensiLuarTinggi, dimensiDalamPanjang, dimensiDalamLebar, dimensiDalamTinggi, bukaPintuLebar, bukaPintuTinggi, volume, beratKotor, beratKosong, muatanBersih', 'numerical'),
			array('kodeKontener, nama, photo, pathPhoto', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kodeKontener, nama, dimensiLuarPanjang, dimensiLuarLebar, dimensiLuarTinggi, photo, pathPhoto, dimensiDalamPanjang, dimensiDalamLebar, dimensiDalamTinggi, bukaPintuLebar, bukaPintuTinggi, volume, beratKotor, beratKosong, muatanBersih, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kodeKontener' => 'Container Code',
			'nama' => 'Name',
			'dimensiLuarPanjang' => 'Outside Lenght',
			'dimensiLuarLebar' => 'Outside Width',
			'dimensiLuarTinggi' => 'Outside Height',
			'photo' => 'Photo',
			'pathPhoto' => 'Path Photo',
			'dimensiDalamPanjang' => 'Inside Lenght',
			'dimensiDalamLebar' => 'Inside Width',
			'dimensiDalamTinggi' => 'Inside Height',
			'bukaPintuLebar' => 'Door Width',
			'bukaPintuTinggi' => 'Door Height',
			'volume' => 'Volume',
			'beratKotor' => 'Gross Weight',
			'beratKosong' => 'Tare Weight',
			'muatanBersih' => 'Net Weight',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kodeKontener',$this->kodeKontener,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('dimensiLuarPanjang',$this->dimensiLuarPanjang);
		$criteria->compare('dimensiLuarLebar',$this->dimensiLuarLebar);
		$criteria->compare('dimensiLuarTinggi',$this->dimensiLuarTinggi);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('pathPhoto',$this->pathPhoto,true);
		$criteria->compare('dimensiDalamPanjang',$this->dimensiDalamPanjang);
		$criteria->compare('dimensiDalamLebar',$this->dimensiDalamLebar);
		$criteria->compare('dimensiDalamTinggi',$this->dimensiDalamTinggi);
		$criteria->compare('bukaPintuLebar',$this->bukaPintuLebar);
		$criteria->compare('bukaPintuTinggi',$this->bukaPintuTinggi);
		$criteria->compare('volume',$this->volume);
		$criteria->compare('beratKotor',$this->beratKotor);
		$criteria->compare('beratKosong',$this->beratKosong);
		$criteria->compare('muatanBersih',$this->muatanBersih);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kontener the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getFilePath() {
        return Yii::getPathOfAlias('site.uploads.container') . DIRECTORY_SEPARATOR;
    }

    public static function getFileUrl() {
        return Yii::app()->baseUrl . '/uploads/container/';
    }
}
