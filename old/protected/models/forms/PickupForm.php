<?php

class PickupForm extends CFormModel{

    public $orderID;
    public $kodeShipper;
    public $kodeConsigne;
    public $kodeDeliveryAgent;
    public $tanggalPickup;
    public $tanggalEstimation;
    public $nomor;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('kodeShipper, kodeConsigne, kodeDeliveryAgent, tanggalPickup, tanggalEstimation, nomor','required'),
            array('nomor', 'length', 'max'=>255),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'orderID' => 'Order ID',
            'kodeConsigne' => 'Consigne',
            'kodeDeliveryAgent' => 'Delivery Agent',
            'kodeShipper' => 'Shipper',
            'tanggalPickup' => 'Pickup Date',
            'tanggalEstimation' => 'Estimation Arrive Date',
            'nomor' => 'Doc Number',
        );
    }

    public function beforeSave(){
        if($this->isNewRecord){
            $this->tanggalPickup = date('Y-m-d', strtotime($this->tanggalPickup));
            $this->tanggalEstimation = date('Y-m-d', strtotime($this->tanggalEstimation));
        }

        return parent::beforeSave();
    }
} 