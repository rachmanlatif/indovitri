<?php

class PayForm extends CFormModel{

    public $tanggalBayar;
    public $jumlahBayar;
    public $orderID;
    public $username;
    public $note;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('tanggalBayar, jumlahBayar','required'),
            array('jumlahBayar', 'numerical'),
            array('note', 'length', 'max'=>255),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'tanggalBayar' => 'Paid Date',
            'jumlahBayar' => 'Amount',
            'orderID' => 'Order ID',
            'username' => 'Username',
            'note' => 'Note',
        );
    }

    public function beforeSave(){
        if($this->isNewRecord){
            $this->tanggalBayar = date('Y-m-d', strtotime($this->tanggalBayar));
        }

        return parent::beforeSave();
    }
}