<?php

class ConfirmationForm extends CFormModel{

    public $kodeBankPenerima;
    public $kodeBank;
    public $tanggal;
    public $tanggalTerima;
    public $total;
    public $namaPengirim;
    public $norekPengirim;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('kodeBankPenerima, kodeBank, tanggal, total, namaPengirim, norekPengirim','required'),
            array('total', 'numerical'),
            array('namaPengirim, norekPengirim', 'length', 'max'=>255),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'kodeBankPenerima' => 'Transfer To',
            'kodeBank' => 'From Bank',
            'tanggal' => 'Transfer Date',
            'tanggalTerima' => 'Receive Date',
            'total' => 'Total',
            'namaPengirim' => 'Transfer Name',
            'norekPengirim' => 'Account Number Transfer',
        );
    }
} 