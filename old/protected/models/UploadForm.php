<?php

class UploadForm extends CFormModel
{
	public $filename;
	public $column;
	public $row;
	public $label;
	public $kodeProductCategory;
	public $sellerID;
	public $status;
	public $stockCode;

	public function rules()
	{
		return array(
			array('filename', 'required'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'filename'=>'File Name',
			'column'=>'Column',
			'row'=>'Row',
			'label'=>'Label',
			'kodeProductCategory'=>'Product Category',
			'sellerID'=>'Seller ID',
			'status'=>'Status',
			'stockCode'=>'Stock Code',
		);
	}

    public static function getFilePath() {
        return Yii::getPathOfAlias('site.uploads.data_upload.'.Yii::app()->user->id) . DIRECTORY_SEPARATOR;
    }

    public static function getFileUrl() {
        return Yii::app()->baseUrl . '/uploads/data_upload/'.Yii::app()->user->id.'/';
    }
}