<?php

/**
 * This is the model class for table "Register".
 *
 * The followings are the available columns in table 'Register':
 * @property integer $id
 * @property string $nama
 * @property string $hp
 * @property integer $status
 * @property string $email
 * @property string $password
 * @property string $tglDibuat
 */
class Register extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Register';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, hp, password, nama', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
            array('email', 'email'),
            array('email', 'isEmailExist', 'on'=>'insert'),
            array('hp', 'isPhoneExist', 'on'=>'insert'),
			array('nama, email', 'length', 'max'=>50),
			array('hp', 'length', 'max'=>15),
			array('password', 'length', 'max'=>100),
			array('tglDibuat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama, hp, status, email, password, tglDibuat', 'safe', 'on'=>'search'),
		);
	}

    /**
     * Check if a username is exists
     */
    public function isEmailExist() {
        $user = self::model()->find('email=?', array($this->email));
        if ($user != null) {
            $this->addError('email', 'Email is already exists.');
        }
    }

    public function isPhoneExist() {
        $user = self::model()->find('hp=?', array($this->hp));
        if ($user != null) {
            $this->addError('hp', 'Phone is already exists.');
        }
    }

    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Name',
			'hp' => 'Phone Number',
			'status' => 'Status',
			'email' => 'Email',
			'password' => 'Password',
			'tglDibuat' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('hp',$this->hp,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('tglDibuat',$this->tglDibuat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Register the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave(){
        if($this->isNewRecord){
            if($this->password !== '' || $this->password !== NULL){
                $this->password = md5($this->password);
            }
            $this->status = Member::STATUS_NOTACTIVE;
            $this->tglDibuat = date('Y-m-d H:i:s');
        }

        return parent::beforeSave();
    }
}
