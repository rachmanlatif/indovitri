<?php

/**
 * This is the model class for table "ListOrder".
 *
 * The followings are the available columns in table 'ListOrder':
 * @property string $orderID
 * @property string $tanggal
 * @property string $ip
 * @property string $kodeMember
 * @property string $kodeToko
 * @property string $kodeKontener
 * @property integer $status
 * @property string $IDFactory
 */
class ListOrder extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ListOrder';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kodeKontener, IDFactory', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('orderID, ip, kodeMember, kodeToko, kodeKontener, IDFactory', 'length', 'max'=>255),
			array('tanggal', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('orderID, tanggal, ip, kodeMember, kodeToko, kodeKontener, status, IDFactory', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'toko' => array(self::BELONGS_TO, 'Toko', 'kodeToko'),
            'member' => array(self::BELONGS_TO, 'Member', 'kodeMember'),
            'kontener' => array(self::BELONGS_TO, 'Kontener', 'kodeKontener'),
            'details' => array(self::HAS_MANY, 'ListOrderDetail', 'orderID'),
            'confirms' => array(self::HAS_MANY, 'KonfirmasiOrderDetail', 'orderID'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'orderID' => 'Order',
			'tanggal' => 'Tanggal',
			'ip' => 'Ip',
			'kodeMember' => 'Kode Member',
			'kodeToko' => 'Kode Toko',
			'kodeKontener' => 'Kode Container',
			'status' => 'Status',
			'IDFactory' => 'ID Factory',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('orderID',$this->orderID,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('kodeMember',$this->kodeMember,true);
		$criteria->compare('kodeToko',$this->kodeToko,true);
		$criteria->compare('kodeKontener',$this->kodeKontener,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('IDFactory',$this->status, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ListOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
