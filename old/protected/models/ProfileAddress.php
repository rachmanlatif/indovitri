<?php

/**
 * This is the model class for table "ProfileAddress".
 *
 * The followings are the available columns in table 'ProfileAddress':
 * @property string $kodeMember
 * @property string $address
 * @property integer $status
 * @property double $longitude
 * @property double $magnitide
 * @property string $map
 * @property string $kodeCountry
 * @property string $kodeState
 * @property string $kodeCity
 * @property string $HP
 * @property string $penerima
 * @property string $kodeProfileMember
 * @property integer $id
 */
class ProfileAddress extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ProfileAddress';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('kodeProfileMember, kodeMember, address, kodeCountry, kodeState, kodeCity', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('longitude, magnitide', 'numerical'),
			array('kodeMember, address, map, kodeCountry, kodeState, kodeCity, HP, penerima', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kodeMember, address, status, longitude, magnitide, map, kodeCountry, kodeState, kodeCity, HP, penerima, kodeProfileMember', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kodeMember' => 'Kode Member',
			'address' => 'Address',
			'status' => 'Status',
			'longitude' => 'Longitude',
			'magnitide' => 'Magnitide',
			'map' => 'Map',
			'kodeCountry' => 'Kode Country',
			'kodeState' => 'Kode State',
			'kodeCity' => 'Kode City',
			'HP' => 'Hp',
			'penerima' => 'Penerima',
			'kodeProfileMember' => 'Kode Profile Member',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kodeMember',$this->kodeMember,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('longitude',$this->longitude);
		$criteria->compare('magnitide',$this->magnitide);
		$criteria->compare('map',$this->map,true);
		$criteria->compare('kodeCountry',$this->kodeCountry,true);
		$criteria->compare('kodeState',$this->kodeState,true);
		$criteria->compare('kodeCity',$this->kodeCity,true);
		$criteria->compare('HP',$this->HP,true);
		$criteria->compare('penerima',$this->penerima,true);
		$criteria->compare('kodeProfileMember',$this->kodeProfileMember);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProfileAddress the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave(){
        if($this->isNewRecord){
            $this->status = EnumStatus::ACTIVE;
        }

        return parent::beforeSave();
    }
}
