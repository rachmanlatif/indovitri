<?php

/**
 * This is the model class for table "OrderStatus".
 *
 * The followings are the available columns in table 'OrderStatus':
 * @property string $OrderID
 * @property string $TanggalCheckOut
 * @property string $TotalCheckout
 * @property string $KeyKonfirmasiFactory
 * @property string $TanggalKonfirmasiFactory
 * @property string $TotalKonfirmasi
 * @property string $TanggalKonfirmasiBuyer
 * @property string $TanggalPikup
 * @property string $KodeShipper
 * @property string $KodeConsigne
 * @property string $KodeDeliveryAgent
 * @property string $TanggalEstimasiPort
 * @property string $KodeDocBil
 * @property string $TanggalTerima
 * @property string $Note
 * @property integer $Status
 * @property string $TanggalBill
 * @property string $TotalBill
 * @property string $TanggalDP
 * @property string $TotalDP
 * @property string $TanggalTempoBill
 * @property string $TanggalKonfirmasiBayarBill
 * @property string $UserApprovalKonfirmasi
 * @property string $TanggalKonfirmasiBayarFactory
 * @property string $UserApprovalKonfirmasiFactory
 * @property string $TanggalBayarFactory
 * @property string $JumlahBayarFactory
 * @property string $TanggalEntryPickup
 * @property string $SisaPembayaran
 * @property string $TanggalBillSisaPembayaran
 * @property string $TanggalTempoSisaPembayaran
 * @property string $TanggalKonfirmasiPembayaranSisa
 * @property string $JumlahKonfirmasiPembayaranSisa
 * @property string $TanggalPembayaranSisa
 * @property integer $StatusBayarSisa
 * @property string $TanggalBayarSisaFactory
 * @property string $JumlahSisaFactory
 * @property string $TanggalKonfirmasiBayarSisaFactory
 * @property string $UsernameBayarSisaFactory
 * @property string $KodeBankPengirimDP
 * @property string $NoRekPengirimDP
 * @property string $AtasNamaPengirimDP
 * @property string $KodeBankPenerimaDP
 * @property string $NoRekPenerimaDP
 * @property string $AtasNamaPenerimaDP
 * @property string $KodeBankPengirimLunas
 * @property string $NoRekPengirimLunas
 * @property string $AtasNamaPengirimLunas
 * @property string $KodeBankPenerimaLunas
 * @property string $NoRekPenerimaLunas
 * @property string $AtasNamaPenerimaLunas
 */
class OrderStatus extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'OrderStatus';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Status, StatusBayarSisa', 'numerical', 'integerOnly'=>true),
			array('OrderID, KeyKonfirmasiFactory, KodeShipper, KodeConsigne, KodeDeliveryAgent, KodeDocBil, Note, UserApprovalKonfirmasi, UserApprovalKonfirmasiFactory, KodeBankPengirimDP, NoRekPengirimDP, AtasNamaPengirimDP, KodeBankPenerimaDP, NoRekPenerimaDP, AtasNamaPenerimaDP, KodeBankPengirimLunas, NoRekPengirimLunas, AtasNamaPengirimLunas, KodeBankPenerimaLunas, NoRekPenerimaLunas, AtasNamaPenerimaLunas', 'length', 'max'=>255),
			array('TotalCheckout, TotalKonfirmasi, TotalBill, TotalDP, JumlahBayarFactory, SisaPembayaran', 'length', 'max'=>19),
			array('TanggalCheckOut, TanggalKonfirmasiFactory, TanggalKonfirmasiBuyer, TanggalPikup, TanggalEstimasiPort, TanggalTerima, TanggalBill, TanggalDP, TanggalTempoBill, TanggalKonfirmasiBayarBill, TanggalKonfirmasiBayarFactory, TanggalBayarFactory, TanggalEntryPickup, TanggalBillSisaPembayaran, TanggalTempoSisaPembayaran, TanggalKonfirmasiPembayaranSisa, JumlahKonfirmasiPembayaranSisa, TanggalPembayaranSisa, TanggalBayarSisaFactory, JumlahSisaFactory, TanggalKonfirmasiBayarSisaFactory, UsernameBayarSisaFactory', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('OrderID, TanggalCheckOut, TotalCheckout, KeyKonfirmasiFactory, TanggalKonfirmasiFactory, TotalKonfirmasi, TanggalKonfirmasiBuyer, TanggalPikup, KodeShipper, KodeConsigne, KodeDeliveryAgent, TanggalEstimasiPort, KodeDocBil, TanggalTerima, Note, Status, TanggalBill, TotalBill, TanggalDP, TotalDP, TanggalTempoBill, TanggalKonfirmasiBayarBill, UserApprovalKonfirmasi, TanggalKonfirmasiBayarFactory, UserApprovalKonfirmasiFactory, TanggalBayarFactory, JumlahBayarFactory, TanggalEntryPickup, SisaPembayaran, TanggalBillSisaPembayaran, TanggalTempoSisaPembayaran, TanggalKonfirmasiPembayaranSisa, JumlahKonfirmasiPembayaranSisa, TanggalPembayaranSisa, StatusBayarSisa, TanggalBayarSisaFactory, JumlahSisaFactory, TanggalKonfirmasiBayarSisaFactory, UsernameBayarSisaFactory, KodeBankPengirimDP, NoRekPengirimDP, AtasNamaPengirimDP, KodeBankPenerimaDP, NoRekPenerimaDP, AtasNamaPenerimaDP, KodeBankPengirimLunas, NoRekPengirimLunas, AtasNamaPengirimLunas, KodeBankPenerimaLunas, NoRekPenerimaLunas, AtasNamaPenerimaLunas', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'OrderID' => 'Order',
			'TanggalCheckOut' => 'Tanggal Check Out',
			'TotalCheckout' => 'Total Checkout',
			'KeyKonfirmasiFactory' => 'Key Konfirmasi Factory',
			'TanggalKonfirmasiFactory' => 'Tanggal Konfirmasi Factory',
			'TotalKonfirmasi' => 'Total Konfirmasi',
			'TanggalKonfirmasiBuyer' => 'Tanggal Konfirmasi Buyer',
			'TanggalPikup' => 'Tanggal Pikup',
			'KodeShipper' => 'Kode Shipper',
			'KodeConsigne' => 'Kode Consigne',
			'KodeDeliveryAgent' => 'Kode Delivery Agent',
			'TanggalEstimasiPort' => 'Tanggal Estimasi Port',
			'KodeDocBil' => 'Kode Doc Bil',
			'TanggalTerima' => 'Tanggal Terima',
			'Note' => 'Note',
			'Status' => 'Status',
			'TanggalBill' => 'Tanggal Bill',
			'TotalBill' => 'Total Bill',
			'TanggalDP' => 'Tanggal Dp',
			'TotalDP' => 'Total Dp',
			'TanggalTempoBill' => 'Tanggal Tempo Bill',
			'TanggalKonfirmasiBayarBill' => 'Tanggal Konfirmasi Bayar Bill',
			'UserApprovalKonfirmasi' => 'User Approval Konfirmasi',
			'TanggalKonfirmasiBayarFactory' => 'Tanggal Konfirmasi Bayar Factory',
			'UserApprovalKonfirmasiFactory' => 'User Approval Konfirmasi Factory',
			'TanggalBayarFactory' => 'Tanggal Bayar Factory',
			'JumlahBayarFactory' => 'Jumlah Bayar Factory',
			'TanggalEntryPickup' => 'Tanggal Entry Pickup',
			'SisaPembayaran' => 'Sisa Pembayaran',
			'TanggalBillSisaPembayaran' => 'Tanggal Bill Sisa Pembayaran',
			'TanggalTempoSisaPembayaran' => 'Tanggal Tempo Sisa Pembayaran',
			'TanggalKonfirmasiPembayaranSisa' => 'Tanggal Konfirmasi Pembayaran Sisa',
			'JumlahKonfirmasiPembayaranSisa' => 'Jumlah Konfirmasi Pembayaran Sisa',
			'TanggalPembayaranSisa' => 'Tanggal Pembayaran Sisa',
			'StatusBayarSisa' => 'Status Bayar Sisa',
			'TanggalBayarSisaFactory' => 'Tanggal Bayar Sisa Factory',
			'JumlahSisaFactory' => 'Jumlah Sisa Factory',
			'TanggalKonfirmasiBayarSisaFactory' => 'Tanggal Konfirmasi Bayar Sisa Factory',
			'UsernameBayarSisaFactory' => 'Username Bayar Sisa Factory',
			'KodeBankPengirimDP' => 'Kode Bank Pengirim Dp',
			'NoRekPengirimDP' => 'No Rek Pengirim Dp',
			'AtasNamaPengirimDP' => 'Atas Nama Pengirim Dp',
			'KodeBankPenerimaDP' => 'Kode Bank Penerima Dp',
			'NoRekPenerimaDP' => 'No Rek Penerima Dp',
			'AtasNamaPenerimaDP' => 'Atas Nama Penerima Dp',
			'KodeBankPengirimLunas' => 'Kode Bank Pengirim Lunas',
			'NoRekPengirimLunas' => 'No Rek Pengirim Lunas',
			'AtasNamaPengirimLunas' => 'Atas Nama Pengirim Lunas',
			'KodeBankPenerimaLunas' => 'Kode Bank Penerima Lunas',
			'NoRekPenerimaLunas' => 'No Rek Penerima Lunas',
			'AtasNamaPenerimaLunas' => 'Atas Nama Penerima Lunas',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('OrderID',$this->OrderID,true);
		$criteria->compare('TanggalCheckOut',$this->TanggalCheckOut,true);
		$criteria->compare('TotalCheckout',$this->TotalCheckout,true);
		$criteria->compare('KeyKonfirmasiFactory',$this->KeyKonfirmasiFactory,true);
		$criteria->compare('TanggalKonfirmasiFactory',$this->TanggalKonfirmasiFactory,true);
		$criteria->compare('TotalKonfirmasi',$this->TotalKonfirmasi,true);
		$criteria->compare('TanggalKonfirmasiBuyer',$this->TanggalKonfirmasiBuyer,true);
		$criteria->compare('TanggalPikup',$this->TanggalPikup,true);
		$criteria->compare('KodeShipper',$this->KodeShipper,true);
		$criteria->compare('KodeConsigne',$this->KodeConsigne,true);
		$criteria->compare('KodeDeliveryAgent',$this->KodeDeliveryAgent,true);
		$criteria->compare('TanggalEstimasiPort',$this->TanggalEstimasiPort,true);
		$criteria->compare('KodeDocBil',$this->KodeDocBil,true);
		$criteria->compare('TanggalTerima',$this->TanggalTerima,true);
		$criteria->compare('Note',$this->Note,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('TanggalBill',$this->TanggalBill,true);
		$criteria->compare('TotalBill',$this->TotalBill,true);
		$criteria->compare('TanggalDP',$this->TanggalDP,true);
		$criteria->compare('TotalDP',$this->TotalDP,true);
		$criteria->compare('TanggalTempoBill',$this->TanggalTempoBill,true);
		$criteria->compare('TanggalKonfirmasiBayarBill',$this->TanggalKonfirmasiBayarBill,true);
		$criteria->compare('UserApprovalKonfirmasi',$this->UserApprovalKonfirmasi,true);
		$criteria->compare('TanggalKonfirmasiBayarFactory',$this->TanggalKonfirmasiBayarFactory,true);
		$criteria->compare('UserApprovalKonfirmasiFactory',$this->UserApprovalKonfirmasiFactory,true);
		$criteria->compare('TanggalBayarFactory',$this->TanggalBayarFactory,true);
		$criteria->compare('JumlahBayarFactory',$this->JumlahBayarFactory,true);
		$criteria->compare('TanggalEntryPickup',$this->TanggalEntryPickup,true);
		$criteria->compare('SisaPembayaran',$this->SisaPembayaran,true);
		$criteria->compare('TanggalBillSisaPembayaran',$this->TanggalBillSisaPembayaran,true);
		$criteria->compare('TanggalTempoSisaPembayaran',$this->TanggalTempoSisaPembayaran,true);
		$criteria->compare('TanggalKonfirmasiPembayaranSisa',$this->TanggalKonfirmasiPembayaranSisa,true);
		$criteria->compare('JumlahKonfirmasiPembayaranSisa',$this->JumlahKonfirmasiPembayaranSisa,true);
		$criteria->compare('TanggalPembayaranSisa',$this->TanggalPembayaranSisa,true);
		$criteria->compare('StatusBayarSisa',$this->StatusBayarSisa);
		$criteria->compare('TanggalBayarSisaFactory',$this->TanggalBayarSisaFactory,true);
		$criteria->compare('JumlahSisaFactory',$this->JumlahSisaFactory,true);
		$criteria->compare('TanggalKonfirmasiBayarSisaFactory',$this->TanggalKonfirmasiBayarSisaFactory,true);
		$criteria->compare('UsernameBayarSisaFactory',$this->UsernameBayarSisaFactory,true);
		$criteria->compare('KodeBankPengirimDP',$this->KodeBankPengirimDP,true);
		$criteria->compare('NoRekPengirimDP',$this->NoRekPengirimDP,true);
		$criteria->compare('AtasNamaPengirimDP',$this->AtasNamaPengirimDP,true);
		$criteria->compare('KodeBankPenerimaDP',$this->KodeBankPenerimaDP,true);
		$criteria->compare('NoRekPenerimaDP',$this->NoRekPenerimaDP,true);
		$criteria->compare('AtasNamaPenerimaDP',$this->AtasNamaPenerimaDP,true);
		$criteria->compare('KodeBankPengirimLunas',$this->KodeBankPengirimLunas,true);
		$criteria->compare('NoRekPengirimLunas',$this->NoRekPengirimLunas,true);
		$criteria->compare('AtasNamaPengirimLunas',$this->AtasNamaPengirimLunas,true);
		$criteria->compare('KodeBankPenerimaLunas',$this->KodeBankPenerimaLunas,true);
		$criteria->compare('NoRekPenerimaLunas',$this->NoRekPenerimaLunas,true);
		$criteria->compare('AtasNamaPenerimaLunas',$this->AtasNamaPenerimaLunas,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
