<?php

/**
 * This is the model class for table "Login".
 *
 * The followings are the available columns in table 'Login':
 * @property integer $id
 * @property string $username
 * @property string $nama
 * @property string $password
 * @property integer $status
 * @property string $lastLoggedIn
 * @property string $sessionKey
 * @property string $sessionExpired
 * @property string $tglDibuat
 * @property integer $dibuatOleh
 * @property string $tglDiubah
 * @property integer $diubahOleh
 */
class Login extends MyActiveRecord
{
    // status
    const STATUS_NOTACTIVE = 0; //not active
    const STATUS_ACTIVE = 1; //active
    const STATUS_BANNED = 9; //banned
    const STATUS_LOCKED = 2; //locked

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Login';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, dibuatOleh, diubahOleh', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>20),
			array('nama, sessionKey', 'length', 'max'=>50),
			array('password', 'length', 'max'=>100),
			array('lastLoggedIn, sessionExpired, tglDibuat, tglDiubah', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, nama, password, status, lastLoggedIn, sessionKey, sessionExpired, tglDibuat, dibuatOleh, tglDiubah, diubahOleh', 'safe', 'on'=>'search'),
		);
	}

    /**
     * @return array
     */
    public function behaviors()
    {
        return array(
            'HasCreatedDate'=>array('class'=>'common.extensions.behaviors.HasCreatedDateBehavior'),
            'HasCreatedBy'=>array('class'=>'common.extensions.behaviors.HasCreatedByBehavior'),
            'HasModifiedDate'=>array('class'=>'common.extensions.behaviors.HasModifiedDateBehavior'),
            'HasModifiedBy'=>array('class'=>'common.extensions.behaviors.HasModifiedByBehavior'),
        );
    }

    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'userLogin'=>array(self::BELONGS_TO, 'Login', 'id'),
            'userCreate'=>array(self::BELONGS_TO, 'Login', 'dibuatOleh'),
            'userModify'=>array(self::BELONGS_TO, 'Login', 'diubahOleh'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'User name',
			'nama' => 'Name',
			'password' => 'Password',
			'status' => 'Status',
			'lastLoggedIn' => 'Last Logged In',
			'sessionKey' => 'Session Key',
			'sessionExpired' => 'Session Expired',
			'tglDibuat' => 'Date Created',
			'dibuatOleh' => 'Created By',
			'tglDiubah' => 'Date Changed',
			'diubahOleh' => 'Changed By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('lastLoggedIn',$this->lastLoggedIn,true);
		$criteria->compare('sessionKey',$this->sessionKey,true);
		$criteria->compare('sessionExpired',$this->sessionExpired,true);
		$criteria->compare('tglDibuat',$this->tglDibuat,true);
		$criteria->compare('dibuatOleh',$this->dibuatOleh);
		$criteria->compare('tglDiubah',$this->tglDiubah,true);
		$criteria->compare('diubahOleh',$this->diubahOleh);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Login the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave(){
        if($this->isNewRecord){
            if($this->password !== '' || $this->password !== NULL){
                $this->password = md5($this->password);
            }
            $this->status = Login::STATUS_ACTIVE;
        }

        return parent::beforeSave();
    }
}
