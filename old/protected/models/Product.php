<?php

/**
 * This is the model class for table "Product".
 *
 * The followings are the available columns in table 'Product':
 * @property string $kodeBarang
 * @property string $namaBarang
 * @property string $desc
 * @property double $qty
 * @property string $stockCode
 * @property double $oz
 * @property double $diameter
 * @property double $weight
 * @property double $volumeml
 * @property double $volumeOz
 * @property double $length
 * @property double $height
 * @property double $cbm
 * @property string $priceFOB
 * @property string $priceMaster
 * @property double $inner
 * @property double $master
 * @property string $moq
 * @property double $artCapasity
 * @property double $nettWeight
 * @property double $grossWeight
 * @property double $obLength
 * @property double $obWidth
 * @property double $obHeight
 * @property double $totalCTN
 * @property double $totalGW
 * @property double $totalCBM
 * @property double $totalUSD
 * @property integer $status
 * @property string $sellerID
 * @property string $kodeProductCategory
 * @property string $productID
 */
class Product extends MyActiveUserRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kodeBarang', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('qty, oz, diameter, weight, volumeml, volumeOz, length, height, cbm, inner, master, artCapasity, nettWeight, grossWeight, obLength, obWidth, obHeight, totalCTN, totalGW, totalCBM, totalUSD', 'numerical'),
			array('kodeBarang, namaBarang, desc, stockCode, moq', 'length', 'max'=>255),
			array('priceFOB, priceMaster', 'length', 'max'=>19),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kodeBarang, namaBarang, kodeProductCategory, desc, qty, stockCode, oz, diameter, weight, volumeml, volumeOz, length, height, cbm, priceFOB, priceMaster, inner, master, moq, artCapasity, nettWeight, grossWeight, obLength, obWidth, obHeight, totalCTN, totalGW, totalCBM, totalUSD, status, sellerID, productID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'stock' => array(self::BELONGS_TO, 'MasterStock', 'stockCode'),
            'image' => array(self::HAS_ONE, 'ProductImage', 'kodeBarang'),
            'medias' => array(self::HAS_MANY, 'ProductImage', 'kodeBarang'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kodeBarang' => 'Product Code',
			'namaBarang' => 'Product Name',
			'kodeProductCategory' => 'Product Category',
			'desc' => 'Description',
			'qty' => 'Quantity',
			'stockCode' => 'Stock Code',
			'oz' => 'Oz',
			'diameter' => 'Diameter(mm)',
			'weight' => 'Weight(mm)',
			'volumeml' => '(ml)',
			'volumeOz' => '(oz)',
			'length' => 'Length',
			'height' => 'Height',
			'cbm' => 'CBM(m3)',
			'priceFOB' => 'Price Fob',
			'priceMaster' => 'Price Master',
			'inner' => 'Inner',
			'master' => 'Master',
			'moq' => 'Moq',
			'artCapasity' => 'Art Capacity',
			'nettWeight' => 'Nett Weight(kg)',
			'grossWeight' => 'Gross Weight(kg)',
			'obLength' => 'Length',
			'obWidth' => 'Width',
			'obHeight' => 'Height',
			'totalCTN' => 'Total Carton',
			'totalGW' => 'Total GW(kgs)',
			'totalCBM' => 'Total CBM(m3)',
			'totalUSD' => 'Total Price',
			'status' => 'Status',
			'sellerID' => 'Seller ID',
			'productID' => 'Product ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kodeBarang',$this->kodeBarang,true);
		$criteria->compare('namaBarang',$this->namaBarang,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('stockCode',$this->stockCode,true);
		$criteria->compare('oz',$this->oz);
		$criteria->compare('diameter',$this->diameter);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('volumeml',$this->volumeml);
		$criteria->compare('volumeOz',$this->volumeOz);
		$criteria->compare('length',$this->length);
		$criteria->compare('height',$this->height);
		$criteria->compare('cbm',$this->cbm);
		$criteria->compare('priceFOB',$this->priceFOB,true);
		$criteria->compare('priceMaster',$this->priceMaster,true);
		$criteria->compare('inner',$this->inner);
		$criteria->compare('master',$this->master);
		$criteria->compare('moq',$this->moq,true);
		$criteria->compare('artCapasity',$this->artCapasity);
		$criteria->compare('nettWeight',$this->nettWeight);
		$criteria->compare('grossWeight',$this->grossWeight);
		$criteria->compare('obLength',$this->obLength);
		$criteria->compare('obWidth',$this->obWidth);
		$criteria->compare('obHeight',$this->obHeight);
		$criteria->compare('totalCTN',$this->totalCTN);
		$criteria->compare('totalGW',$this->totalGW);
		$criteria->compare('totalCBM',$this->totalCBM);
		$criteria->compare('totalUSD',$this->totalUSD);
		$criteria->compare('status',$this->status);
		$criteria->compare('sellerID',$this->sellerID, true);
		$criteria->compare('productID',$this->productID, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
