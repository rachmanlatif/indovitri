<?php

/**
 * This is the model class for table "DeliveryAgent".
 *
 * The followings are the available columns in table 'DeliveryAgent':
 * @property string $KodeDeliveryAgent
 * @property string $nama
 * @property string $address
 * @property integer $status
 * @property string $phone
 * @property string $email
 * @property string $contactPerson
 * @property string $kodeCity
 * @property string $kodeState
 * @property string $kodeCountry
 * @property string $zipcode
 */
class DeliveryAgent extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'DeliveryAgent';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('KodeDeliveryAgent', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('KodeDeliveryAgent, nama, address, phone, email, contactPerson, kodeCity, kodeState, kodeCountry, zipcode', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('KodeDeliveryAgent, nama, address, status, phone, email, contactPerson, kodeCity, kodeState, kodeCountry, zipcode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'city' => array(self::BELONGS_TO, 'City', 'kodeCity'),
            'state' => array(self::BELONGS_TO, 'State', 'kodeState'),
            'country' => array(self::BELONGS_TO, 'Country', 'kodeCountry'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'KodeDeliveryAgent' => 'Kode Delivery Agent',
			'nama' => 'Nama',
			'address' => 'Address',
			'status' => 'Status',
			'phone' => 'Phone',
			'email' => 'Email',
			'contactPerson' => 'Contact Person',
			'kodeCity' => 'Kode City',
			'kodeState' => 'Kode State',
			'kodeCountry' => 'Kode Country',
			'zipcode' => 'Zipcode',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('KodeDeliveryAgent',$this->KodeDeliveryAgent,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('contactPerson',$this->contactPerson,true);
		$criteria->compare('kodeCity',$this->kodeCity,true);
		$criteria->compare('kodeState',$this->kodeState,true);
		$criteria->compare('kodeCountry',$this->kodeCountry,true);
		$criteria->compare('zipcode',$this->zipcode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DeliveryAgent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
