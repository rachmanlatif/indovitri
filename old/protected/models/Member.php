<?php

/**
 * This is the model class for table "Member".
 *
 * The followings are the available columns in table 'Member':
 * @property string $kodeMember
 * @property string $email
 * @property string $hp
 * @property string $nama
 * @property string $password
 * @property integer $status
 * @property string $lastLoggedIn
 * @property string $sessionKey
 * @property string $sessionExpired
 * @property string $tglDibuat
 * @property integer $idRegister
 */
class Member extends MyActiveRecord
{
    // status
    const STATUS_NOTACTIVE = 0; //not active
    const STATUS_ACTIVE = 1; //active
    const STATUS_BANNED = 9; //banned
    const STATUS_LOCKED = 2; //locked

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Member';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idRegister', 'required'),
			array('idRegister', 'numerical', 'integerOnly'=>true),
			array('kodeMember, hp', 'length', 'max'=>15),
			array('email, nama, sessionKey', 'length', 'max'=>50),
			array('password', 'length', 'max'=>100),
			array('lastLoggedIn, sessionExpired, tglDibuat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kodeMember, idRegister, email, hp, nama, password, status, lastLoggedIn, sessionKey, sessionExpired, tglDibuat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'store' => array(self::HAS_ONE, 'Toko', 'kodeMember'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kodeMember' => 'Member Code',
			'idRegister' => 'ID Register',
			'email' => 'Email',
			'hp' => 'Phone Number',
			'nama' => 'Name',
			'password' => 'Password',
			'status' => 'Status',
			'lastLoggedIn' => 'Last Logged In',
			'sessionKey' => 'Session Key',
			'sessionExpired' => 'Session Expired',
			'tglDibuat' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kodeMember',$this->kodeMember,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('hp',$this->hp,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('status',$this->status, true);
		$criteria->compare('lastLoggedIn',$this->lastLoggedIn,true);
		$criteria->compare('sessionKey',$this->sessionKey,true);
		$criteria->compare('sessionExpired',$this->sessionExpired,true);
		$criteria->compare('tglDibuat',$this->tglDibuat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Member the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave(){
        if($this->isNewRecord){
            $this->status = Member::STATUS_ACTIVE;
            $this->tglDibuat = date('Y-m-d H:i:s');
        }

        return parent::beforeSave();
    }
}
