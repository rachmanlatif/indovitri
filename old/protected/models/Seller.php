<?php

/**
 * This is the model class for table "Seller".
 *
 * The followings are the available columns in table 'Seller':
 * @property string $sellerID
 * @property string $sellerName
 * @property string $address
 * @property string $telp
 * @property integer $status
 * @property string $HP
 * @property string $contactPerson
 * @property string $kodeCountry
 * @property string $kodeState
 * @property string $kodeCity
 * @property string $email
 */
class Seller extends MyActiveUserRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Seller';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sellerID, email', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('sellerID, sellerName, address, telp, HP, contactPerson, kodeCountry, kodeState, kodeCity', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('sellerID, sellerName, address, telp, status, HP, contactPerson, kodeCountry, kodeState, kodeCity, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'sellerID' => 'Seller',
			'sellerName' => 'Seller Name',
			'address' => 'Address',
			'telp' => 'Phone',
			'status' => 'Status',
			'HP' => 'Hand Phone',
			'contactPerson' => 'Contact Person',
			'kodeCountry' => 'Coutry Code',
			'kodeState' => 'Province Code',
			'kodeCity' => 'City Code',
			'email' => 'Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sellerID',$this->sellerID,true);
		$criteria->compare('sellerName',$this->sellerName,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('telp',$this->telp,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('HP',$this->HP,true);
		$criteria->compare('contactPerson',$this->contactPerson,true);
		$criteria->compare('kodeCountry',$this->kodeCountry,true);
		$criteria->compare('kodeState',$this->kodeState,true);
		$criteria->compare('kodeCity',$this->kodeCity,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Seller the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
