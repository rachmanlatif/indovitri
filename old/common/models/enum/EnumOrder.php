<?php

class EnumOrder extends MyEnum
{
    const CANCEL = 0;
    const PROSPECT = 1;
    const CHECKOUT = 2;
    const APPROVE_FACTORY = 3;
    const REJECT_FACTORY = 5;
    const CONFIRM_ORDER = 6;
    const BILL_DP = 7;
    const PAY_FACTORY = 8;
    const PICKUP_ORDER = 9;
    const BILL_AUTO = 10;
    const CONFIRM_LAST = 11;
    const BILL_LAST = 12;

    public static function getList()
    {
        return array(
            self::CANCEL => 'Cancel Order',
            self::PROSPECT => 'Prospect Order',
            self::CHECKOUT => 'Checkout Order',
            self::APPROVE_FACTORY => 'Seller Has Send Available Item',
            self::REJECT_FACTORY => 'Seller Has Reject All Item',
            self::CONFIRM_ORDER => 'Order Confirmed',
            self::BILL_DP => 'Down Payment Order',
            self::PAY_FACTORY => 'Processing To Factory',
            self::PICKUP_ORDER => 'Pickup Order To Shipper',
            self::BILL_AUTO => 'Auto Billing Last Payment Created',
            self::CONFIRM_LAST => 'Last Payment Paid',
            self::BILL_LAST => 'Finish Order',
        );
    }

    public static function getLabel($id)
    {
        $list = static::getList();
        if (isset($list[$id])) {
            return $list[$id];
        }

        return '-';
    }
}
