<?php

class IDGenerator
{
    public static function generate($keyword)
    {
        $id = '';

        $format = Counter::getFormat($keyword);
        $counter = Counter::getCounter($keyword);

        // parse format
        // date
        $format = str_replace('{YYYY}', date('Y'), $format);
        $format = str_replace('{YY}', date('y'), $format);
        $format = str_replace('{MM}', date('m'), $format);
        $format = str_replace('{MMM}', date('M'), $format);
        $format = str_replace('{DD}', date('d'), $format);

        // replace sequence to format
        preg_match('/{(x+)}/', $format, $matchesNum);
        preg_match('/{(n+)}/', $format, $matchesAlNum);

        if (!empty($matchesNum) || !empty($matchesAl) || !empty($matchesAlNum)) {
            $matches = null;
            $generatedNumber = 0;

            if (!empty($matchesNum)) {
                $matches = $matchesNum;
                $generatedNumber = $counter + 1;
            } else {
                $matches = $matchesAlNum;
                $generatedNumber = base_convert($counter+1, 10, 36);
            }

            $sequence_format = $matches[1];
            $digit = strlen($sequence_format);

            // insert sequence
            $sequence = sprintf("%0". $digit ."s", $generatedNumber);
            $id = str_replace($matches[0], $sequence, $format);

            // add counter
            Counter::addCounter($keyword);
        } else {
            throw new CDbException('The format doesn\'t contain sequence number.');
        }

        return strtoupper($id);
		//return $id;
    }
	
	public static function generateForOnlineBooking($keyword)
    {
        $id = '';

        $format = Counter::getFormat($keyword);
        $counter = Counter::getCounter($keyword);

        // parse format
        // date
        $format = str_replace('{YYYY}', date('Y'), $format);
        $format = str_replace('{YY}', date('y'), $format);
        $format = str_replace('{MM}', date('m'), $format);
        $format = str_replace('{MMM}', date('M'), $format);
        $format = str_replace('{DD}', date('d'), $format);

        // replace sequence to format
        preg_match('/{(x+)}/', $format, $matchesNum);
        preg_match('/{(n+)}/', $format, $matchesAlNum);

        if (!empty($matchesNum) || !empty($matchesAl) || !empty($matchesAlNum)) {
            $matches = null;
            $generatedNumber = 0;

            if (!empty($matchesNum)) {
                $matches = $matchesNum;
                $generatedNumber = $counter + 1;
            } else {
                $matches = $matchesAlNum;
                $generatedNumber = base_convert($counter+1, 10, 36);
            }

            $sequence_format = $matches[1];
            $digit = strlen($sequence_format);

            // insert sequence
            $sequence = sprintf("%0". $digit ."s", $generatedNumber);
            $id = str_replace($matches[0], $sequence, $format);

            // add counter
            Counter::addCounter($keyword);
        } else {
            throw new CDbException('The format doesn\'t contain sequence number.');
        }

        return strtoupper($id);
		//return $id;
    }

    public static function num2alpha($n) {
        $r = '';
        for ($i = 1; $n >= 0 && $i < 10; $i++) {
            $r = chr(0x41 + ($n % pow(26, $i) / pow(26, $i - 1))) . $r;
            $n -= pow(26, $i);
        }
        return $r;
    }
    
    public static function restartCounter($keyword){
        $result = NULL;
        try {
            $result  = Counter::restartCounter($keyword);
        } catch (Exception $ex) {
            $result = false;
            Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
        }
        
    }
}