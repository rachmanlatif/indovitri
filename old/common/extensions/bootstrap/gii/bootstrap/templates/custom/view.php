<h1>View <?php echo $this->class2name($this->modelClass) . " #<?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></h1>
<hr />

<?php echo "<?php\n"; ?>$this->widget('ext.widgets.form.FormLinkButtons', array(
	'linkButtons'=>array(
		array('label'=>'List', 'url'=>array('index'), 'access'=>array('<?php echo $this->modelClass; ?>Index')),
		array('label'=>'Create', 'url'=>array('create'), 'access'=>array('<?php echo $this->modelClass; ?>Create')),
		array('label'=>'Update','url'=>array('update','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>), 'access'=>array('<?php echo $this->modelClass; ?>Update'), 'optionalAccess'=>array('<?php echo $this->modelClass; ?>Own')),
		array('label'=>'Delete','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>),'confirm'=>'Are you sure you want to delete this item?'), 'access'=>array('<?php echo $this->modelClass; ?>Delete'), 'optionalAccess'=>array('<?php echo $this->modelClass; ?>Own')),
	),
)); ?>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
<?php
foreach($this->tableSchema->columns as $column) {
	if ($column->name == 'created_by' || $column->name == 'modified_by') {
		echo "\t\tMyHelper::detailViewUsername(\$model, '".$column->name."'),\n";
	} else if ($column->name == 'created_date' || $column->name == 'modified_date') {
		echo "\t\tMyHelper::detailViewDateTime(\$model, '".$column->name."'),\n";
	} else {
		echo "\t\t'".$column->name."',\n";
	}
}
?>
),
)); ?>
