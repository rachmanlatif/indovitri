<?php

class TaskList extends CWidget
{
    /**
     * @var string
     */
    public $baseScriptUrl;

    /**
     * Initialize widget
     */
    public function init()
    {
        if ($this->baseScriptUrl === null)
            $this->baseScriptUrl = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets');
    }

    /**
     * register client script needed
     */
    protected function registerClientScript()
	{
		$cs = Yii::app()->clientScript;
        $cs->registerCssFile($this->baseScriptUrl . '/style.css');
	}

    /**
     * run the widget
     */
    public function run()
	{
        $taskList = Task::model()->findAll("user_id=? AND (status=? OR status=?)", array(
            Yii::app()->user->id,
            EnumTaskStatus::PENDING,
            EnumTaskStatus::READ,
        ));

        $this->registerClientScript();
        $this->render('index', array(
            'taskList'=>$taskList,
        ));
	}
}