<?php

class Lookup extends CWidget
{
    public $layout='layout';
    public $id='popup-search';
    public $paramName=null;
    public $cssFile;
    public $gridViewConfig;
    public $defaultGridViewConfig;	
    public $view='lookup';
    public $title;
    public $model;
    public $baseScriptUrl;

    public function init()
    {		
            parent::init();

            if ($this->cssFile == null) {
                    //$this->cssFile = Yii::app()->request->baseUrl . '/css/gridview.css';
            }
            if ($this->baseScriptUrl === null)
                $this->baseScriptUrl = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets');
    }

    public function initAttributes($modelName)
    {
        $this->model = new $modelName('search');
        $this->model->unsetAttributes();

        if ($this->paramName == null) {
            if (isset($_REQUEST['paramName'])) {
                $this->paramName = $_REQUEST['paramName'];
            } else {
                $this->paramName = $modelName;
            }
        }

        if(isset($_GET[$this->paramName]))
            $this->model->attributes=$_GET[$this->paramName];
    }
	
	public function addConfig($configs)
	{
		foreach ($configs as $key=>$config) {
			$this->_gridview_config[$key] = $config;
		}
	}
	
	protected function registerClientScript()
	{
		$cs=Yii::app()->clientScript;
		//$cs->registerScriptFile(Yii::app()->request->baseUrl. '/js/ui/jquery-ui-1.8.16.custom.js');
		//$cs->registerScriptFile(Yii::app()->request->baseUrl."/js/jquery.valign.js");
                $cs->registerScriptFile($this->baseScriptUrl . '/js/jquery.valign.js');
		$cs->registerScript("CloseLookup", "
			// close when close button is clicked
			$('.popup-search-close-button').live('click', function(){
				$(this).parents('.popup-search-container').hide();
			});
			// close when clicked outside
			$(document).mouseup(function (e){
				var container = $('.popup-search-box');
				if (container.has(e.target).length === 0) {
					container.parent().hide();
				}
			});
			$('.popup-search-box').draggable();
		");
	}
	
	public function renderWidget($view, $data=null)
	{
		$this->registerClientScript();
		
		if (!isset($data['title'])) {
			$data['title'] = $this->title;
		}
		$content = $this->render($view, $data, true);
		
		if ($this->layout != null) {
			$this->render($this->layout, array(
				'id'=>$this->id,
				'content'=>$content,
			));
		} else {
			echo $content;
		}
	}

	public function run()
	{
		if ($this->view != null && $this->defaultGridViewConfig != null)
			$this->renderWidget($this->view, $this->finalizeData($this->defaultGridViewConfig));
	}
	
	public function finalizeData($defaultGridViewConfig)
	{
		if ($this->gridViewConfig != null) {
			foreach ($this->gridViewConfig as $key=>$config) {
				$defaultGridViewConfig[$key] = $config;
			}
		}
		$this->gridViewConfig = $defaultGridViewConfig;
		
		return array(
			'gridViewConfig'=>$this->gridViewConfig,			
		);
	}
}