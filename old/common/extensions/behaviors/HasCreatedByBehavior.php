<?php

class HasCreatedByBehavior extends CActiveRecordBehavior
{
    public function attach($owner)
    {
        parent::attach($owner);
        $this->owner->getMetaData()->addRelation('userCreate', array(CActiveRecord::BELONGS_TO, 'Login', 'dibuatOleh'));
    }

    public function beforeValidate($event)
    {
        if ($this->owner->isNewRecord) {
	        if (isset(Yii::app()->user->isMemberShipLogin)) {
		        $this->owner->dibuatOleh = 0;
	        }
            else if (Yii::app()->user->isGuest){
                $this->owner->dibuatOleh = 0;
            }
            else {
	            if($this->owner->dibuatOleh===NULL || $this->owner->dibuatOleh=== '')
		            $this->owner->dibuatOleh = Yii::app()->user->id;
	        }
        }

        return parent::beforeValidate($event);
    }
}