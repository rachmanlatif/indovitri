<?php

class SoftDeleteBehavior extends CActiveRecordBehavior
{
    /**
     * @var string
     */
    public $flagField = 'is_deleted';

    /**
     * @return CComponent
     */
    public function remove() {
        $this->getOwner()->{$this->flagField} = 1;
        return $this->getOwner();
    }

    /**
     * @return CComponent
     */
    public function restore() {
        $this->getOwner()->{$this->flagField} = 0;
        return $this->getOwner();
    }

    /**
     * @return CComponent
     */
    public function notRemoved() {
        $this->getOwner()->getDbCriteria()->mergeWith(array(
            'condition'=>$this->flagField.' = 0',
            'params'=>array(),
        ));

        return $this->getOwner();
        /*
        $criteria = $this->getOwner()->getDbCriteria();
        $criteria->compare('t.' . $this->flagField, 0);
        return $this->getOwner();
        */
    }

    /**
     * @return CComponent
     */
    public function removed() {
        $this->getOwner()->getDbCriteria()->mergeWith(array(
            'condition'=>$this->flagField.' = 1',
            'params'=>array(),
        ));

        return $this->getOwner();
        /*
        $criteria = $this->getOwner()->getDbCriteria();
        $criteria->compare('t.' . $this->flagField, 1);
        return $this->getOwner();
        */
    }

    /**
     * @return bool
     */
    public function isRemoved() {
        return (boolean)$this->getOwner()->{$this->flagField};
    }
}
