<?php

class HasModifiedByBehavior extends CActiveRecordBehavior
{
    public function attach($owner)
    {
        parent::attach($owner);
        $this->owner->getMetaData()->addRelation('userModify', array(CActiveRecord::BELONGS_TO, 'Login', 'diubahOleh'));
    }

    public function beforeValidate($event)
    {
        if (!$this->owner->isNewRecord) {
	        if (isset(Yii::app()->user->isMemberShipLogin)) {
		        $this->owner->diubahOleh = 0;
	        }
            else if (Yii::app()->user->isGuest){
                $this->owner->diubahOleh = 0;
            }
            else if ($this->owner->diubahOleh === null || $this->owner->diubahOleh === '') {
                $this->owner->diubahOleh = Yii::app()->user->id;
	        }
        }

        return parent::beforeValidate($event);
    }
}