<?php

class MyActiveRecord extends CActiveRecord
{
	/**
	 * The name of the selected database
	 * @var string
	 */
	private static $dbName = 'db';
	public $oldItem = null;
	public $logDescription = "";

	/**
	 * Select a database which is defined in the configuration file
	 * @param string $dbName
	 */
	public function getDbConnection() {
		if (self::$db === null) {
			$dbName = self::$dbName;
			self::$db = Yii::app()->$dbName;
			if (self::$db instanceof CDbConnection === false) {
				throw new CDbException(Yii::t('yii','Active Record requires a "db" CDbConnection application component.'));
			}
		}

		return self::$db;
	}

	public function getPrimaryKey() {
		$table = $this->getMetaData()->tableSchema;
		if (is_string($table->primaryKey)) {
			return $this->{$table->primaryKey};
		} else if (is_array($table->primaryKey)) {
			$arr = array();
			foreach ($table->primaryKey as $primaryKey) {
				array_push($arr, $this->$primaryKey);
			}
			return implode(',', $arr);
		}

		return "";
	}

	protected function afterFind ()
	{
		$this->oldItem = clone $this;
		parent::afterFind ();
	}

	protected function afterSave()
	{
		parent::afterSave();
	}

	/**
	 * @param bool $runValidation
	 * @param null|array $attributes
	 * @throws ARException
	 */
	public function save2($runValidation=true, $attributes=null)
	{
		if (!$this->save($runValidation, $attributes)) {
			throw new ARException();
		}
	}

	/**
	 * @param null $prefix
	 * @param string $translationContext
	 * @return array
	 */
	public static function getConstants($prefix=null, $translationContext='string')
	{
		$class = static::model();
		$reflection = new ReflectionClass( get_class( $class ) );

		$list = array();
		$constants = $reflection->getConstants();

		foreach ($constants as $key=>$constant) {
			if ($prefix === null || ($prefix !== null && strpos(strtolower($key), strtolower($prefix)) === 0)) {
				$list[$constant] = Yii::t($translationContext,$constant);
			}
		}

		return $list;
	}

	/**
	 * @param string $id
	 * @param string $label
	 * @param CDbCriteria|string|null $criteria
	 * @return array
	 */
	public static function toArray($id, $label, $criteria=null)
	{
		$models = static::model()->findAll($criteria);
		return CHtml::listData($models, $id, $label);
	}
}
