<?php
date_default_timezone_set("Asia/Jakarta");

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);

// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

// change the following paths if necessary
$yii = include dirname(__FILE__) . '/common/yii.php';
require_once($yii);

$commonConfig = include dirname(__FILE__).'/common/config/main.php';
$appConfig = include dirname(__FILE__) . '/protected/config/main.php';
$config = CMap::mergeArray($commonConfig, $appConfig);

//require_once dirname(__FILE__) . '/../ezcomponents/Base/src/base.php';
//Yii::registerAutoLoader(array('ezcBase', 'autoload'), true);

//error_log("plkj89156 config : " . print_r($config, true) . " --- "); // mydebugger

Yii::createWebApplication($config)->run();
