<?php
/**
 * This is the bootstrap file for test application.
 * This file should be removed when the application is deployed for production.
 */

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

// change the following paths if necessary
$yii = include dirname(__FILE__).'/common/yii.php';
require_once($yii);

$commonConfig = include dirname(__FILE__).'/common/config/main.php';
$appConfig = include dirname(__FILE__) . '/protected/config/main.php';
$config = CMap::mergeArray($commonConfig, $appConfig);

Yii::createWebApplication($config)->run();