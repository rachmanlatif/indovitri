<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta charset="utf-8" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <meta name="description" content="login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl;?>/frontend/images/p5logo.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/bootstrap.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/font-awesome.min.css" rel="stylesheet" />

    <!--Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

    <!--Beyond styles-->
    <!-- <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/beyond.css" rel="stylesheet" /> -->
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/demo.min.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/animate.min.css" rel="stylesheet" />
    <link id="skin-link" href="" rel="stylesheet" type="text/css" />

    <!-- Jquery -->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/jquery-2.0.3.min.js"></script>

    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/skins.min.js"></script>
</head>

<body style="background-image: url('<?php echo Yii::app()->theme->baseUrl;?>/backend/img/back-login.jpg'); background-size: cover; background-position: top center; min-height: 50vh;">
  <div class="login-container animated fadeInDown" >
      <?php echo $content?>
  </div>



<!--Basic Scripts-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/beyond.min.js"></script>

</body>
</html>
