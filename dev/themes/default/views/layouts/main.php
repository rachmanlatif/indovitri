<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta charset="utf-8" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <meta name="description" content="login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl;?>/backend/img/p5logo.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/bootstrap.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/font-awesome.min.css" rel="stylesheet" />

    <!--Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

    <!--Beyond styles-->
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/beyond.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/demo.min.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/animate.min.css" rel="stylesheet" />
    <link id="skin-link" href="" rel="stylesheet" type="text/css" />

    <!-- Jquery -->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/jquery-2.0.3.min.js"></script>

    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/skins.min.js"></script>

    <!--Datatables-->
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/dataTables.bootstrap.css" rel="stylesheet" />

    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/slick-master/slick/slick.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/slick-master/slick/slick-theme.css" rel="stylesheet" />


</head>

<body>
<!-- Loading Container -->
<div class="loading-container">
    <div class="loading-progress">
        <div class="rotator">
            <div class="rotator">
                <div class="rotator colored">
                    <div class="rotator">
                        <div class="rotator colored">
                            <div class="rotator colored"></div>
                            <div class="rotator"></div>
                        </div>
                        <div class="rotator colored"></div>
                    </div>
                    <div class="rotator"></div>
                </div>
                <div class="rotator"></div>
            </div>
            <div class="rotator"></div>
        </div>
        <div class="rotator"></div>
    </div>
</div>
<!--  /Loading Container -->
<!-- Navbar -->
<div class="navbar">
    <div class="navbar-inner">
        <div class="navbar-container">
            <!-- Navbar Barnd -->
            <div class="navbar-header">
                <a href="#" class="navbar-brand">
                        <img width="90" height="50" src="<?php echo Yii::app()->theme->baseUrl;?>/backend/img/p5logo.png" alt="" />
                </a>
            </div>
            <!-- /Navbar Barnd -->

            <!-- Sidebar Collapse -->
            <div class="sidebar-collapse" id="sidebar-collapse">
                <i class="collapse-icon fa fa-bars"></i>
            </div>
            <!-- /Sidebar Collapse -->
            <!-- Account Area and Settings --->
            <div class="navbar-header pull-right">
                <div class="navbar-account">
                    <ul class="account-area">
                        <li>
                            <a class="login-area dropdown-toggle" data-toggle="dropdown">
                                <!-- <div class="avatar" title="View your public profile">
                                    <img src="<?php echo Yii::app()->theme->baseUrl;?>/backend/img/avatars/adam-jansen.jpg">
                                </div> -->
                                <section>
                                    <?php
                                    $username = '';
                                    $nama = '';
                                    $user = MyHelper::getUser();
                                    if($user->FirstName != ''){
                                        $username = $user->Username;
                                        $nama = $user->FirstName.' '.$user->LastName;
                                    }
                                    ?>
                                    <h2><span class="profile"><span>Hi!  <?php echo ucfirst($nama); ?></span></span></h2>
                                </section>
                            </a>
                            <!--Login Area Dropdown-->
                            <ul class="pull-right dropdown-menu dropdown-arrow dropdown-login-area">
                                <!-- <li class="username"><a><?php echo $username; ?></a></li>
                                <li class="email"><a><?php echo $username; ?></a></li> -->
                                <!--Avatar Area-->
                                <li>
                                    <!-- <div class="avatar-area">
                                        <img src="<?php echo Yii::app()->theme->baseUrl;?>/backend/img/avatars/adam-jansen.jpg" class="avatar">
                                        <span class="caption">Change Photo</span>
                                    </div> -->
                                </li>
                                <!--Avatar Area-->
                                <li class="dropdown-footer">
                                    <a href="<?php echo Yii::app()->baseUrl;?>/logout">
                                        Sign out
                                    </a>
                                </li>
                            </ul>
                            <!--/Login Area Dropdown-->
                        </li>
                        <!-- /Account Area -->
                        <!--Note: notice that setting div must start right after account area list.
                        no space must be between these elements-->
                        <!-- Settings -->
                    </ul><div class="setting">
                        <a id="btn-setting" title="Setting" href="#">
                            <i class="icon glyphicon glyphicon-cog"></i>
                        </a>
                    </div><div class="setting-container">
                        <label>
                            <input type="checkbox" id="checkbox_fixednavbar">
                            <span class="text">Fixed Navbar</span>
                        </label>
                        <label>
                            <input type="checkbox" id="checkbox_fixedsidebar">
                            <span class="text">Fixed SideBar</span>
                        </label>
                        <label>
                            <input type="checkbox" id="checkbox_fixedbreadcrumbs">
                            <span class="text">Fixed BreadCrumbs</span>
                        </label>
                        <label>
                            <input type="checkbox" id="checkbox_fixedheader">
                            <span class="text">Fixed Header</span>
                        </label>
                    </div>
                    <!-- Settings -->
                </div>
            </div>
            <!-- /Account Area and Settings -->
        </div>
    </div>
</div>
<!-- /Navbar -->
<!-- Main Container -->
<div class="main-container container-fluid">
    <!-- Page Container -->
    <div class="page-container">
        <!-- Page Sidebar -->
        <div class="page-sidebar" id="sidebar">
            <!-- Page Sidebar Header-->
            <div class="sidebar-header-wrapper">
                <input type="text" class="searchinput" />
                <i class="searchicon fa fa-search"></i>
                <div class="searchhelper">Search Reports, Charts, Emails or Notifications</div>
            </div>
            <!-- /Page Sidebar Header -->
            <!-- Sidebar Menu -->
            <ul class="nav sidebar-menu">
                <!--Dashboard-->
                <li class="<?php echo $this->id == 'dashboard' ? 'active' : '' ?>">
                    <a href="<?php echo Yii::app()->baseUrl.'/dashboard'?>">
                        <i class="menu-icon glyphicon glyphicon-home"></i>
                        <span class="menu-text"> Dashboard </span>
                    </a>
                </li>

                <?php $master = array('position','users','bankList','bankAccount','shipper','country','city','state','container','agent','consignee','notifyParty','incoterm','port','commisionSchema','orderChargeMaster','uom') ?>

                <li class="<?php echo in_array($this->id, $master) ? 'open' : '' ?>">
                    <a href="#" class="menu-dropdown">
                        <i class="menu-icon fa fa-tasks"></i>
                        <span class="menu-text"> Master </span>
                        <i class="menu-expand"></i>
                    </a>

                    <ul class="submenu">
                        <li class="<?php echo $this->id == 'country' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/country'?>">
                                <span class="menu-text"> Country</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'state' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/state'?>">
                                <span class="menu-text"> State</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'city' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/city'?>">
                                <span class="menu-text"> City</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'position' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/position'?>">
                                <span class="menu-text"> Position</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'users' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/users'?>">
                                <span class="menu-text"> User Login</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'bankList' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/bankList'?>">
                                <span class="menu-text"> Bank List</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'bankAccount' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/bankAccount'?>">
                                <span class="menu-text"> Bank Account</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'shipper' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/shipper'?>">
                                <span class="menu-text"> Shipper</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'container' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/container'?>">
                                <span class="menu-text"> Container</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'incoterm' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/incoterm'?>">
                                <span class="menu-text"> Incoterm</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'agent' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/agent'?>">
                                <span class="menu-text"> Agent</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'consignee' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/consignee'?>">
                                <span class="menu-text"> Consignee</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'notifyParty' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/notifyParty'?>">
                                <span class="menu-text"> Notify Party</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'port' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/port'?>">
                                <span class="menu-text"> Port</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'commisionSchema' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/commisionSchema'?>">
                                <span class="menu-text"> Contract Scheme</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'orderChargeMaster' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/orderChargeMaster'?>">
                                <span class="menu-text"> Charge List</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'uom' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/uom'?>">
                                <span class="menu-text"> UOM</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="<?php echo $this->id == 'seller' ? 'active' : '' ?>">
                    <a href="<?php echo Yii::app()->baseUrl.'/seller'?>">
                        <i class="menu-icon glyphicon glyphicon-stats"></i>
                        <span class="menu-text"> Seller </span>
                    </a>
                </li>

                <?php $data = array('atributeMaster','categoryProduct','product','import') ?>

                <li class="<?php echo in_array($this->id, $data) ? 'open' : '' ?>">
                    <a href="#" class="menu-dropdown">
                        <i class="menu-icon fa fa-tags"></i>
                        <span class="menu-text"> Product </span>
                        <i class="menu-expand"></i>
                    </a>

                    <ul class="submenu">
                        <li class="<?php echo $this->id == 'atributeMaster' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/atributeMaster'?>">
                                <span class="menu-text"> Custom Attributes</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'categoryProduct' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/categoryProduct'?>">
                                <span class="menu-text"> Product Category</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'product' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/product'?>">
                                <span class="menu-text"> Catalogue</span>
                            </a>
                        </li>
                        <li class="<?php echo $this->id == 'import' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/product/import'?>">
                                <span class="menu-text"> Import</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="<?php echo $this->id == 'customer' ? 'active' : '' ?>">
                    <a href="<?php echo Yii::app()->baseUrl.'/customer'?>">
                        <i class="menu-icon glyphicon glyphicon-user"></i>
                        <span class="menu-text"> Customer </span>
                    </a>
                </li>

                <li class="<?php echo $this->id == 'order' ? 'active' : '' ?>">
                    <a href="<?php echo Yii::app()->baseUrl.'/order'?>">
                        <i class="menu-icon glyphicon glyphicon-folder-close"></i>
                        <span class="menu-text"> Order </span>
                    </a>
                </li>

                <?php $data = array('counter') ?>

                <li class="<?php echo in_array($this->id, $data) ? 'open' : '' ?>">
                    <a href="#" class="menu-dropdown">
                        <i class="menu-icon fa fa-gear"></i>
                        <span class="menu-text"> Setting </span>
                        <i class="menu-expand"></i>
                    </a>

                    <ul class="submenu">
                        <li class="<?php echo $this->id == 'counter' ? 'active' : '' ?>">
                            <a href="<?php echo Yii::app()->baseUrl.'/counter'?>">
                                <span class="menu-text"> Counter</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- /Sidebar Menu -->
        </div>
        <!-- /Page Sidebar -->
        <!-- Page Content -->
        <div class="page-content">
            <!-- Page Breadcrumb -->
            <div class="page-breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="<?php echo Yii::app()->baseUrl;?>">Home</a>
                    </li>
                    <li class="active">
                        <a href="<?php echo Yii::app()->baseUrl.'/'.$this->id;?>"><?php echo ucfirst($this->id); ?></a>
                    </li>
                </ul>
            </div>
            <!-- /Page Breadcrumb -->
            <!-- Page Header -->
            <div class="page-header position-relative">
                <!--Header Buttons-->
                <div class="header-buttons">
                    <a class="sidebar-toggler" href="#">
                        <i class="fa fa-arrows-h"></i>
                    </a>
                    <a class="refresh" id="refresh-toggler" href="">
                        <i class="glyphicon glyphicon-refresh"></i>
                    </a>
                    <a class="fullscreen" id="fullscreen-toggler" href="#">
                        <i class="glyphicon glyphicon-fullscreen"></i>
                    </a>
                </div>
                <!--Header Buttons End-->
            </div>
            <!-- /Page Header -->
            <!-- Page Body -->
            <div class="page-body">
                <?php echo $content?>
                <!-- Your Content Goes Here -->
            </div>
            <!-- /Page Body -->
        </div>
        <!-- /Page Content -->
    </div>
    <!-- /Page Container -->
    <!-- Main Container -->
</div>

<?php
Yii::app()->clientScript->registerScript('delete',

    "
    $('.btn-delete').click(function(){
      if(!confirm('Are you sure delete this ?')){
        return false;
      }
    });
    ",

    CClientScript::POS_END);
?>

<!--Basic Scripts-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/beyond.min.js"></script>

<!--Sparkline Charts Needed Scripts-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/sparkline/jquery.sparkline.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/sparkline/sparkline-init.js"></script>

<!--Easy Pie Charts Needed Scripts-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/easypiechart/jquery.easypiechart.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/easypiechart/easypiechart-init.js"></script>

<!--Flot Charts Needed Scripts-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/flot/jquery.flot.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/flot/jquery.flot.resize.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/flot/jquery.flot.pie.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/flot/jquery.flot.tooltip.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/charts/flot/jquery.flot.orderBars.js"></script>

<!--Bootstrap Date Picker-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/datetime/bootstrap-datepicker.js"></script>

<!--Jquery Select2-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/select2/select2.js"></script>

<!-- Wizard -->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/fuelux/wizard/wizard-custom.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/toastr/toastr.js"></script>

<!--Datatables-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/datatable/ZeroClipboard.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/datatable/dataTables.tableTools.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/datatable/dataTables.bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/datatable/datatables-init.js"></script>

<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/slick-master/slick/slick.min.js"></script>
<script>
    InitiateSimpleDataTable.init();
    InitiateEditableDataTable.init();
    InitiateExpandableDataTable.init();
    InitiateSearchableDataTable.init();
</script>

<script type="text/javascript">
jQuery(function ($) {
    $('#simplewizardinwidget').wizard();
    $('#simplewizard').wizard();
    $('#tabbedwizard').wizard().on('finished', function (e) {
        Notify('Thank You! All of your information saved successfully.', 'bottom-right', '5000', 'blue', 'fa-check', true);
    });
    $('#WiredWizard').wizard();
});

$(".select2").select2();
$(".select1").select2({
    placeholder: "Multiple Select",
    allowClear: true
});

//--Bootstrap Date Picker--
$('.date-picker').datepicker();

</script>

</body>
</html>
