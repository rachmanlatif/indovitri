<?php

/**
 * This is the model class for table "Port".
 *
 * The followings are the available columns in table 'Port':
 * @property string $PortID
 * @property string $PortName
 * @property string $ShortName
 * @property string $Note
 * @property string $CityCode
 * @property integer $Status
 */
class Port extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Port';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('PortID', 'required'),
			array('PortID, PortName, ShortName, Note, CityCode', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('PortID, PortName, ShortName, Note, CityCode, Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'city' => array(self::BELONGS_TO, 'City', 'CityCode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'PortID' => 'Port',
			'PortName' => 'Port Name',
			'ShortName' => 'Short Name',
			'Note' => 'Note',
			'CityCode' => 'City Code',
			'Status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('PortID',$this->PortID,true);
		$criteria->compare('PortName',$this->PortName,true);
		$criteria->compare('ShortName',$this->ShortName,true);
		$criteria->compare('Note',$this->Note,true);
		$criteria->compare('CityCode',$this->CityCode,true);
		$criteria->compare('Status',$this->Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Port the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
