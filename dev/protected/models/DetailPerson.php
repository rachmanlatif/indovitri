<?php

/**
 * This is the model class for table "DetailPerson".
 *
 * The followings are the available columns in table 'DetailPerson':
 * @property string $EntityCode
 * @property integer $Status
 * @property string $PositionCode
 * @property string $Name
 * @property string $Phone
 * @property string $Address
 * @property string $Email
 * @property string $Note
 * @property integer $IDDetailPerson
 */
class DetailPerson extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'DetailPerson';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Status', 'numerical', 'integerOnly'=>true),
			array('EntityCode, PositionCode, Name, Phone, Address, Email, Note', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('EntityCode, Status, PositionCode, Name, Phone, Address, Email, Note, IDDetailPerson', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'position' => array(self::BELONGS_TO, 'Position', 'PositionCode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'EntityCode' => 'Entity Code',
			'Status' => 'Status',
			'PositionCode' => 'Position Name',
			'Name' => 'Name',
			'Phone' => 'Phone',
			'Address' => 'Address',
			'Email' => 'Email',
			'Note' => 'Note',
			'IDDetailPerson' => 'Id Detail Person',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('EntityCode',$this->EntityCode,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('PositionCode',$this->PositionCode,true);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Phone',$this->Phone,true);
		$criteria->compare('Address',$this->Address,true);
		$criteria->compare('Email',$this->Email,true);
		$criteria->compare('Note',$this->Note,true);
		$criteria->compare('IDDetailPerson',$this->IDDetailPerson);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DetailPerson the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
