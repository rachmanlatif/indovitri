<?php

/**
 * This is the model class for table "UOM".
 *
 * The followings are the available columns in table 'UOM':
 * @property string $UOMCode
 * @property string $UOMName
 * @property string $ShortName
 * @property string $Note
 * @property integer $Status
 */
class UOM extends MyActiveRecord
{
	public $QTYSmaller;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'UOM';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('UOMCode, UOMName', 'required'),
			array('Status, QTYSmaller', 'numerical', 'integerOnly'=>true),
			array('UOMCode, UOMName, ShortName, Note', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('UOMCode, UOMName, ShortName, Note, Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'define' => array(self::HAS_ONE, 'UOMDefine', 'UOMCode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'UOMCode' => 'UOM Code',
			'UOMName' => 'Name',
			'ShortName' => 'Short Name',
			'Note' => 'Note',
			'Status' => 'Status',
			'QTYSmaller' => 'UOM Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('UOMCode',$this->UOMCode,true);
		$criteria->compare('UOMName',$this->UOMName,true);
		$criteria->compare('Note',$this->Note,true);
		$criteria->compare('Status',$this->Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UOM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
