<?php

/**
 * This is the model class for table "ProductStockHistory".
 *
 * The followings are the available columns in table 'ProductStockHistory':
 * @property string $Number
 * @property string $ProductID
 * @property string $Date
 * @property double $BeginQTY
 * @property double $QTYIN
 * @property double $QTYOUT
 * @property double $ENDQTY
 * @property string $Note
 */
class ProductStockHistory extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ProductStockHistory';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('BeginQTY, QTYIN, QTYOUT, ENDQTY', 'numerical'),
			array('ProductID, Note', 'length', 'max'=>255),
			array('Date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Number, ProductID, Date, BeginQTY, QTYIN, QTYOUT, ENDQTY, Note', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Number' => 'Number',
			'ProductID' => 'Product',
			'Date' => 'Date',
			'BeginQTY' => 'Begin Qty',
			'QTYIN' => 'Qtyin',
			'QTYOUT' => 'Qtyout',
			'ENDQTY' => 'Endqty',
			'Note' => 'Note',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Number',$this->Number,true);
		$criteria->compare('ProductID',$this->ProductID,true);
		$criteria->compare('Date',$this->Date,true);
		$criteria->compare('BeginQTY',$this->BeginQTY);
		$criteria->compare('QTYIN',$this->QTYIN);
		$criteria->compare('QTYOUT',$this->QTYOUT);
		$criteria->compare('ENDQTY',$this->ENDQTY);
		$criteria->compare('Note',$this->Note,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductStockHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
