<?php

/**
 * This is the model class for table "OrderBL".
 *
 * The followings are the available columns in table 'OrderBL':
 * @property string $OrderID
 * @property string $BLNumber
 * @property integer $DocStatus
 * @property string $CourierTrackNo
 * @property string $LoadingDate
 * @property string $EstimatedTimeDelivery
 * @property string $EstimatedTimeArive
 * @property string $BLReleasePayment
 * @property string $CBDocs
 * @property string $ContainerCode
 * @property integer $IDOrderBL
 */
class OrderBL extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'OrderBL';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('BLNumber', 'isBlNumberExist', 'on'=>'insert, update'),
			array('CourierTrackNo', 'isCourierNumberExist', 'on'=>'insert, update'),
			array('DocStatus', 'numerical', 'integerOnly'=>true),
			array('OrderID, BLNumber, CourierTrackNo, ContainerCode', 'length', 'max'=>255),
			array('CBDocs', 'length', 'max'=>19),
			array('LoadingDate, EstimatedTimeDelivery, EstimatedTimeArive, BLReleasePayment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('OrderID, BLNumber, DocStatus, CourierTrackNo, LoadingDate, EstimatedTimeDelivery, EstimatedTimeArive, BLReleasePayment, CBDocs, ContainerCode, IDOrderBL', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'container' => array(self::BELONGS_TO, 'Container', 'ContainerCode'),
		);
	}

	public function isBLNumberExist() {
			$seal = self::model()->find('BLNumber=?', array($this->BLNumber));
			if ($seal != null) {
					$this->addError('BLNumber', 'BL number already exists.');
			}
	}

	public function isCourierNumberExist() {
			$seal = self::model()->find('CourierTrackNo=?', array($this->CourierTrackNo));
			if ($seal != null) {
					$this->addError('BLNumber', 'Courier track number already exists.');
			}
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'OrderID' => 'Order',
			'BLNumber' => 'Bl Number',
			'DocStatus' => 'Doc Status',
			'CourierTrackNo' => 'Courier Track No',
			'LoadingDate' => 'Loading Date',
			'EstimatedTimeDelivery' => 'Estimated Time Delivery',
			'EstimatedTimeArive' => 'Estimated Time Arive',
			'BLReleasePayment' => 'Bl Release Payment',
			'CBDocs' => 'Cbdocs',
			'ContainerCode' => 'Container Name',
			'IDOrderBL' => 'Id Order BL',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('OrderID',$this->OrderID,true);
		$criteria->compare('BLNumber',$this->BLNumber,true);
		$criteria->compare('DocStatus',$this->DocStatus);
		$criteria->compare('CourierTrackNo',$this->CourierTrackNo,true);
		$criteria->compare('LoadingDate',$this->LoadingDate,true);
		$criteria->compare('EstimatedTimeDelivery',$this->EstimatedTimeDelivery,true);
		$criteria->compare('EstimatedTimeArive',$this->EstimatedTimeArive,true);
		$criteria->compare('BLReleasePayment',$this->BLReleasePayment,true);
		$criteria->compare('CBDocs',$this->CBDocs,true);
		$criteria->compare('ContainerCode',$this->ContainerCode,true);
		$criteria->compare('IDOrderBL',$this->IDOrderBL);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderBL the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave(){
        $this->LoadingDate = date('Y-m-d H:i:s', strtotime($this->LoadingDate));
        $this->EstimatedTimeArive = date('Y-m-d H:i:s', strtotime($this->EstimatedTimeArive));
        $this->EstimatedTimeDelivery = date('Y-m-d H:i:s', strtotime($this->EstimatedTimeDelivery));
        $this->BLReleasePayment = date('Y-m-d H:i:s', strtotime($this->BLReleasePayment));

        return parent::beforeSave();
    }
}
