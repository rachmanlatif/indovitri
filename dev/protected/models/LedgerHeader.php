<?php

/**
 * This is the model class for table "LedgerHeader".
 *
 * The followings are the available columns in table 'LedgerHeader':
 * @property string $LedgerCode
 * @property string $DateLedger
 * @property string $PersonCode
 * @property string $BussinesCode
 * @property string $SumDebit
 * @property string $SumCredit
 * @property integer $Status
 */
class LedgerHeader extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'LedgerHeader';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Status', 'numerical', 'integerOnly'=>true),
			array('LedgerCode, PersonCode, BussinesCode', 'length', 'max'=>255),
			array('SumDebit, SumCredit', 'length', 'max'=>19),
			array('DateLedger', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('LedgerCode, DateLedger, PersonCode, BussinesCode, SumDebit, SumCredit, Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'LedgerCode' => 'Ledger Code',
			'DateLedger' => 'Date Ledger',
			'PersonCode' => 'Person Code',
			'BussinesCode' => 'Bussines Code',
			'SumDebit' => 'Sum Debit',
			'SumCredit' => 'Sum Credit',
			'Status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LedgerCode',$this->LedgerCode,true);
		$criteria->compare('DateLedger',$this->DateLedger,true);
		$criteria->compare('PersonCode',$this->PersonCode,true);
		$criteria->compare('BussinesCode',$this->BussinesCode,true);
		$criteria->compare('SumDebit',$this->SumDebit,true);
		$criteria->compare('SumCredit',$this->SumCredit,true);
		$criteria->compare('Status',$this->Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LedgerHeader the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
