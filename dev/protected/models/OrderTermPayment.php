<?php

/**
 * This is the model class for table "OrderTermPayment".
 *
 * The followings are the available columns in table 'OrderTermPayment':
 * @property string $OrderID
 * @property integer $NumberTermPayment
 * @property double $PercentageofPayment
 * @property string $ValuePercentageOfPayment
 * @property string $IssueDate
 * @property string $ExpiredDate
 * @property integer $Status
 * @property integer $IDOrderTermPayment
 */
class OrderTermPayment extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'OrderTermPayment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('NumberTermPayment, Status', 'numerical', 'integerOnly'=>true),
			array('PercentageofPayment', 'numerical'),
			array('OrderID', 'length', 'max'=>255),
			array('ValuePercentageOfPayment', 'length', 'max'=>19),
			array('IssueDate, ExpiredDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('OrderID, NumberTermPayment, PercentageofPayment, ValuePercentageOfPayment, IssueDate, ExpiredDate, Status, IDOrderTermPayment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'OrderID' => 'Order',
			'NumberTermPayment' => 'Number Term Payment',
			'PercentageofPayment' => 'Percentageof Payment',
			'ValuePercentageOfPayment' => 'Value Percentage Of Payment',
			'IssueDate' => 'Issue Date',
			'ExpiredDate' => 'Expired Date',
			'Status' => 'Status',
			'IDOrderTermPayment' => 'Idorder Term Payment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('OrderID',$this->OrderID,true);
		$criteria->compare('NumberTermPayment',$this->NumberTermPayment);
		$criteria->compare('PercentageofPayment',$this->PercentageofPayment);
		$criteria->compare('ValuePercentageOfPayment',$this->ValuePercentageOfPayment,true);
		$criteria->compare('IssueDate',$this->IssueDate,true);
		$criteria->compare('ExpiredDate',$this->ExpiredDate,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('IDOrderTermPayment',$this->IDOrderTermPayment);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderTermPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
