<?php

class UploadForm extends CFormModel
{
	public $filename;
	public $column;
	public $row;
	public $label;
	public $kodeProductCategory;
	public $sellerID;
	public $status;
	public $stockCode;
	public $isFromExcel;
	public $isStock;
	public $isProduction;
	public $isCatalogue;

	public function rules()
	{
		return array(
			array('filename', 'required'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'filename'=>'File Name',
			'column'=>'Column',
			'row'=>'Row',
			'label'=>'Label',
			'kodeProductCategory'=>'Category Product',
			'sellerID'=>'Seller ID',
			'status'=>'Status',
			'stockCode'=>'Stock Code',
			'isStock'=>'is Stock',
			'isProduction'=>'is Production',
			'isCatalogue'=>'is Catalogue',
			'isFromExcel'=>'Category Product From Excel ?',
			'isSellerFromExcel'=>'Seller From Excel ?',
		);
	}

    public static function getFilePath() {
        return Yii::getPathOfAlias('site.uploads.data_upload.'.Yii::app()->user->id) . DIRECTORY_SEPARATOR;
    }

    public static function getFileUrl() {
        return Yii::app()->baseUrl . '/uploads/data_upload/'.Yii::app()->user->id.'/';
    }
}
