<?php

/**
 * This is the model class for table "TicketSuportHeader".
 *
 * The followings are the available columns in table 'TicketSuportHeader':
 * @property string $TicketCode
 * @property string $TicketDate
 * @property string $TicketCloseDate
 * @property string $PersonCode
 * @property string $Subject
 * @property string $Message
 * @property string $FileName
 * @property string $TicketStatusID
 * @property integer $IDTicketSupportHeader
 */
class TicketSuportHeader extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'TicketSuportHeader';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TicketCode, PersonCode, Subject, Message, FileName, TicketStatusID', 'length', 'max'=>255),
			array('TicketDate, TicketCloseDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TicketCode, TicketDate, TicketCloseDate, PersonCode, Subject, Message, FileName, TicketStatusID, IDTicketSupportHeader', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TicketCode' => 'Ticket Code',
			'TicketDate' => 'Ticket Date',
			'TicketCloseDate' => 'Ticket Close Date',
			'PersonCode' => 'Person Code',
			'Subject' => 'Subject',
			'Message' => 'Message',
			'FileName' => 'File Name',
			'TicketStatusID' => 'Ticket Status',
			'IDTicketSupportHeader' => 'Idticket Support Header',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TicketCode',$this->TicketCode,true);
		$criteria->compare('TicketDate',$this->TicketDate,true);
		$criteria->compare('TicketCloseDate',$this->TicketCloseDate,true);
		$criteria->compare('PersonCode',$this->PersonCode,true);
		$criteria->compare('Subject',$this->Subject,true);
		$criteria->compare('Message',$this->Message,true);
		$criteria->compare('FileName',$this->FileName,true);
		$criteria->compare('TicketStatusID',$this->TicketStatusID,true);
		$criteria->compare('IDTicketSupportHeader',$this->IDTicketSupportHeader);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TicketSuportHeader the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
