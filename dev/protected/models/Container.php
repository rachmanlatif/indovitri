<?php

/**
 * This is the model class for table "Container".
 *
 * The followings are the available columns in table 'Container':
 * @property string $ContainerCode
 * @property string $ContainerName
 * @property double $ContainerMaxVolume
 * @property double $UsableVolume
 * @property string $Note
 * @property integer $Status
 * @property integer $NettWeight
 * @property integer $GrossWeight
 */
class Container extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Container';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ContainerCode, ContainerName, ContainerMaxVolume, UsableVolume', 'required'),
			array('Status', 'numerical', 'integerOnly'=>true),
			array('ContainerMaxVolume, UsableVolume, NettWeight, GrossWeight', 'numerical'),
			array('ContainerCode, ContainerName, Note', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ContainerCode, ContainerName, ContainerMaxVolume, UsableVolume, Note, Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ContainerCode' => 'Container Code',
			'ContainerName' => 'Container Name',
			'ContainerMaxVolume' => 'Container Max Volume',
			'UsableVolume' => 'Usable Volume',
			'Note' => 'Note',
			'Status' => 'Status',
			'NettWeight' => 'Payload',
			'GrossWeight' => 'Gross Weight',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ContainerCode',$this->ContainerCode,true);
		$criteria->compare('ContainerName',$this->ContainerName,true);
		$criteria->compare('ContainerMaxVolume',$this->ContainerMaxVolume);
		$criteria->compare('UsableVolume',$this->UsableVolume);
		$criteria->compare('Note',$this->Note,true);
		$criteria->compare('Status',$this->Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Container the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
