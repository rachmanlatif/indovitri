<?php

/**
 * This is the model class for table "LedgerDetail".
 *
 * The followings are the available columns in table 'LedgerDetail':
 * @property string $LedgerCode
 * @property string $AccountID
 * @property string $Note
 * @property string $Debit
 * @property string $Credit
 */
class LedgerDetail extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'LedgerDetail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('LedgerCode, AccountID, Note', 'length', 'max'=>255),
			array('Debit, Credit', 'length', 'max'=>19),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('LedgerCode, AccountID, Note, Debit, Credit', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'LedgerCode' => 'Ledger Code',
			'AccountID' => 'Account',
			'Note' => 'Note',
			'Debit' => 'Debit',
			'Credit' => 'Credit',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LedgerCode',$this->LedgerCode,true);
		$criteria->compare('AccountID',$this->AccountID,true);
		$criteria->compare('Note',$this->Note,true);
		$criteria->compare('Debit',$this->Debit,true);
		$criteria->compare('Credit',$this->Credit,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LedgerDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
