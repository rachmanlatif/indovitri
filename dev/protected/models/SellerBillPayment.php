<?php

/**
 * This is the model class for table "SellerBillPayment".
 *
 * The followings are the available columns in table 'SellerBillPayment':
 * @property string $OrderID
 * @property string $PaymentCode
 * @property string $DatePayment
 * @property string $SellerComercial
 * @property string $Ammount
 * @property string $BankCode
 * @property string $Note
 * @property integer $Status
 * @property integer $IDSellerBillPayment
 */
class SellerBillPayment extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'SellerBillPayment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Status', 'numerical', 'integerOnly'=>true),
			array('OrderID, PaymentCode, SellerComercial, BankCode, Note', 'length', 'max'=>255),
			array('Ammount', 'length', 'max'=>19),
			array('DatePayment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('OrderID, PaymentCode, DatePayment, SellerComercial, Ammount, BankCode, Note, Status, IDSellerBillPayment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bank' => array(self::BELONGS_TO, 'BankList', 'BankCode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'OrderID' => 'Order',
			'PaymentCode' => 'Payment Code',
			'DatePayment' => 'Date Payment',
			'SellerComercial' => 'Seller Comercial',
			'Ammount' => 'Ammount',
			'BankCode' => 'Bank Name',
			'Note' => 'Note',
			'Status' => 'Status',
			'IDSellerBillPayment' => 'Idseller Bill Payment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('OrderID',$this->OrderID,true);
		$criteria->compare('PaymentCode',$this->PaymentCode,true);
		$criteria->compare('DatePayment',$this->DatePayment,true);
		$criteria->compare('SellerComercial',$this->SellerComercial,true);
		$criteria->compare('Ammount',$this->Ammount,true);
		$criteria->compare('BankCode',$this->BankCode,true);
		$criteria->compare('Note',$this->Note,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('IDSellerBillPayment',$this->IDSellerBillPayment);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SellerBillPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave(){
        $this->DatePayment = date('Y-m-d H:i:s');

        return parent::beforeSave();
    }
}
