<?php

/**
 * This is the model class for table "ShipperDetail".
 *
 * The followings are the available columns in table 'ShipperDetail':
 * @property string $ShipperCode
 * @property string $CountryCode
 * @property string $RegistrationNumberName
 * @property string $TaxCodeName
 * @property string $RegistrationNumberValue
 * @property string $TaxCodeValue
 * @property integer $IDShipperDetail
 */
class ShipperDetail extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ShipperDetail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ShipperCode, CountryCode', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ShipperCode, CountryCode, RegistrationNumberName, TaxCodeName, RegistrationNumberValue, TaxCodeValue, IDShipperDetail', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ShipperCode' => 'Shipper Code',
			'CountryCode' => 'Country Code',
			'RegistrationNumberName' => 'Registration Number Name',
			'TaxCodeName' => 'Tax Code Name',
			'RegistrationNumberValue' => 'Registration Number Value',
			'TaxCodeValue' => 'Tax Code Value',
			'IDShipperDetail' => 'Idshipper Detail',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ShipperCode',$this->ShipperCode,true);
		$criteria->compare('CountryCode',$this->CountryCode,true);
		$criteria->compare('RegistrationNumberName',$this->RegistrationNumberName,true);
		$criteria->compare('TaxCodeName',$this->TaxCodeName,true);
		$criteria->compare('RegistrationNumberValue',$this->RegistrationNumberValue,true);
		$criteria->compare('TaxCodeValue',$this->TaxCodeValue,true);
		$criteria->compare('IDShipperDetail',$this->IDShipperDetail);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ShipperDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
