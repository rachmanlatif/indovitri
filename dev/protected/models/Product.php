<?php

/**
 * This is the model class for table "Product".
 *
 * The followings are the available columns in table 'Product':
 * @property string $ProductID
 * @property string $Barcode
 * @property string $ProductName
 * @property string $ThubnailsImage
 * @property string $Price
 * @property double $StokQTY
 * @property double $InnerCarton
 * @property double $NettWeight
 * @property double $GrossWeight
 * @property double $VolumeGross
 * @property string $SellerCode
 * @property double $CartonLenght
 * @property double $CartonHeight
 * @property double $CartonDepth
 * @property string $EANCode
 * @property string $CategoryCode
 * @property string $SellerSKU
 * @property string $UOMCode
 * @property integer $Status
 * @property integer $MinQTY
 * @property string $ProductCode
 * @property integer $isStock
 * @property integer $isProduction
 * @property integer $isCatalogue
 * @property string $Note
 */
class Product extends MyActiveRecord
{
    public $ImagesProduct;
    public $StartProduction;
    public $EndProduction;
    public $DeadlineProduction;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ProductID, ProductCode, SellerCode, UOMCode', 'required'),
			array('Status', 'numerical', 'integerOnly'=>true),
      array('ProductID', 'isProductCodeExist', 'on'=>'insert'),
			array('StokQTY, Price, InnerCarton, NettWeight, GrossWeight, VolumeGross, CartonLenght, CartonHeight, CartonDepth, MinQTY, isStock, isProduction, isCatalogue', 'numerical'),
			array('ProductID, Barcode, ProductName, ThubnailsImage, ProductCode, SellerCode, EANCode, CategoryCode, SellerSKU, UOMCode, Note', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ProductID, Barcode, ProductName, ProductCode, ThubnailsImage, Price, StokQTY, InnerCarton, NettWeight, GrossWeight, VolumeGross, SellerCode, CartonLenght, CartonHeight, CartonDepth, EANCode, Note, CategoryCode, SellerSKU, UOMCode, Status, isStock, isProduction, isCatalogue', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'category' => array(self::BELONGS_TO, 'CategoryProduct', 'CategoryCode'),
            'seller' => array(self::BELONGS_TO, 'Seller', 'SellerCode'),
            'prices' => array(self::HAS_MANY, 'ProductPriceHistory', 'ProductID'),
            'atributes' => array(self::HAS_MANY, 'ProductAtribute', 'ProductID'),
            'images' => array(self::HAS_MANY, 'ProductImage', 'ProductID'),
            'stocks' => array(self::HAS_MANY, 'ProductStockHistory', 'ProductID'),
            'info' => array(self::BELONGS_TO, 'ProductListInfo', 'ProductID'),
      			'uom' => array(self::BELONGS_TO, 'UOM', 'UOMCode'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ProductID' => 'Product',
			'Barcode' => 'Barcode',
			'ProductName' => 'Product Name',
			'ThubnailsImage' => 'Thumbnails Image',
			'Price' => 'Price',
			'StokQTY' => 'Stock Qty',
			'InnerCarton' => 'Inner Carton',
			'NettWeight' => 'Nett Weight',
			'GrossWeight' => 'Gross Weight',
			'VolumeGross' => 'Volume Gross',
			'SellerCode' => 'Seller Code',
			'CartonLenght' => 'Carton Lenght',
			'CartonHeight' => 'Carton Height',
			'CartonDepth' => 'Carton Depth',
			'EANCode' => 'EAN Code',
			'CategoryCode' => 'Category Name',
			'SellerSKU' => 'Seller SKU',
			'UOMCode' => 'UOM Code',
			'Status' => 'Status',
			'ProductCode' => 'Product Code',
      'ImagesProduct' => 'Images Product',
      'MinQTY' => 'MoQ',
      'StartProduction' => 'Start Production',
      'EndProduction' => 'End Production',
      'DeadlineProduction' => 'Deadline Production',
      'isStock' => 'is Stock',
      'isProduction' => 'is Production',
      'isCatalogue' => 'is Catalogue',
      'Note' => 'Note',
		);
	}

  /**
   * Check if a username is exists
   */
  public function isProductCodeExist() {
      if($this->ProductCode != null || $this->ProductCode != ''){
          $data = self::model()->findByAttributes(array('ProductCode' => $this->ProductCode));
          if ($data != null) {
              $data->Barcode = $this->Barcode;
              $data->ProductName = $this->ProductName;
              $data->ThubnailsImage = $this->ThubnailsImage;
              $data->Price = $this->Price;
              $data->StokQTY = $this->StokQTY;
              $data->InnerCarton = $this->InnerCarton;
              $data->NettWeight = $this->NettWeight;
              $data->GrossWeight = $this->GrossWeight;
              $data->VolumeGross = $this->VolumeGross;
              $data->SellerCode = $this->SellerCode;
              $data->CartonLenght = $this->CartonLenght;
              $data->CartonHeight = $this->CartonHeight;
              $data->CartonDepth = $this->CartonDepth;
              $data->EANCode = $this->EANCode;
              $data->CategoryCode = $this->CategoryCode;
              $data->SellerSKU = $this->SellerSKU;
              $data->UOMCode = $this->UOMCode;
              $data->Status = $this->Status;
              $data->ImagesProduct = $this->ImagesProduct;
              $data->MinQTY = $this->MinQTY;
              $data->StartProduction = $this->StartProduction;
              $data->EndProduction = $this->EndProduction;
              $data->ProductCode = $this->ProductCode;
              $data->DeadlineProduction = $this->DeadlineProduction;
              $data->save();

              $this->addError('ProductCode', 'Product code '.$this->ProductID.' is already exists. Data updated.');
          }
      }
  }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ProductCode',$this->ProductCode,true);
		$criteria->compare('ProductName',$this->ProductName,true);
		$criteria->compare('Price',$this->Price);
		$criteria->compare('StokQTY',$this->StokQTY);
		$criteria->compare('InnerCarton',$this->InnerCarton);
		$criteria->compare('NettWeight',$this->NettWeight);
		$criteria->compare('GrossWeight',$this->GrossWeight);
		$criteria->compare('VolumeGross',$this->VolumeGross);
		$criteria->compare('SellerCode',$this->SellerCode,true);
		$criteria->compare('CartonLenght',$this->CartonLenght);
		$criteria->compare('CartonHeight',$this->CartonHeight);
		$criteria->compare('CartonDepth',$this->CartonDepth);
		$criteria->compare('EANCode',$this->EANCode,true);
		$criteria->compare('CategoryCode',$this->CategoryCode,true);
		$criteria->compare('SellerSKU',$this->SellerSKU,true);
		$criteria->compare('UOMCode',$this->UOMCode,true);
		$criteria->compare('Status',$this->Status);
    $criteria->compare('isStock',$this->isStock);
    $criteria->compare('isProduction',$this->isProduction);
    $criteria->compare('isCatalogue',$this->isCatalogue);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getFilePath() {
        return Yii::getPathOfAlias('site.uploads.product.') . DIRECTORY_SEPARATOR;
    }

    public static function getFileUrl() {
        return Yii::app()->baseUrl . '/uploads/product/';
    }

    public function beforeSave(){
        $this->StartProduction = date('Y-m-d H:i:s', strtotime($this->StartProduction));
        $this->EndProduction = date('Y-m-d H:i:s', strtotime($this->EndProduction));
        $this->DeadlineProduction = date('Y-m-d H:i:s', strtotime($this->DeadlineProduction));

        return parent::beforeSave();
    }
}
