<?php

/**
 * This is the model class for table "ProductAtribute".
 *
 * The followings are the available columns in table 'ProductAtribute':
 * @property string $ProductID
 * @property string $AtributeCode
 * @property string $AtributeValue
 * @property string $Note
 * @property string $IDProductAtribute
 */
class ProductAtribute extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ProductAtribute';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ProductID, AtributeCode, AtributeValue', 'required'),
			array('ProductID, AtributeCode, AtributeValue, Note', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ProductID, AtributeCode, AtributeValue, Note, IDProductAtribute', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'atribute' => array(self::BELONGS_TO, 'AtributeMaster', 'AtributeCode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ProductID' => 'Product',
			'AtributeCode' => 'Atribute Code',
			'AtributeValue' => 'Atribute Value',
			'Note' => 'Note',
			'IDProductAtribute' => 'ID Product Atribute',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ProductID',$this->ProductID,true);
		$criteria->compare('AtributeCode',$this->AtributeCode,true);
		$criteria->compare('AtributeValue',$this->AtributeValue,true);
		$criteria->compare('Note',$this->Note,true);
		$criteria->compare('IDProductAtribute',$this->IDProductAtribute);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductAtribute the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
