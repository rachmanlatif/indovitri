<?php

/**
 * This is the model class for table "SellerBill".
 *
 * The followings are the available columns in table 'SellerBill':
 * @property string $BillCode
 * @property string $Description
 * @property string $AccountID
 * @property string $DateBill
 * @property string $Ammount
 */
class SellerBill extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'SellerBill';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('BillCode', 'required'),
			array('BillCode, Description, AccountID', 'length', 'max'=>255),
			array('Ammount', 'length', 'max'=>19),
			array('DateBill', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('BillCode, Description, AccountID, DateBill, Ammount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'BillCode' => 'Bill Code',
			'Description' => 'Description',
			'AccountID' => 'Account',
			'DateBill' => 'Date Bill',
			'Ammount' => 'Ammount',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('BillCode',$this->BillCode,true);
		$criteria->compare('Description',$this->Description,true);
		$criteria->compare('AccountID',$this->AccountID,true);
		$criteria->compare('DateBill',$this->DateBill,true);
		$criteria->compare('Ammount',$this->Ammount,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SellerBill the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
