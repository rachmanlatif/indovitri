<?php

/**
 * This is the model class for table "Users".
 *
 * The followings are the available columns in table 'Users':
 * @property string $Username
 * @property string $FirstName
 * @property string $MiddleName
 * @property string $LastName
 * @property string $PositionCode
 * @property string $Phone
 * @property string $email
 * @property string $Address
 * @property string $PostCode
 * @property string $CityCode
 * @property string $Password
 * @property integer $Status
 * @property string $LastLogin
 * @property integer $IDUser
 * @property string $SessionKey
 * @property string $SessionExpired
 */
class Users extends MyActiveRecord
{
    // status
    const STATUS_NOTACTIVE = 0; //not active
    const STATUS_ACTIVE = 1; //active
    const STATUS_BANNED = 9; //banned
    const STATUS_LOCKED = 2; //locked

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Status', 'numerical', 'integerOnly'=>true),
			array('Username, FirstName, MiddleName, LastName, PositionCode, Phone, email, Address, PostCode, CityCode, Password, SessionKey', 'length', 'max'=>255),
			array('LastLogin', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Username, FirstName, MiddleName, LastName, PositionCode, Phone, email, Address, PostCode, CityCode, Password, Status, LastLogin, IDUser, SessionKey, SessionExpired', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
      'position' => array(self::BELONGS_TO, 'Position', 'PositionCode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Username' => 'Username',
			'FirstName' => 'First Name',
			'MiddleName' => 'Middle Name',
			'LastName' => 'Last Name',
			'PositionCode' => 'Position Name',
			'Phone' => 'Phone',
			'email' => 'Email',
			'Address' => 'Address',
			'PostCode' => 'Post Code',
			'CityCode' => 'City Code',
			'Password' => 'Password',
			'Status' => 'Status',
			'LastLogin' => 'Last Login',
			'IDUser' => 'Iduser',
			'SessionKey' => 'Session Key',
			'SessionExpired' => 'Session Expired',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Username',$this->Username,true);
		$criteria->compare('FirstName',$this->FirstName,true);
		$criteria->compare('MiddleName',$this->MiddleName,true);
		$criteria->compare('LastName',$this->LastName,true);
		$criteria->compare('PositionCode',$this->PositionCode,true);
		$criteria->compare('Phone',$this->Phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('Address',$this->Address,true);
		$criteria->compare('PostCode',$this->PostCode,true);
		$criteria->compare('CityCode',$this->CityCode,true);
		$criteria->compare('Password',$this->Password,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('LastLogin',$this->LastLogin,true);
		$criteria->compare('IDUser',$this->IDUser);
		$criteria->compare('SessionKey',$this->SessionKey,true);
		$criteria->compare('SessionExpired',$this->SessionExpired);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
