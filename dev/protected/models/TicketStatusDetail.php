<?php

/**
 * This is the model class for table "TicketStatusDetail".
 *
 * The followings are the available columns in table 'TicketStatusDetail':
 * @property string $TicketCode
 * @property string $AnswerCode
 * @property string $AnswerDate
 * @property string $PersonCode
 * @property string $Message
 * @property string $FileName
 * @property integer $IDTicketStatusDetail
 */
class TicketStatusDetail extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'TicketStatusDetail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TicketCode, AnswerCode, PersonCode, Message, FileName', 'length', 'max'=>255),
			array('AnswerDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TicketCode, AnswerCode, AnswerDate, PersonCode, Message, FileName, IDTicketStatusDetail', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TicketCode' => 'Ticket Code',
			'AnswerCode' => 'Answer Code',
			'AnswerDate' => 'Answer Date',
			'PersonCode' => 'Person Code',
			'Message' => 'Message',
			'FileName' => 'File Name',
			'IDTicketStatusDetail' => 'Idticket Status Detail',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TicketCode',$this->TicketCode,true);
		$criteria->compare('AnswerCode',$this->AnswerCode,true);
		$criteria->compare('AnswerDate',$this->AnswerDate,true);
		$criteria->compare('PersonCode',$this->PersonCode,true);
		$criteria->compare('Message',$this->Message,true);
		$criteria->compare('FileName',$this->FileName,true);
		$criteria->compare('IDTicketStatusDetail',$this->IDTicketStatusDetail);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TicketStatusDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
