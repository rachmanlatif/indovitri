<?php

/**
 * This is the model class for table "OrderDetailTemp".
 *
 * The followings are the available columns in table 'OrderDetailTemp':
 * @property string $OrderID
 * @property string $ProductID
 * @property double $QTY
 * @property double $TotalVolume
 * @property double $TotalGrossWeight
 * @property double $TotalNettWeight
 * @property double $QtyUnit
 * @property integer $MasterUnit
 * @property string $TotalOrder
 * @property string $Note
 * @property string $SupplierFee
 * @property integer $StatusOrder
 * @property integer $Status
 * @property string $ContainerID
 * @property integer $IDOrderDetailTemp
 */
class OrderDetailTemp extends MyActiveRecord
{
    public $BLContainer;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'OrderDetailTemp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('StatusOrder, Status', 'numerical', 'integerOnly'=>true),
			array('QTY, TotalVolume, TotalGrossWeight, TotalNettWeight, QtyUnit, MasterUnit', 'numerical'),
			array('OrderID, ProductID, Note, ContainerID', 'length', 'max'=>255),
			array('TotalOrder, SupplierFee', 'length', 'max'=>19),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('OrderID, ProductID, QTY, TotalVolume, TotalGrossWeight, TotalNettWeight, QtyUnit, MasterUnit, TotalOrder, Note, SupplierFee, StatusOrder, Status, ContainerID, IDOrderDetailTemp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'product' => array(self::BELONGS_TO, 'Product', 'ProductID'),
            'container' => array(self::BELONGS_TO, 'Container', 'ContainerID'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'OrderID' => 'Order',
			'ProductID' => 'Product',
			'QTY' => 'Qty',
			'TotalVolume' => 'Total Volume',
			'TotalGrossWeight' => 'Total Gross Weight',
			'TotalNettWeight' => 'Total Nett Weight',
			'QtyUnit' => 'Total Unit',
			'MasterUnit' => 'Master Unit',
			'TotalOrder' => 'Total Order',
			'Note' => 'Note',
			'SupplierFee' => 'Supplier Fee',
			'StatusOrder' => 'Status Order',
			'Status' => 'Status',
			'ContainerID' => 'Container',
			'IDOrderDetailTemp' => 'Idorder Detail Temp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('OrderID',$this->OrderID,true);
		$criteria->compare('ProductID',$this->ProductID,true);
		$criteria->compare('QTY',$this->QTY);
		$criteria->compare('TotalVolume',$this->TotalVolume);
		$criteria->compare('TotalGrossWeight',$this->TotalGrossWeight);
		$criteria->compare('TotalNettWeight',$this->TotalNettWeight);
		$criteria->compare('QtyUnit',$this->QtyUnit);
		$criteria->compare('MasterUnit',$this->MasterUnit);
		$criteria->compare('TotalOrder',$this->TotalOrder,true);
		$criteria->compare('Note',$this->Note,true);
		$criteria->compare('SupplierFee',$this->SupplierFee,true);
		$criteria->compare('StatusOrder',$this->StatusOrder);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('ContainerID',$this->ContainerID,true);
		$criteria->compare('IDOrderDetailTemp',$this->IDOrderDetailTemp);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderDetailTemp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
