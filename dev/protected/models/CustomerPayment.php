<?php

/**
 * This is the model class for table "CustomerPayment".
 *
 * The followings are the available columns in table 'CustomerPayment':
 * @property string $CustomerCode
 * @property string $PaymentCode
 * @property string $DatePayment
 * @property string $CustomerComercial
 * @property string $Ammount
 * @property string $BankCode
 * @property string $Note
 * @property integer $Status
 * @property integer $IDCustomerPayment
 */
class CustomerPayment extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'CustomerPayment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Status', 'numerical', 'integerOnly'=>true),
			array('CustomerCode, PaymentCode, CustomerComercial, BankCode, Note', 'length', 'max'=>255),
			array('Ammount', 'length', 'max'=>19),
			array('DatePayment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('CustomerCode, PaymentCode, DatePayment, CustomerComercial, Ammount, BankCode, Note, Status, IDCustomerPayment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'CustomerCode' => 'Customer Code',
			'PaymentCode' => 'Payment Code',
			'DatePayment' => 'Date Payment',
			'CustomerComercial' => 'Customer Comercial',
			'Ammount' => 'Ammount',
			'BankCode' => 'Bank Code',
			'Note' => 'Note',
			'Status' => 'Status',
			'IDCustomerPayment' => 'Idcustomer Payment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CustomerCode',$this->CustomerCode,true);
		$criteria->compare('PaymentCode',$this->PaymentCode,true);
		$criteria->compare('DatePayment',$this->DatePayment,true);
		$criteria->compare('CustomerComercial',$this->CustomerComercial,true);
		$criteria->compare('Ammount',$this->Ammount,true);
		$criteria->compare('BankCode',$this->BankCode,true);
		$criteria->compare('Note',$this->Note,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('IDCustomerPayment',$this->IDCustomerPayment);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CustomerPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave(){
        $this->DatePayment = date('Y-m-d H:i:s');

        return parent::beforeSave();
    }
}
