<?php

/**
 * This is the model class for table "OrderBLContainer".
 *
 * The followings are the available columns in table 'OrderBLContainer':
 * @property string $OrderID
 * @property string $ContainerCode
 * @property string $NumberContainer
 * @property string $SealCode
 * @property integer $IDOrderBLContainer
 */
class OrderBLContainer extends MyActiveRecord
{
    public $RemainVolume;
    public $RemainWeight;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'OrderBLContainer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
      array('SealCode', 'isSealCodeExist', 'on'=>'update'),
			array('OrderID, ContainerCode, NumberContainer, SealCode', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('IDOrderBLContainer, OrderID, ContainerCode, NumberContainer, SealCode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'container' => array(self::BELONGS_TO, 'Container', 'ContainerCode'),
        );
	}

  public function isSealCodeExist() {
      $seal = self::model()->find('SealCode=?', array($this->SealCode));
      if ($seal != null) {
          $this->addError('SealCode', 'Seal Code already exists.');
      }
  }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'IDOrderBLContainer' => 'ID Order BL Container',
			'OrderID' => 'Order',
			'ContainerCode' => 'Container Code',
			'NumberContainer' => 'Number Container',
			'SealCode' => 'Seal Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('IDOrderBLContainer',$this->IDOrderBLContainer);
		$criteria->compare('OrderID',$this->OrderID,true);
		$criteria->compare('ContainerCode',$this->ContainerCode,true);
		$criteria->compare('NumberContainer',$this->NumberContainer,true);
		$criteria->compare('SealCode',$this->SealCode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderBLContainer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
