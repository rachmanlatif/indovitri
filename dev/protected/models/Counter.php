<?php

/**
 * This is the model class for table "counter".
 *
 * The followings are the available columns in table 'counter':
 * @property string $keyword
 * @property string $value
 */
class Counter extends MyActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Counter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'counter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('keyword, value', 'required'),
			array('keyword, value', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('keyword, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'keyword' => 'Keyword',
			'value' => 'Value',
		);
	}

    /**
     * @param string $keyword
     * @return string
     */
    public static function getFormat($keyword) {
        $model = Counter::model()->findByPk('format_' . $keyword);
        if ($model == null) {
            throw new CDbException('The format to generate ID is not found.');
        }

        return $model->value;
    }

    /**
     * @param string $keyword
     * @throws CDbException
     * @return int
     */
    public static function getCounter($keyword) {
        $counter = 0;
        $model = Counter::model()->findByPk('counter_' . $keyword);
        if ($model == null) {
            $model = new Counter();
            $model->keyword = 'counter_'. $keyword;
            $model->value = 0;
            if (!$model->save()) {
                throw new CDbException('Failed to save counter.');
            }
        } else {
            $counter = $model->value;
        }

        return (int)$counter;
    }

    /**
     * @param string $keyword
     * @throws CDbException
     * @return boolean
     */
    public static function restartCounter($keyword) {
        $result = true;
        $model = Counter::model()->findByPk('counter_' . $keyword);
        if ($model == null) {
            $result = false;
        } else {
            $model->value = 0;
            if (!$model->save()) {
                throw new CDbException('Failed to restart counter.');
            }
        }

        return $result;
    }
    
    /**
     * @param $keyword
     * @throws CDbException
     */
    public static function addCounter($keyword) {
        $model = Counter::model()->findByPk('counter_'. $keyword);
        if ($model == null) {
            $model = new Counter();
            $model->keyword = 'counter_'. $keyword;
            $model->value = 0;
        }

        $model->value += 1;
        if (!$model->save()) {
            throw new CDbException('Failed to save counter.');
        }
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('keyword',$this->keyword,true);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}