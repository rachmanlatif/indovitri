<?php

/**
 * This is the model class for table "OrderHeader".
 *
 * The followings are the available columns in table 'OrderHeader':
 * @property string $OrderCode
 * @property string $OrderDate
 * @property string $SellerCode
 * @property string $POLCode
 * @property string $PODCode
 * @property string $ConsigneeCode
 * @property string $NotifyPartyCode
 * @property string $OrderStatusCode
 * @property string $IncontermID
 * @property string $PORefCode
 * @property string $POStatusCode
 * @property string $PODate
 * @property string $BillCodeSupplier
 * @property string $BillSupplierDate
 * @property double $BuyerFee
 * @property string $BuyerFeeAmount
 * @property string $BillSupplierTermofPayment
 * @property string $SupplierFee
 * @property string $FreightFee
 * @property string $ComisionCodeSkema
 * @property string $CustomerCode
 * @property string $OrderTotal
 * @property string $AgentCodeOrigin
 * @property string $AgentCodeDestination
 * @property integer $TotalQty
 * @property integer $TotalUnit
 * @property integer $TotalGrossWeight
 * @property integer $TotalNettWeight
 */
class OrderHeader extends MyActiveRecord
{
    public $ProductID;
    public $QTY;
    public $Note;
    public $TotalVolume;
    public $TotalWeight;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'OrderHeader';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('OrderCode, SellerCode, ConsigneeCode, NotifyPartyCode, IncontermID, CustomerCode, POLCode, PODCode, AgentCodeDestination, AgentCodeOrigin', 'required'),
            array('BuyerFee, TotalQty, TotalUnit, TotalGrossWeight, TotalNettWeight', 'numerical'),
            array('OrderCode, SellerCode, POLCode, PODCode, ConsigneeCode, NotifyPartyCode, OrderStatusCode, IncontermID, PORefCode, POStatusCode, BillCodeSupplier, BillSupplierTermofPayment, ComisionCodeSkema, CustomerCode, AgentCodeOrigin, AgentCodeDestination', 'length', 'max'=>255),
            array('BuyerFeeAmount, SupplierFee, FreightFee, OrderTotal', 'length', 'max'=>19),
            array('OrderDate, PODate, BillSupplierDate', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('OrderCode, OrderDate, SellerCode, POLCode, PODCode, ConsigneeCode, NotifyPartyCode, OrderStatusCode, IncontermID, PORefCode, POStatusCode, PODate, BillCodeSupplier, BillSupplierDate, BuyerFee, BuyerFeeAmount, BillSupplierTermofPayment, SupplierFee, FreightFee, ComisionCodeSkema, CustomerCode, OrderTotal, AgentCodeOrigin, AgentCodeDestination, TotalQty, TotalUnit, TotalGrossWeight, TotalNettWeight', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'seller' => array(self::BELONGS_TO, 'Seller', 'SellerCode'),
            'consignee' => array(self::BELONGS_TO, 'Consignee', 'ConsigneeCode'),
            'incoterm' => array(self::BELONGS_TO, 'Incoterm', 'IncontermID'),
            'notifyParty' => array(self::BELONGS_TO, 'Consignee', 'NotifyPartyCode'),
            'customer' => array(self::BELONGS_TO, 'Customer', 'CustomerCode'),
            'agentOrigin' => array(self::BELONGS_TO, 'Agent', 'AgentCodeOrigin'),
            'agentDestination' => array(self::BELONGS_TO, 'Agent', 'AgentCodeDestination'),
            'pol' => array(self::BELONGS_TO, 'City', 'POLCode'),
            'pod' => array(self::BELONGS_TO, 'City', 'PODCode'),

            'sellerBills' => array(self::HAS_MANY, 'SellerBillPayment', 'OrderID'),
            'details' => array(self::HAS_MANY, 'OrderDetail', 'OrderID'),
            'detailsTemp' => array(self::HAS_MANY, 'OrderDetailTemp', 'OrderID'),
            'terms' => array(self::HAS_MANY, 'OrderTermPayment', 'OrderID'),
            'blContainer' => array(self::HAS_MANY, 'OrderBLContainer', 'OrderID'),
            'charges' => array(self::HAS_MANY, 'OrderChargeDetail', 'OrderID'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'OrderCode' => 'Order Code',
            'OrderDate' => 'Order Date',
            'SellerCode' => 'Seller',
            'POLCode' => 'Port Of Loading',
            'PODCode' => 'Port Of Destination',
            'ConsigneeCode' => 'Consignee',
            'NotifyPartyCode' => 'Notify Party',
            'OrderStatusCode' => 'Order Status',
            'IncontermID' => 'Inconterm',
            'PORefCode' => 'PO Ref Code',
            'POStatusCode' => 'PO Status',
            'PODate' => 'Po Date',
            'BillCodeSupplier' => 'Bill Supplier',
            'BillSupplierDate' => 'Bill Supplier Date',
            'BuyerFee' => 'Buyer Fee %',
            'BuyerFeeAmount' => 'Buyer Fee Amount',
            'BillSupplierTermofPayment' => 'Bill Supplier Term Of Payment',
            'SupplierFee' => 'Seller Fee %',
            'FreightFee' => 'Freight Fee',
            'ComisionCodeSkema' => 'Comision Code Scheme',
            'CustomerCode' => 'Customer',
            'OrderTotal' => 'Order Total',
            'AgentCodeOrigin' => 'Agent Origin',
            'AgentCodeDestination' => 'Agent Destination',
            'TotalQty' => 'Total Qty',
            'TotalUnit' => 'Total Unit',
            'TotalGrossWeight' => 'Total Gross Weight',
            'TotalNettWeight' => 'Total Nett Weight',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('OrderCode',$this->OrderCode,true);
        $criteria->compare('OrderDate',$this->OrderDate,true);
        $criteria->compare('SellerCode',$this->SellerCode,true);
        $criteria->compare('POLCode',$this->POLCode,true);
        $criteria->compare('PODCode',$this->PODCode,true);
        $criteria->compare('ConsigneeCode',$this->ConsigneeCode,true);
        $criteria->compare('NotifyPartyCode',$this->NotifyPartyCode,true);
        $criteria->compare('OrderStatusCode',$this->OrderStatusCode,true);
        $criteria->compare('IncontermID',$this->IncontermID,true);
        $criteria->compare('PORefCode',$this->PORefCode,true);
        $criteria->compare('POStatusCode',$this->POStatusCode,true);
        $criteria->compare('PODate',$this->PODate,true);
        $criteria->compare('BillCodeSupplier',$this->BillCodeSupplier,true);
        $criteria->compare('BillSupplierDate',$this->BillSupplierDate,true);
        $criteria->compare('BuyerFee',$this->BuyerFee);
        $criteria->compare('BuyerFeeAmount',$this->BuyerFeeAmount,true);
        $criteria->compare('BillSupplierTermofPayment',$this->BillSupplierTermofPayment,true);
        $criteria->compare('SupplierFee',$this->SupplierFee,true);
        $criteria->compare('FreightFee',$this->FreightFee,true);
        $criteria->compare('ComisionCodeSkema',$this->ComisionCodeSkema,true);
        $criteria->compare('CustomerCode',$this->CustomerCode,true);
        $criteria->compare('AgentCodeOrigin',$this->AgentCodeOrigin,true);
        $criteria->compare('AgentCodeDestination',$this->AgentCodeDestination,true);
        $criteria->compare('OrderTotal',$this->OrderTotal);

        $criteria->order = 'OrderDate DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OrderHeader the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave(){
        $this->OrderDate = date('Y-m-d H:i:s', strtotime($this->OrderDate));
        $this->PODate = date('Y-m-d H:i:s', strtotime($this->PODate));
        $this->BillSupplierDate = date('Y-m-d H:i:s', strtotime($this->BillSupplierDate));

        return parent::beforeSave();
    }
}
