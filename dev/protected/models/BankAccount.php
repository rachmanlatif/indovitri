<?php

/**
 * This is the model class for table "BankAccount".
 *
 * The followings are the available columns in table 'BankAccount':
 * @property string $BankAccountCode
 * @property string $BankCode
 * @property string $Branch
 * @property string $Address
 * @property string $AccountNumber
 * @property string $AccountName
 * @property string $SwiftCode
 * @property string $IbanCode
 * @property integer $Status
 * @property string $Note
 */
class BankAccount extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'BankAccount';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('BankAccountCode, BankCode', 'required'),
			array('Status', 'numerical', 'integerOnly'=>true),
			array('BankCode, Branch, Address, AccountNumber, AccountName, SwiftCode, IbanCode, Note', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('BankCode, Branch, Address, AccountNumber, AccountName, SwiftCode, IbanCode, Note', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bank' => array(self::BELONGS_TO, 'BankList', 'BankCode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'BankAccountCode' => 'Bank Account Code',
			'BankCode' => 'Bank Name',
			'Branch' => 'Branch',
			'Address' => 'Address',
			'AccountNumber' => 'Account Number',
			'AccountName' => 'Account Name',
			'SwiftCode' => 'SWIFT Code',
			'IbanCode' => 'IBAN Code',
			'Status' => 'Status',
			'Note' => 'Note',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('BankAccountCode',$this->BankAccountCode,true);
		$criteria->compare('BankCode',$this->BankCode,true);
		$criteria->compare('Branch',$this->Branch,true);
		$criteria->compare('Address',$this->Address,true);
		$criteria->compare('AccountNumber',$this->AccountNumber,true);
		$criteria->compare('AccountName',$this->AccountName,true);
		$criteria->compare('SwiftCode',$this->SwiftCode,true);
		$criteria->compare('IbanCode',$this->IbanCode,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Note',$this->Note,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BankList the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
