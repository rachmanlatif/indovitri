<?php

/**
 * This is the model class for table "State".
 *
 * The followings are the available columns in table 'State':
 * @property string $StateCode
 * @property string $CountryCode
 * @property string $StateName
 * @property string $ShortName
 * @property string $Note
 * @property integer $Status
 */
class State extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'State';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('StateCode', 'required'),
			array('StateCode, CountryCode, StateName, ShortName, Note', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('StateCode, CountryCode, StateName, ShortName, Note, Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'country' => array(self::BELONGS_TO, 'Country', 'CountryCode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'StateCode' => 'State Code',
			'CountryCode' => 'Country Name',
			'StateName' => 'State Name',
			'ShortName' => 'Short Name',
			'Note' => 'Note',
			'Status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('StateCode',$this->StateCode,true);
		$criteria->compare('CountryCode',$this->CountryCode,true);
		$criteria->compare('StateName',$this->StateName,true);
		$criteria->compare('ShortName',$this->ShortName,true);
		$criteria->compare('Note',$this->Note,true);
		$criteria->compare('Status',$this->Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return State the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
