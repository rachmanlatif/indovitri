<?php

/**
 * This is the model class for table "Shipper".
 *
 * The followings are the available columns in table 'Shipper':
 * @property string $ShipperCode
 * @property string $Name
 * @property string $Phone
 * @property string $email
 * @property string $Address
 * @property string $PostCode
 * @property string $CityCode
 * @property integer $Status
 * @property string $BankCode
 * @property string $AccountNo
 * @property string $AccountName
 * @property string $Website
 * @property string $Reference
 */
class Shipper extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Shipper';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ShipperCode', 'required'),
			array('Status', 'numerical', 'integerOnly'=>true),
			array('ShipperCode, Reference, Name, Phone, email, Address, PostCode, CityCode, BankCode, AccountNo, AccountName, Website', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ShipperCode, Reference, Name, Phone, email, Address, PostCode, CityCode, Status, BankCode, AccountNo, AccountName, Website', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'bank' => array(self::BELONGS_TO, 'BankList', 'BankCode'),
            'city' => array(self::BELONGS_TO, 'City', 'CityCode'),
            'details' => array(self::HAS_MANY, 'ShipperDetail', 'ShipperCode'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ShipperCode' => 'Shipper Code',
			'Reference' => 'Reference',
			'Name' => 'Name',
			'Phone' => 'Phone',
			'email' => 'Email',
			'Address' => 'Address',
			'PostCode' => 'Post Code',
			'CityCode' => 'City Name',
			'Status' => 'Status',
            'BankCode' => 'Bank Name',
            'AccountNo' => 'Account No',
            'AccountName' => 'Account Name',
            'Website' => 'Website',
        );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ShipperCode',$this->ShipperCode,true);
		$criteria->compare('Reference',$this->Reference,true);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Phone',$this->Phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('Address',$this->Address,true);
		$criteria->compare('PostCode',$this->PostCode,true);
		$criteria->compare('CityCode',$this->CityCode,true);
		$criteria->compare('Status',$this->Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Shipper the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
