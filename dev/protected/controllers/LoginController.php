<?php

class LoginController extends AdminController
{
    /**
     * layout for login page
     * @var string
     */
    public $layout = '//layouts/login';

    /**
     * Render login form
     * @return void
     */
    public function actionIndex()
    {
        if (!Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->homeUrl.'dashboard');
        } else {
            $model = new AdminForm();

            // collect user input data
            if(isset($_POST['AdminForm']))
            {
                $model->attributes=$_POST['AdminForm'];

                // validate user input and redirect to the previous page if valid
                if($model->validate() && $model->login()) {
                    // redirect to select module page
                    $this->redirect(Yii::app()->homeUrl.'dashboard');
                }
            }

            $this->render('index',array('model'=>$model));
        }
    }

    public function actionForgotPassword()
    {
        $model = new AdminForm();
        $this->render('_forgotPassword',array('model'=>$model));

    }
}
