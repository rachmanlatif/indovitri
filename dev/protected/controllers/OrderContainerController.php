<?php

class OrderContainerController extends AdminController{

    public function actionUpdate($id){
        $order = $this->loadModelOrder($id);
        $models = $this->loadModel($id);

        if(isset($_POST['OrderBLContainer'])){
            foreach ($_POST['OrderBLContainer'] as $d) {
                $detail = OrderBLContainer::model()->findByPk($d['IDOrderBLContainer']);
                if($detail != null){
                    $detail->NumberContainer = $d['NumberContainer'];
                    $detail->SealCode = $d['SealCode'];
                    if($detail->validate()){
                        $detail->save();
                    }
                    else{
                        Yii::app()->user->setFlash('info','Failed update seal code '.$d['SealCode'].'. Already exist.');
                        $this->redirect(Yii::app()->baseUrl.'/orderContainer/update/id/'.$order->OrderCode);
                    }
                }
            }

            Yii::app()->user->setFlash('success','Success update order BL container');
            $this->redirect(Yii::app()->baseUrl.'/order/view/id/'.$order->OrderCode);
        }

        $this->render('update', array(
            'order'=>$order,
            'models'=>$models,
        ));
    }

    public function loadModel($id)
    {
        $model=OrderBLContainer::model()->findAllByAttributes(array('OrderID'=>$id));
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function loadModelOrder($id)
    {
        $model=OrderHeader::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}
