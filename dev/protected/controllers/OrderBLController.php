<?php

class OrderBLController extends AdminController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd($id)
	{
        $order = $this->loadModelOrder($id);

		$model=new OrderBL;
        $model->OrderID = $order->OrderCode;

				$listContainer = array();
        $container = OrderBLContainer::model()->findAllByAttributes(array('OrderID'=>$order->OrderCode));
				$no = 1;
        foreach ($container as $value) {
            $listContainer[$value->ContainerCode] = $no.'. '.$value->container->ContainerName;

						$no++;
				}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OrderBL']))
		{
            $model->attributes=$_POST['OrderBL'];

			if($model->save()){
                $wiz = Wiz::model()->findByAttributes(array('OrderID'=>$model->OrderID));
                if($wiz == null){
                    $w = new Wiz();
                    $w->OrderID = $model->OrderID;
                    $w->WizStep = 7;
                    $w->save();
                }
                else{
                    if($wiz->WizStep < 7){
                        $wiz->WizStep = 7;
                        $wiz->save();
                    }
                }

                Yii::app()->user->setFlash('success','Success add order BL');
                $this->redirect(array('order/view/id/'.$model->OrderID));
            }
		}

		$this->render('add',array(
			'model'=>$model,
			'order'=>$order,
			'container'=>$container,
			'listContainer'=>$listContainer,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $order = $this->loadModelOrder($model->OrderID);
				$model->LoadingDate = date('Y-m-d', strtotime($model->LoadingDate));
				$model->EstimatedTimeDelivery = date('Y-m-d', strtotime($model->EstimatedTimeDelivery));
				$model->EstimatedTimeArive = date('Y-m-d', strtotime($model->EstimatedTimeArive));
				$model->BLReleasePayment = date('Y-m-d', strtotime($model->BLReleasePayment));
				$model->CBDocs = round($model->CBDocs, 0);

				$listContainer = array();
				$container = OrderBLContainer::model()->findAllByAttributes(array('OrderID'=>$order->OrderCode));
				$no = 1;
				foreach ($container as $key => $value) {
						$listContainer[$value->ContainerCode] = $no.'. '.$value->container->ContainerName;

						$no++;
				}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OrderBL']))
		{
			$model->attributes=$_POST['OrderBL'];

			if($model->save()){
                $wiz = Wiz::model()->findByAttributes(array('OrderID'=>$model->OrderID));
                if($wiz == null){
                    $w = new Wiz();
                    $w->OrderID = $model->OrderID;
                    $w->WizStep = 7;
                    $w->save();
                }
                else{
                    if($wiz->WizStep < 7){
                        $wiz->WizStep = 7;
                        $wiz->save();
                    }
                }

                Yii::app()->user->setFlash('success','Success update order BL');
                $this->redirect(array('order/view/id/'.$model->OrderID));
            }
		}

		$this->render('update',array(
			'model'=>$model,
			'order'=>$order,
			'listContainer'=>$listContainer,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        if($model->delete()){
            $orderDetail = OrderDetail::model()->findByAttributes(array('BLNumber'=>$model->BLNumber));
            if($orderDetail != null){
                $orderDetail->BLNumber = '';
                $orderDetail->save();
            }

            $orderDetailTemp = OrderDetailTemp::model()->findByAttributes(array('BLNumber'=>$model->BLNumber));
            if($orderDetailTemp != null){
                $orderDetailTemp->BLNumber = '';
                $orderDetailTemp->save();
            }

            Yii::app()->user->setFlash('success','Success delete order BL');
            $this->redirect(array('order/view/id/'.$model->OrderID));
		}
		else{
            Yii::app()->user->setFlash('danger','Failed delete order BL. Please do not repeat this request again.');
            $this->redirect(array('order/view/id/'.$model->OrderID));
        }
	}

	/**
	 * Deletes a list of model.
	 */
	public function actionDeleteSelected()
	{
		if(Yii::app()->request->isPostRequest && isset($_POST['ids']))
		{
			// delete
			foreach ($_POST['ids'] as $id) {
				$this->loadModel($id)->delete();
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new OrderBL('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['OrderBL']))
			$model->attributes=$_GET['OrderBL'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=OrderBL::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function loadModelOrder($id)
    {
        $model=OrderHeader::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='order-bl-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
