<?php

class ProfileController extends AdminController{

    public function actionIndex(){

        $model = MyHelper::getLogin();

        if(isset($_POST['Users'])){
            $model->attributes=$_POST['Users'];
            if($model->save()){
                Yii::app()->user->setFlash('success','Success update user');
                $this->redirect(Yii::app()->baseUrl.'/profile');
            }
            else{
              Yii::app()->user->setFlash('danger','Failed update user');
              $this->redirect(Yii::app()->baseUrl.'/profile');
            }
        }
        $get_country = Yii::app()->db->createCommand('SELECT * FROM Country')->queryAll();
        $this->render('index', array(
            'model'=>$model,
            'get_country' => $get_country,
        ));
    }

    public function actionAddState(){
  		// print_r($_POST);die;
  		$state = Yii::app()->db->createCommand("SELECT * FROM State WHERE CountryCode = '".$_POST['countryID']."'")->queryAll();
  		echo '<select class="form-control" style="width : 100%" tabindex="-1" id="stateID">';
        	foreach ($state as $key => $value) {
          	echo '<option value="'.$value['StateCode'].'">'.$value['StateName'].'</option>';
        }
    	echo '</select>';
  	}

  	public function actionAddCity(){
  		if(isset($_POST['stateID'])){
  			$city = Yii::app()->db->createCommand("SELECT * FROM City WHERE StateCode = '".$_POST['stateID']."' AND Status = '1'")->queryAll();
  			echo '<select class="form-control" style="width : 100%" tabindex="-1" id="cityID" name="Users[CityCode]">';
  	      	foreach ($city as $key => $value) {
  	        	echo '<option value="'.$value['CityCode'].'">'.$value['CityName'].'</option>';
  	      }
  	  	echo '</select>';
  		}else{
  			echo '<select class="form-control" style="width : 100%" tabindex="-1" id="cityID" name="Users[CityCode]">';

  	        	echo '<option value="">-</option>';
  	  	echo '</select>';
  		}

  	}
}

?>
