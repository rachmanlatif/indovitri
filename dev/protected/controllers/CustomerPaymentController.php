<?php

class CustomerPaymentController extends AdminController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$customer = Customer::model()->findByAttributes(array('CustomerCode'=>$model->CustomerCode));

		$this->render('view',array(
			'model'=>$model,
			'customer'=>$customer,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd($id = null)
	{
		$model=new CustomerPayment;
        $customer = Customer::model()->findByPk($id);
        if($customer != null){
            $model->CustomerCode = $customer->CustomerCode;
        }
				$model->PaymentCode = IDGenerator::generate('customerPayment');

				$totalPayment = 0;
				$payment = CustomerPayment::model()->findAllByAttributes(array('CustomerCode'=>$customer->CustomerCode));
				foreach($payment as $p){
						$totalPayment+=$p->Ammount;
				}

				$totalOrder = 0;
				$orders = OrderHeader::model()->findAllByAttributes(array('CustomerCode'=>$customer->CustomerCode));
				foreach($orders as $o){
						$totalCharge = 0;
						$charge = OrderChargeDetail::model()->findAllByAttributes(array('OrderID'=>$o->OrderCode));
						foreach($charge as $c){
								$totalCharge+=$c->Value;
						}

						$total = ($o->OrderTotal - $o->BuyerFeeAmount) + $o->FreightFee + $totalCharge;
						$totalOrder+=$total;
				}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CustomerPayment']))
		{
			$model->attributes=$_POST['CustomerPayment'];
            $model->Status = EnumStatus::ACTIVE;
			if($model->save()){
				$this->redirect(Yii::app()->homeUrl.'customer/view/id/'.$customer->CustomerCode);
            }
		}

		$this->render('add',array(
			'model'=>$model,
			'customer'=>$customer,
			'totalPayment'=>$totalPayment,
			'totalOrder'=>$totalOrder,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$customer = Customer::model()->findByAttributes(array('CustomerCode'=>$model->CustomerCode));

		$model->Ammount = round($model->Ammount,2);

		$totalPayment = 0;
		$payment = CustomerPayment::model()->findAllByAttributes(array('CustomerCode'=>$customer->CustomerCode));
		foreach($payment as $p){
				$totalPayment+=$p->Ammount;
		}

		$totalOrder = 0;
		$orders = OrderHeader::model()->findAllByAttributes(array('CustomerCode'=>$customer->CustomerCode));
		foreach($orders as $o){
				$totalCharge = 0;
				$charge = OrderChargeDetail::model()->findAllByAttributes(array('OrderID'=>$o->OrderCode));
				foreach($charge as $c){
						$totalCharge+=$c->Value;
				}

				$total = ($o->OrderTotal - $o->BuyerFeeAmount) + $o->FreightFee + $totalCharge;
				$totalOrder+=$total;
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CustomerPayment']))
		{
			$model->attributes=$_POST['CustomerPayment'];

			if($model->save()){
					$this->redirect(Yii::app()->homeUrl.'customer/view/id/'.$customer->CustomerCode);
            }
		}

		$this->render('update',array(
			'model'=>$model,
			'customer'=>$customer,
			'totalPayment'=>$totalPayment,
			'totalOrder'=>$totalOrder,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        if($model->delete()){
            $this->redirect(array('view','id'=>$model->IDCustomerPayment));
		}
		else{
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
	}

	/**
	 * Deletes a list of model.
	 */
	public function actionDeleteSelected()
	{
		if(Yii::app()->request->isPostRequest && isset($_POST['ids']))
		{
			// delete
			foreach ($_POST['ids'] as $id) {
				$this->loadModel($id)->delete();
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new CustomerPayment('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CustomerPayment']))
			$model->attributes=$_GET['CustomerPayment'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=CustomerPayment::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='customer-payment-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
