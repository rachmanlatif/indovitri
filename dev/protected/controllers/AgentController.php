<?php

class AgentController extends AdminController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$detailPerson=DetailPerson::model()->findAllByAttributes(array('EntityCode'=>$model->AgentCode, 'Status'=>EnumEntity::AGENT));

		$order = new OrderHeader('search');
		$order->AgentCodeOrigin = $model->AgentCode;

		$this->render('view',array(
			'model'=>$model,
			'detailPerson'=>$detailPerson,
			'order'=>$order,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd()
	{
		$model=new Agent;
		$detailPerson=array();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Agent']))
		{
			$detailPerson = array();
			if (isset($_POST['DetailPerson'])) {
					foreach ($_POST['DetailPerson'] as $d) {
							$detail = new DetailPerson();
							$detail->attributes = $d;

							array_push($detailPerson, $detail);
					}
			}

			$model->attributes=$_POST['Agent'];
            $model->AgentCode = IDGenerator::generate('agent');

			if($model->save()){
				foreach($detailPerson as $d){
						$d->Status = EnumEntity::AGENT;
						$d->EntityCode = $model->AgentCode;
						if(!$d->save()){
								$message = '';
								$errors = $d->getErrors();
								foreach ($errors as $error){
										foreach ($error as $e){
												$message.= $e.'<br />';
										}
								}
								throw new Exception('Error when saving detail person '.$message);
						}
				}

				$this->redirect(array('view','id'=>$model->AgentCode));
            }
		}
		$get_country = Yii::app()->db->createCommand('SELECT * FROM Country')->queryAll();
		$this->render('add',array(
			'model'=>$model,
			'detailPerson'=>$detailPerson,
			'get_country' => $get_country,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$detailPerson=DetailPerson::model()->findAllByAttributes(array('EntityCode'=>$model->AgentCode, 'Status'=>EnumEntity::AGENT));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Agent']))
		{
			$detailPerson = array();
			if (isset($_POST['DetailPerson'])) {
					foreach ($_POST['DetailPerson'] as $d) {
							$id = $d['IDDetailPerson'];
							if($id != null){
									$person = DetailPerson::model()->findByPk($id);
									if($person != null){
											$person->PositionCode = $d['PositionCode'];
											$person->Name = $d['Name'];
											$person->Address = $d['Address'];
											$person->Email = $d['Email'];
											$person->Note = $d['Note'];
											$person->save();
									}
							}
							else{
								$detail = new DetailPerson();
								$detail->attributes = $d;

								array_push($detailPerson, $detail);
							}
					}
			}

			$model->attributes=$_POST['Agent'];

			if($model->save()){
				foreach($detailPerson as $d){
						$d->Status = EnumEntity::AGENT;
						$d->EntityCode = $model->AgentCode;
						if(!$d->save()){
								$message = '';
								$errors = $d->getErrors();
								foreach ($errors as $error){
										foreach ($error as $e){
												$message.= $e.'<br />';
										}
								}
								throw new Exception('Error when saving detail person '.$message);
						}
				}

				$this->redirect(array('view','id'=>$model->AgentCode));
            }
		}
		$get_country = Yii::app()->db->createCommand('SELECT * FROM Country')->queryAll();
		$this->render('update',array(
			'model'=>$model,
			'detailPerson'=>$detailPerson,
			'get_country' => $get_country,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(isset($_GET['id']))
		{
				$model = $this->loadModel($id);
				$model->Status = EnumStatus::NON_ACTIVE;
				if($model->save()){
					$this->redirect(array('index'));
				}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionDeletePerson($id)
	{
			$detail = DetailPerson::model()->findByPk($id);
			if($detail != null){
					if($detail->delete()){
							$this->redirect(array('update','id'=>$detail->EntityCode));
					}
					else{
							Yii::app()->user->setFlash('danger','Failed delete detail');
							$this->redirect(array('view','id'=>$detail->EntityCode));
					}
			}
			else{
					Yii::app()->user->setFlash('danger','Failed delete detail');
					$this->redirect(array('view','id'=>$detail->EntityCode));
			}
	}

	/**
	 * Deletes a list of model.
	 */
	public function actionDeleteSelected()
	{
		if(Yii::app()->request->isPostRequest && isset($_POST['ids']))
		{
			// delete
			foreach ($_POST['ids'] as $id) {
				$this->loadModel($id)->delete();
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Agent('search');
		$model->unsetAttributes();  // clear any default values
		$model->Status = EnumStatus::ACTIVE;
		
		if(isset($_GET['Agent'])){
			$data_get = $_GET['Agent'];
			unset($_GET['Agent']['StateCode']);
			$model->attributes=$data_get;
		}


		$get_country = Yii::app()->db->createCommand('SELECT * FROM Country')->queryAll();
		$this->render('index',array(
			'model'=>$model,
			'get_country' => $get_country,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Agent::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='agent-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionAddPerson(){
			if(Yii::app()->request->isAjaxRequest){
					$json = array(
							'success'=>false,
							'content'=>'',
							'message'=>'',
					);

					if (isset($_POST['index'])) {
							$detail = new DetailPerson();

							$json['content'] = $this->renderPartial('_formPerson', array(
									'detail'=>$detail,
									'index'=>$_POST['index'],
							), true);
							$json['success'] = true;
					}
					else{
							$json['message'] = 'The requested page does not exist.';
					}

					echo CJSON::encode($json);
					Yii::app()->end();
			}
	}

	public function actionAddState(){
		// print_r($_POST);die;
		$state = Yii::app()->db->createCommand("SELECT * FROM State WHERE CountryCode = '".$_POST['countryID']."'")->queryAll();
		echo '<select class="form-control" style="width : 100%" tabindex="-1" id="stateID" name="Agent[StateCode]">';
      	foreach ($state as $key => $value) {
        	echo '<option value="'.$value['StateCode'].'">'.$value['StateName'].'</option>';
      }
  	echo '</select>';
	}

	public function actionAddCity(){
		if(isset($_POST['stateID'])){
			$city = Yii::app()->db->createCommand("SELECT * FROM City WHERE StateCode = '".$_POST['stateID']."' AND Status = '1'")->queryAll();
			echo '<select class="form-control" style="width : 100%" tabindex="-1" id="cityID" name="Agent[CityCode]">';
	      	foreach ($city as $key => $value) {
	        	echo '<option value="'.$value['CityCode'].'">'.$value['CityName'].'</option>';
	      }
	  	echo '</select>';
		}else{
			echo '<select class="form-control" style="width : 100%" tabindex="-1" id="cityID" name="Agent[CityCode]">';

	        	echo '<option value="">-</option>';
	  	echo '</select>';
		}

	}
}
