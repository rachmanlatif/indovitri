<?php

class ShipperController extends AdminController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$detailPerson=DetailPerson::model()->findAllByAttributes(array('EntityCode'=>$model->ShipperCode, 'Status'=>EnumEntity::SHIPPER));

		$this->render('view',array(
			'model'=>$model,
			'detailPerson'=>$detailPerson,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd()
	{
		$model=new Shipper;
		$detailPerson=array();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Shipper']))
		{
            $tempDetail = array();
            if (isset($_POST['ShipperDetail'])) {
                foreach ($_POST['ShipperDetail'] as $d) {
                    $detail = new ShipperDetail();
                    $detail->attributes = $d;

                    array_push($tempDetail, $detail);
                }
            }
            $model->details = $tempDetail;

						$detailPerson = array();
						if (isset($_POST['DetailPerson'])) {
								foreach ($_POST['DetailPerson'] as $d) {
										$detail = new DetailPerson();
										$detail->attributes = $d;

										array_push($detailPerson, $detail);
								}
						}

            $model->attributes=$_POST['Shipper'];
            $model->ShipperCode = IDGenerator::generate('shipper');

			if($model->save()){
                foreach($model->details as $d){
                    $d->ShipperCode = $model->ShipperCode;
                    if(!$d->save()){
                        $message = '';
                        $errors = $d->getErrors();
                        foreach ($errors as $error){
                            foreach ($error as $e){
                                $message.= $e.'<br />';
                            }
                        }
                        throw new Exception('Error when saving detail consignee '.$message);
                    }
                }

								foreach($detailPerson as $d){
										$d->Status = EnumEntity::AGENT;
										$d->EntityCode = $model->ShipperCode;
										if(!$d->save()){
												$message = '';
												$errors = $d->getErrors();
												foreach ($errors as $error){
														foreach ($error as $e){
																$message.= $e.'<br />';
														}
												}
												throw new Exception('Error when saving detail person '.$message);
										}
								}

				$this->redirect(array('view','id'=>$model->ShipperCode));
            }
		}

		$this->render('add',array(
			'model'=>$model,
			'detailPerson'=>$detailPerson,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$detailPerson=DetailPerson::model()->findAllByAttributes(array('EntityCode'=>$model->ShipperCode, 'Status'=>EnumEntity::SHIPPER));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Shipper']))
		{
            $tempDetail = array();
            if (isset($_POST['ShipperDetail'])) {
                foreach ($_POST['ShipperDetail'] as $d) {
                    $code = $d['IDShipperDetail'];
                    if($code != null){
                        $detail = ShipperDetail::model()->findByPk($code);
                        if($detail != null){
                            $detail->CountryCode = $d['CountryCode'];
                            $detail->RegistrationNumberName = $d['RegistrationNumberName'];
                            $detail->RegistrationNumberValue = $d['RegistrationNumberValue'];
                            $detail->TaxCodeName = $d['TaxCodeName'];
                            $detail->TaxCodeValue = $d['TaxCodeValue'];
                            $detail->save();
                        }
                    }
                    else{
                        $detail = new ShipperDetail();
                        $detail->attributes = $d;

                        array_push($tempDetail, $detail);
                    }
                }
            }
            $model->details = $tempDetail;

						$detailPerson = array();
						if (isset($_POST['DetailPerson'])) {
								foreach ($_POST['DetailPerson'] as $d) {
										$id = $d['IDDetailPerson'];
										if($id != null){
												$person = DetailPerson::model()->findByPk($id);
												if($person != null){
														$person->PositionCode = $d['PositionCode'];
														$person->Name = $d['Name'];
														$person->Address = $d['Address'];
														$person->Email = $d['Email'];
														$person->Note = $d['Note'];
														$person->save();
												}
										}
										else{
											$detail = new DetailPerson();
											$detail->attributes = $d;

											array_push($detailPerson, $detail);
										}
								}
						}

            $model->attributes=$_POST['Shipper'];
			if($model->save()){
                foreach($model->details as $d){
                    $d->ShipperCode = $model->ShipperCode;
                    if(!$d->save()){
                        $message = '';
                        $errors = $d->getErrors();
                        foreach ($errors as $error){
                            foreach ($error as $e){
                                $message.= $e.'<br />';
                            }
                        }
                        throw new Exception('Error when saving detail consignee '.$message);
                    }
                }

								foreach($detailPerson as $d){
										$d->Status = EnumEntity::SHIPPER;
										$d->EntityCode = $model->ShipperCode;
										if(!$d->save()){
												$message = '';
												$errors = $d->getErrors();
												foreach ($errors as $error){
														foreach ($error as $e){
																$message.= $e.'<br />';
														}
												}
												throw new Exception('Error when saving detail person '.$message);
										}
								}

                $this->redirect(array('view','id'=>$model->ShipperCode));
            }
		}

		$this->render('update',array(
			'model'=>$model,
			'detailPerson'=>$detailPerson,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
    public function actionDelete($id)
    {
			if(isset($_GET['id']))
			{
					$model = $this->loadModel($id);
					$model->Status = EnumStatus::NON_ACTIVE;
					if($model->save()){
						$this->redirect(array('index'));
					}
			}
			else
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    public function actionDeleteDetail($id)
    {
        $detail = ShipperDetail::model()->findByPk($id);
        if($detail != null){
            if($detail->delete()){
                $this->redirect(array('update','id'=>$detail->ShipperCode));
            }
            else{
                Yii::app()->user->setFlash('danger','Failed delete detail');
                $this->redirect(array('view','id'=>$detail->ConsigneeCode));
            }
        }
        else{
            Yii::app()->user->setFlash('danger','Failed delete detail');
            $this->redirect(array('view','id'=>$detail->ShipperCode));
        }
    }

		public function actionDeletePerson($id)
		{
				$detail = DetailPerson::model()->findByPk($id);
				if($detail != null){
						if($detail->delete()){
								$this->redirect(array('update','id'=>$detail->EntityCode));
						}
						else{
								Yii::app()->user->setFlash('danger','Failed delete detail');
								$this->redirect(array('view','id'=>$detail->EntityCode));
						}
				}
				else{
						Yii::app()->user->setFlash('danger','Failed delete detail');
						$this->redirect(array('view','id'=>$detail->EntityCode));
				}
		}

	/**
	 * Deletes a list of model.
	 */
	public function actionDeleteSelected()
	{
		if(Yii::app()->request->isPostRequest && isset($_POST['ids']))
		{
			// delete
			foreach ($_POST['ids'] as $id) {
				$this->loadModel($id)->delete();
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Shipper('search');
		$model->unsetAttributes();  // clear any default values
		$model->Status = EnumStatus::ACTIVE;

		if(isset($_GET['Shipper']))
			$model->attributes=$_GET['Shipper'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Shipper::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='shipper-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionAddDetail(){
        if(Yii::app()->request->isAjaxRequest){
            $json = array(
                'success'=>false,
                'content'=>'',
                'message'=>'',
            );

            if (isset($_POST['index'])) {
                $detail = new ShipperDetail();

                $json['content'] = $this->renderPartial('_formDetail', array(
                    'detail'=>$detail,
                    'index'=>$_POST['index'],
                ), true);
                $json['success'] = true;
            }
            else{
                $json['message'] = 'The requested page does not exist.';
            }

            echo CJSON::encode($json);
            Yii::app()->end();
        }
    }

		public function actionAddPerson(){
				if(Yii::app()->request->isAjaxRequest){
						$json = array(
								'success'=>false,
								'content'=>'',
								'message'=>'',
						);

						if (isset($_POST['index'])) {
								$detail = new DetailPerson();

								$json['content'] = $this->renderPartial('_formPerson', array(
										'detail'=>$detail,
										'index'=>$_POST['index'],
								), true);
								$json['success'] = true;
						}
						else{
								$json['message'] = 'The requested page does not exist.';
						}

						echo CJSON::encode($json);
						Yii::app()->end();
				}
		}
}
