<?php
class DownloadController extends AdminController
{
    public function actionArcPdf($id){
      $model = $this->loadModelCustomer($id);

      // create new PDF document
      $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      // set document information
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Indovitri');
      $pdf->SetTitle('Account Receivable Report');
      $pdf->SetSubject('Account Receivable Report');

      // set header and footer fonts
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      // set default monospaced font
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
      // set margins
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
      // set auto page breaks
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
      // set image scale factor
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

      // set font
      $pdf->SetFont('dejavusans', '', 10);
      // add a page
      $pdf->AddPage();

      $list = '';
      $no = 1;
      $orders = OrderHeader::model()->findAllByAttributes(array('CustomerCode'=>$model->CustomerCode));
      if($orders != null){
          $total = 0;
          foreach($orders as $order) {
              $totalCharge = 0;
              foreach($order->charges as $charge){
                  $totalCharge+=$charge->Value;
              }

              $totalOrder = $order->OrderTotal+$totalCharge+$order->FreightFee;
              foreach($order->sellerBills as $bill){
                  $list .= '<tr>';
                  $list .= '<td>'.$no.'</td>';
                  $list .= '<td>SS</td>';
                  $list .= '<td>'.$order->SellerCode.'</td>';
                  $list .= '<td>'.$order->OrderCode.'</td>';
                  $list .= '<td>'.date('d-M-Y', strtotime($bill->DatePayment)).'</td>';
                  $list .= '<td style="text-align: right;">$'.number_format($totalOrder,2).'</td>';
                  $list .= '<td style="text-align: right;">$'.number_format($order->FreightFee,2).'</td>';
                  $list .= '<td style="text-align: right;">$'.number_format($totalCharge,2).'</td>';
                  $list .= '<td>100%</td>';
                  $list .= '<td style="text-align: right;">$'.number_format($totalOrder,2).'</td>';
                  $list .= '<td style="text-align: right;">$'.number_format(0,2).'</td>';
                  $list .= '<td style="text-align: right;">$'.number_format(0,2).'</td>';
                  $list .= '</tr>';

                  $no++;
                  $total+=$totalOrder;
              }
          }

          $list .= '<tr>';
          $list .= '<td></td>';
          $list .= '<td></td>';
          $list .= '<td></td>';
          $list .= '<td colspan="2">VALOR TOTAL DE COMPRA</td>';
          $list .= '<td style="text-align: right;">$'.number_format($total,2).'</td>';
          $list .= '<td></td>';
          $list .= '<td colspan="2">TOTAL A PAGAR</td>';
          $list .= '<td style="text-align: right;">$'.number_format($total,2).'</td>';
          $list .= '<td style="text-align: right;">$'.number_format(0,2).'</td>';
          $list .= '<td style="text-align: right;">$'.number_format(0,2).'</td>';
          $list .= '</tr>';
      }

      $transfer = '';
      $summary = '';
      if($model->payments != null){
          $index = 1;
          $totalPayment = 0;
          foreach($model->payments as $payment){
              $transfer .= '<tr>';
              $transfer .= '<td>Bank Transfer #'.$index.'</td>';
              $transfer .= '<td>'.date('d-M-Y', strtotime($payment->DatePayment)).'</td>';
              $transfer .= '<td style="text-align: right;">$'.number_format($payment->Ammount,2).'</td>';
              $transfer .= '</tr>';

              $index++;
              $totalPayment+=$payment->Ammount;
          }

          $summary .= '<tr>';
          $summary .= '<td>CONFIRMACAO PEDIDO</td>';
          $summary .= '<td style="text-align: right;">$'.number_format($totalOrder,2).'</td>';
          $summary .= '<td>Pgto para embarque imediato ou comeco de producao (Pend.)</td>';
          $summary .= '</tr>';

          $summary .= '<tr>';
          $summary .= '<td>EMBARQUE</td>';
          $summary .= '<td style="text-align: right;">$'.number_format(0,2).'</td>';
          $summary .= '<td>Pagamento final para Estufar (Pend.)</td>';
          $summary .= '</tr>';

          $summary .= '<tr>';
          $summary .= '<td>LIBERACAO BL</td>';
          $summary .= '<td style="text-align: right;">$'.number_format(0,2).'</td>';
          $summary .= '<td>Balanco pendete contra liberacao do BL (Pend.)</td>';
          $summary .= '</tr>';

          $totalFinal = $totalPayment - $totalOrder;

          $summary .= '<tr>';
          $summary .= '<td>TOTAL PENDETE (USD)</td>';
          $summary .= '<td style="text-align: right;">$'.number_format($totalFinal,2).'</td>';
          $summary .= '<td>(Valor Total de Compra - Fundos Recebidos)</td>';
          $summary .= '</tr>';

          $summary .= '<tr>';
          $summary .= '<td>BALANCO DE CONTA - A VENCER</td>';
          $summary .= '<td style="text-align: right;">$'.number_format(0,2).'</td>';
          $summary .= '<td>Balanco pendete contra liberacao do BL</td>';
          $summary .= '</tr>';

          $summary .= '<tr>';
          $summary .= '<td>Acct Funds Xfer to '.date('Y').'</td>';
          $summary .= '<td style="text-align: right;">$'.number_format($totalFinal,2).'</td>';
          $summary .= '<td></td>';
          $summary .= '</tr>';

          $summary .= '<tr>';
          $summary .= '<td>'.$model->Name.' Balance @ '.date('d-M-Y').'</td>';
          $summary .= '<td style="text-align: right;">$'.number_format(0,2).'</td>';
          $summary .= '<td></td>';
          $summary .= '</tr>';
      }

      // create some HTML content
      $html = '
      <table cellpadding="3" cellspacing="1" style="font-size: xx-small">
        <tr>
            <td colspan="12" style="text-align: center;"><h1>Account Receivable Report</h1></td>
        </tr>
        <tr>
          <td colspan="12"></td>
        </tr>
        <tr>
          <td colspan="12">('.$model->CustomerCode.') '.$model->Name.'</td>
        </tr>
        <tr>
          <td colspan="12">PEDIDOS EM ANDAMENTO</td>
        </tr>
        <tr>
          <td colspan="12"></td>
        </tr>
        <tr>
          <td colspan="12">DATA: '.date('d-M-Y').'</td>
        </tr>
        <tr>
          <td colspan="12"></td>
        </tr>
        <tr>
          <td>Seq</td>
          <td>Acct</td>
          <td>Factory</td>
          <td>Invoice</td>
          <td>Inv Date</td>
          <td>Total</td>
          <td>FOB</td>
          <td>Indo Cost</td>
          <td>DP 100%</td>
          <td>Confirmacao do Pedido</td>
          <td>Embarque</td>
          <td>Liberacao BL</td>
        </tr>
        '.$list.'
      </table>
      <br>
      <br>
      <table cellpadding="3" cellspacing="1" style="font-size: xx-small;">
        <tr style="background-color: #f3f3f3;">
          <td colspan="3" style="text-align: center;">TRANSFERENCIAS / CREDITO EM CONTA</td>
        </tr>
        '.$transfer.'
      </table>
      <br>
      <table cellpadding="3" cellspacing="1" style="font-size: xx-small;">
        <tr style="background-color: #f3f3f3;">
          <td colspan="3" style="text-align: center;">RESUMO DE CONTA</td>
        </tr>
        '.$summary.'
      </table>
      ';

      // output the HTML content
      $pdf->writeHTML($html, true, false, true, false, '');

      // reset pointer to the last page
      $pdf->lastPage();

      //Close and output PDF document
      $pdf->Output('account_receivable_'.$model->Name, 'I');
    }

    public function actionPlPdf($id){
      $model = $this->loadModel($id);

      // create new PDF document
      $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      // set document information
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Indovitri');
      $pdf->SetTitle('Packing List');
      $pdf->SetSubject('Packing List');

      // set header and footer fonts
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      // set default monospaced font
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
      // set margins
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
      // set auto page breaks
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
      // set image scale factor
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

      // set font
      $pdf->SetFont('dejavusans', '', 10);
      // add a page
      $pdf->AddPage();

      $cons = '';
      $consignee = Consignee::model()->findByPk($model->ConsigneeCode);
      if($consignee != null){
          $cons .= $consignee->Name.'<br>';
          $cons .= $consignee->Address.' '.$consignee->PostCode.'<br>';

          if($consignee->city != null){
              $city = $consignee->city;
              if($city->state != null){
                  $state = $city->state;
                  if($state->country != null){
                      $country = $state->country;

                      $cons .= $city->CityName.' - '.$state->StateName.' - '.$country->CountryName.'<br>';
                  }
              }
          }
      }

      $notif = '';
      $consignee = Consignee::model()->findByPk($model->NotifyPartyCode);
      if($consignee != null){
          $notif .= $consignee->Name.'<br>';
          $notif .= $consignee->Address.' '.$consignee->PostCode.'<br>';

          if($consignee->city != null){
              $city = $consignee->city;
              if($city->state != null){
                  $state = $city->state;
                  if($state->country != null){
                      $country = $state->country;

                      $notif .= $city->CityName.' - '.$state->StateName.' - '.$country->CountryName.'<br>';
                  }
              }
          }
      }

      $manuf = '';
      if($model->seller !== null){
          $manuf .= $model->seller->Name;
          $manuf .= $model->seller->Address;
          $manuf .= MyHelper::getCityDetail($model->seller->CityCode).' '.$model->seller->PostCode;
      }

      $list = '';
      $no = 1;
      $a = 0;
      $b = 0;
      $c = 0;
      $d = 0;
      $e = 0;
      $f = 0;
      foreach($model->details as $detail) {
          if($detail->product != null){
              $product = $detail->product;

              $smallQTY = 0;
              if($product->uom->define->QTYUOMSmall != null){
                  $smallQTY = $product->uom->define->QTYUOMSmall;
              }

              $totalPrice = $smallQTY * $product->Price;
              $totalNW = $smallQTY * $product->NettWeight;
              $totalGW = $smallQTY * $product->GrossWeight;
              $totalQTY = $smallQTY * $detail->QTY;

              $list .= '<tr>';
              $list .= '<td>'.$no.'</td>';
              $list .= '<td>'.$product->ProductName.'</td>';
              $list .= '<td>'.$product->ProductID.'</td>';
              $list .= '<td>'.number_format($smallQTY).'</td>';
              $list .= '<td>'.number_format($detail->QTY).'</td>';
              $list .= '<td>'.$product->uom->UOMName.'</td>';
              $list .= '<td>'.number_format($totalQTY).'</td>';
              $list .= '<td>'.number_format($product->GrossWeight,2).'</td>';
              $list .= '<td>'.number_format($totalGW,2).'</td>';
              $list .= '<td>'.number_format($product->NettWeight,2).'</td>';
              $list .= '<td>'.number_format($totalNW,2).'</td>';
              $list .= '<td>'.number_format($product->VolumeGross,3).'</td>';
              $list .= '<td>'.number_format($detail->TotalVolume,2).'</td>';
              $list .= '</tr>';
          }

          $a+=$detail->QTY;
          $b+=$totalQTY;
          $c+=$totalPrice;
          $d+=$totalGW;
          $e+=$totalNW;
          $f+=$detail->TotalVolume;

          $no++;
      }

      $porto = '';
      if($model->pol !== null){
          $porto = MyHelper::getCityCountry($model->pol->CityCode);
      }

      $portd = '';
      if($model->pod !== null){
          $portd = MyHelper::getCityCountry($model->pod->CityCode);
      }

      $agent = '';
      if($model->agentOrigin !== null){
          $agent = $model->agentOrigin->Name;
      }

      $containerArr = array();
      $sealArr = array();
      $container = '';
      $seal = '';
      if($model->blContainer !== null){
          foreach($model->blContainer as $bl){
              array_push($containerArr, $bl->NumberContainer);
              array_push($sealArr, $bl->SealCode);
          }

          $container = implode(' / ',$containerArr);
          $seal = implode(' / ',$sealArr);
      }

      // create some HTML content
      $html = '
      <table cellpadding="3" cellspacing="1" style="font-size: xx-small">
        <tr>
            <td colspan="9" style="text-align: center;"><h1>Packing List</h1></td>
        </tr>
        <tr>
          <td colspan="9"></td>
        </tr>
        <tr>
            <td>Date:</td>
            <td colspan="8">'.date('d-F-Y').'</td>
        </tr>
        <tr>
            <td>Ref:</td>
            <td colspan="8">'.$model->BillCodeSupplier.'</td>
        </tr>
        <tr>
          <td colspan="9"></td>
        </tr>
        <tr>
          <td>Shipper:</td>
          <td colspan="3">
            CV. CASA BALI<br>
            WIRA BHUANA DALUNG PERMAI BLOK X2 NO. 17<br>
            LINK. WIRA BHUANA, KEL. KEROBOKAN KAJA<br>
            KEC. KUTA UTARA, KAB. BADUNG, 80361<br>
            BALI - INDONESIA<br>
          </td>
          <td></td>
          <td>Consignee:</td>
          <td colspan="3">'.$cons.'</td>
        </tr>
        <tr>
          <td colspan="9"></td>
        </tr>
        <tr>
          <td>Origin:</td>
          <td>'.MyHelper::getCityCountry($model->POLCode).'</td>
          <td></td>
          <td></td>
          <td></td>
          <td>Notify Party:</td>
          <td colspan="3">'.$notif.'</td>
        </tr>
        <tr>
          <td>Destination:</td>
          <td colspan="3">'.MyHelper::getCityCountry($model->PODCode).'</td>
          <td></td>
          <td>Delivery Date:</td>
          <td colspan="3">15 days from order confirmation</td>
        </tr>
      </table>
      <br>
      <br>
      <br>
      <table cellpadding="3" cellspacing="1" style="font-size: xx-small; text-align: center;">
        <tr style="background-color: #f3f3f3;">
          <th>Seq</th>
          <th>Description</th>
          <th>Product Code</th>
          <th>Unit per Carton</th>
          <th>Qty Carton</th>
          <th>Unit SET/PC</th>
          <th>Qty Units</th>
          <th>G.W (kgs)</th>
          <th>G.W Total</th>
          <th>N.W (kgs)</th>
          <th>N.W Total</th>
          <th>CBM</th>
          <th>CBM Total</th>
        </tr>
        '.$list.'
        <tr style="background-color: #f3f3f3;">
          <td></td>
          <td colspan="3">Grand Total FOB</td>
          <td>'.$a.'</td>
          <td></td>
          <td>'.$c.'</td>
          <td></td>
          <td>'.$d.'</td>
          <td></td>
          <td>'.$e.'</td>
          <td></td>
          <td>'.$f.'</td>
        </tr>
      </table>
      <br>
      <br>
      <table cellpadding="3" cellspacing="1" style="font-size: xx-small;">
        <tr>
          <td colspan="3">
            1 X 40NOR SHIPPERs LOAD AND COUNT
          </td>
        </tr>
        <tr>
          <td colspan="3">
            Quantity Pieces (IN PACKAGES AND SEVERAL PARTS) QUANTIDADES EM NUMERO DE VOLUMES: '.number_format($a).' PCS IN '.number_format($b).' CARTONS
          </td>
        </tr>
        <tr>
          <td colspan="2">
            COUNTRY OF ORIGIN OF GOODS / PAIS DE ORIGEM: '.$porto.'<br>
            COUNTRY OF FINAL DESTINATION / PAIS DE DESTINO: '.$portd.'<br>
            COUNTRY WHERE GOODS WERE SHIPPED / PAIS EM QUE A CARGA FOI EMBARCADA: '.$porto.'<br>
            COUNTRY WHERE GOODS WERE PURCHASED / PAIS EM QUE A CARGA FOI COMPRADA: '.$portd.'<br>
            CARRIAGE BY: SEA / TRANSPORTE: '.$agent.'
          </td>
          <td>
            Manufacturer Details:<br>
            '.$manuf.'
          </td>
        </tr>
        <tr>
          <td colspan="2">
            There are no dealers nor commission / sem intermediarios ou comissoes<br>
            Net prices for wholesale condition, there are no discount / precos e condicoes de atacado<br>
            There above prices are FOB JAKARTA / precos FOB JAKARTA<br>
            The above price follow the current international market / precos seguem valor de mercado internacional<br>
            Container No: '.$container.' / Seal: '.$seal.'
          </td>
          <td>
            Total G.W : '.number_format($model->TotalGrossWeight,2).'<br>
            Total N.W : '.number_format($model->TotalNettWeight,2).'<br>
            Total MEAS :
          </td>
        </tr>
      </table>
      ';

      // output the HTML content
      $pdf->writeHTML($html, true, false, true, false, '');

      // reset pointer to the last page
      $pdf->lastPage();

      //Close and output PDF document
      $pdf->Output('proforma_invoice_'.$model->PORefCode, 'I');
    }

    public function actionPiPdf($id){
      $model = $this->loadModel($id);

      // create new PDF document
      $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      // set document information
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Indovitri');
      $pdf->SetTitle('Proforma Invoice');
      $pdf->SetSubject('Proforma Invoice');

      // set header and footer fonts
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      // set default monospaced font
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
      // set margins
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
      // set auto page breaks
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
      // set image scale factor
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

      // set font
      $pdf->SetFont('dejavusans', '', 10);
      // add a page
      $pdf->AddPage();

      $cons = '';
      $consignee = Consignee::model()->findByPk($model->ConsigneeCode);
      if($consignee != null){
          $cons .= $consignee->Name.'<br>';
          $cons .= $consignee->Address.' '.$consignee->PostCode.'<br>';

          if($consignee->city != null){
              $city = $consignee->city;
              if($city->state != null){
                  $state = $city->state;
                  if($state->country != null){
                      $country = $state->country;

                      $cons .= $city->CityName.' - '.$state->StateName.' - '.$country->CountryName.'<br>';
                  }
              }
          }
      }

      $notif = '';
      $consignee = Consignee::model()->findByPk($model->NotifyPartyCode);
      if($consignee != null){
          $notif .= $consignee->Name.'<br>';
          $notif .= $consignee->Address.' '.$consignee->PostCode.'<br>';

          if($consignee->city != null){
              $city = $consignee->city;
              if($city->state != null){
                  $state = $city->state;
                  if($state->country != null){
                      $country = $state->country;

                      $notif .= $city->CityName.' - '.$state->StateName.' - '.$country->CountryName.'<br>';
                  }
              }
          }
      }

      $manuf = '';
      if($model->seller !== null){
          $manuf .= $model->seller->Name;
          $manuf .= $model->seller->Address;
          $manuf .= MyHelper::getCityDetail($model->seller->CityCode).' '.$model->seller->PostCode;
      }

      $list = '';
      $no = 1;
      $a = 0;
      $b = 0;
      $c = 0;
      foreach($model->details as $detail) {
          if($detail->product != null){
              $product = $detail->product;

              $smallQTY = 0;
              if($product->uom->define->QTYUOMSmall != null){
                  $smallQTY = $product->uom->define->QTYUOMSmall;
              }

              $totalPrice = $smallQTY * $product->Price;
              $totalQTY = $smallQTY * $detail->QTY;

              $list .= '<tr>';
              $list .= '<td>'.$no.'</td>';
              $list .= '<td>'.$product->ProductName.'</td>';
              $list .= '<td>'.$product->ProductID.'</td>';
              $list .= '<td>'.number_format($smallQTY).'</td>';
              $list .= '<td>'.number_format($detail->QTY).'</td>';
              $list .= '<td>'.$product->uom->UOMName.'</td>';
              $list .= '<td>'.number_format($totalQTY).'</td>';
              $list .= '<td>$'.number_format($product->Price,2).'</td>';
              $list .= '<td>$'.number_format($totalPrice,2).'</td>';
              $list .= '</tr>';
          }

          $a+=$detail->QTY;
          $b+=$totalQTY;
          $c+=$totalPrice;

          $no++;
      }

      $porto = '';
      if($model->pol !== null){
          $porto = MyHelper::getCityCountry($model->pol->CityCode);
      }

      $portd = '';
      if($model->pod !== null){
          $portd = MyHelper::getCityCountry($model->pod->CityCode);
      }

      $agent = '';
      if($model->agentOrigin !== null){
          $agent = $model->agentOrigin->Name;
      }

      $containerArr = array();
      $sealArr = array();
      $container = '';
      $seal = '';
      if($model->blContainer !== null){
          foreach($model->blContainer as $bl){
              array_push($containerArr, $bl->NumberContainer);
              array_push($sealArr, $bl->SealCode);
          }

          $container = implode(' / ',$containerArr);
          $seal = implode(' / ',$sealArr);
      }

      // create some HTML content
      $html = '
      <table cellpadding="3" cellspacing="1" style="font-size: xx-small">
        <tr>
            <td colspan="9" style="text-align: center;"><h1>Proforma Invoice</h1></td>
        </tr>
        <tr>
          <td colspan="9"></td>
        </tr>
        <tr>
            <td>Date:</td>
            <td colspan="8">'.date('d-F-Y').'</td>
        </tr>
        <tr>
            <td>Ref:</td>
            <td colspan="8">'.$model->BillCodeSupplier.'</td>
        </tr>
        <tr>
          <td colspan="9"></td>
        </tr>
        <tr>
          <td>Shipper:</td>
          <td colspan="3">
            CV. CASA BALI<br>
            WIRA BHUANA DALUNG PERMAI BLOK X2 NO. 17<br>
            LINK. WIRA BHUANA, KEL. KEROBOKAN KAJA<br>
            KEC. KUTA UTARA, KAB. BADUNG, 80361<br>
            BALI - INDONESIA<br>
          </td>
          <td></td>
          <td>Consignee:</td>
          <td colspan="3">'.$cons.'</td>
        </tr>
        <tr>
          <td colspan="9"></td>
        </tr>
        <tr>
          <td>Origin:</td>
          <td>'.MyHelper::getCityCountry($model->POLCode).'</td>
          <td></td>
          <td></td>
          <td></td>
          <td>Notify Party:</td>
          <td colspan="3">'.$notif.'</td>
        </tr>
        <tr>
          <td>Destination:</td>
          <td colspan="8">'.MyHelper::getCityCountry($model->PODCode).'</td>
        </tr>
      </table>
      <br>
      <br>
      <br>
      <table cellpadding="3" cellspacing="1" style="font-size: xx-small; text-align: center;">
        <tr style="background-color: #f3f3f3;">
          <th>Seq</th>
          <th>Description</th>
          <th>Product Code</th>
          <th>Unit per Carton</th>
          <th>Qty Carton</th>
          <th>Unit SET/PC</th>
          <th>Qty Units</th>
          <th>USD Unit</th>
          <th>Total USD</th>
        </tr>
        '.$list.'
        <tr style="background-color: #f3f3f3;">
          <td></td>
          <td colspan="3">Grand Total FOB</td>
          <td>'.$a.'</td>
          <td></td>
          <td>'.$b.'</td>
          <td></td>
          <td>$'.$c.'</td>
        </tr>
        <tr style="background-color: #f3f3f3;text-align: left;">
          <td></td>
          <td colspan="8">TERMS: DEPOSIT / BALANCE PRIOR SHIPMENT</td>
        </tr>
        <tr style="background-color: #f3f3f3;text-align: left;">
          <td></td>
          <td colspan="8">TOTAL SAYS</td>
        </tr>
      </table>
      <br>
      <br>
      <table cellpadding="3" cellspacing="1" style="font-size: xx-small;">
        <tr>
          <td colspan="3">
            Quantity Pieces (IN PACKAGES AND SEVERAL PARTS) QUANTIDADES EM NUMERO DE VOLUMES: '.number_format($a).' PCS IN '.number_format($b).' CARTONS<br>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            Total Amount in dollar: '.'$'.number_format($c,2).'<br>
            Term and condition of payment (INCOTERM) TERMOS E CONDICOES DE PAGAMENTO: FOB<br>
            PAYMENT IN ADVANCE / PAGAMENTO ANTECIPADO
          </td>
          <td>
            Manufacturer Details:<br>
            '.$manuf.'
          </td>
        </tr>
        <tr>
          <td colspan="2">
            COUNTRY OF ORIGIN OF GOODS / PAIS DE ORIGEM: '.$porto.'<br>
            COUNTRY OF FINAL DESTINATION / PAIS DE DESTINO: '.$portd.'<br>
            COUNTRY WHERE GOODS WERE SHIPPED / PAIS EM QUE A CARGA FOI EMBARCADA: '.$porto.'<br>
            COUNTRY WHERE GOODS WERE PURCHASED / PAIS EM QUE A CARGA FOI COMPRADA: '.$portd.'<br>
            CARRIAGE BY: SEA / TRANSPORTE: '.$agent.'
          </td>
          <td>
            Total G.W : '.number_format($model->TotalGrossWeight,2).'<br>
            Total N.W : '.number_format($model->TotalNettWeight,2).'<br>
            Total MEAS :
          </td>
        </tr>
        <tr>
          <td>
            SHIPPERs BANK DETAILS:<br>
            Name of the Bank: PT. Bank Mandiri (PERSERO)<br>
            City / Country: BALI - INDONESIA<br>
            Account name: CV. Casa Bali<br>
            USD ACCOUNT NO: 175-00-0046970-9<br>
            SWIFT CODE: B M R I I D J A
          </td>
          <td colspan="2">
            There are no dealers nor commission / sem intermediarios ou comissoes<br>
            Net prices for wholesale condition, there are no discount / precos e condicoes de atacado<br>
            There above prices are FOB JAKARTA / precos FOB JAKARTA<br>
            The above price follow the current international market / precos seguem valor de mercado internacional<br>
            Container No: '.$container.' / Seal: '.$seal.'
          </td>
        </tr>
      </table>
      ';

      // output the HTML content
      $pdf->writeHTML($html, true, false, true, false, '');

      // reset pointer to the last page
      $pdf->lastPage();

      //Close and output PDF document
      $pdf->Output('proforma_invoice_'.$model->PORefCode, 'I');
    }

    public function actionPoPdf($id){
      $model = $this->loadModel($id);

      // create new PDF document
      $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      // set document information
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Indovitri');
      $pdf->SetTitle('Purchase Order');
      $pdf->SetSubject('Purchase Order');

      // set header and footer fonts
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      // set default monospaced font
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
      // set margins
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
      // set auto page breaks
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
      // set image scale factor
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

      // set font
      $pdf->SetFont('dejavusans', '', 10);
      // add a page
      $pdf->AddPage();

      $cons = '';
      $consignee = Consignee::model()->findByPk($model->ConsigneeCode);
      if($consignee != null){
          $cons .= $consignee->Name.'<br>';
          $cons .= $consignee->Address.' '.$consignee->PostCode.'<br>';

          if($consignee->city != null){
              $city = $consignee->city;
              if($city->state != null){
                  $state = $city->state;
                  if($state->country != null){
                      $country = $state->country;

                      $cons .= $city->CityName.' - '.$state->StateName.' - '.$country->CountryName.'<br>';
                  }
              }
          }
      }

      $notif = '';
      $consignee = Consignee::model()->findByPk($model->NotifyPartyCode);
      if($consignee != null){
          $notif .= $consignee->Name.'<br>';
          $notif .= $consignee->Address.' '.$consignee->PostCode.'<br>';

          if($consignee->city != null){
              $city = $consignee->city;
              if($city->state != null){
                  $state = $city->state;
                  if($state->country != null){
                      $country = $state->country;

                      $notif .= $city->CityName.' - '.$state->StateName.' - '.$country->CountryName.'<br>';
                  }
              }
          }
      }

      $manuf = '';
      if($model->seller !== null){
          $manuf .= $model->seller->Name;
          $manuf .= $model->seller->Address;
          $manuf .= MyHelper::getCityDetail($model->seller->CityCode).' '.$model->seller->PostCode;
      }

      $list = '';
      $no = 1;
      $a = 0;
      $b = 0;
      $c = 0;
      $d = 0;
      $e = 0;
      $f = 0;
      foreach($model->details as $detail) {
          if($detail->product != null){
              $product = $detail->product;

              $smallQTY = 0;
              if($product->uom->define->QTYUOMSmall != null){
                  $smallQTY = $product->uom->define->QTYUOMSmall;
              }

              $totalPrice = $smallQTY * $product->Price;
              $totalNW = $smallQTY * $product->NettWeight;
              $totalGW = $smallQTY * $product->GrossWeight;
              $totalQTY = $smallQTY * $detail->QTY;

              $list .= '<tr>';
              $list .= '<td>'.$no.'</td>';
              $list .= '<td>'.$product->ProductName.'</td>';
              $list .= '<td>'.$product->ProductID.'</td>';
              $list .= '<td>'.number_format($smallQTY).'</td>';
              $list .= '<td>'.number_format($detail->QTY).'</td>';
              $list .= '<td>'.$product->uom->UOMName.'</td>';
              $list .= '<td>'.number_format($totalQTY).'</td>';
              $list .= '<td></td>';
              $list .= '<td>$'.number_format($totalPrice,2).'</td>';
              $list .= '<td>'.number_format($product->GrossWeight,2).'</td>';
              $list .= '<td>'.number_format($totalGW,2).'</td>';
              $list .= '<td>'.number_format($product->NettWeight,2).'</td>';
              $list .= '<td>'.number_format($totalNW,2).'</td>';
              $list .= '<td>'.number_format($product->VolumeGross,3).'</td>';
              $list .= '<td>'.number_format($detail->TotalVolume,2).'</td>';
              $list .= '</tr>';
          }

          $a+=$detail->QTY;
          $b+=$totalQTY;
          $c+=$totalPrice;
          $d+=$totalGW;
          $e+=$totalNW;
          $f+=$detail->TotalVolume;

          $no++;
      }

      // create some HTML content
      $html = '
      <table cellpadding="3" cellspacing="1" style="font-size: xx-small">
        <tr>
            <td colspan="7" style="text-align: center;"><h1>Purchase Order</h1></td>
        </tr>
        <tr>
          <td colspan="7"></td>
        </tr>
        <tr>
            <td>Date:</td>
            <td colspan="6">'.date('d-F-Y').'</td>
        </tr>
        <tr>
            <td>Ref:</td>
            <td colspan="6">'.$model->PORefCode.'</td>
        </tr>
        <tr>
          <td colspan="7"></td>
        </tr>
        <tr>
          <td>Shipper:</td>
          <td colspan="2">
            CV. CASA BALI<br>
            WIRA BHUANA DALUNG PERMAI BLOK X2 NO. 17<br>
            LINK. WIRA BHUANA, KEL. KEROBOKAN KAJA<br>
            KEC. KUTA UTARA, KAB. BADUNG, 80361<br>
            BALI - INDONESIA<br>
          </td>
          <td>Consignee:</td>
          <td colspan="2">'.$cons.'</td>
          <td colspan="2">
              TOTAL G.W: '.number_format($model->TotalGrossWeight,2).'<br>
              TOTAL N.W: '.number_format($model->TotalNettWeight,2).'<br>
              TOTAL MEAS: <br>
          </td>
        </tr>
        <tr>
          <td colspan="7"></td>
        </tr>
        <tr>
          <td>Origin:</td>
          <td>'.MyHelper::getCityCountry($model->POLCode).'</td>
          <td>Notify Party:</td>
          <td colspan="2">'.$notif.'</td>
          <td>Manfacturer Details:</td>
          <td>'.$manuf.'</td>
        </tr>
        <tr>
          <td>Destination:</td>
          <td colspan="3">'.MyHelper::getCityCountry($model->PODCode).'</td>
          <td colspan="3" style="text-align: right;">
          Delivery Date: 15 days from order confirmation
          </td>
        </tr>
      </table>
      <br>
      <br>
      <br>
      <table cellpadding="3" cellspacing="1" style="font-size: xx-small; text-align: center;">
        <tr style="background-color: #f3f3f3;">
          <th>Seq</th>
          <th>Description</th>
          <th>Product Code</th>
          <th>Unit per Carton</th>
          <th>Qty Carton</th>
          <th>Unit SET/PC</th>
          <th>Qty Units</th>
          <th>FOB Price USD Unit</th>
          <th>Total USD</th>
          <th>G.W (kgs)</th>
          <th>G.W Total</th>
          <th>N.W (kgs)</th>
          <th>N.W Total</th>
          <th>CBM</th>
          <th>CBM Total</th>
        </tr>
        '.$list.'
        <tr style="background-color: #f3f3f3;">
          <td></td>
          <td colspan="3">Grand Total FOB</td>
          <td>'.$a.'</td>
          <td></td>
          <td>'.$b.'</td>
          <td></td>
          <td>'.$c.'</td>
          <td></td>
          <td>'.$d.'</td>
          <td></td>
          <td>'.$e.'</td>
          <td></td>
          <td>'.$f.'</td>
        </tr>
        <tr style="background-color: #f3f3f3;">
          <td></td>
          <td colspan="3"></td>
          <td>Cartons</td>
          <td></td>
          <td>Units</td>
          <td></td>
          <td>USD</td>
          <td></td>
          <td>Kg</td>
          <td></td>
          <td>Kg</td>
          <td></td>
          <td>CBM</td>
        </tr>
        <tr>
          <td colspan="15" style="text-align: left;">*** Labels separately provided in container</td>
        </tr>
      </table>
      ';

      // output the HTML content
      $pdf->writeHTML($html, true, false, true, false, '');

      // reset pointer to the last page
      $pdf->lastPage();

      //Close and output PDF document
      $pdf->Output('purchase_order_'.$model->PORefCode, 'I');
    }

    public function actionOciPdf($id){
      $model = $this->loadModel($id);

      // create new PDF document
      $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      // set document information
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Indovitri');
      $pdf->SetTitle('Order Confirmation Images');
      $pdf->SetSubject('Order Confirmation Images');

      // set header and footer fonts
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      // set default monospaced font
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
      // set margins
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
      // set auto page breaks
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
      // set image scale factor
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

      // set font
      $pdf->SetFont('dejavusans', '', 10);
      // add a page
      $pdf->AddPage();

      $list = '';
      if($model->blContainer != null){
          $no=1;
          foreach($model->blContainer as $bl) {
              if($bl->container != null){
                  $list .= '<tr>';
                  $list .= '<td colspan="3">CONTAINER LOAD #'.$no.' - '.$bl->container->ContainerName.'</td>';
                  $list .= '</tr>';

                  if($model->details != null){
                      $no2 = 1;
                      foreach($model->details as $detail){
                          if($detail->ContainerID = $bl->ContainerCode){
                              if($detail->product != null){
                                  $list .= '<tr>';
                                  $list .= '<td style="text-align: center;">'.$no2.'</td>';
                                  $list .= '<td>'.$detail->product->ProductID.' '.$detail->product->ProductName.'</td>';
                                  $list .= '<td></td>';
                                  $list .= '</tr>';
                              }
                          }

                          $no2++;
                      }
                  }
              }
              else{
                  $list .= '<tr>';
                  $list .= '<td colspan="3">Failed load container detail</td>';
                  $list .= '</tr>';
              }

              $no++;
          }
      }

      // create some HTML content
      $html = '
      <table cellpadding="3" cellspacing="1">
        <tr>
            <td colspan="3" style="text-align: center;"><h1>Product Images</h1></td>
        </tr>
        <tr>
          <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="3">'.$model->BillCodeSupplier.'</td>
        </tr>
        <tr style="background-color: #f3f3f3">
            <td>SEQ</td>
            <td>ITEM</td>
            <td>IMAGES</td>
        </tr>
        '.$list.'
      </table>
      ';

      // output the HTML content
      $pdf->writeHTML($html, true, false, true, false, '');

      // reset pointer to the last page
      $pdf->lastPage();

      //Close and output PDF document
      $pdf->Output('confirmation_images_'.$model->BillCodeSupplier, 'I');
    }

    public function actionOcsPdf($id){
      $model = $this->loadModel($id);

      // create new PDF document
      $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      // set document information
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Indovitri');
      $pdf->SetTitle('Order Summary');
      $pdf->SetSubject('Order Summary');

      // set header and footer fonts
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      // set default monospaced font
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
      // set margins
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
      // set auto page breaks
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
      // set image scale factor
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

      // set font
      $pdf->SetFont('dejavusans', '', 10);
      // add a page
      $pdf->AddPage();

      $list = '';
      $total = 0;
      if($model->charges != null){
          foreach($model->charges as $detail) {
              if($detail->master != null){
                  $list .= '<tr>';
                  $list .= '<td>'.$detail->master->Name.'</td>';
                  $list .= '<td style="text-align: right;">$'.number_format($detail->Value).'</td>';
                  $list .= '</tr>';

                  $total+=$detail->Value;
              }
              else{
                  $list .= '<tr>';
                  $list .= '<td colspan="2">Failed load order charge detail</td>';
                  $list .= '</tr>';
              }
          }
      }

      $terms = '';
      if($model->terms != null){
          foreach($model->terms as $term){
              $terms .= '<tr>';
              $terms .= '<td>$'.number_format($term->PercentageofPayment).'</td>';
              $terms .= '<td style="text-align: right;">$'.number_format($term->ValuePercentageOfPayment).'</td>';
              $terms .= '</tr>';
          }
      }

      // create some HTML content
      $html = '
      <h1>Summary</h1>
      <table cellpadding="3" cellspacing="1">
        <tr>
            <td colspan="2">REFERENCE: '.$model->BillCodeSupplier.'</td>
        </tr>
        <tr style="background-color: #f3f3f3">
            <td colspan="2">CUSTO INDONESIA (USD)</td>
        </tr>
        <tr>
          <td>Taxas do banco de origem:</td>
          <td style="text-align: right;">Origem</td>
        </tr>
        '.$list.'
        <tr style="font-align: center;background-color: #f3f3f3;">
            <td colspan="2">SUMMARY</td>
        </tr>
        <tr style="font-align: center;background-color: #f3f3f3;">
            <td>Total Order FOB - Product</td>
            <td style="text-align: right;">$'.number_format($model->OrderTotal).'</td>
        </tr>
        <tr style="font-align: center;background-color: #f3f3f3;">
            <td>Total Cost Indonesia</td>
            <td style="text-align: right;">$'.number_format($total).'</td>
        </tr>
        <tr style="font-align: center;background-color: #f3f3f3;">
            <td>Total Sale Amount</td>
            <td style="text-align: right;">$'.number_format($model->OrderTotal + $total).'</td>
        </tr>
        <tr>
          <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2">Terms:</td>
        </tr>
        '.$terms.'
        <tr>
            <td>Ocean Freight from IndoVitri</td>
            <td style="text-align: right;">$'.number_format($model->FreightFee).'</td>
        </tr>
        <tr>
          <td colspan="2"></td>
        </tr>
        <tr>
          <td>Total G.W</td>
          <td style="text-align: right;">'.number_format($model->TotalGrossWeight, 2).'</td>
        </tr>
        <tr>
          <td>Total N.W</td>
          <td style="text-align: right;">'.number_format($model->TotalNettWeight, 2).'</td>
        </tr>
        <tr>
          <td>Total MEAS</td>
          <td></td>
        </tr>
        <tr>
          <td colspan="2"></td>
        </tr>
        <tr>
          <td colspan="2">2 x 40NOR SHIPPERs LOAD AND COUNT</td>
        </tr>
      </table>
      ';

      // output the HTML content
      $pdf->writeHTML($html, true, false, true, false, '');

      // reset pointer to the last page
      $pdf->lastPage();

      //Close and output PDF document
      $pdf->Output('summary_charge_'.$model->BillCodeSupplier, 'I');
    }

    public function actionPl($id){
        $model = $this->loadModel($id);

        $filePath = Yii::getPathOfAlias('site.uploads.downloads');
        $phpExcelPath = Yii::getPathOfAlias('site.vendors.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("p5trade.com");
        $objPHPExcel->getProperties()->setTitle("Packing List ".$model->BillCodeSupplier);
        $objWorkSheet = $objPHPExcel->createSheet(0);

        $colorGrey = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'CCCCCC')
            )
        );

        $fontCenter = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $fontRight = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );

        $fontLeft = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:O1');
        $objWorkSheet->SetCellValue('A1', 'PACKING LIST');
        $objWorkSheet->getStyle("A1:O1")->applyFromArray($fontCenter);

        $objWorkSheet->SetCellValue('A3', 'Date:');
        $objWorkSheet->SetCellValue('B3', date('d-F-Y'));

        $objWorkSheet->SetCellValue('A4', 'Ref:');
        $objWorkSheet->SetCellValue('B4', $model->BillCodeSupplier);

        $objWorkSheet->SetCellValue('A6', 'Shipper:');
        $objWorkSheet->SetCellValue('B6', 'CV. CASA BALI');
        $objWorkSheet->SetCellValue('B7', 'WIRA BHUANA DALUNG PERMAI BLOK X2 NO. 17');
        $objWorkSheet->SetCellValue('B8', 'LINK. WIRA BHUANA, KEL. KEROBOKAN KAJA');
        $objWorkSheet->SetCellValue('B9', 'KEC. KUTA UTARA, KAB. BADUNG, 80361');
        $objWorkSheet->SetCellValue('B10', 'BALI - INDONESIA');

        $objWorkSheet->SetCellValue('A12', 'Origin:');
        $objWorkSheet->SetCellValue('B12', MyHelper::getCityCountry($model->POLCode));

        $objWorkSheet->SetCellValue('A14', 'Destination:');
        $objWorkSheet->SetCellValue('B14', MyHelper::getCityCountry($model->PODCode));

        $objWorkSheet->SetCellValue('D6', 'Consignee:');
        $consignee = Consignee::model()->findByPk($model->ConsigneeCode);
        if($consignee != null){
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E6:G6');
            $objWorkSheet->SetCellValue('E6', $consignee->Name);

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E7:G7');
            $objWorkSheet->SetCellValue('E7', $consignee->Address.' '.$consignee->PostCode);
            if($consignee->city != null){
                $city = $consignee->city;
                if($city->state != null){
                    $state = $city->state;
                    if($state->country != null){
                        $country = $state->country;

                        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E8:G8');
                        $objWorkSheet->SetCellValue('E8', $city->CityName.' - '.$state->StateName.' - '.$country->CountryName);
                    }
                }
            }
        }

        $objWorkSheet->SetCellValue('D12', 'Notify Party:');
        $notify = Consignee::model()->findByPk($model->NotifyPartyCode);
        if($notify != null){
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E12:G12');
            $objWorkSheet->SetCellValue('E12', $notify->Name);

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E13:G13');
            $objWorkSheet->SetCellValue('E13', $notify->Address.' '.$notify->PostCode);
            if($notify->city != null){
                $city = $notify->city;
                if($city->state != null){
                    $state = $city->state;
                    if($state->country != null){
                        $country = $state->country;

                        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E14:G14');
                        $objWorkSheet->SetCellValue('E14', $city->CityName.' - '.$state->StateName.' - '.$country->CountryName);
                    }
                }
            }
        }

        $objWorkSheet->SetCellValue('I6', 'TOTAL G.W');
        $objWorkSheet->SetCellValue('I7', 'TOTAL N.W');
        $objWorkSheet->SetCellValue('I8', 'TOTAL MEAS');

        $objWorkSheet->SetCellValue('A16', 'Delivery Date:');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B16:C16');
        $objWorkSheet->SetCellValue('B16', '15 days from order confirmation');

        $objWorkSheet->SetCellValue('A18', 'Seq');
        $objWorkSheet->SetCellValue('B18', 'Description');
        $objWorkSheet->SetCellValue('C18', 'Product Code');
        $objWorkSheet->SetCellValue('D18', 'Unit per Carton');
        $objWorkSheet->SetCellValue('E18', 'Qty Cartons');
        $objWorkSheet->SetCellValue('F18', 'Unit Per Carton');
        $objWorkSheet->SetCellValue('G18', 'Qty Units');
        $objWorkSheet->SetCellValue('H18', 'G.W(kgs)');
        $objWorkSheet->SetCellValue('I18', 'G.W Total');
        $objWorkSheet->SetCellValue('J18', 'N.W(kgs)');
        $objWorkSheet->SetCellValue('K18', 'N.W Total');
        $objWorkSheet->SetCellValue('L18', 'CBM');
        $objWorkSheet->SetCellValue('M18', 'CBM Total');

        $objWorkSheet->getStyle("A18:M18")->applyFromArray($colorGrey);
        $objWorkSheet->getStyle("A18:M18")->applyFromArray($fontCenter);

        // set autowidth
        for($col = 'A'; $col !== 'Z'; $col++) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        $no = 1;
        $row = 19;
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;
        $e = 0;
        foreach($model->details as $detail) {
            $objWorkSheet->SetCellValue("A".$row, $no);
            if($detail->product != null){
                $product = $detail->product;

                $smallQTY = 0;
                if($product->uom->define->QTYUOMSmall != null){
                    $smallQTY = $product->uom->define->QTYUOMSmall;
                }

                $totalPrice = $smallQTY * $product->Price;
                $totalNW = $smallQTY * $product->NettWeight;
                $totalGW = $smallQTY * $product->GrossWeight;
                $totalQTY = $smallQTY * $detail->QTY;

                $objWorkSheet->SetCellValue("B".$row, $product->ProductName);
                $objWorkSheet->SetCellValue("C".$row, $product->ProductID);
                $objWorkSheet->SetCellValue("D".$row, number_format($smallQTY));
                $objWorkSheet->SetCellValue("E".$row, number_format($detail->QTY));
                $objWorkSheet->SetCellValue("F".$row, $product->uom->UOMName);
                $objWorkSheet->SetCellValue("G".$row, number_format($totalQTY));
                $objWorkSheet->SetCellValue("H".$row, number_format($product->GrossWeight,2));
                $objWorkSheet->SetCellValue("I".$row, number_format($totalGW,2));
                $objWorkSheet->SetCellValue("J".$row, number_format($product->NettWeight,2));
                $objWorkSheet->SetCellValue("K".$row, number_format($totalNW,2));
                $objWorkSheet->SetCellValue("L".$row, number_format($product->VolumeGross,3));
                $objWorkSheet->SetCellValue("M".$row, number_format($detail->TotalVolume,2));

                $objWorkSheet->getStyle("A".$row)->applyFromArray($fontCenter);
                $objWorkSheet->getStyle("C".$row.":M".$row)->applyFromArray($fontCenter);
            }

            $a+=$detail->QTY;
            $b+=$totalQTY;
            $c+=$totalGW;
            $d+=$totalNW;
            $e+=$detail->TotalVolume;

            $no++;
            $row++;
        }
        $objWorkSheet->SetCellValue("B".$row, 'Grand Total FOB');
        $objWorkSheet->SetCellValue("E".$row, $a);
        $objWorkSheet->SetCellValue("G".$row, $b);
        $objWorkSheet->SetCellValue("I".$row, $c);
        $objWorkSheet->SetCellValue("K".$row, $d);
        $objWorkSheet->SetCellValue("M".$row, $e);

        $objWorkSheet->getStyle("A".$row.":M".$row)->applyFromArray($colorGrey);
        $objWorkSheet->getStyle("E".$row.":M".$row)->applyFromArray($fontCenter);

        $row2 = $row+2;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row2.":C".$row2);
        $objWorkSheet->SetCellValue("A".$row2, "1 X 40NOR SHIPPER'S LOAD AND COUNT");

        $row3 = $row2+2;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row3.":G".$row3);
        $objWorkSheet->SetCellValue("A".$row3, 'Quantity Pieces (IN PACKAGES AND SEVERAL PARTS) QUANTIDADES EM NUMERO DE VOLUMES: '.number_format($a).' PCS IN '.number_format($b).' CARTONS');

        $row4 = $row3+2;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row4.":C".$row4);
        $porto = '';
        if($model->pol !== null){
            $porto = MyHelper::getCityCountry($model->pol->CityCode);
        }
        $objWorkSheet->SetCellValue("A".$row4, 'COUNTRY OF ORIGIN OF GOODS / PAIS DE ORIGEM: '.$porto);

        $row5 = $row4+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row5.":C".$row5);
        $portd = '';
        if($model->pod !== null){
            $portd = MyHelper::getCityCountry($model->pod->CityCode);
        }
        $objWorkSheet->SetCellValue("A".$row5, 'COUNTRY OF FINAL DESTINATION / PAIS DE DESTINO: '.$portd);

        $row6 = $row5+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row6.":C".$row6);
        $objWorkSheet->SetCellValue("A".$row6, 'COUNTRY WHERE GOODS WERE SHIPPED / PAIS EM QUE A CARGA FOI EMBARCADA: '.$porto);

        $row7 = $row6+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row7.":C".$row7);
        $objWorkSheet->SetCellValue("A".$row7, 'COUNTRY WHERE GOODS WERE PURCHASED / PAIS EM QUE A CARGA FOI COMPRADA: '.$portd);

        $row8 = $row7+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row8.":C".$row8);
        $agent = '';
        if($model->agentOrigin !== null){
            $agent = $model->agentOrigin->Name;
        }
        $objWorkSheet->SetCellValue("A".$row8, 'CARRIAGE BY: SEA / TRANSPORTE: '.$agent);

        $row9 = $row8+2;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row9.":G".$row9);
        $objWorkSheet->SetCellValue("A".$row9, 'There are no dealers nor commission / sem intermediarios ou comissoes');

        $row10 = $row9+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row10.":G".$row10);
        $objWorkSheet->SetCellValue("A".$row10, 'Net prices for wholesale condition, there are no discount / precos e condicoes de atacado');

        $row11 = $row10+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row11.":G".$row11);
        $objWorkSheet->SetCellValue("A".$row11, 'There above prices are FOB JAKARTA / precos FOB JAKARTA');

        $row12 = $row11+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row12.":G".$row12);
        $objWorkSheet->SetCellValue("A".$row12, 'The above price follow the current international market / precos seguem valor de mercado internacional');

        $row13 = $row12+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row13.":G".$row13);
        $containerArr = array();
        $sealArr = array();
        $container = '';
        $seal = '';
        if($model->blContainer !== null){
            foreach($model->blContainer as $bl){
                array_push($containerArr, $bl->NumberContainer);
                array_push($sealArr, $bl->SealCode);
            }

            $container = implode(' / ',$containerArr);
            $seal = implode(' / ',$sealArr);
        }
        $objWorkSheet->SetCellValue("A".$row13, 'Container No: '.$container.' / Seal: '.$seal);

        $row14 = $row2+2;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("H".$row14.":I".$row14);
        $objWorkSheet->SetCellValue("H".$row14, 'Manufacturer Details:');

        $sellerName = '';
        $sellerAddress = '';
        $sellerDetail = '';
        if($model->seller !== null){
            $sellerName = $model->seller->Name;
            $sellerAddress = $model->seller->Address;
            $sellerDetail = MyHelper::getCityDetail($model->seller->CityCode).' '.$model->seller->PostCode;
        }
        $row15 = $row14+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("H".$row15.":K".$row15);
        $objWorkSheet->SetCellValue("H".$row15, $sellerName);

        $row16 = $row15+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("H".$row16.":K".$row16);
        $objWorkSheet->SetCellValue("H".$row16, $sellerAddress);

        $row17 = $row16+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("H".$row17.":K".$row17);
        $objWorkSheet->SetCellValue("H".$row17, $sellerDetail);

        $objWorkSheet->setTitle($model->BillCodeSupplier);

        $fileName = 'packing_list_'.$model->BillCodeSupplier.'.xls';
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

        // save ke local
        $objWriter->save($filePath.'/'.$fileName);

        // download ke client
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $objWriter->save('php://output');

        // Once we have finished using the library, give back the
        // power to Yii...
        spl_autoload_register(array('YiiBase','autoload'));

        return $filePath.'/'.$fileName;
    }

    public function actionPo($id){
        $model = $this->loadModel($id);

        $filePath = Yii::getPathOfAlias('site.uploads.downloads');
        $phpExcelPath = Yii::getPathOfAlias('site.vendors.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("p5trade.com");
        $objPHPExcel->getProperties()->setTitle("Purchase Order ".$model->PORefCode);
        $objWorkSheet = $objPHPExcel->createSheet(0);

        $colorGrey = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'CCCCCC')
            )
        );

        $fontCenter = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $fontRight = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );

        $fontLeft = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:O1');
        $objWorkSheet->SetCellValue('A1', 'PURCHASE ORDER');
        $objWorkSheet->getStyle("A1:O1")->applyFromArray($fontCenter);

        $objWorkSheet->SetCellValue('A3', 'Date:');
        $objWorkSheet->SetCellValue('B3', date('d-F-Y'));

        $objWorkSheet->SetCellValue('A4', 'Ref:');
        $objWorkSheet->SetCellValue('B4', $model->PORefCode);

        $objWorkSheet->SetCellValue('A6', 'Shipper:');
        $objWorkSheet->SetCellValue('B6', 'CV. CASA BALI');
        $objWorkSheet->SetCellValue('B7', 'WIRA BHUANA DALUNG PERMAI BLOK X2 NO. 17');
        $objWorkSheet->SetCellValue('B8', 'LINK. WIRA BHUANA, KEL. KEROBOKAN KAJA');
        $objWorkSheet->SetCellValue('B9', 'KEC. KUTA UTARA, KAB. BADUNG, 80361');
        $objWorkSheet->SetCellValue('B10', 'BALI - INDONESIA');

        $objWorkSheet->SetCellValue('A12', 'Origin:');
        $objWorkSheet->SetCellValue('B12', MyHelper::getCityCountry($model->POLCode));

        $objWorkSheet->SetCellValue('A14', 'Destination:');
        $objWorkSheet->SetCellValue('B14', MyHelper::getCityCountry($model->PODCode));

        $objWorkSheet->SetCellValue('D6', 'Consignee:');
        $consignee = Consignee::model()->findByPk($model->ConsigneeCode);
        if($consignee != null){
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E6:G6');
            $objWorkSheet->SetCellValue('E6', $consignee->Name);

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E7:G7');
            $objWorkSheet->SetCellValue('E7', $consignee->Address.' '.$consignee->PostCode);
            if($consignee->city != null){
                $city = $consignee->city;
                if($city->state != null){
                    $state = $city->state;
                    if($state->country != null){
                        $country = $state->country;

                        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E8:G8');
                        $objWorkSheet->SetCellValue('E8', $city->CityName.' - '.$state->StateName.' - '.$country->CountryName);
                    }
                }
            }
        }

        $objWorkSheet->SetCellValue('D12', 'Notify Party:');
        $notify = Consignee::model()->findByPk($model->NotifyPartyCode);
        if($notify != null){
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E12:G12');
            $objWorkSheet->SetCellValue('E12', $notify->Name);

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E13:G13');
            $objWorkSheet->SetCellValue('E13', $notify->Address.' '.$notify->PostCode);
            if($notify->city != null){
                $city = $notify->city;
                if($city->state != null){
                    $state = $city->state;
                    if($state->country != null){
                        $country = $state->country;

                        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E14:G14');
                        $objWorkSheet->SetCellValue('E14', $city->CityName.' - '.$state->StateName.' - '.$country->CountryName);
                    }
                }
            }
        }

        $objWorkSheet->SetCellValue('I6', 'TOTAL G.W');
        $objWorkSheet->SetCellValue('I7', 'TOTAL N.W');
        $objWorkSheet->SetCellValue('I8', 'TOTAL MEAS');

        $objWorkSheet->SetCellValue('I10', 'Delivery Date:');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J10:l10');
        $objWorkSheet->SetCellValue('J10', '15 days from order confirmation');

        $objWorkSheet->SetCellValue('I12', 'Manufacturer Details:');
        $sellerName = '';
        $sellerAddress = '';
        $sellerDetail = '';
        if($model->seller !== null){
            $sellerName = $model->seller->Name;
            $sellerAddress = $model->seller->Address;
            $sellerDetail = MyHelper::getCityDetail($model->seller->CityCode).' '.$model->seller->PostCode;
        }
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J12:M12');
        $objWorkSheet->SetCellValue('J12', $sellerName);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J13:M13');
        $objWorkSheet->SetCellValue('J13', $sellerAddress);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J14:M14');
        $objWorkSheet->SetCellValue('J14', $sellerDetail);

        $objWorkSheet->SetCellValue('A18', 'Seq');
        $objWorkSheet->SetCellValue('B18', 'Description');
        $objWorkSheet->SetCellValue('C18', 'Product Code');
        $objWorkSheet->SetCellValue('D18', 'Unit per Carton');
        $objWorkSheet->SetCellValue('E18', 'Qty Cartons');
        $objWorkSheet->SetCellValue('F18', 'Unit SET/PC');
        $objWorkSheet->SetCellValue('G18', 'Qty Units');
        $objWorkSheet->SetCellValue('H18', 'FOB Price USD Unit');
        $objWorkSheet->SetCellValue('I18', 'Total USD');
        $objWorkSheet->SetCellValue('J18', 'G.W(kgs)');
        $objWorkSheet->SetCellValue('K18', 'G.W Total');
        $objWorkSheet->SetCellValue('L18', 'N.W(kgs)');
        $objWorkSheet->SetCellValue('M18', 'N.W Total');
        $objWorkSheet->SetCellValue('N18', 'CBM');
        $objWorkSheet->SetCellValue('O18', 'CBM Total');

        $objWorkSheet->getStyle("A18:O18")->applyFromArray($colorGrey);
        $objWorkSheet->getStyle("A18:O18")->applyFromArray($fontCenter);

        // set autowidth
        for($col = 'A'; $col !== 'Z'; $col++) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        $no = 1;
        $row = 19;
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;
        $e = 0;
        $f = 0;
        foreach($model->details as $detail) {
            $objWorkSheet->SetCellValue("A".$row, $no);
            if($detail->product != null){
                $product = $detail->product;

                $smallQTY = 0;
                if($product->uom->define->QTYUOMSmall != null){
                    $smallQTY = $product->uom->define->QTYUOMSmall;
                }

                $totalPrice = $smallQTY * $product->Price;
                $totalNW = $smallQTY * $product->NettWeight;
                $totalGW = $smallQTY * $product->GrossWeight;
                $totalQTY = $smallQTY * $detail->QTY;

                $objWorkSheet->SetCellValue("B".$row, $product->ProductName);
                $objWorkSheet->SetCellValue("C".$row, $product->ProductID);
                $objWorkSheet->SetCellValue("D".$row, number_format($smallQTY));
                $objWorkSheet->SetCellValue("E".$row, number_format($detail->QTY));
                $objWorkSheet->SetCellValue("F".$row, $product->uom->UOMName);
                $objWorkSheet->SetCellValue("G".$row, number_format($totalQTY));
                $objWorkSheet->SetCellValue("H".$row, '');
                $objWorkSheet->SetCellValue("I".$row, '$'.number_format($totalPrice,2));
                $objWorkSheet->SetCellValue("J".$row, number_format($product->GrossWeight,2));
                $objWorkSheet->SetCellValue("K".$row, number_format($totalGW,2));
                $objWorkSheet->SetCellValue("L".$row, number_format($product->NettWeight,2));
                $objWorkSheet->SetCellValue("M".$row, number_format($totalNW,2));
                $objWorkSheet->SetCellValue("N".$row, number_format($product->VolumeGross,3));
                $objWorkSheet->SetCellValue("O".$row, number_format($detail->TotalVolume,2));

                $objWorkSheet->getStyle("A".$row)->applyFromArray($fontCenter);
                $objWorkSheet->getStyle("C".$row.":O".$row)->applyFromArray($fontCenter);
            }

            $a+=$detail->QTY;
            $b+=$totalQTY;
            $c+=$totalPrice;
            $d+=$totalGW;
            $e+=$totalNW;
            $f+=$detail->TotalVolume;

            $no++;
            $row++;
        }
        $objWorkSheet->SetCellValue("B".$row, 'Grand Total FOB');
        $objWorkSheet->SetCellValue("E".$row, $a);
        $objWorkSheet->SetCellValue("G".$row, $b);
        $objWorkSheet->SetCellValue("I".$row, $c);
        $objWorkSheet->SetCellValue("K".$row, $d);
        $objWorkSheet->SetCellValue("M".$row, $e);
        $objWorkSheet->SetCellValue("O".$row, $f);

        $row2 = $row+1;
        $row3 = $row2+1;

        $objWorkSheet->SetCellValue("E".$row2, 'Cartons');
        $objWorkSheet->SetCellValue("G".$row2, 'Units');
        $objWorkSheet->SetCellValue("I".$row2, 'US Dollar');
        $objWorkSheet->SetCellValue("K".$row2, 'Kg');
        $objWorkSheet->SetCellValue("M".$row2, 'Kg');
        $objWorkSheet->SetCellValue("O".$row2, 'CBM');

        $objWorkSheet->getStyle("A".$row.":O".$row2)->applyFromArray($colorGrey);
        $objWorkSheet->getStyle("E".$row.":O".$row2)->applyFromArray($fontCenter);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row3.":O".$row3);
        $objWorkSheet->SetCellValue("A".$row3, '*** Labels separately provided in container');

        $objWorkSheet->setTitle($model->PORefCode);

        $fileName = 'purchase_order_'.$model->PORefCode.'.xlsx';
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

        // save ke local
        $objWriter->save($filePath.'/'.$fileName);

        // download ke client
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $objWriter->save('php://output');

        // Once we have finished using the library, give back the
        // power to Yii...
        spl_autoload_register(array('YiiBase','autoload'));

        return $filePath.'/'.$fileName;
    }

    public function actionPi($id){
        $model = $this->loadModel($id);

        $filePath = Yii::getPathOfAlias('site.uploads.downloads');
        $phpExcelPath = Yii::getPathOfAlias('site.vendors.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("p5trade.com");
        $objPHPExcel->getProperties()->setTitle("Proforma Invoice");
        $objWorkSheet = $objPHPExcel->createSheet(0);

        $colorGrey = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'CCCCCC')
            )
        );

        $fontCenter = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $fontRight = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );

        $fontLeft = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');
        $objWorkSheet->SetCellValue('A1', 'PROFORMA INVOICE');
        $objWorkSheet->getStyle('A1:I1')->applyFromArray($fontCenter);

        $objWorkSheet->SetCellValue('A3', 'Date:');
        $objWorkSheet->SetCellValue('B3', date('d-F-Y', strtotime($model->BillSupplierDate)));

        $objWorkSheet->SetCellValue('A4', 'Ref:');
        $objWorkSheet->SetCellValue('B4', $model->BillCodeSupplier);

        $objWorkSheet->SetCellValue('A6', 'Shipper:');
        $objWorkSheet->SetCellValue('B6', 'CV. CASA BALI');
        $objWorkSheet->SetCellValue('B7', 'WIRA BHUANA DALUNG PERMAI BLOK X2 NO. 17');
        $objWorkSheet->SetCellValue('B8', 'LINK. WIRA BHUANA, KEL. KEROBOKAN KAJA');
        $objWorkSheet->SetCellValue('B9', 'KEC. KUTA UTARA, KAB. BADUNG, 80361');
        $objWorkSheet->SetCellValue('B10', 'BALI - INDONESIA');

        $objWorkSheet->SetCellValue('A12', 'Origin:');
        $objWorkSheet->SetCellValue('B12', MyHelper::getCityCountry($model->POLCode));

        $objWorkSheet->SetCellValue('A14', 'Destination:');
        $objWorkSheet->SetCellValue('B14', MyHelper::getCityCountry($model->PODCode));

        $objWorkSheet->SetCellValue('D6', 'Consignee:');
        $consignee = Consignee::model()->findByPk($model->ConsigneeCode);
        if($consignee != null){
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E6:G6');
            $objWorkSheet->SetCellValue('E6', $consignee->Name);

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E7:G7');
            $objWorkSheet->SetCellValue('E7', $consignee->Address.' '.$consignee->PostCode);
            if($consignee->city != null){
                $city = $consignee->city;
                if($city->state != null){
                    $state = $city->state;
                    if($state->country != null){
                        $country = $state->country;

                        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E8:G8');
                        $objWorkSheet->SetCellValue('E8', $city->CityName.' - '.$state->StateName.' - '.$country->CountryName);
                    }
                }
            }
        }

        $objWorkSheet->SetCellValue('D12', 'Notify Party:');
        $notify = Consignee::model()->findByPk($model->NotifyPartyCode);
        if($notify != null){
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E12:G12');
            $objWorkSheet->SetCellValue('E12', $notify->Name);

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E13:G13');
            $objWorkSheet->SetCellValue('E13', $notify->Address.' '.$notify->PostCode);
            if($notify->city != null){
                $city = $notify->city;
                if($city->state != null){
                    $state = $city->state;
                    if($state->country != null){
                        $country = $state->country;

                        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E14:G14');
                        $objWorkSheet->SetCellValue('E14', $city->CityName.' - '.$state->StateName.' - '.$country->CountryName);
                    }
                }
            }
        }

        $objWorkSheet->SetCellValue('A18', 'Seq');
        $objWorkSheet->SetCellValue('B18', 'Description');
        $objWorkSheet->SetCellValue('C18', 'Product Code');
        $objWorkSheet->SetCellValue('D18', 'Unit per Carton');
        $objWorkSheet->SetCellValue('E18', 'Qty Cartons');
        $objWorkSheet->SetCellValue('F18', 'Unit SET/PC');
        $objWorkSheet->SetCellValue('G18', 'Qty Units');
        $objWorkSheet->SetCellValue('H18', 'USD Unit');
        $objWorkSheet->SetCellValue('I18', 'Total USD');

        $objWorkSheet->getStyle('A18:I18')->applyFromArray($colorGrey);
        $objWorkSheet->getStyle('A18:I18')->applyFromArray($fontCenter);

        // set autowidth
        for($col = 'A'; $col !== 'Z'; $col++) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        $no = 1;
        $row = 19;
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;
        $e = 0;
        $f = 0;
        foreach($model->details as $detail) {
            $objWorkSheet->SetCellValue("A".$row, $no);
            if($detail->product != null){
                $product = $detail->product;

                $smallQTY = 0;
                if($product->uom->define->QTYUOMSmall != null){
                    $smallQTY = $product->uom->define->QTYUOMSmall;
                }

                $totalPrice = $smallQTY * $product->Price;
                $totalNW = $smallQTY * $product->NettWeight;
                $totalQTY = $smallQTY * $detail->QTY;

                $objWorkSheet->SetCellValue("B".$row, $product->ProductName);
                $objWorkSheet->SetCellValue("C".$row, $product->ProductID);
                $objWorkSheet->SetCellValue("D".$row, number_format($smallQTY));
                $objWorkSheet->SetCellValue("E".$row, number_format($detail->QTY));
                $objWorkSheet->SetCellValue("F".$row, $product->uom->UOMName);
                $objWorkSheet->SetCellValue("G".$row, number_format($totalQTY));
                $objWorkSheet->SetCellValue("H".$row, '$'.number_format($product->Price,3));
                $objWorkSheet->SetCellValue("I".$row, '$'.number_format($totalPrice,2));
            }

            $objWorkSheet->getStyle("A".$row)->applyFromArray($fontCenter);
            $objWorkSheet->getStyle("C".$row.":I".$row)->applyFromArray($fontCenter);

            $a+=$detail->QTY;
            $b+=$totalQTY;
            $c+=$totalPrice;

            $no++;
            $row++;
        }
        $objWorkSheet->SetCellValue("B".$row, 'Grand Total FOB');
        $objWorkSheet->SetCellValue("E".$row, number_format($a));
        $objWorkSheet->SetCellValue("G".$row, number_format($b));
        $objWorkSheet->SetCellValue("I".$row, '$'.number_format($c,2));

        $objWorkSheet->getStyle("E".$row.":I".$row)->applyFromArray($fontCenter);

        $row2 = $row+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("B".$row2.":I".$row2);
        $objWorkSheet->SetCellValue("B".$row2, 'TERMS: DEPOSIT / BALANCE PRIOR SHIPMENT');
        $row3 = $row2+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("B".$row3.":I".$row3);
        $objWorkSheet->SetCellValue("B".$row3, 'TOTAL SAYS');

        $objWorkSheet->getStyle("A".$row.":I".$row3)->applyFromArray($colorGrey);

        $row3 = $row2+3;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row3.":I".$row3);
        $objWorkSheet->SetCellValue("A".$row3, 'Quantity Pieces (IN PACKAGES AND SEVERAL PARTS) QUANTIDADES EM NUMERO DE VOLUMES: '.number_format($a).' PCS IN '.number_format($b).' CARTONS');

        $row4 = $row3+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row4.":B".$row4);
        $objWorkSheet->SetCellValue("A".$row4, 'Total Amount in dollar: '.'$'.number_format($c,2));

        $row5 = $row4+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row5.":C".$row5);
        $objWorkSheet->SetCellValue("A".$row5, 'Term and condition of payment (INCOTERM) TERMOS E CONDICOES DE PAGAMENTO: FOB');

        $row6 = $row5+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row6.":C".$row6);
        $objWorkSheet->SetCellValue("A".$row6, 'PAYMENT IN ADVANCE / PAGAMENTO ANTECIPADO');

        $row7 = $row6+2;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row7.":C".$row7);
        $porto = '';
        if($model->pol !== null){
            $porto = MyHelper::getCityCountry($model->pol->CityCode);
        }
        $objWorkSheet->SetCellValue("A".$row7, 'COUNTRY OF ORIGIN OF GOODS / PAIS DE ORIGEM: '.$porto);

        $row8 = $row7+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row8.":C".$row8);
        $portd = '';
        if($model->pod !== null){
            $portd = MyHelper::getCityCountry($model->pod->CityCode);
        }
        $objWorkSheet->SetCellValue("A".$row8, 'COUNTRY OF FINAL DESTINATION / PAIS DE DESTINO: '.$portd);

        $row9 = $row8+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row9.":C".$row9);
        $objWorkSheet->SetCellValue("A".$row9, 'COUNTRY WHERE GOODS WERE SHIPPED / PAIS EM QUE A CARGA FOI EMBARCADA: '.$porto);

        $row10 = $row9+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row10.":C".$row10);
        $objWorkSheet->SetCellValue("A".$row10, 'COUNTRY WHERE GOODS WERE PURCHASED / PAIS EM QUE A CARGA FOI COMPRADA: '.$portd);

        $row11 = $row10+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row11.":C".$row11);
        $agent = '';
        if($model->agentOrigin !== null){
            $agent = $model->agentOrigin->Name;
        }
        $objWorkSheet->SetCellValue("A".$row11, 'CARRIAGE BY: SEA / TRANSPORTE: '.$agent);

        $row12 = $row11+2;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row12.":B".$row12);
        $objWorkSheet->SetCellValue("A".$row12, "SHIPPER'S BANK DETAILS:");

        $row13 = $row12+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row13.":B".$row13);
        $objWorkSheet->SetCellValue("A".$row13, 'Name of the Bank: PT. Bank Mandiri (PERSERO)');

        $row14 = $row13+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row14.":B".$row14);
        $objWorkSheet->SetCellValue("A".$row14, 'City / Country: BALI - INDONESIA');

        $row15 = $row14+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row15.":B".$row15);
        $objWorkSheet->SetCellValue("A".$row15, 'Account name: CV. Casa Bali');

        $row16 = $row15+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row16.":B".$row16);
        $objWorkSheet->SetCellValue("A".$row16, 'USD ACCOUNT NO: 175-00-0046970-9');

        $row17 = $row16+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row17.":B".$row17);
        $objWorkSheet->SetCellValue("A".$row17, 'SWIFT CODE: B M R I I D J A');

        $row13 = $row12+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("C".$row13.":I".$row13);
        $objWorkSheet->SetCellValue("C".$row13, 'There are no dealers nor commission / sem intermediarios ou comissoes');

        $row14 = $row13+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("C".$row14.":I".$row14);
        $objWorkSheet->SetCellValue("C".$row14, 'Net prices for wholesale condition, there are no discount / precos e condicoes de atacado');

        $row15 = $row14+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("C".$row15.":I".$row15);
        $objWorkSheet->SetCellValue("C".$row15, 'There above prices are FOB JAKARTA / precos FOB JAKARTA');

        $row16 = $row15+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("C".$row16.":I".$row16);
        $objWorkSheet->SetCellValue("C".$row16, 'The above price follow the current international market / precos seguem valor de mercado internacional');

        $row17 = $row16+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("C".$row17.":I".$row17);
        $containerArr = array();
        $sealArr = array();
        $container = '';
        $seal = '';
        if($model->blContainer !== null){
            foreach($model->blContainer as $bl){
                array_push($containerArr, $bl->NumberContainer);
                array_push($sealArr, $bl->SealCode);
            }

            $container = implode(' / ',$containerArr);
            $seal = implode(' / ',$sealArr);
        }
        $objWorkSheet->SetCellValue("C".$row17, 'Container No: '.$container.' / Seal: '.$seal);

        $row4 = $row3+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("E".$row4.":I".$row4);
        $objWorkSheet->SetCellValue("E".$row4, 'Manufacturer Details:');

        $sellerName = '';
        $sellerAddress = '';
        $sellerDetail = '';
        if($model->seller !== null){
            $sellerName = $model->seller->Name;
            $sellerAddress = $model->seller->Address;
            $sellerDetail = MyHelper::getCityDetail($model->seller->CityCode).' '.$model->seller->PostCode;
        }
        $row20 = $row4+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("E".$row20.":H".$row20);
        $objWorkSheet->SetCellValue("E".$row20, $sellerName);

        $row21 = $row20+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("E".$row21.":H".$row21);
        $objWorkSheet->SetCellValue("E".$row21, $sellerAddress);

        $row22 = $row21+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("E".$row22.":H".$row22);
        $objWorkSheet->SetCellValue("E".$row22, $sellerDetail);

        $row9 = $row8+1;
        $objWorkSheet->SetCellValue("F".$row9, 'TOTAL G.W');
        $objWorkSheet->getStyle("F".$row9)->applyFromArray($fontRight);

        $row10 = $row9+1;
        $objWorkSheet->SetCellValue("F".$row10, 'TOTAL N.W');
        $objWorkSheet->getStyle("F".$row10)->applyFromArray($fontRight);

        $row11 = $row10+1;
        $objWorkSheet->SetCellValue("F".$row11, 'TOTAL MEAS');
        $objWorkSheet->getStyle("F".$row11)->applyFromArray($fontRight);

        $objWorkSheet->setTitle($model->BillCodeSupplier);
        $fileName = 'proforma_invoice_'.$model->BillCodeSupplier.'.xls';
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

        // save ke local
        $objWriter->save($filePath.'/'.$fileName);

        // download ke client
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $objWriter->save('php://output');

        // Once we have finished using the library, give back the
        // power to Yii...
        spl_autoload_register(array('YiiBase','autoload'));

        return $filePath.'/'.$fileName;
    }

    public function actionOcs($id){
        $model = $this->loadModel($id);

        $filePath = Yii::getPathOfAlias('site.uploads.downloads');
        $phpExcelPath = Yii::getPathOfAlias('site.vendors.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("p5trade.com");
        $objPHPExcel->getProperties()->setTitle("Order Confirmation Summary");
        $objWorkSheet = $objPHPExcel->createSheet(0);

        $colorGrey = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'CCCCCC')
            )
        );

        $fontCenter = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $fontRight = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );

        $fontLeft = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:B1');
        $objWorkSheet->SetCellValue('A1', 'SUMMARY');

        $objWorkSheet->SetCellValue('A3', 'REFERENCE:');
        $objWorkSheet->SetCellValue('B3', $model->BillCodeSupplier);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:E4');
        $objWorkSheet->SetCellValue('A4', 'CUSTO INDONESIA (USD)');
        $objWorkSheet->getStyle("A4:E4")->applyFromArray($colorGrey);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:C5');
        $objWorkSheet->SetCellValue('A5', 'Taxas do banco de origem:');
        $objWorkSheet->SetCellValue('D5', 'Origem');
        $objWorkSheet->getStyle("D5")->applyFromArray($fontRight);

        // set autowidth
        for($col = 'A'; $col !== 'Z'; $col++) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        $row = 6;
        $total = 0;
        if($model->charges != null){
            foreach($model->charges as $detail) {
                if($detail->master != null){
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row.":C".$row);
                    $objWorkSheet->SetCellValue("A".$row, $detail->master->Name);
                    $objWorkSheet->SetCellValue("D".$row, '$'.number_format($detail->Value));
                    $objWorkSheet->getStyle("D".$row)->applyFromArray($fontRight);

                    $total+=$detail->Value;
                }
                else{
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row.":E".$row);
                    $objWorkSheet->SetCellValue("A".$row, 'Failed load order charge detail');
                }

                $row++;
            }
        }

        $row2 = $row;
        $objWorkSheet->SetCellValue("B".$row2, 'SUMMARY');
        $row3 = $row2+1;
        $objWorkSheet->SetCellValue("C".$row3, 'Total Order FOB - Product');
        $objWorkSheet->SetCellValue("D".$row3, '$'.number_format($model->OrderTotal));
        $row4 = $row3+1;
        $objWorkSheet->SetCellValue("C".$row4, 'Total Cost Indonesia');
        $objWorkSheet->SetCellValue("D".$row4, '$'.number_format($total));
        $row5 = $row4+1;
        $objWorkSheet->SetCellValue("C".$row5, 'Total Sale Amount');
        $objWorkSheet->SetCellValue("D".$row5, '$'.number_format($model->OrderTotal + $total));

        $objWorkSheet->getStyle("A".$row2.":D".$row5)->applyFromArray($colorGrey);
        $objWorkSheet->getStyle("C".$row2.":D".$row5)->applyFromArray($fontRight);

        $row6 = $row5+2;
        $objWorkSheet->SetCellValue("A".$row6, 'Terms:');
        if($model->terms != null){
            $row7 = $row6+1;
            foreach($model->terms as $term){
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row7.":C".$row7);
                $objWorkSheet->SetCellValue("A".$row7, number_format($term->PercentageofPayment).'%');
                $objWorkSheet->SetCellValue("D".$row7, '$'.number_format($term->ValuePercentageOfPayment));
                $objWorkSheet->getStyle("D".$row7)->applyFromArray($fontRight);

                $row7++;
            }
        }
        $row8 = $row7;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row8.":C".$row8);
        $objWorkSheet->SetCellValue("A".$row8, 'Ocean Freight from IndoVitri');
        $objWorkSheet->SetCellValue("D".$row8, '$'.number_format($model->FreightFee));
        $objWorkSheet->getStyle("D".$row8)->applyFromArray($fontRight);

        $row9 = $row8+2;
        $objWorkSheet->SetCellValue("A".$row9, 'Total G.W: ');
        $row10 = $row9+1;
        $objWorkSheet->SetCellValue("A".$row10, 'Total N.W: ');
        $row11 = $row10+1;
        $objWorkSheet->SetCellValue("A".$row11, 'Total MEAS: ');
        $row12 = $row11+2;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row12.":C".$row12);
        $objWorkSheet->SetCellValue("A".$row12, "2 x 40NOR SHIPPER'S LOAD AND COUNT");

        $objWorkSheet->setTitle($model->BillCodeSupplier);
        $fileName = 'summary_charge_'.$model->BillCodeSupplier.'.xls';
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

        // save ke local
        $objWriter->save($filePath.'/'.$fileName);

        // download ke client
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $objWriter->save('php://output');

        // Once we have finished using the library, give back the
        // power to Yii...
        spl_autoload_register(array('YiiBase','autoload'));

        return $filePath.'/'.$fileName;
    }

    public function actionOci($id){
        $model = $this->loadModel($id);

        $filePath = Yii::getPathOfAlias('site.uploads.downloads');
        $phpExcelPath = Yii::getPathOfAlias('site.vendors.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("p5trade.com");
        $objPHPExcel->getProperties()->setTitle("Order Confirmation Images");
        $objWorkSheet = $objPHPExcel->createSheet(0);

        $colorGrey = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'CCCCCC')
            )
        );

        $fontCenter = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $fontRight = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );

        $fontLeft = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');
        $objWorkSheet->SetCellValue('A1', 'PRODUCT IMAGES');
        $objWorkSheet->getStyle("A1:D1")->applyFromArray($fontCenter);

        $objWorkSheet->SetCellValue('A3', $model->BillCodeSupplier);

        $objWorkSheet->SetCellValue('A4', 'SEQ');
        $objWorkSheet->SetCellValue('B4', 'ITEM');
        $objWorkSheet->SetCellValue('D4', 'IMAGES');
        $objWorkSheet->getStyle("A4:D4")->applyFromArray($fontCenter);
        $objWorkSheet->getStyle("A4:D4")->applyFromArray($colorGrey);

        // set autowidth
        for($col = 'A'; $col !== 'Z'; $col++) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        $row = 5;
        if($model->blContainer != null){
            $no = 1;
            foreach($model->blContainer as $bl) {
                if($bl->container != null){
                    $objWorkSheet->SetCellValue("A".$row, 'CONTAINER LOAD #'.$no.' - '.$bl->container->ContainerName);

                    if($model->details != null){
                        $row2 = $row+1;
                        $no = 1;
                        foreach($model->details as $detail){
                            if($detail->ContainerID = $bl->ContainerCode){
                                $objWorkSheet->SetCellValue("A".$row2, $no);

                                if($detail->product != null){
                                    $objWorkSheet->SetCellValue("B".$row2, $detail->product->ProductID);
                                    $objWorkSheet->SetCellValue("C".$row2, $detail->product->ProductName);
                                }

                                $objWorkSheet->getStyle("A".$row2.":D".$row2)->applyFromArray($fontCenter);

                                $no++;
                                $row2++;
                            }
                        }
                    }
                }
                else{
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".$row.":D".$row);
                    $objWorkSheet->SetCellValue("A".$row, 'Failed load container detail');
                }

                $objWorkSheet->getStyle("A".$row.":D".$row)->applyFromArray($fontCenter);

                $row=$row2+1;
                $no++;
            }
        }

        $objWorkSheet->setTitle($model->BillCodeSupplier);
        $fileName = 'confirmation_images_'.$model->BillCodeSupplier.'.xls';
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

        // save ke local
        $objWriter->save($filePath.'/'.$fileName);

        // download ke client
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $objWriter->save('php://output');

        // Once we have finished using the library, give back the
        // power to Yii...
        spl_autoload_register(array('YiiBase','autoload'));

        return $filePath.'/'.$fileName;
    }

    public function actionArc($id){
        $model = $this->loadModelCustomer($id);

        $filePath = Yii::getPathOfAlias('site.uploads.downloads');
        $phpExcelPath = Yii::getPathOfAlias('site.vendors.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("p5trade.com");
        $objPHPExcel->getProperties()->setTitle("Account Receivable Report");
        $objWorkSheet = $objPHPExcel->createSheet(0);

        $colorGrey = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'CCCCCC')
            )
        );

        $fontCenter = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $fontRight = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );

        $fontLeft = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        // set autowidth
        for($col = 'A'; $col !== 'Z'; $col++) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:P1');
        $objWorkSheet->SetCellValue('A1', 'ACCOUNT RECEIVABLE REPORT');
        $objWorkSheet->getStyle("A1:P1")->applyFromArray($fontCenter);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:D2');
        $objWorkSheet->SetCellValue('B2', '('.$model->CustomerCode.') '.$model->customer->Name);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B3:D3');
        $objWorkSheet->SetCellValue('B3', 'PEDIDOS EM ANDAMENTO');
        $objWorkSheet->SetCellValue('B5', 'DATA:');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('C5:D5');
        $objWorkSheet->SetCellValue('C5', date('d-M-Y'));

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J7:L7');
        $objWorkSheet->SetCellValue('J7', 'Plano de Pagamento');
        $objWorkSheet->getStyle("J7:L7")->applyFromArray($fontCenter);

        $objWorkSheet->SetCellValue('B8', 'Acct');
        $objWorkSheet->SetCellValue('C8', 'Factory');
        $objWorkSheet->SetCellValue('D8', 'Invoice');
        $objWorkSheet->SetCellValue('E8', 'Inv Date');
        $objWorkSheet->SetCellValue('F8', 'Total');
        $objWorkSheet->SetCellValue('G8', 'FOB');
        $objWorkSheet->SetCellValue('H8', 'Indo Cost');
        $objWorkSheet->SetCellValue('I8', 'DP 100%');
        $objWorkSheet->SetCellValue('J8', 'Confirmacao do Pedido');
        $objWorkSheet->SetCellValue('K8', 'Embarque');
        $objWorkSheet->SetCellValue('L8', 'Liberacao BL');

        $objWorkSheet->getStyle("F8:L8")->applyFromArray($fontRight);

        $no = 1;
        $row = 9;
        $orders = OrderHeader::model()->findAllByAttributes(array('CustomerCode'=>$model->CustomerCode));
        if($orders != null){
            $total = 0;
            foreach($orders as $order) {
                $totalCharge = 0;
                foreach($order->charges as $charge){
                    $totalCharge+=$charge->Value;
                }

                $totalOrder = $order->OrderTotal+$totalCharge+$order->FreightFee;
                foreach($order->sellerBills as $bill){
                    $objWorkSheet->SetCellValue("A".$row, $no);
                    $objWorkSheet->SetCellValue("B".$row, 'SS');
                    $objWorkSheet->SetCellValue("C".$row, $order->SellerCode);
                    $objWorkSheet->SetCellValue("D".$row, $order->OrderCode);
                    $objWorkSheet->SetCellValue("E".$row, date('d-M-Y', strtotime($bill->DatePayment)));
                    $objWorkSheet->SetCellValue("F".$row, '$'.number_format($totalOrder,2));
                    $objWorkSheet->SetCellValue("G".$row, '$'.number_format($order->FreightFee,2));
                    $objWorkSheet->SetCellValue("H".$row, '$'.number_format($totalCharge,2));
                    $objWorkSheet->SetCellValue("I".$row, '100%');
                    $objWorkSheet->SetCellValue("J".$row, '$'.number_format($totalOrder,2));
                    $objWorkSheet->SetCellValue("K".$row, '$'.number_format(0,2));
                    $objWorkSheet->SetCellValue("L".$row, '$'.number_format(0,2));

                    $objWorkSheet->getStyle("F".$row.":L".$row)->applyFromArray($fontRight);

                    $no++;
                    $row++;
                    $total+=$totalOrder;
                }
            }

            $row2=$row+1;
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("D".$row2.":E".$row2);
            $objWorkSheet->SetCellValue("D".$row2, 'VALOR TOTAL DE COMPRA');
            $objWorkSheet->getStyle("D".$row2.":E".$row2)->applyFromArray($fontRight);

            $objWorkSheet->SetCellValue("F".$row2, '$'.number_format($total,2));
            $objWorkSheet->getStyle("F".$row2)->applyFromArray($fontRight);

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("H".$row2.":I".$row2);
            $objWorkSheet->SetCellValue("H".$row2, 'TOTAL A PAGAR');
            $objWorkSheet->getStyle("H".$row2.":I".$row2)->applyFromArray($fontRight);

            $objWorkSheet->SetCellValue("J".$row2, '$'.number_format($total,2));
            $objWorkSheet->SetCellValue("K".$row2, '$'.number_format(0,2));
            $objWorkSheet->SetCellValue("L".$row2, '$'.number_format(0,2));
            $objWorkSheet->getStyle("J".$row2.":L".$row2)->applyFromArray($fontRight);
        }

        if($model->payments != null){
            $row3 = $row2+2;
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("B".$row3.":F".$row3);
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("J".$row3.":P".$row3);
            $objWorkSheet->SetCellValue("B".$row3, 'TRANSFERENCIAS / CREDITO EM CONTA');
            $objWorkSheet->getStyle("B".$row3.":F".$row3)->applyFromArray($fontCenter);
            $objWorkSheet->getStyle("J".$row3.":P".$row3)->applyFromArray($fontCenter);
            $objWorkSheet->getStyle("B".$row3.":F".$row3)->applyFromArray($colorGrey);
            $objWorkSheet->getStyle("J".$row3.":P".$row3)->applyFromArray($colorGrey);

            $row4 = $row3+1;
            $index = 1;
            $totalPayment = 0;
            foreach($model->payments as $payment){
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("B".$row4.":C".$row4);
                $objWorkSheet->SetCellValue("B".$row4, 'Bank Transfer #'.$index);
                $objWorkSheet->SetCellValue("E".$row4, date('d-M-Y', strtotime($payment->DatePayment)));
                $objWorkSheet->SetCellValue("F".$row4, '$'.number_format($payment->Ammount,2));

                $index++;
                $totalPayment+=$payment->Ammount;
            }

            $row5 = $row4+1;
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("D".$row5.":E".$row5);
            $objWorkSheet->SetCellValue("D".$row5, 'FUNDOS RECEBIDOS / CREDITOS');
            $objWorkSheet->SetCellValue("F".$row5, '$'.number_format($totalPayment,2));

            $objWorkSheet->SetCellValue("J".$row3, 'RESUMO DE CONTA');
            $objWorkSheet->getStyle("J".$row3.":L".$row3)->applyFromArray($fontCenter);

            $objWorkSheet->SetCellValue("J".$row4, 'CONFIRMACAO PEDIDO');
            $objWorkSheet->SetCellValue("K".$row4, '$'.number_format($totalOrder,2));
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("L".$row4.":P".$row4);
            $objWorkSheet->SetCellValue("L".$row4, 'Pgto para embarque imediato ou comeco de producao (Pend.)');
            $objWorkSheet->getStyle("J".$row4.":K".$row4)->applyFromArray($fontRight);

            $row6=$row4+1;
            $objWorkSheet->SetCellValue("J".$row6, 'EMBARQUE');
            $objWorkSheet->SetCellValue("K".$row6, '$'.number_format(0,2));
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("L".$row6.":O".$row6);
            $objWorkSheet->SetCellValue("L".$row6, 'Pagamento final para Estufar (Pend.)');
            $objWorkSheet->getStyle("J".$row6.":K".$row6)->applyFromArray($fontRight);

            $row7=$row6+1;
            $objWorkSheet->SetCellValue("J".$row7, 'LIBERACAO BL');
            $objWorkSheet->SetCellValue("K".$row7, '$'.number_format(0,2));
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("L".$row7.":O".$row7);
            $objWorkSheet->SetCellValue("L".$row7, 'Balanco pendete contra liberacao do BL (Pend.)');
            $objWorkSheet->getStyle("J".$row7.":K".$row7)->applyFromArray($fontRight);

            $row8=$row7+1;
            $objWorkSheet->SetCellValue("J".$row8, 'TOTAL PENDETE (USD)');
            $totalFinal = $totalPayment - $totalOrder;
            $objWorkSheet->SetCellValue("K".$row8, '$'.number_format($totalFinal,2));
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("L".$row8.":O".$row8);
            $objWorkSheet->SetCellValue("L".$row8, '(Valor Total de Compra - Fundos Recebidos)');
            $objWorkSheet->getStyle("J".$row8.":K".$row8)->applyFromArray($fontRight);

            $row9=$row8+1;
            $objWorkSheet->SetCellValue("J".$row9, 'BALANCO DE CONTA - A VENCER');
            $objWorkSheet->SetCellValue("K".$row9, '$'.number_format(0,2));
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("L".$row9.":M".$row9);
            $objWorkSheet->SetCellValue("L".$row9, 'Balanco pendete contra liberacao do BL');
            $objWorkSheet->getStyle("J".$row9.":K".$row9)->applyFromArray($fontRight);

            $row10=$row9+2;
            $objWorkSheet->SetCellValue("J".$row10, 'Acct Funds Xfer to '.date('Y'));
            $objWorkSheet->SetCellValue("K".$row10, '$'.number_format($totalFinal,2));
            $objWorkSheet->getStyle("J".$row10.":K".$row10)->applyFromArray($fontRight);

            $row11=$row10+2;
            $objWorkSheet->SetCellValue("J".$row11, $model->customer->Name.' Balance @ '.date('d-M-Y'));
            $objWorkSheet->SetCellValue("K".$row11, '$'.number_format(0,2));
            $objWorkSheet->getStyle("J".$row11.":K".$row11)->applyFromArray($fontRight);
        }

        $objWorkSheet->setTitle('AR Report '.$model->CustomerCode);
        $fileName = 'account_receivable_'.$model->CustomerCode.'.xls';
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

        // save ke local
        $objWriter->save($filePath.'/'.$fileName);

        // download ke client
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $objWriter->save('php://output');

        // Once we have finished using the library, give back the
        // power to Yii...
        spl_autoload_register(array('YiiBase','autoload'));

        return $filePath.'/'.$fileName;
    }

    public function loadModel($id)
    {
        $model=OrderHeader::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function loadModelCustomer($id)
    {
        $model=Customer::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}
?>
