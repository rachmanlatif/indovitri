<?php

class IncotermController extends AdminController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);

		$order = new OrderHeader('search');
		$order->IncontermID = $model->IncotermID;

		$this->render('view',array(
			'model'=>$model,
			'order'=>$order,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd()
	{
		$model=new Incoterm;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Incoterm']))
		{
			$model->attributes=$_POST['Incoterm'];
            $model->IncotermID = IDGenerator::generate('incoterm');

			if($model->save()){
				$this->redirect(array('view','id'=>$model->IncotermID));
            }
		}

		$this->render('add',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Incoterm']))
		{
			$model->attributes=$_POST['Incoterm'];

			if($model->save()){
				$this->redirect(array('view','id'=>$model->IncotermID));
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(isset($_GET['id']))
		{
				$model = $this->loadModel($id);
				$model->Status = EnumStatus::NON_ACTIVE;
				if($model->save()){
					$this->redirect(array('index'));
				}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Deletes a list of model.
	 */
	public function actionDeleteSelected()
	{
		if(Yii::app()->request->isPostRequest && isset($_POST['ids']))
		{
			// delete
			foreach ($_POST['ids'] as $id) {
				$this->loadModel($id)->delete();
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Incoterm('search');
		$model->unsetAttributes();  // clear any default values
		$model->Status = EnumStatus::ACTIVE;
		
		if(isset($_GET['Incoterm']))
			$model->attributes=$_GET['Incoterm'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Incoterm::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='incoterm-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
