<?php

class SellerController extends AdminController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model=$this->loadModel($id);

        $factories = SellerFactory::model()->findAllByAttributes(array('SellerCode'=>$model->SellerCode));

				$detailPerson=DetailPerson::model()->findAllByAttributes(array('EntityCode'=>$model->SellerCode, 'Status'=>EnumEntity::SELLER));

				$order = new OrderHeader('search');
				$order->SellerCode = $model->SellerCode;

				$product = new Product('search');
				$product->SellerCode = $model->SellerCode;

				if(isset($_GET['showImage'])){
					$show_image = $_GET['showImage'];
				}else{
					$show_image = false;
				}

		$this->render('view',array(
			'model'=>$model,
			'factories'=>$factories,
			'detailPerson'=>$detailPerson,
			'order'=>$order,
			'product'=>$product,
			'showImage'=>$show_image,
		));
	}

	public function actionViewProduct($id)
	{
		$model = $this->loadModelProduct($id);
		$info = ProductListInfo::model()->findByPk($model->ProductID);

		$this->render('viewProduct',array(
			'model'=>$model,
			'info'=>$info,
		));
	}

	public function loadModelProduct($id)
	{
		$model=Product::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionViewOrder($id)
	{
				$model = OrderHeader::model()->findByAttributes(array('OrderCode'=>$id));
				if($model===null){
						throw new CHttpException(404,'The requested page does not exist.');
				}

				$sellerPayment=new SellerBillPayment('search');
				$sellerPayment->OrderID = $model->OrderCode;

				$sellerPaid = SellerBillPayment::model()->findByAttributes(array('OrderID'=>$model->OrderCode));

				$orderBl=new OrderBL('search');
				$orderBl->OrderID = $model->OrderCode;

				$customerPayment=new CustomerPayment('search');
				$customerPayment->CustomerCode = $model->CustomerCode;

				$this->render('viewOrder',array(
			'model'=>$model,
			'sellerPayment'=>$sellerPayment,
			'orderBl'=>$orderBl,
			'customerPayment'=>$customerPayment,
			'sellerPaid'=>$sellerPaid,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd()
	{
		$model=new Seller;
        $factories = array();
				$detailPerson=array();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Seller']))
		{
            $tempDetail = array();
            if (isset($_POST['SellerFactory'])) {
                foreach ($_POST['SellerFactory'] as $d) {
                    $detail = new SellerFactory();
                    $detail->attributes = $d;

                    array_push($tempDetail, $detail);
                }
            }
            $model->details = $tempDetail;

						$detailPerson = array();
						if (isset($_POST['DetailPerson'])) {
								foreach ($_POST['DetailPerson'] as $d) {
										$detail = new DetailPerson();
										$detail->attributes = $d;

										array_push($detailPerson, $detail);
								}
						}

			$model->attributes=$_POST['Seller'];
            $model->FactoryCount = count($model->details);
			if($model->save()){
                foreach($model->details as $d){
                    $d->FactoryCode = IDGenerator::generate('factory');
                    $d->SellerCode = $model->SellerCode;
                    if(!$d->save()){
                        $message = '';
                        $errors = $d->getErrors();
                        foreach ($errors as $error){
                            foreach ($error as $e){
                                $message.= $e.'<br />';
                            }
                        }
                        throw new Exception('Error when saving detail consignee '.$message);
                    }
                }

								foreach($detailPerson as $d){
										$d->Status = EnumEntity::SELLER;
										$d->EntityCode = $model->SellerCode;
										if(!$d->save()){
												$message = '';
												$errors = $d->getErrors();
												foreach ($errors as $error){
														foreach ($error as $e){
																$message.= $e.'<br />';
														}
												}
												throw new Exception('Error when saving detail person '.$message);
										}
								}

				$this->redirect(array('view','id'=>$model->SellerCode));
            }
		}
		$get_country = Yii::app()->db->createCommand('SELECT * FROM Country')->queryAll();

		$this->render('add',array(
			'model'=>$model,
			'get_country' => $get_country,
			'factories'=>$factories,
			'detailPerson'=>$detailPerson,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->Furnase = round($model->Furnase);
		$model->CapasityTon = round($model->CapasityTon);

        $factories = SellerFactory::model()->findAllByAttributes(array('SellerCode'=>$model->SellerCode));
				$detailPerson=DetailPerson::model()->findAllByAttributes(array('EntityCode'=>$model->SellerCode, 'Status'=>EnumEntity::SELLER));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Seller']))
		{
            $tempDetail = array();
            if (isset($_POST['SellerFactory'])) {
                foreach ($_POST['SellerFactory'] as $d) {
                    $code = $d['FactoryCode'];
                    if($code != null){
                        $detail = SellerFactory::model()->findByPk($code);
                        if($detail != null){
                            $detail->FactoryName = $d['FactoryName'];
                            $detail->Address = $d['Address'];
                            $detail->Phone = $d['Phone'];
                            $detail->Note = $d['Note'];
                            $detail->save();
                        }
                    }
                    else{
                        $detail = new SellerFactory();
                        $detail->attributes = $d;

                        array_push($tempDetail, $detail);
                    }
                }
            }
            $model->details = $tempDetail;

						$detailPerson = array();
						if (isset($_POST['DetailPerson'])) {
								foreach ($_POST['DetailPerson'] as $d) {
										$id = $d['IDDetailPerson'];
										if($id != null){
												$person = DetailPerson::model()->findByPk($id);
												if($person != null){
														$person->PositionCode = $d['PositionCode'];
														$person->Name = $d['Name'];
														$person->Address = $d['Address'];
														$person->Email = $d['Email'];
														$person->Note = $d['Note'];
														$person->save();
												}
										}
										else{
											$detail = new DetailPerson();
											$detail->attributes = $d;

											array_push($detailPerson, $detail);
										}
								}
						}

			$model->attributes=$_POST['Seller'];
            $model->FactoryCount = $model->FactoryCount+count($model->details);
			if($model->save()){
                foreach($model->details as $d){
                    $d->FactoryCode = IDGenerator::generate('factory');
                    $d->SellerCode = $model->SellerCode;
                    if(!$d->save()){
                        $message = '';
                        $errors = $d->getErrors();
                        foreach ($errors as $error){
                            foreach ($error as $e){
                                $message.= $e.'<br />';
                            }
                        }
                        throw new Exception('Error when saving detail consignee '.$message);
                    }
                }

								foreach($detailPerson as $d){
										$d->Status = EnumEntity::SELLER;
										$d->EntityCode = $model->SellerCode;
										if(!$d->save()){
												$message = '';
												$errors = $d->getErrors();
												foreach ($errors as $error){
														foreach ($error as $e){
																$message.= $e.'<br />';
														}
												}
												throw new Exception('Error when saving detail person '.$message);
										}
								}

				$this->redirect(array('view','id'=>$model->SellerCode));
            }
		}

		$get_country = Yii::app()->db->createCommand('SELECT * FROM Country')->queryAll();
		$this->render('update',array(
			'model'=>$model,
			'factories'=>$factories,
			'get_country' => $get_country,
			'detailPerson'=>$detailPerson,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
				$model->Furnase = round($model->Furnase);
				$model->FactoryCount = round($model->FactoryCount);
				$model->CapasityTon = round($model->CapasityTon);
        $model->Status = EnumStatus::NON_ACTIVE;
        if($model->save()){
            Yii::app()->user->setFlash('success','Success delete seller');
            $this->redirect(array('index'));
        }
        else{
            Yii::app()->user->setFlash('danger','Failed delete seller');
            $this->redirect(array('view','id'=>$model->SellerCode));
        }
    }

    public function actionDeleteDetail($id)
    {
        $detail = SellerFactory::model()->findByPk($id);
        if($detail != null){
            if($detail->delete()){
                $this->redirect(array('update','id'=>$detail->SellerCode));
            }
            else{
                Yii::app()->user->setFlash('danger','Failed delete factory');
                $this->redirect(array('view','id'=>$detail->SellerCode));
            }
        }
        else{
            Yii::app()->user->setFlash('danger','Failed delete factory');
            $this->redirect(array('view','id'=>$detail->SellerCode));
        }
    }

		public function actionDeletePerson($id)
		{
				$detail = DetailPerson::model()->findByPk($id);
				if($detail != null){
						if($detail->delete()){
								$this->redirect(array('update','id'=>$detail->EntityCode));
						}
						else{
								Yii::app()->user->setFlash('danger','Failed delete detail');
								$this->redirect(array('view','id'=>$detail->EntityCode));
						}
				}
				else{
						Yii::app()->user->setFlash('danger','Failed delete detail');
						$this->redirect(array('view','id'=>$detail->EntityCode));
				}
		}

    /**
	 * Deletes a list of model.
	 */
	public function actionDeleteSelected()
	{
		if(Yii::app()->request->isPostRequest && isset($_POST['ids']))
		{
			// delete
			foreach ($_POST['ids'] as $id) {
				$this->loadModel($id)->delete();
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Seller('search');
		$model->unsetAttributes();  // clear any default values
		$model->Status = EnumStatus::ACTIVE;
		
		if(isset($_GET['Seller']))
			$model->attributes=$_GET['Seller'];

		$get_country = Yii::app()->db->createCommand('SELECT * FROM Country')->queryAll();
		$this->render('index',array(
			'model'=>$model,
			'get_country' => $get_country,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Seller::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='seller-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionAddDetail(){
        if(Yii::app()->request->isAjaxRequest){
            $json = array(
                'success'=>false,
                'content'=>'',
                'message'=>'',
            );

            if (isset($_POST['index'])) {
                $detail = new SellerFactory();

                $json['content'] = $this->renderPartial('_formDetail', array(
                    'detail'=>$detail,
                    'index'=>$_POST['index'],
                ), true);
                $json['success'] = true;
            }
            else{
                $json['message'] = 'The requested page does not exist.';
            }

            echo CJSON::encode($json);
            Yii::app()->end();
        }
    }

		public function actionAddPerson(){
        if(Yii::app()->request->isAjaxRequest){
            $json = array(
                'success'=>false,
                'content'=>'',
                'message'=>'',
            );

            if (isset($_POST['index'])) {
                $detail = new DetailPerson();

                $json['content'] = $this->renderPartial('_formPerson', array(
                    'detail'=>$detail,
                    'index'=>$_POST['index'],
                ), true);
                $json['success'] = true;
            }
            else{
                $json['message'] = 'The requested page does not exist.';
            }

            echo CJSON::encode($json);
            Yii::app()->end();
        }
    }

		public function actionAddState(){
			// print_r($_POST);die;
			$state = Yii::app()->db->createCommand("SELECT * FROM State WHERE CountryCode = '".$_POST['countryID']."'")->queryAll();
			echo '<br><select class="form-control" style="width : 100%" tabindex="-1" id="stateID">';
	      	foreach ($state as $key => $value) {
	        	echo '<option value="'.$value['StateCode'].'">'.$value['StateName'].'</option>';
	      }
	  	echo '</select>';
		}

		public function actionAddCity(){
			if(isset($_POST['stateID'])){
				$city = Yii::app()->db->createCommand("SELECT * FROM City WHERE StateCode = '".$_POST['stateID']."' AND Status = '1'")->queryAll();
				echo '<br><select class="form-control" style="width : 100%" tabindex="-1" id="cityID" name="Seller[CityCode]">';
		      	foreach ($city as $key => $value) {
		        	echo '<option value="'.$value['CityCode'].'">'.$value['CityName'].'</option>';
		      }
		  	echo '</select><br>';
			}else{
				echo '<br><select class="form-control" style="width : 100%" tabindex="-1" id="cityID" name="Seller[CityCode]">';

		        	echo '<option value="">-</option>';
		  	echo '</select><br>';
			}

		}
}
