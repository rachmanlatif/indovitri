<?php

class CityController extends AdminController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd()
	{
		$model=new City;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['City']))
		{
			// echo'<pre>';print_r($_POST);die;
			$model->attributes=$_POST['City'];
            $model->CityCode = IDGenerator::generate('city');

            if($model->save()){
				$this->redirect(array('view','id'=>$model->CityCode));
            }
		}
		$get_country = Yii::app()->db->createCommand('SELECT * FROM Country')->queryAll();
		$this->render('add',array(
			'model'=>$model,
			'get_country'=>$get_country
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['City']))
		{
			$model->attributes=$_POST['City'];

			if($model->save()){
				$this->redirect(array('view','id'=>$model->CityCode));
            }
		}
		$get_country = Yii::app()->db->createCommand('SELECT * FROM Country')->queryAll();
		$this->render('update',array(
			'model'=>$model,
			'get_country'=>$get_country
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(isset($_GET['id']))
		{
				$model = $this->loadModel($id);
				$model->Status = EnumStatus::NON_ACTIVE;
				if($model->save()){
					$this->redirect(array('index'));
				}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Deletes a list of model.
	 */
	public function actionDeleteSelected()
	{
		if(Yii::app()->request->isPostRequest && isset($_POST['ids']))
		{
			// delete
			foreach ($_POST['ids'] as $id) {
				$this->loadModel($id)->delete();
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new City('search');
		$model->unsetAttributes();  // clear any default values
		$model->Status = EnumStatus::ACTIVE;

		if(isset($_GET['City'])){
			$data_get = $_GET['City'];
			unset($_GET['City']['StateCode']);
			$model->attributes=$data_get;
		}


		$get_country = Yii::app()->db->createCommand('SELECT * FROM Country')->queryAll();

		$this->render('index',array(
			'model'=>$model,
			'get_country' => $get_country,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=City::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='city-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionAddState(){
		// print_r($_POST);die;
		$state = Yii::app()->db->createCommand("SELECT * FROM State WHERE CountryCode = '".$_POST['countryID']."'")->queryAll();
		echo '<select class="select2" style="width : 100%" tabindex="-1" id="stateID" name="City[StateCode]">';
      	foreach ($state as $key => $value) {
        	echo '<option value="'.$value['StateCode'].'">'.$value['StateName'].'</option>';
      }
  	echo '</select>';
	}

	public function actionAddCity(){
		if(isset($_POST['stateID'])){
			$city = Yii::app()->db->createCommand("SELECT * FROM City WHERE StateCode = '".$_POST['stateID']."' AND Status = '1'")->queryAll();
			echo '<select class="select2" style="width : 100%" tabindex="-1" id="cityID" name="City[CityCode]">';
	      	foreach ($city as $key => $value) {
	        	echo '<option value="'.$value['CityCode'].'">'.$value['CityName'].'</option>';
	      }
	  	echo '</select>';
		}else{
			echo '<select class="select2" style="width : 100%" tabindex="-1" id="cityID" name="City[CityCode]">';

	        	echo '<option value="">-</option>';
	  	echo '</select>';
		}

	}
}
