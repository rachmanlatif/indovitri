<?php

class OrderController extends AdminController
{

    public function actionChooseProduct(){

        $product = array();
        $action = '';
        if(isset($_POST['seller']) and isset($_POST['action'])){
          $products = Product::model()->findAllByAttributes(array('SellerCode'=>$_POST['seller']));
          $action = $_POST['action'];
        }

        $this->renderPartial('product', array(
            'products'=>$products,
            'action'=>$action,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);

        $sellerPayment=new SellerBillPayment('search');
        $sellerPayment->OrderID = $model->OrderCode;

        $totalPaid = 0;
        $sellerPaid = SellerBillPayment::model()->findAllByAttributes(array('OrderID'=>$model->OrderCode));
        foreach($sellerPaid as $paid){
            $totalPaid+=$paid->Ammount;
        }

        $orderBl=new OrderBL('search');
        $orderBl->OrderID = $model->OrderCode;

        $customerPayment=new CustomerPayment('search');
        $customerPayment->CustomerCode = $model->CustomerCode;

        $totalSummary = 0;
        $chargeDetail = OrderChargeDetail::model()->findAllByAttributes(array('OrderID'=>$model->OrderCode));
        foreach($chargeDetail as $detail){
            $totalSummary+=$detail->Value;
        }

        $step = 1;
        $wiz = Wiz::model()->findByAttributes(array('OrderID'=>$model->OrderCode));
        if($wiz != null){
            $step = $wiz->WizStep;
        }

        $this->render('view',array(
            'model'=>$model,
            'sellerPayment'=>$sellerPayment,
            'orderBl'=>$orderBl,
            'customerPayment'=>$customerPayment,
            'totalPaid'=>$totalPaid,
            'totalSummary'=>$totalSummary,
            'step'=>$step,
            'chargeDetail'=>$chargeDetail,
        ));
    }

    public function actionViewOrder($id)
    {
        $model = $this->loadModel($id);

        $sellerPayment=new SellerBillPayment('search');
        $sellerPayment->OrderID = $model->OrderCode;

        $totalPaid = 0;
        $sellerPaid = SellerBillPayment::model()->findAllByAttributes(array('OrderID'=>$model->OrderCode));
        foreach($sellerPaid as $paid){
            $totalPaid+=$paid->Ammount;
        }

        $orderBl=new OrderBL('search');
        $orderBl->OrderID = $model->OrderCode;

        $customerPayment=new CustomerPayment('search');
        $customerPayment->CustomerCode = $model->CustomerCode;

        $totalSummary = 0;
        $chargeDetail = OrderChargeDetail::model()->findAllByAttributes(array('OrderID'=>$model->OrderCode));
        foreach($chargeDetail as $detail){
            $totalSummary+=$detail->Value;
        }

        $step = 1;
        $wiz = Wiz::model()->findByAttributes(array('OrderID'=>$model->OrderCode));
        if($wiz != null){
            $step = $wiz->WizStep;
        }

        $this->render('viewOrder',array(
            'model'=>$model,
            'sellerPayment'=>$sellerPayment,
            'orderBl'=>$orderBl,
            'customerPayment'=>$customerPayment,
            'totalPaid'=>$totalPaid,
            'totalSummary'=>$totalSummary,
            'step'=>$step,
            'chargeDetail'=>$chargeDetail,
        ));
    }

    public function actionAddCharge(){
        if(Yii::app()->request->isAjaxRequest){
            $json = array(
                'success'=>false,
                'content'=>'',
                'message'=>'',
            );

            if (isset($_POST['index'])) {
                $detail = new OrderChargeDetail();

                $json['content'] = $this->renderPartial('_formCharge', array(
                    'detail'=>$detail,
                    'index'=>$_POST['index'],
                ), true);
                $json['success'] = true;
            }
            else{
                $json['message'] = 'The requested page does not exist.';
            }

            echo CJSON::encode($json);
            Yii::app()->end();
        }
    }

    public function actionAddProduct(){
        if(Yii::app()->request->isAjaxRequest){
            $json = array(
                'success'=>false,
                'content'=>'',
                'message'=>'',
                'volume'=>'',
                'qty'=>'',
                'qtyUnit'=>'',
                'grossWeight'=>'',
                'nettWeight'=>'',
                'price'=>'',
            );

            if (isset($_POST['index']) and isset($_POST['product']) and isset($_POST['qty'])) {
                $detail = new OrderDetailTemp();

                $product = Product::model()->findByAttributes(array('Status'=>EnumStatus::ACTIVE, 'ProductID'=>$_POST['product']));
                if($product == NULL){
                    $json['success'] = false;
                    $json['message'] = 'Product not found';
                }
                else if($_POST['qty'] < $product->MinQTY){
                    $json['success'] = false;
                    $json['message'] = "Min QTY for this product is ".number_format($product->MinQTY).". Failed add product";
                }
                else{
                    $smallQTY = 1;
                    if($product->uom->define != null){
                        $smallQTY = $product->uom->define->QTYUOMSmall;
                    }

                    $totalVolumeGross = $_POST['qty'] * $product->VolumeGross;
                    $totalWeightGross = $_POST['qty'] * $product->GrossWeight;
                    $totalWeightNett = $_POST['qty'] * $product->NettWeight;
                    $totalOrder = ($_POST['qty'] * $smallQTY) * $product->Price;
                    $totalUnit = $_POST['qty'] * $smallQTY;

                    $detail->ProductID = $product->ProductID;
                    $detail->QTY = $_POST['qty'];
                    $detail->TotalOrder = $totalOrder;
                    $detail->TotalGrossWeight = $totalWeightGross;
                    $detail->TotalNettWeight = $totalWeightNett;
                    $detail->TotalVolume = $totalVolumeGross;

                    $json['content'] = $this->renderPartial('_formProduct', array(
                        'detail'=>$detail,
                        'index'=>$_POST['index'],
                        'action'=>$_POST['action'],
                    ), true);
                    $json['volume'] = $totalVolumeGross;
                    $json['grossWeight'] = $totalWeightGross;
                    $json['nettWeight'] = $totalWeightNett;
                    $json['price'] = $totalOrder;
                    $json['qty'] = $_POST['qty'];
                    $json['qtyUnit'] = $totalUnit;
                    $json['success'] = true;
                }
            }
            else{
                $json['message'] = 'The requested page does not exist.';
            }

            echo CJSON::encode($json);
            Yii::app()->end();
        }
    }

    public function actionAddDetailTerm(){
        if(Yii::app()->request->isAjaxRequest){
            $json = array(
                'success'=>false,
                'content'=>'',
                'message'=>'',
            );

            if (isset($_POST['index'])) {
                $model = new OrderTermPayment();

                $json['content'] = $this->renderPartial('_formTerm', array(
                    'model'=>$model,
                    'index'=>$_POST['index'],
                ), true);
                $json['success'] = true;
            }
            else{
                $json['message'] = 'The requested page does not exist.';
            }

            echo CJSON::encode($json);
            Yii::app()->end();
        }
    }

    public function actionDeleteItem($id){
        $model = OrderDetailTemp::model()->findByPk($id);
        if($model->delete()){
            Yii::app()->user->setFlash('info','Success delete item');
            $this->redirect(array('update','id'=>$model->OrderID));
        }
    }

    public function actionDeleteContainer($id){
        $model = OrderBLContainer::model()->findByPk($id);
        if($model->delete()){
            Yii::app()->user->setFlash('info','Success delete item');
            $this->redirect(array('addContainer','id'=>$model->OrderID));
        }
    }

    public function actionDeleteItems($id){
        $model = OrderDetail::model()->findByPk($id);
        if($model->delete()){
            Yii::app()->user->setFlash('info','Success delete item');
            $this->redirect(array('update','id'=>$model->OrderID));
        }
    }

    public function actionFee($id)
    {
        $model=$this->loadModel($id);

        if(isset($_POST['OrderHeader']))
        {
            $tempDetail = array();
            if (isset($_POST['OrderDetailTemp'])) {
                foreach ($_POST['OrderDetailTemp'] as $d) {
                    $code = $d['IDOrderDetailTemp'];
                    $orderDetailTemp = OrderDetailTemp::model()->findByPk($code);
                    if($orderDetailTemp != null){
                        $orderDetailTemp->SupplierFee = $d['SupplierFee'];
                        $orderDetailTemp->save();
                    }

                    $orderDetail = OrderDetail::model()->findByPk($code);
                    if($orderDetail != null){
                        $orderDetail->QTY = $d['QTY'];
                        $orderDetail->SupplierFee = $d['SupplierFee'];
                        $orderDetail->save();
                    }
                }
            }
            $model->detailsTemp = $tempDetail;

            $model->attributes=$_POST['OrderHeader'];
            if($model->save()){
                $this->redirect(array('view','id'=>$model->OrderCode));
            }
        }

        $this->render('fee',array(
            'model'=>$model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionAddContainer($id){
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $listContainer = array();
        $container = OrderBLContainer::model()->findAllByAttributes(array('OrderID'=>$model->OrderCode));
        $no = 1;
        foreach ($container as $key => $value) {
            $listContainer[$value->IDOrderBLContainer] = $no.'. '.$value->container->ContainerName;

            $no++;
        }

        $detail = new OrderDetail();
        $seal = new OrderBLContainer();

        if(isset($_POST['OrderHeader'])){
            $dets = OrderDetail::model()->findAllByAttributes(array('OrderID'=>$model->OrderCode));
            foreach($dets as $det){
                $det->delete();
            }

            $tempDetail = array();
            if (isset($_POST['OrderDetailTemp'])) {
                foreach ($_POST['OrderDetailTemp'] as $d) {
                    $code = $d['IDOrderDetailTemp'];
                    if($code == ''){
                        $detail = new OrderDetailTemp();
                        $detail->attributes = $d;

                        array_push($tempDetail, $detail);
                    }
                    else{
                        $container = OrderBLContainer::model()->findByAttributes(array('IDOrderBLContainer'=>$d['BLContainer']));
                        $orderDetailTemp = OrderDetailTemp::model()->findByPk($code);
                        if($orderDetailTemp != null){
                            $orderDetailTemp->QTY = $d['QTY'];
                            $orderDetailTemp->TotalOrder = $d['TotalOrder'];
                            $orderDetailTemp->TotalVolume = $d['TotalVolume'];
                            $orderDetailTemp->TotalGrossWeight = $d['TotalGrossWeight'];
                            $orderDetailTemp->TotalNettWeight = $d['TotalNettWeight'];
                            $orderDetailTemp->QtyUnit = $d['MasterUnit'] * $d['QTY'];
                            $orderDetailTemp->ContainerID = $container->ContainerCode;
                            if($orderDetailTemp->save()){
                                $orderDetail = new OrderDetail();
                                $orderDetail->OrderID = $model->OrderCode;
                                $orderDetail->StatusOrder = $model->OrderStatusCode;
                                $orderDetail->Status = EnumStatus::NON_ACTIVE;
                                $orderDetail->ProductID = $d['ProductID'];
                                $orderDetail->QTY = $d['QTY'];
                                $orderDetail->TotalOrder = $d['TotalOrder'];
                                $orderDetail->TotalVolume = $d['TotalVolume'];
                                $orderDetail->TotalGrossWeight = $d['TotalGrossWeight'];
                                $orderDetail->TotalNettWeight = $d['TotalNettWeight'];
                                $orderDetail->QtyUnit = $d['MasterUnit'] * $d['QTY'];
                                $orderDetail->MasterUnit = $d['MasterUnit'];
                                $orderDetail->ContainerID = $container->ContainerCode;
                                if($orderDetail->save()){
                                    $bl = new OrderBLContainerDetail();
                                    $bl->IDOrderDetail = $orderDetail->IDOrderDetail;
                                    $bl->save();
                                }
                            }
                        }
                    }
                }
            }
            $model->detailsTemp = $tempDetail;

            $model->attributes=$_POST['OrderHeader'];
            $model->OrderStatusCode = EnumOrder::ORDER;
            if($model->save()){
                foreach($model->detailsTemp as $d){
                    $d->OrderID = $model->OrderCode;
                    $d->StatusOrder = $model->OrderStatusCode;
                    $d->Status = EnumStatus::NON_ACTIVE;
                    if($d->save()){
                        $orderDetail = new OrderDetail();
                        $orderDetail->OrderID = $model->OrderCode;
                        $orderDetail->StatusOrder = $model->OrderStatusCode;
                        $orderDetail->Status = EnumStatus::NON_ACTIVE;
                        $orderDetail->ProductID = $d['ProductID'];
                        $orderDetail->QTY = $d['QTY'];
                        $orderDetail->TotalOrder = $d['TotalOrder'];
                        $orderDetail->TotalVolume = $d['TotalVolume'];
                        $orderDetail->TotalGrossWeight = $d['TotalGrossWeight'];
                        $orderDetail->TotalNettWeight = $d['TotalNettWeight'];
                        $orderDetail->QtyUnit = $d['MasterUnit'] * $d['QTY'];
                        $orderDetail->MasterUnit = $d['MasterUnit'];
                        $orderDetail->ContainerID = $container->ContainerCode;
                        if($orderDetail->save()){
                            $bl = new OrderBLContainerDetail();
                            $bl->IDOrderDetail = $orderDetail->IDOrderDetail;
                            $bl->save();
                        }
                    }
                    else{
                        $message = '';
                        $errors = $d->getErrors();
                        foreach ($errors as $error){
                            foreach ($error as $e){
                                $message.= $e.'<br />';
                            }
                        }
                        throw new Exception('Error when saving detail order '.$message);
                    }
                }

                $wiz = Wiz::model()->findByAttributes(array('OrderID'=>$model->OrderCode));
                if($wiz == null){
                    $w = new Wiz();
                    $w->OrderID = $model->OrderCode;
                    $w->WizStep = 2;
                    $w->save();
                }
                else{
                    if($wiz->WizStep < 2){
                        $wiz->WizStep = 2;
                        $wiz->save();
                    }
                }

                $this->redirect(array('view','id'=>$model->OrderCode));
            }
        }

        $this->render('addContainer', array(
            'model'=>$model,
            'detail'=>$detail,
            'listContainer'=>$listContainer,
            'seal'=>$seal,
        ));
    }

    public function actionAdd()
    {
        $model=new OrderHeader;
        $model->OrderStatusCode = 1;

        $detail=new OrderDetailTemp();

        $container = Container::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE));
        $seal = new OrderBLContainer();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['OrderHeader']))
        {
            $tempDetail = array();
            if (isset($_POST['OrderDetailTemp'])) {
                foreach ($_POST['OrderDetailTemp'] as $d) {
                    $detail = new OrderDetailTemp();
                    $detail->attributes = $d;

                    array_push($tempDetail, $detail);
                }
            }
            $model->detailsTemp = $tempDetail;

            $tempContainer = array();
            if (isset($_POST['OrderBLContainer'])) {
                foreach ($_POST['OrderBLContainer'] as $d) {
                    $detail = new OrderBLContainer();
                    $detail->attributes = $d;

                    array_push($tempContainer, $detail);
                }
            }
            $model->blContainer = $tempContainer;

            $model->attributes=$_POST['OrderHeader'];
            $model->OrderCode = IDGenerator::generate('order');
            if($model->save()){
                $tempCon = array();
                foreach($model->detailsTemp as $d){
                    $d->OrderID = $model->OrderCode;
                    $d->StatusOrder = $model->OrderStatusCode;
                    $d->Status = EnumStatus::NON_ACTIVE;
                    if(!$d->save()){
                        $message = '';
                        $errors = $d->getErrors();
                        foreach ($errors as $error){
                            foreach ($error as $e){
                                $message.= $e.'<br />';
                            }
                        }
                        throw new Exception('Error when saving detail order '.$message);
                    }

                    array_push($tempCon, $d->ContainerID);
                }

                foreach($model->blContainer as $d){
                    if(in_array($d->ContainerCode, $tempCon)){
                        $d->OrderID = $model->OrderCode;
                        if(!$d->save()){
                            $message = '';
                            $errors = $d->getErrors();
                            foreach ($errors as $error){
                                foreach ($error as $e){
                                    $message.= $e.'<br />';
                                }
                            }
                            throw new Exception('Error when saving detail container '.$message);
                        }
                    }
                }

                $wiz = Wiz::model()->findByAttributes(array('OrderID'=>$model->OrderCode));
                if($wiz == null){
                    $w = new Wiz();
                    $w->OrderID = $model->OrderCode;
                    $w->WizStep = 1;
                    $w->save();
                }
                else{
                    if($wiz->WizStep < 1){
                        $wiz->WizStep = 1;
                        $wiz->save();
                    }
                }

                $this->redirect(array('view','id'=>$model->OrderCode));
            }
        }

        $this->render('add',array(
            'model'=>$model,
            'detail'=>$detail,
            'container'=>$container,
            'seal'=>$seal,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $container = Container::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE));
        $seal = new OrderBLContainer();

        if(isset($_POST['OrderHeader']))
        {
            $tempDetail = array();
            if (isset($_POST['OrderDetailTemp'])) {
                foreach ($_POST['OrderDetailTemp'] as $d) {
                    $code = $d['IDOrderDetailTemp'];
                    if($code == ''){
                        $detail = new OrderDetailTemp();
                        $detail->attributes = $d;

                        array_push($tempDetail, $detail);
                    }
                    else{
                        $orderDetailTemp = OrderDetailTemp::model()->findByPk($code);
                        if($orderDetailTemp != null){
                            $orderDetailTemp->QTY = $d['QTY'];
                            $orderDetailTemp->TotalOrder = $d['TotalOrder'];
                            $orderDetailTemp->TotalVolume = $d['TotalVolume'];
                            $orderDetailTemp->save();
                        }
                    }
                }
            }
            $model->detailsTemp = $tempDetail;

            $model->attributes=$_POST['OrderHeader'];
            if($model->save()){
                foreach($model->detailsTemp as $d){
                    $d->OrderID = $model->OrderCode;
                    $d->StatusOrder = $model->OrderStatusCode;
                    $d->Status = EnumStatus::NON_ACTIVE;
                    if(!$d->save()){
                        $message = '';
                        $errors = $d->getErrors();
                        foreach ($errors as $error){
                            foreach ($error as $e){
                                $message.= $e.'<br />';
                            }
                        }
                        throw new Exception('Error when saving detail order '.$message);
                    }
                }

                $this->redirect(array('view','id'=>$model->OrderCode));
            }
        }

        $this->render('update',array(
            'model'=>$model,
            'seal'=>$seal,
            'container'=>$container,
        ));
    }

    public function actionCreatePo($id)
    {
        $model=$this->loadModel($id);
        $model->PORefCode = IDGenerator::generate('po', $model->OrderCode);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['OrderHeader']))
        {
            $model->attributes=$_POST['OrderHeader'];
            if($model->save()){
                $wiz = Wiz::model()->findByAttributes(array('OrderID'=>$model->OrderCode));
                if($wiz == null){
                    $w = new Wiz();
                    $w->OrderID = $model->OrderCode;
                    $w->WizStep = 3;
                    $w->save();
                }
                else{
                    if($wiz->WizStep < 3){
                        $wiz->WizStep = 3;
                        $wiz->save();
                    }
                }

                Yii::app()->user->setFlash('success','Success create PO');
                $this->redirect(array('view','id'=>$model->OrderCode));
            }
        }

        $this->render('addPo',array(
            'model'=>$model,
        ));
    }

    public function actionReceiveBill($id)
    {
        $model=$this->loadModel($id);
        $model->PORefCode = IDGenerator::generate('po', $model->OrderCode);
        $model->SupplierFee = round($model->SupplierFee);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['OrderHeader']))
        {
            $tempDetail = array();
            if (isset($_POST['OrderTermPayment'])) {
                foreach ($_POST['OrderTermPayment'] as $d) {
                    $code = $d['IDOrderTermPayment'];
                    if($code == ''){
                        $detail = new OrderTermPayment();
                        $detail->attributes = $d;

                        array_push($tempDetail, $detail);
                    }
                    else{
                        $term = OrderTermPayment::model()->findByPk($code);
                        if($term != null){
                            $term->PercentageofPayment = $d['PercentageofPayment'];
                            $term->ValuePercentageOfPayment = $d['ValuePercentageOfPayment'];
                            $term->IssueDate = $d['IssueDate'];
                            $term->ExpiredDate = $d['ExpiredDate'];
                            $term->save();
                        }
                    }
                }
            }
            $model->terms = $tempDetail;

            $model->attributes=$_POST['OrderHeader'];
            $model->BillSupplierTermofPayment = count($model->terms);
            if($model->save()){
                foreach($model->terms as $d){
                    $d->OrderID = $model->OrderCode;
                    $d->Status = EnumStatus::ACTIVE;
                    if(!$d->save()){
                        $message = '';
                        $errors = $d->getErrors();
                        foreach ($errors as $error){
                            foreach ($error as $e){
                                $message.= $e.'<br />';
                            }
                        }
                        throw new Exception('Error when saving terms order '.$message);
                    }
                }

                $wiz = Wiz::model()->findByAttributes(array('OrderID'=>$model->OrderCode));
                if($wiz == null){
                    $w = new Wiz();
                    $w->OrderID = $model->OrderCode;
                    $w->WizStep = 4;
                    $w->save();
                }
                else{
                    if($wiz->WizStep < 4){
                        $wiz->WizStep = 4;
                        $wiz->save();
                    }
                }

                Yii::app()->user->setFlash('success','Success create PO');
                $this->redirect(array('view','id'=>$model->OrderCode));
            }
        }

        $this->render('addBill',array(
            'model'=>$model,
        ));
    }

    public function actionUpdatePo($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['OrderHeader']))
        {
            $model->attributes=$_POST['OrderHeader'];
            if($model->save()){
                $wiz = Wiz::model()->findByAttributes(array('OrderID'=>$model->OrderCode));
                if($wiz == null){
                    $w = new Wiz();
                    $w->OrderID = $model->OrderCode;
                    $w->WizStep = 3;
                    $w->save();
                }
                else{
                    if($wiz->WizStep < 3){
                        $wiz->WizStep = 3;
                        $wiz->save();
                    }
                }

                Yii::app()->user->setFlash('success','Success update PO');
                $this->redirect(array('view','id'=>$model->OrderCode));
            }
        }

        $this->render('updatePo',array(
            'model'=>$model,
        ));
    }

    public function actionCreatePi($id)
    {
        $model=$this->loadModel($id);
        $model->BillCodeSupplier = IDGenerator::generate('pi', $model->OrderCode);
        $model->OrderTotal = round($model->OrderTotal, 0);
        $model->BuyerFee = round($model->BuyerFee, 0);
        $model->BuyerFeeAmount = round($model->BuyerFeeAmount, 0);
        $model->FreightFee = round($model->FreightFee, 0);

        $listCommision = array();
    		$commision = CustomerCommisionSchema::model()->findAllByAttributes(array('CustomerCode'=>$model->CustomerCode));
    		foreach ($commision as $key => $value) {
    				$listCommision[$value->commision->Percentage] = $value->commision->ComisionNameSkema.' ('.$value->commision->Percentage.'%)';
    		}

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['OrderHeader']))
        {
            $tempDetail = array();
            if (isset($_POST['OrderChargeDetail'])) {
                foreach ($_POST['OrderChargeDetail'] as $d) {
                    $detail = new OrderChargeDetail();
                    $detail->attributes = $d;

                    array_push($tempDetail, $detail);
                }
            }
            $model->charges = $tempDetail;

            $model->attributes=$_POST['OrderHeader'];
            if($model->save()){
                foreach($model->charges as $d){
                    $d->OrderID = $model->OrderCode;
                    if(!$d->save()){
                        $message = '';
                        $errors = $d->getErrors();
                        foreach ($errors as $error){
                            foreach ($error as $e){
                                $message.= $e.'<br />';
                            }
                        }
                        throw new Exception('Error when saving order charge '.$message);
                    }
                }

                $wiz = Wiz::model()->findByAttributes(array('OrderID'=>$model->OrderCode));
                if($wiz == null){
                    $w = new Wiz();
                    $w->OrderID = $model->OrderCode;
                    $w->WizStep = 5;
                    $w->save();
                }
                else{
                    if($wiz->WizStep < 5){
                        $wiz->WizStep = 5;
                        $wiz->save();
                    }
                }

                Yii::app()->user->setFlash('success','Success create PI');
                $this->redirect(array('view','id'=>$model->OrderCode));
            }
        }

        $this->render('addPi',array(
            'model'=>$model,
            'listCommision'=>$listCommision,
        ));
    }

    public function actionUpdatePi($id)
    {
        $model=$this->loadModel($id);
        $amount = ($model->OrderTotal * $model->BuyerFee)/100;

        $model->BuyerFeeAmount = round($amount, 0);
        $model->FreightFee = round($model->FreightFee, 0);
        $model->OrderTotal = round($model->OrderTotal, 0);

        $listCommision = array();
    		$commision = CustomerCommisionSchema::model()->findAllByAttributes(array('CustomerCode'=>$model->CustomerCode));
    		foreach ($commision as $key => $value) {
    				$listCommision[$value->commision->Percentage] = $value->commision->ComisionNameSkema.' ('.$value->commision->Percentage.'%)';
    		}

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['OrderHeader']))
        {
            $tempDetail = array();
            if (isset($_POST['OrderChargeDetail'])) {
                foreach ($_POST['OrderChargeDetail'] as $d) {
                    $code = $d['IDOrderChargeDetail'];
                    if($code == ''){
                        $detail = new OrderChargeDetail();
                        $detail->attributes = $d;

                        array_push($tempDetail, $detail);
                    }
                    else{
                        $detail = OrderChargeDetail::model()->findByPk($code);
                        if($detail != null){
                            $detail->Value = $d['Value'];
                            $detail->save();
                        }
                    }
                }
            }
            $model->charges = $tempDetail;

            $model->attributes=$_POST['OrderHeader'];
            if($model->save()){
                foreach($model->charges as $d){
                    $d->OrderID = $model->OrderCode;
                    if(!$d->save()){
                        $message = '';
                        $errors = $d->getErrors();
                        foreach ($errors as $error){
                            foreach ($error as $e){
                                $message.= $e.'<br />';
                            }
                        }
                        throw new Exception('Error when saving order charge '.$message);
                    }
                }

                $wiz = Wiz::model()->findByAttributes(array('OrderID'=>$model->OrderCode));
                if($wiz == null){
                    $w = new Wiz();
                    $w->OrderID = $model->OrderCode;
                    $w->WizStep = 5;
                    $w->save();
                }
                else{
                    if($wiz->WizStep < 5){
                        $wiz->WizStep = 5;
                        $wiz->save();
                    }
                }

                Yii::app()->user->setFlash('success','Success update PI');
                $this->redirect(array('view','id'=>$model->OrderCode));
            }
        }

        $this->render('updatePi',array(
            'model'=>$model,
            'listCommision'=>$listCommision,
        ));
    }

    public function actionRelease($id)
    {
        $model=$this->loadModel($id);
        $model->OrderStatusCode = EnumOrder::ORDER;
        if($model->save()){
            foreach($model->detailsTemp as $d){
                $d->Status = EnumStatus::ACTIVE;
                if($d->save()){
                    $details = OrderDetail::model()->findByAttributes(array('OrderID'=>$d->OrderID));
                    if($details == null){
                        $detail = new OrderDetail();
                        $detail->OrderID = $d->OrderID;
                        $detail->ProductID = $d->ProductID;
                        $detail->QTY = $d->QTY;
                        $detail->TotalVolume = $d->TotalVolume;
                        $detail->TotalOrder = $d->TotalOrder;
                        $detail->Note = $d->Note;
                        $detail->SupplierFee = $d->SupplierFee;
                        $detail->StatusOrder = $d->StatusOrder;
                        $detail->Status = EnumStatus::ACTIVE;
                        $detail->ContainerID = $d->ContainerID;
                        if(!$detail->save()){
                            $message = '';
                            $errors = $detail->getErrors();
                            foreach ($errors as $error){
                                foreach ($error as $e){
                                    $message.= $e.'<br />';
                                }
                            }
                            throw new Exception('Error when saving detail order '.$message);
                        }
                    }
                }
            }

            Yii::app()->user->setFlash('success','Success release order');
            $this->redirect(array('view','id'=>$model->OrderCode));
        }
        else{
            $message = '';
            $errors = $model->getErrors();
            foreach ($errors as $error){
                foreach ($error as $e){
                    $message.= $e.'<br />';
                }
            }

            Yii::app()->user->setFlash('info', $message);
            $this->redirect(array('view','id'=>$model->OrderCode));
        }
    }

    public function actionFinish($id)
    {
        $model=$this->loadModel($id);
        $model->OrderStatusCode = EnumOrder::FINISH;
        if($model->save()){
            Yii::app()->user->setFlash('success','Success finish order');
            $this->redirect(array('view','id'=>$model->OrderCode));
        }
        else{
            Yii::app()->user->setFlash('danger','Failed finish order');
            $this->redirect(array('view','id'=>$model->OrderCode));
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        // we only allow deletion via POST request
        $model = $this->loadModel($id);
        $model->OrderStatusCode = EnumOrder::CANCEL;
        if($model->save()){
            Yii::app()->user->setFlash('info','Success cancel order');
            $this->redirect(array('index'));
        }
        else{
            Yii::app()->user->setFlash('danger','Failed cancel order');
            $this->redirect(array('view','id'=>$model->OrderCode));
        }
    }

    /**
     * Deletes a list of model.
     */
    public function actionDeleteSelected()
    {
        if(Yii::app()->request->isPostRequest && isset($_POST['ids']))
        {
            // delete
            foreach ($_POST['ids'] as $id) {
                $this->loadModel($id)->delete();
            }
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model=new OrderHeader('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['OrderHeader']))
            $model->attributes=$_GET['OrderHeader'];

        $this->render('index',array(
            'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model=OrderHeader::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='order-header-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAddDetail(){
        if(Yii::app()->request->isAjaxRequest){
            $json = array(
                'success'=>false,
                'content'=>'',
                'volume'=>'',
                'grossWeight'=>'',
                'nettWeight'=>'',
                'price'=>'',
                'message'=>'',
            );

            if (isset($_POST['product']) && isset($_POST['qty'])) {
                $detail = new OrderDetailTemp();

                $product = Product::model()->findByAttributes(array('Status'=>EnumStatus::ACTIVE, 'ProductID'=>$_POST['product']));
                if($product == NULL){
                    $json['success'] = false;
                    $json['message'] = 'Product not found';
                }
                else if($_POST['qty'] < $product->MinQTY){
                    $json['success'] = false;
                    $json['message'] = "Min QTY for this product is ".number_format($product->MinQTY).". Failed add product";
                }
                else{
                    if($product->uom->define->QTYUOMSmall == null){
                        $json['message'] = 'Failed calculate quantity.';
                    }
                    else{
                        $totalVolumeGross = $_POST['qty'] * $product->VolumeGross;
                        $total = ($_POST['qty'] * $product->uom->define->QTYUOMSmall) * $product->Price;
                        $totalWeightGross = $_POST['qty'] * $product->GrossWeight;
                        $totalWeightNett = $_POST['qty'] * $product->NettWeight;

                        $detail->ProductID = $product->ProductID;
                        $detail->QTY = $_POST['qty'];
                        $detail->Note = $_POST['note'];
                        $detail->TotalVolume = $totalVolumeGross;
                        $detail->TotalGrossWeight = $totalWeightGross;
                        $detail->TotalNettWeight = $totalWeightNett;
                        $detail->TotalOrder = $total;

                        if($detail->validate()){
                            $json['content'] = $this->renderPartial('_formDetail', array(
                                'detail'=>$detail,
                                'index'=>$_POST['index'],
                            ), true);
                            $json['volume'] = $totalVolumeGross;
                            $json['grossWeight'] = $totalWeightGross;
                            $json['nettWeight'] = $totalWeightNett;
                            $json['price'] = $total;
                            $json['success'] = true;
                        }
                        else{
                            $message = '';
                            $errors = $detail->getErrors();
                            foreach ($errors as $key => $error) {
                                foreach ($error as $key2 => $e) {
                                    $message.= $e."\n";
                                }
                            }

                            $json['message'] = $message;
                        }
                    }
                }
            }
            else{
                $json['message'] = 'The requested page does not exist.';
            }

            echo CJSON::encode($json);
            Yii::app()->end();
        }
    }

    public function actionAddDetailBL(){
        if(Yii::app()->request->isAjaxRequest){
            $json = array(
                'success'=>false,
                'content'=>'',
                'volume'=>'',
                'lastvolume'=>'',
                'weight'=>'',
                'lastweight'=>'',
                'container'=>'',
                'price'=>'',
                'message'=>'',
            );

            if (isset($_POST['product']) && isset($_POST['qty'])) {
                $detail = new OrderDetailTemp();

                $product = Product::model()->findByAttributes(array('Status'=>EnumStatus::ACTIVE, 'ProductID'=>$_POST['product']));
                if($product == NULL){
                    $json['success'] = false;
                    $json['message'] = 'Product not found';
                }
                else if($_POST['qty'] < $product->MinQTY){
                    $json['success'] = false;
                    $json['message'] = "Min QTY for this product is ".number_format($product->MinQTY).". Failed add product";
                }
                else{
                    if($product->uom->define->QTYUOMSmall == null){
                        $json['message'] = 'Failed calculate quantity.';
                    }
                    else{
                        $totalVolumeGross = $_POST['qty'] * $product->VolumeGross;
                        $total = ($_POST['qty'] * $product->uom->define->QTYUOMSmall) * $product->Price;
                        $totalWeightGross = $_POST['qty'] * $product->GrossWeight;

                        $criteria=new CDbCriteria;
                        $criteria->condition = "UsableVolume >= :totalVolumeGross and NettWeight >= :totalWeightGross and Status = '".EnumStatus::ACTIVE."'";
                        $criteria->params = array (
                            ':totalVolumeGross' => $totalVolumeGross,
                            ':totalWeightGross' => $totalWeightGross,
                        );
                        $container = Container::model()->find($criteria);

                        if($container != null){
                            $detail->ProductID = $product->ProductID;
                            $detail->QTY = $_POST['qty'];
                            $detail->Note = $_POST['note'];
                            $detail->TotalVolume = $totalVolumeGross;
                            $detail->TotalWeight = $totalWeightGross;
                            $detail->TotalOrder = $total;
                            $detail->ContainerID = $container->ContainerCode;

                            if($detail->validate()){
                                $listContainer = array();
                                $containerCek = OrderBLContainer::model()->findAllByAttributes(array('OrderID'=>$_POST['order']));
                                $no = 1;
                                foreach ($containerCek as $key => $value) {
                                    $listContainer[$value->IDOrderBLContainer] = $no.'. '.$value->container->ContainerName;

                                    $no++;
                                }

                                $json['content'] = $this->renderPartial('_formDetailBL', array(
                                    'detail'=>$detail,
                                    'container'=>$container,
                                    'listContainer'=>$listContainer,
                                    'index'=>$_POST['index'],
                                ), true);
                                $json['volume'] = $totalVolumeGross;
                                $json['lastvolume'] = $container->UsableVolume - $totalVolumeGross;
                                $json['weight'] = $totalWeightGross;
                                $json['lastweight'] = $container->NettWeight - $totalWeightGross;
                                $json['container'] = $container->ContainerCode;
                                $json['price'] = $total;
                                $json['success'] = true;
                            }
                            else{
                                $message = '';
                                $errors = $detail->getErrors();
                                foreach ($errors as $key => $error) {
                                    foreach ($error as $key2 => $e) {
                                        $message.= $e."\n";
                                    }
                                }

                                $json['message'] = $message;
                            }
                        }
                        else{
                            $json['message'] = 'No container matches with total volume '.$totalVolumeGross.' and total weight '.$totalWeightGross.'. Please check container';
                        }
                    }
                }
            }
            else{
                $json['message'] = 'The requested page does not exist.';
            }

            echo CJSON::encode($json);
            Yii::app()->end();
        }
    }

    public function actionCheckOrder(){
        if(Yii::app()->request->isAjaxRequest){
            $json = array(
                'success'=>false,
                'container'=>'',
                'totalVolume'=>'',
                'totalOrder'=>'',
                'totalWeight'=>'',
            );

            if (isset($_POST['container']) && isset($_POST['totalWeight']) && isset($_POST['totalOrder'])  && isset($_POST['totalVolume'])) {

                $totalVolume = $_POST['totalVolume'];
                $totalWeight = $_POST['totalWeight'];
                $totalOrder = $_POST['totalOrder'];

                $container = null;
                $containerCek = OrderBLContainer::model()->findByPk($_POST['container']);
                if($containerCek != null){
                    $criteria=new CDbCriteria;
                    $criteria->condition = "ContainerCode = :containerCode and UsableVolume >= :totalVolumeGross and NettWeight >= :totalWeightGross and Status = '".EnumStatus::ACTIVE."'";
                    $criteria->params = array (
                        ':containerCode' => $containerCek->ContainerCode,
                        ':totalVolumeGross' => $totalVolume,
                        ':totalWeightGross' => $totalWeight,
                    );
                    $container = Container::model()->find($criteria);
                }

                if($container == null){
                    $json['message'] = 'No container matches with total volume '.$totalVolume.' and total weight '.$totalWeight.'. Please check container';
                }
                else{
                    $json['containerBL'] = $containerCek->IDOrderBLContainer;

                    $json['totalVolume'] = $totalVolume;
                    $json['totalWeight'] = $totalWeight;
                    $json['totalOrder'] = $totalOrder;
                    $json['success'] = true;
                }
            }
            else{
                $json['message'] = 'The requested page does not exist.';
            }

            echo CJSON::encode($json);
            Yii::app()->end();
        }
    }

    public function actionUpdateOrder(){
        if(Yii::app()->request->isAjaxRequest){
            $json = array(
                'success'=>false,
                'qty'=>'',
                'qtyUnit'=>'',
                'qtyUnitOld'=>'',
                'totalVolume'=>'',
                'totalVolumeOld'=>'',
                'totalOrder'=>'',
                'totalOrderOld'=>'',
                'totalGrossWeight'=>'',
                'totalGrossWeightOld'=>'',
                'totalNettWeight'=>'',
                'totalNettWeightOld'=>'',
            );

            if (isset($_POST['product']) && isset($_POST['qty'])) {
                $product = Product::model()->findByAttributes(array('Status'=>EnumStatus::ACTIVE, 'ProductID'=>$_POST['product']));
                $totalVolumeGross = 0;
                $totalWeightGross = 0;
                $totalWeightNett = 0;
                $totalOrder = 0;
                $smallQTY = 0;
                if($product->uom->define->QTYUOMSmall != null){
                    $smallQTY = $product->uom->define->QTYUOMSmall;
                }

                if($product != null){
                    $totalVolumeGross = $_POST['qty'] * $product->VolumeGross;
                    $totalWeightGross = $_POST['qty'] * $product->GrossWeight;
                    $totalWeightNett = $_POST['qty'] * $product->NettWeight;
                    $totalOrder = ($_POST['qty'] * $smallQTY) * $product->Price;
                    $totalUnit = $_POST['qty'] * $smallQTY;
                }

                if($product == NULL){
                    $json['success'] = false;
                    $json['qty'] = $_POST['qtyOld'];
                    $json['message'] = 'Product not found';
                }
                else{
                    $totalVolumeGrossOld = $_POST['qtyOld'] * $product->VolumeGross;
                    $totalWeightGrossOld = $_POST['qtyOld'] * $product->GrossWeight;
                    $totalWeightNettOld = $_POST['qtyOld'] * $product->NettWeight;
                    $totalOrderOld = ($_POST['qtyOld'] * $smallQTY) * $product->Price;
                    $totalUnitOld = $_POST['qtyOld'] * $smallQTY;

                    $json['totalVolumeOld'] = $totalVolumeGrossOld;
                    $json['totalGrossWeightOld'] = $totalWeightGrossOld;
                    $json['totalNettWeightOld'] = $totalWeightNettOld;
                    $json['totalNettWeightOld'] = $totalWeightNettOld;
                    $json['totalOrderOld'] = $totalOrderOld;

                    $json['qtyUnit'] = $totalUnit;
                    $json['qtyUnitOld'] = $totalUnitOld;

                    $json['qty'] = $_POST['qty'];
                    $json['qtyOld'] = $_POST['qtyOld'];

                    $json['totalVolume'] = $totalVolumeGross;
                    $json['totalGrossWeight'] = $totalWeightGross;
                    $json['totalNettWeight'] = $totalWeightNett;
                    $json['totalOrder'] = $totalOrder;
                    $json['success'] = true;
                }
            }
            else{
                $json['message'] = 'The requested page does not exist.';
            }

            echo CJSON::encode($json);
            Yii::app()->end();
        }
    }

    public function actionAddBLContainer(){
        if(Yii::app()->request->isAjaxRequest){
            $json = array(
                'success'=>false,
                'content'=>'',
                'message'=>'',
            );

            if (isset($_POST['order']) && isset($_POST['container'])) {
                $detail = new OrderBLContainer();

                $order = OrderHeader::model()->findByPk($_POST['order']);
                $container = Container::model()->findByPk($_POST['container']);
                if($order == NULL){
                    $json['success'] = false;
                    $json['message'] = 'Order not found';
                }
                else if($container == NULL){
                    $json['success'] = false;
                    $json['message'] = 'Container not found';
                }
                else{
                    $detail->OrderID = $order->OrderCode;
                    $detail->ContainerCode = $container->ContainerCode;
                    if($detail->save()){
                        $json['content'] = $this->renderPartial('_formDetailContainer', array(
                            'detail'=>$detail,
                            'container'=>$container,
                        ), true);
                        $json['success'] = true;
                    }
                    else{
                        $message = '';
                        $errors = $detail->getErrors();
                        foreach ($errors as $key => $error) {
                            foreach ($error as $key2 => $e) {
                                $message.= $e."\n";
                            }
                        }

                        $json['message'] = $message;
                    }
                }
            }
            else{
                $json['message'] = 'The requested page does not exist.';
            }

            echo CJSON::encode($json);
            Yii::app()->end();
        }
    }
}
