<?php

class ProductController extends AdminController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$info = ProductListInfo::model()->findByPk($model->ProductID);

		$this->render('view',array(
			'model'=>$model,
			'info'=>$info,
		));
	}

    public function actionImport(){
        $model = new UploadForm();

				if(isset($_POST['UploadForm']))
				{
						$phpExcelPath = Yii::getPathOfAlias('site.vendors.phpexcel.Classes');
						include_once($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
						require_once $phpExcelPath. '/PHPExcel/IOFactory.php';

						if(file_exists(UploadForm::getFilePath().'product.xlsx')){
								$path = UploadForm::getFilePath().'product.xlsx';
						}
						else{
								$path = UploadForm::getFilePath().'product.xls';
						}

						try{
								$inputFileType = PHPExcel_IOFactory::identify($path);
								$objReader = PHPExcel_IOFactory::createReader($inputFileType);
								$objPHPExcel = $objReader->load($path);
								$objPHPExcel->setActiveSheetIndex(0);
								$data2 = $objPHPExcel->getActiveSheet();

								$i = 1;
								foreach ($objPHPExcel->getActiveSheet()->getDrawingCollection() as $drawing) {
										if ($drawing instanceof PHPExcel_Worksheet_MemoryDrawing) {
												ob_start();
												call_user_func(
														$drawing->getRenderingFunction(),
														$drawing->getImageResource()
												);
												$imageContents = ob_get_contents();
												ob_end_clean();
												switch ($drawing->getMimeType()) {
														case PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_PNG :
																		$extension = 'png'; break;
														case PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_GIF:
																		$extension = 'gif'; break;
														case PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_JPEG :
																		$extension = 'jpg'; break;
												}
										} else {
												$zipReader = fopen($drawing->getPath(),'r');
												$imageContents = '';
												while (!feof($zipReader)) {
														$imageContents .= fread($zipReader,1024);
												}
												fclose($zipReader);
												//$extension = $drawing->getExtension();
												$extension = 'png';
										}

										$imgname = preg_replace('/[^0-9]/', '', $drawing->getCoordinates());
										$myFileName = UploadForm::getFilePath().'Image_'.$imgname.'.png';
										file_put_contents($myFileName,$imageContents);
								}

								$this->redirect(array('reviewUpload'));

						} catch(Exception $e) {
								throw new CHttpException(404,'Error read excel: '.$e->getMessage());
						}
				}
				else{
						if(file_exists(UploadForm::getFilePath().'product.xls') || file_exists(UploadForm::getFilePath().'product.xlsx')){
								@unlink(UploadForm::getFilePath().'product.xls');
								@unlink(UploadForm::getFilePath().'product.xlsx');

						}
				}

        $this->render('import',array(
            'model'=>$model,
        ));
    }

    public function actionReviewUpload(){
        if(file_exists(UploadForm::getFilePath().'product.xls') || file_exists(UploadForm::getFilePath().'product.xlsx')){
            $model = new UploadForm();

            //get column name product
            $sql = "SELECT *FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'Product' and COLUMN_NAME not in('SellerCode','Status','EANCode','SellerSKU','StokQTY','MinQTY')";
            $c =  Yii::app()->db->createCommand($sql)->queryAll();
            $list = array();
            /*foreach($c as $col){
                $list[$col['COLUMN_NAME']] = $col['COLUMN_NAME'];
            }*/
						$list['ProductName'] = 'Description';
						$list['ProductCode'] = 'Product Code';
						$list['CategoryCode'] = 'Product Category';
						$list['SellerCode'] = 'Seller';
						$list['UOMCode'] = 'UOM';
						$list['Price'] = 'Price';
						$list['UPC'] = 'Unit Per Carton';
						$list['StokQTY'] = 'Stock QTY';
						$list['InnerCarton'] = 'Inner Carton';
						$list['NettWeight'] = 'Nett Weight';
						$list['GrossWeight'] = 'Gross Weight';
						$list['VolumeGross'] = 'CBM';

						$attr = AtributeMaster::model()->findAll();
						foreach($attr as $att){
								$list[$att->AtributeCode] = $att->AtributeName;
						}

            if(isset($_POST['UploadForm'])){
                $model->setAttributes($_POST['UploadForm']);
                $this->uploadFile();
            }

            $this->render('dataUpload',array(
                'model'=>$model,
                'list'=>$list,
            ));
        }
        else{
            $this->redirect('import');
        }
    }

    function getExcel(){
        $phpExcelPath = Yii::getPathOfAlias('site.vendors.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        require_once $phpExcelPath. '/PHPExcel/IOFactory.php';

        if(file_exists(UploadForm::getFilePath().'product.xlsx')){
            $path = UploadForm::getFilePath().'product.xlsx';
        }
        else{
            $path = UploadForm::getFilePath().'product.xls';
        }

        try{
            $inputFileType = PHPExcel_IOFactory::identify($path);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($path);
            $objPHPExcel->setActiveSheetIndex(0);
            $data = $objPHPExcel->getActiveSheet();
        } catch(Exception $e) {
            throw new CHttpException(404,'Error read excel: '.$e->getMessage());
        }

        $highestRow = $data->getHighestRow();
        $highestColumn = $data->getHighestColumn();
        if(!($highestRow -1) >0)
        {
            throw new CHttpException('No data found in Sheet 0 or Sheet not available.');
        }

        try {

            $row = 1;
            $lastColumn = $highestColumn;
            $lastColumn++;
            for ($column = 'A'; $column != $lastColumn; $column++) {
                $rowData = $data->rangeToArray($column.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);

                return $rowData;
            }
        } catch(Exception $e) {
            throw new CHttpException(404,'Failed reading excel: '.$e->getMessage());
        }
    }

    public function actionDeleteImport()
    {
        if(file_exists(UploadForm::getFilePath().'product.xls')){
            $filename = 'product.xls';
        }
        else{
            $filename = 'product.xlsx';
        }

        @unlink(UploadForm::getFilePath().$filename);
        Yii::app()->user->setFlash('success','Delete success!');
        $this->redirect('import');
    }

    function uploadFile(){
        $phpExcelPath = Yii::getPathOfAlias('site.vendors.phpexcel.Classes');
        include_once($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        require_once $phpExcelPath. '/PHPExcel/IOFactory.php';

        if(file_exists(UploadForm::getFilePath().'product.xlsx')){
            $path = UploadForm::getFilePath().'product.xlsx';
        }
        else{
            $path = UploadForm::getFilePath().'product.xls';
        }

        try{
            $inputFileType = PHPExcel_IOFactory::identify($path);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($path);
            $objPHPExcel->setActiveSheetIndex(0);
            $data2 = $objPHPExcel->getActiveSheet();
        } catch(Exception $e) {
            throw new CHttpException(404,'Error read excel: '.$e->getMessage());
        }

        $highestRow = $data2->getHighestRow();
        $highestColumn = $data2->getHighestColumn();
        if(!($highestRow -1) >0)
        {
            throw new CHttpException('No data found in Sheet 0 or Sheet not available.');
        }

				function toNumber($dest)
				{
						if ($dest)
								return ord(strtolower($dest)) - 96;
						else
								return 0;
				}

				// datanya dimulai dari row 2
				$message = '';
				for($a=2; $a<=$highestRow; ++$a) {
            $rowData = $data2->rangeToArray('A'.$a.':'.$highestColumn.$a,NULL,TRUE,FALSE);

						if($rowData[0][0] != ''){
								$kode = IDGenerator::generate('product');

								$product = new Product();
								$product->ProductID = $kode;
								$product->SellerCode = $_POST['UploadForm']['sellerID'];
								$product->Status = $_POST['UploadForm']['status'];
								if(isset($_POST['UploadForm']['kodeProductCategory'])){
										$product->CategoryCode = $_POST['UploadForm']['kodeProductCategory'];
								}
								if(isset($_POST['UploadForm']['isStock'])){
										$product->isStock = $_POST['UploadForm']['isStock'];
								}
								if(isset($_POST['UploadForm']['isProduction'])){
										$product->isProduction = $_POST['UploadForm']['isProduction'];
								}
								if(isset($_POST['UploadForm']['isCatalogue'])){
										$product->isCatalogue = $_POST['UploadForm']['isCatalogue'];
								}

								if(file_exists(UploadForm::getFilePath().'Image_'.$a.'.png')){
										copy(UploadForm::getFilePath().'Image_'.$a.'.png', Product::getFilePath().$kode.'.png');
										@unlink(UploadForm::getFilePath().'Image_'.$a.'.png');

										$product->ThubnailsImage = $kode.'.png';
								}

								for($aa=0;$aa<tonumber($highestColumn);$aa++){
										if($rowData[0][0] != ''){
												if($_POST['UploadForm']['column'][$aa] != ''){
														$column = $_POST['UploadForm']['column'][$aa];
														$field = $rowData[0][$aa];

														if($column == 'CategoryCode'){
																$cp = CategoryProduct::model()->findByAttributes(array('CategoryName'=>trim($field)));
																if($cp != null){
																		$product->CategoryCode = $cp->CategoryCode;
																}
																else{
																		$dcp = new CategoryProduct();
																		$dcp->CategoryCode = IDGenerator::generate('categoryProduct');
																		$dcp->CategoryName = $field;
																		$dcp->Status = EnumStatus::ACTIVE;
																		if($dcp->save()){
																				$product->CategoryCode = $dcp->CategoryCode;
																		}
																}
														}
														else if($column == 'Price'){
																$d = new ProductPriceHistory();
																$d->ProductID = $kode;
																$d->Date = date('Y-m-d H:i:s');
																$d->BeginPrice = $field;
																$d->ENDPrice = $field;
																$d->save();
														}
														else if($column == 'StokQTY'){
																$d = new ProductStockHistory();
																$d->ProductID = $kode;
																$d->Date = date('Y-m-d H:i:s');
																$d->BeginQTY = $field;
																$d->QTYIN = 0;
																$d->QTYOUT = 0;
																$d->ENDQTY = $field;
																$d->save();
														}
														else if($column == 'UOMCode'){
																$uom = UOM::model()->findByAttributes(array('UOMName'=>trim($field)));
																if($uom != null){
																		$product->UOMCode = $uom->UOMCode;
																}
																else{
																		$duom = new UOM();
																		$duom->UOMCode = IDGenerator::generate('uom');;
																		$duom->UOMName = $field;
																		$duom->Status = EnumStatus::ACTIVE;
																		if($duom->save()){
																				$product->UOMCode = $duom->UOMCode;
																		}
																}
														}
														else if($column == 'UPC'){
																$upc = UOMDefine::model()->findByAttributes(array('QTYUOMSmall'=>trim($field)));
																if($upc != null){
																		$product->UOMCode = $upc->UOMCode;
																}
														}
														else{
																$attr = AtributeMaster::model()->findByPk($column);
																if($attr != null){
																		$attProd = new ProductAtribute();
																		$attProd->ProductID = $kode;
																		$attProd->AtributeCode = $attr->AtributeCode;
																		$attProd->AtributeValue = $field;
																		$attProd->save();
																}
																else{
																		if(!isset($_POST['UploadForm']['StartProduction']) || !isset($_POST['UploadForm']['EndProduction']) || !isset($_POST['UploadForm']['DeadlineProduction'])){
																				$product->$column = $field;
																		}
																}
														}
												}
										}
								}

								if(!$product->save()){

										if(isset($_POST['UploadForm']['StartProduction']) || isset($_POST['UploadForm']['EndProduction']) || isset($_POST['UploadForm']['DeadlineProduction'])){
												$info = new ProductListInfo();
												$info->ProductID = $kode;
												if(isset($_POST['UploadForm']['StartProduction'])){
														$info->$_POST['UploadForm']['StartProduction'] = $_POST['UploadForm']['StartProduction'];
												}
												if(isset($_POST['UploadForm']['EndProduction'])){
														$info->$_POST['UploadForm']['EndProduction'] = $_POST['UploadForm']['EndProduction'];
												}
												if(isset($_POST['UploadForm']['DeadlineProduction'])){
														$info->$_POST['UploadForm']['DeadlineProduction'] = $_POST['UploadForm']['DeadlineProduction'];
												}
												$info->save();
										}

										$errors = $product->getErrors();
										foreach ($errors as $error){
												foreach ($error as $e){
														$message.= $e.'<br />';
												}
										}
								}
						}
        }

				$kata = '';
				if($message != ''){
						$kata = '<b>With message:<b> <br><br>'.$message;
				}

        @unlink(UploadForm::getFilePath().$_POST['UploadForm']['filename']);
        Yii::app()->user->setFlash('success','Upload success! '.$kata);
        $this->redirect('index');
    }

    public function actionUploadExcel()
    {
        $allowedExtensions = Yii::app()->params['excelExtension'];
        $sizeLimit = 10 * 1024 * 1024; // 10mb

        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $replaceOldFile = TRUE;
        $newFileName = 'product';
        $result = $uploader->handleUpload(UploadForm::getFilePath(), $replaceOldFile, $newFileName);

        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
    }

    /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd()
	{
		$model=new Product;

		$listUOM = array();
		$UOM = UOM::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE));
		foreach ($UOM as $key => $value) {
				$small = 0;
				if($value->define != null){
						$small = $value->define->QTYUOMSmall;
				}
				$listUOM[$value->UOMCode] = $value->UOMName.' (UOM Value: '.number_format($small).')';
		}
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Product']))
		{
            $tempDetail = array();
            if (isset($_POST['ProductAtribute'])) {
                foreach ($_POST['ProductAtribute'] as $d) {
                    $detail = new ProductAtribute();
                    $detail->attributes = $d;

                    array_push($tempDetail, $detail);
                }
            }
            $model->atributes = $tempDetail;

			$model->attributes=$_POST['Product'];
            $model->ProductID = IDGenerator::generate('product');
            $model->ThubnailsImage = Product::getFilePath().$model->ThubnailsImage;
            $model->Barcode = Product::getFilePath().$model->Barcode;

			if($model->save()){
							$info = new ProductListInfo();
							$info->ProductID = $model->ProductID;
							$info->StartProduction = $model->StartProduction;
							$info->EndProduction = $model->EndProduction;
							$info->DeadlineOrder = $model->DeadlineProduction;
							$info->save();

							$d = new ProductPriceHistory();
							$d->ProductID = $model->ProductID;
							$d->Date = date('Y-m-d H:i:s');
							$d->BeginPrice = $model->Price;
							$d->ENDPrice = $model->Price;
							$d->save();

							$dc = new ProductStockHistory();
							$dc->ProductID = $model->ProductID;
							$dc->Date = date('Y-m-d H:i:s');
							$dc->BeginQTY = $model->StokQTY;
							$dc->QTYIN = 0;
							$dc->QTYOUT = 0;
							$dc->ENDQTY = $model->StokQTY;
							$dc->save();

							$i=0;
							if(isset($_POST['Product']['ImageProduct'])){
								foreach($_POST['Product']['ImageProduct'] as $image){
									$rnd = rand(0,9999);
									$uploadedFile=CUploadedFile::getInstance($model,"ImageProduct[$i]");
									$fileName = "{$rnd}-{$uploadedFile}";

									$imageProduct = new ProductImage();
									$imageProduct->ProductImageName = $fileName;
									$imageProduct->ImagePath = Product::getFilePath().$fileName;
									$imageProduct->Index = $i+1;
									$imageProduct->ProductID = $model->ProductID;
									$imageProduct->save();

									if($uploadedFile !== null){
											$uploadedFile->saveAs(Product::getFilePath().$fileName);
									}

									$i++;
								}
							}


                foreach($model->atributes as $d){
                    $d->ProductID = $model->ProductID;
                    if(!$d->save()){
                        $message = '';
                        $errors = $d->getErrors();
                        foreach ($errors as $error){
                            foreach ($error as $e){
                                $message.= $e.'<br />';
                            }
                        }
                        throw new Exception('Error when saving atribute product '.$message);
                    }
                }

				$this->redirect(array('view','id'=>$model->ProductID));
            }
		}

		$this->render('add',array(
			'model'=>$model,
			'listUOM'=>$listUOM,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->Price = number_format((float)$model->Price, 2, '.', '');
		$model->GrossWeight = number_format((float)$model->GrossWeight, 3, '.', '');
		$model->NettWeight = number_format((float)$model->NettWeight, 3, '.', '');
		$model->VolumeGross = number_format((float)$model->VolumeGross, 3, '.', '');
		$model->CartonLenght = number_format((float)$model->CartonLenght, 3, '.', '');
		$model->CartonDepth = number_format((float)$model->CartonDepth, 3, '.', '');
		$model->CartonHeight = number_format((float)$model->CartonHeight, 3, '.', '');

		$info = ProductListInfo::model()->findByAttributes(array('ProductID'=>$model->ProductID));
		if($info != null){
				$model->StartProduction = $info->StartProduction;
				$model->EndProduction = $info->EndProduction;
				$model->DeadlineOrder = $info->DeadlineProduction;
		}

		$listUOM = array();
		$UOM = UOM::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE));
		foreach ($UOM as $key => $value) {
				$small = 0;
				if($value->define != null){
						$small = $value->define->QTYUOMSmall;
				}
				$listUOM[$value->UOMCode] = $value->UOMName.' (UOM Value: '.number_format($small).')';
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Product']))
		{
            $tempDetail = array();
            if (isset($_POST['ProductAtribute'])) {
                foreach ($_POST['ProductAtribute'] as $d) {
                    $code = $d['IDProductAtribute'];
                    if($code == ''){
                        $detail = new ProductAtribute();
                        $detail->attributes = $d;

                        array_push($tempDetail, $detail);
                    }
                    else{
                        $attribute = ProductAtribute::model()->findByPk($code);
                        if($attribute != null){
                            $attribute->AtributeCode = $d['AtributeCode'];
                            $attribute->AtributeValue = $d['AtributeValue'];
                            $attribute->save();
                        }
                    }
                }
            }
            $model->atributes = $tempDetail;

			$model->attributes=$_POST['Product'];
            if($model->ThubnailsImage != null){
                $model->ThubnailsImage = $model->ThubnailsImage;
            }

						if($model->Barcode != null){
                $model->Barcode = $model->Barcode;
            }

						if($model->save()){
							$info = ProductListInfo::model()->findByAttributes(array('ProductID'=>$model->ProductID));
							if($info != null){
									$info->StartProduction = $_POST['Product']['StartProduction'];
									$info->EndProduction = $_POST['Product']['EndProduction'];
									$info->DeadlineOrder = $_POST['Product']['DeadlineProduction'];
									$info->save();
							}
							else{
								$info = new ProductListInfo();
								$info->ProductID = $model->ProductID;
								$info->StartProduction = $_POST['Product']['StartProduction'];
								$info->EndProduction = $_POST['Product']['EndProduction'];
								$info->DeadlineOrder = $_POST['Product']['DeadlineProduction'];
								$info->save();
							}

								$d = new ProductPriceHistory();
								$d->ProductID = $model->ProductID;
								$d->Date = date('Y-m-d H:i:s');
								$d->BeginPrice = $_POST['Product']['Price'];
								$d->ENDPrice = $model->Price;
								$d->save();

								if(isset($_POST['Product']['ImageProduct'])){
									$i=0;
									foreach($_POST['Product']['ImageProduct'] as $image){
										$rnd = rand(0,9999);
										$uploadedFile=CUploadedFile::getInstance($model,"ImageProduct[$i]");
										$fileName = "{$rnd}-{$uploadedFile}";

										$imageProduct = new ProductImage();
										$imageProduct->ProductImageName = $fileName;
										$imageProduct->ImagePath = Product::getFilePath().$fileName;
										$imageProduct->Index = $i+1;
										$imageProduct->ProductID = $model->ProductID;
										$imageProduct->save();

										if($uploadedFile !== null){
												$uploadedFile->saveAs(Product::getFilePath().$fileName);
										}

										$i++;
									}
								}

                foreach($model->atributes as $d){
                    $d->ProductID = $model->ProductID;
                    if(!$d->save()){
                        $message = '';
                        $errors = $d->getErrors();
                        foreach ($errors as $error){
                            foreach ($error as $e){
                                $message.= $e.'<br />';
                            }
                        }
                        throw new Exception('Error when saving atribute product '.$message);
                    }
                }

				$this->redirect(array('view','id'=>$model->ProductID));
            }
		}

		$this->render('update',array(
			'model'=>$model,
			'listUOM'=>$listUOM,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
	    $model = $this->loadModel($id);
        $model->Status = EnumStatus::NON_ACTIVE;
        if($model->save()){
            Yii::app()->user->setFlash('info','Success delete product. Status change Not Active');
            $this->redirect(array('view','id'=>$model->ProductID));
		}
		else{
            Yii::app()->user->setFlash('danger','Failed delete product');
            $this->redirect(array('view','id'=>$model->ProductID));
        }
	}

    public function actionDeleteAtribute($id)
    {
        $model = ProductAtribute::model()->findByPk($id);
        if($model != null){
            if($model->delete()){
                $this->redirect(array('update','id'=>$model->ProductID));
            }
            else{
                $this->redirect(array('update','id'=>$model->ProductID));
            }
        }
        else{
            $this->redirect(array('update','id'=>$model->ProductID));
        }
    }

	/**
	 * Deletes a list of model.
	 */
	public function actionDeleteSelected()
	{
		if(Yii::app()->request->isPostRequest && isset($_POST['ids']))
		{
			// delete
			foreach ($_POST['ids'] as $id) {
				$this->loadModel($id)->delete();
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Product('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['showImage'])){
			$show_image = $_GET['showImage'];
			// echo'<pre>';print_r($_GET);echo'</pre>';die;
			if(isset($_GET['Product'])){
				$model->attributes=$_GET['Product'];
			}
		}else{
			$show_image = false;
		}




		$this->render('index',array(
			'model'=>$model,
			'showImage' => $show_image
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Product::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionUpload()
    {
        $allowedExtensions = Yii::app()->params['imageExtension'];
        $sizeLimit = 10 * 1024 * 1024; // 10mb

        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $replaceOldFile = FALSE;
        $result = $uploader->handleUpload(Product::getFilePath(), $replaceOldFile, '');

        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
    }

		public function actionUploadBarcode()
    {
        $allowedExtensions = Yii::app()->params['imageExtension'];
        $sizeLimit = 10 * 1024 * 1024; // 10mb

        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $replaceOldFile = FALSE;
        $result = $uploader->handleUpload(Product::getFilePath(), $replaceOldFile, '');

        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
    }

    public function actionDeleteFile() {
        if (isset($_POST['filename'])) {
            self::deleteFile($_POST['filename']);
        }
    }

    public static function deleteFile($file) {
        @unlink(Product::getFilePath() . $file);
    }

    public static function deleteUnlinkedFile()
    {

        $images = array();
        $models = Product::model()->findAll();
        foreach ($models as $model) {
            array_push($images, $model['ThubnailsImage']);
        }

        RFile::deleteUnexcludedFile(Product::getFilePath(), $images);

    }

    public function actionAddAtribute(){
        if(Yii::app()->request->isAjaxRequest){
            $json = array(
                'success'=>false,
                'content'=>'',
                'message'=>'',
            );

            if (isset($_POST['index'])) {
                $atribute = new ProductAtribute();

                $json['content'] = $this->renderPartial('_formAtribute', array(
                    'atribute'=>$atribute,
                    'index'=>$_POST['index'],
                ), true);
                $json['success'] = true;
            }
            else{
                $json['message'] = 'The requested page does not exist.';
            }

            echo CJSON::encode($json);
            Yii::app()->end();
        }
    }

		public function actionAddImages(){
				if(Yii::app()->request->isAjaxRequest){
						$json = array(
								'success'=>false,
								'content'=>'',
								'message'=>'',
						);

						if (isset($_POST['index'])) {
								$model = new Product();

								$json['content'] = $this->renderPartial('_formImages', array(
										'model'=>$model,
										'index'=>$_POST['index'],
								), true);
								$json['success'] = true;
						}
						else{
								$json['message'] = 'The requested page does not exist.';
						}

						echo CJSON::encode($json);
						Yii::app()->end();
				}
		}
}
