<?php

class SellerBillPaymentController extends AdminController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd($id)
	{
        $order = $this->loadModelOrder($id);
		$model=new SellerBillPayment;
        $model->OrderID = $order->OrderCode;
				$model->PaymentCode = IDGenerator::generate('sellerBillPayment');

				$totalPaid = 0;
				$sellerPaid = SellerBillPayment::model()->findAllByAttributes(array('OrderID'=>$order->OrderCode));
				foreach($sellerPaid as $paid){
						$totalPaid+=$paid->Ammount;
				}

				$diskon = ($order->OrderTotal * $order->SupplierFee)/100;
				$total = $order->OrderTotal - $diskon;
				$remain = $total - $totalPaid;

				$model->Ammount = $remain;
				
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SellerBillPayment']))
		{
			$model->attributes=$_POST['SellerBillPayment'];
            $model->Status = EnumStatus::ACTIVE;
			if($model->save()){
                $wiz = Wiz::model()->findByAttributes(array('OrderID'=>$model->OrderID));
                if($wiz == null){
                    $w = new Wiz();
                    $w->OrderID = $model->ID;
                    $w->WizStep = 6;
                    $w->save();
                }
                else{
                    if($wiz->WizStep < 6){
                        $wiz->WizStep = 6;
                        $wiz->save();
                    }
                }

                Yii::app()->user->setFlash('success','Success add seller bill payment');
                $this->redirect(array('order/view/id/'.$model->OrderID));
            }
		}

		$this->render('add',array(
			'model'=>$model,
			'order'=>$order,
			'totalPaid'=>$totalPaid,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$order=$this->loadModelOrder($model->OrderID);

		$totalPaid = 0;
		$sellerPaid = SellerBillPayment::model()->findAllByAttributes(array('OrderID'=>$order->OrderCode));
		foreach($sellerPaid as $paid){
				$totalPaid+=$paid->Ammount;
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SellerBillPayment']))
		{
			$model->attributes=$_POST['SellerBillPayment'];

			if($model->save()){
                $wiz = Wiz::model()->findByAttributes(array('OrderID'=>$model->OrderID));
                if($wiz == null){
                    $w = new Wiz();
                    $w->OrderID = $model->ID;
                    $w->WizStep = 6;
                    $w->save();
                }
                else{
                    if($wiz->WizStep < 6){
                        $wiz->WizStep = 6;
                        $wiz->save();
                    }
                }

                Yii::app()->user->setFlash('success','Success update seller bill payment');
                $this->redirect(array('order/view/id/'.$model->OrderID));
            }
		}

		$this->render('update',array(
			'model'=>$model,
			'order'=>$order,
			'totalPaid'=>$totalPaid,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $model = $this->loadModel($id);
        if($model->delete()){
            Yii::app()->user->setFlash('success','Success delete seller bill payment');
            $this->redirect(array('order/view/id/'.$model->OrderID));
        }
		else{
            Yii::app()->user->setFlash('danger','Failed delete. Please do not repeat this request again.');
            $this->redirect(array('order/view/id/'.$model->OrderID));
        }
	}

	/**
	 * Deletes a list of model.
	 */
	public function actionDeleteSelected()
	{
		if(Yii::app()->request->isPostRequest && isset($_POST['ids']))
		{
			// delete
			foreach ($_POST['ids'] as $id) {
				$this->loadModel($id)->delete();
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new SellerBillPayment('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SellerBillPayment']))
			$model->attributes=$_GET['SellerBillPayment'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SellerBillPayment::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function loadModelOrder($id)
    {
        $model=OrderHeader::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='seller-bill-payment-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
