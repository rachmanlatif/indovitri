<?php

class CustomerController extends AdminController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model = $this->loadModel($id);

        $customerPayment=new CustomerPayment('search');
        $customerPayment->CustomerCode = $model->CustomerCode;

        $detailCommision = new CustomerCommisionSchema();
        $detailCommision->CustomerCode =$model->CustomerCode;

				$detailPerson=DetailPerson::model()->findAllByAttributes(array('EntityCode'=>$model->CustomerCode, 'Status'=>EnumEntity::CUSTOMER));

				$totalPayment = 0;
				$payment = CustomerPayment::model()->findAllByAttributes(array('CustomerCode'=>$model->CustomerCode));
				foreach($payment as $p){
						$totalPayment+=$p->Ammount;
				}

				$totalOrder = 0;
				$orders = OrderHeader::model()->findAllByAttributes(array('CustomerCode'=>$model->CustomerCode, 'OrderStatusCode'=>EnumOrder::ORDER));
				foreach($orders as $o){
						$totalCharge = 0;
						$charge = OrderChargeDetail::model()->findAllByAttributes(array('OrderID'=>$o->OrderCode));
						foreach($charge as $c){
								$totalCharge+=$c->Value;
						}

						$total = ($o->OrderTotal - $o->BuyerFeeAmount) + $o->FreightFee + $totalCharge;
						$totalOrder+=$total;
				}

				$order = new OrderHeader('search');
				$order->CustomerCode = $model->CustomerCode;

        $this->render('view',array(
			'model'=>$model,
			'customerPayment'=>$customerPayment,
			'detailCommision'=>$detailCommision,
			'detailPerson'=>$detailPerson,
			'order'=>$order,
			'totalPayment'=>$totalPayment,
			'totalOrder'=>$totalOrder,
		));
	}

	public function actionViewOrder($id)
	{
        $model = OrderHeader::model()->findByAttributes(array('OrderCode'=>$id));
				if($model===null){
						throw new CHttpException(404,'The requested page does not exist.');
				}

        $sellerPayment=new SellerBillPayment('search');
        $sellerPayment->OrderID = $model->OrderCode;

        $sellerPaid = SellerBillPayment::model()->findByAttributes(array('OrderID'=>$model->OrderCode));

        $orderBl=new OrderBL('search');
        $orderBl->OrderID = $model->OrderCode;

        $customerPayment=new CustomerPayment('search');
        $customerPayment->CustomerCode = $model->CustomerCode;

        $this->render('viewOrder',array(
			'model'=>$model,
			'sellerPayment'=>$sellerPayment,
			'orderBl'=>$orderBl,
			'customerPayment'=>$customerPayment,
			'sellerPaid'=>$sellerPaid,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd()
	{
		$model=new Customer;
		$detailPerson=array();

		$listCommision = array();
		$commision = CommisionSchema::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE));
		foreach ($commision as $key => $value) {
				$listCommision[$value->ComisionCodeSkema] = $value->ComisionNameSkema.' ('.$value->Percentage.'%)';
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Customer']))
		{
			//echo'<pre>';print_r($_POST);die;
						$detailPerson = array();
						if (isset($_POST['DetailPerson'])) {
								foreach ($_POST['DetailPerson'] as $d) {
										$detail = new DetailPerson();
										$detail->attributes = $d;

										array_push($detailPerson, $detail);
								}
						}

						$detailCommision = array();
						if (isset($_POST['CustomerCommisionSchema'])) {
								foreach ($_POST['CustomerCommisionSchema'] as $d) {
										$detail = new CustomerCommisionSchema();
										$detail->attributes = $d;

										array_push($detailCommision, $detail);
								}
						}

						$model->commisionDetails = $detailCommision;

						$model->attributes=$_POST['Customer'];
            $model->CustomerCode = IDGenerator::generate('customer');

			if($model->save()){
							foreach($detailPerson as $d){
									$d->Status = EnumEntity::CUSTOMER;
									$d->EntityCode = $model->CustomerCode;
									if(!$d->save()){
											$message = '';
											$errors = $d->getErrors();
											foreach ($errors as $error){
													foreach ($error as $e){
															$message.= $e.'<br />';
													}
											}
											throw new Exception('Error when saving detail person '.$message);
									}
							}

							foreach($model->commisionDetails as $d){
									$d->CustomerCode = $model->CustomerCode;
									if(!$d->save()){
											$message = '';
											$errors = $d->getErrors();
											foreach ($errors as $error){
													foreach ($error as $e){
															$message.= $e.'<br />';
													}
											}
											throw new Exception('Error when saving detail commision '.$message);
									}
							}

							$this->redirect(array('view','id'=>$model->CustomerCode));
            }
		}

		$get_country = Yii::app()->db->createCommand('SELECT * FROM Country')->queryAll();
		// echo'<pre>';print_r($get_country);echo'</pre>';die;

		$this->render('add',array(
			'model'=>$model,
			'get_country' => $get_country,
			'listCommision'=>$listCommision,
			'detailPerson'=>$detailPerson,
		));
	}

    public function actionAddSkema()
    {
        $model = new CustomerCommisionSchema();

        if(isset($_POST['CustomerCommisionSchema']))
        {
            $model->attributes=$_POST['CustomerCommisionSchema'];
            if($model->save()){
                Yii::app()->user->setFlash('danger','Failed delete detail');
                $this->redirect(array('view','id'=>$model->CustomerCode));
            }
        }
        else{
            Yii::app()->user->setFlash('info','Invalid request data');
            $this->redirect(array('index'));
        }
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$detailPerson=DetailPerson::model()->findAllByAttributes(array('EntityCode'=>$model->CustomerCode, 'Status'=>EnumEntity::CUSTOMER));

		$listCommision = array();
		$commision = CommisionSchema::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE));
		foreach ($commision as $key => $value) {
				$listCommision[$value->ComisionCodeSkema] = $value->ComisionNameSkema.' ('.$value->Percentage.'%)';
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Customer']))
		{
			$detailPerson = array();
			if (isset($_POST['DetailPerson'])) {
					foreach ($_POST['DetailPerson'] as $d) {
							$id = $d['IDDetailPerson'];
							if($id != null){
									$person = DetailPerson::model()->findByPk($id);
									if($person != null){
											$person->PositionCode = $d['PositionCode'];
											$person->Name = $d['Name'];
											$person->Address = $d['Address'];
											$person->Email = $d['Email'];
											$person->Note = $d['Note'];
											$person->save();
									}
							}
							else{
								$detail = new DetailPerson();
								$detail->attributes = $d;

								array_push($detailPerson, $detail);
							}
					}
			}

			$detailCommision = array();
			if (isset($_POST['CustomerCommisionSchema'])) {
					foreach ($_POST['CustomerCommisionSchema'] as $d) {
							$id = $d['IDCustomerCommisionSkema'];
							if($id != null){
									$scheme = CustomerCommisionSchema::model()->findByPk($id);
									if($scheme != null){
											$scheme->ComisionCodeSkema = $d['ComisionCodeSkema'];
											$scheme->save();
									}
							}
							else{
								$detail = new CustomerCommisionSchema();
								$detail->attributes = $d;

								array_push($detailCommision, $detail);
							}
					}
			}
			$model->commisionDetails = $detailCommision;

			$model->attributes=$_POST['Customer'];

			if($model->save()){
							foreach($detailPerson as $d){
									$d->Status = EnumEntity::CUSTOMER;
									$d->EntityCode = $model->CustomerCode;
									if(!$d->save()){
											$message = '';
											$errors = $d->getErrors();
											foreach ($errors as $error){
													foreach ($error as $e){
															$message.= $e.'<br />';
													}
											}
											throw new Exception('Error when saving detail person '.$message);
									}
							}

							foreach($model->commisionDetails as $d){
									$d->CustomerCode = $model->CustomerCode;
									if(!$d->save()){
											$message = '';
											$errors = $d->getErrors();
											foreach ($errors as $error){
													foreach ($error as $e){
															$message.= $e.'<br />';
													}
											}
											throw new Exception('Error when saving detail commision '.$message);
									}
							}

							$this->redirect(array('view','id'=>$model->CustomerCode));
            }
		}

		$get_country = Yii::app()->db->createCommand('SELECT * FROM Country')->queryAll();

		$this->render('update',array(
			'model'=>$model,
			'get_country' => $get_country,
			'detailPerson'=>$detailPerson,
			'listCommision'=>$listCommision,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(isset($_GET['id']))
		{
				$model = $this->loadModel($id);
				$model->Status = EnumStatus::NON_ACTIVE;
				if($model->save()){
					$this->redirect(array('index'));
				}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Deletes a list of model.
	 */
	public function actionDeleteSelected()
	{
		if(Yii::app()->request->isPostRequest && isset($_POST['ids']))
		{
			// delete
			foreach ($_POST['ids'] as $id) {
				$this->loadModel($id)->delete();
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionDeletePerson($id)
	{
			$detail = DetailPerson::model()->findByPk($id);
			if($detail != null){
					if($detail->delete()){
							$this->redirect(array('update','id'=>$detail->EntityCode));
					}
					else{
							Yii::app()->user->setFlash('danger','Failed delete detail');
							$this->redirect(array('view','id'=>$detail->EntityCode));
					}
			}
			else{
					Yii::app()->user->setFlash('danger','Failed delete detail');
					$this->redirect(array('view','id'=>$detail->EntityCode));
			}
	}

	public function actionDeleteScheme($id)
	{
			$detail = CustomerCommisionSchema::model()->findByPk($id);
			if($detail != null){
					if($detail->delete()){
							$this->redirect(array('update','id'=>$detail->CustomerCode));
					}
					else{
							Yii::app()->user->setFlash('danger','Failed delete commision');
							$this->redirect(array('view','id'=>$detail->CustomerCode));
					}
			}
			else{
					Yii::app()->user->setFlash('danger','Failed delete commision');
					$this->redirect(array('view','id'=>$detail->CustomerCode));
			}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Customer('search');
		$model->unsetAttributes();  // clear any default values
		$model->Status = EnumStatus::ACTIVE;
		
		if(isset($_GET['Customer']))
			$model->attributes=$_GET['Customer'];

		$get_country = Yii::app()->db->createCommand('SELECT * FROM Country')->queryAll();
		$this->render('index',array(
			'model'=>$model,
			'get_country' => $get_country,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Customer::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='customer-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionAddPerson(){
			if(Yii::app()->request->isAjaxRequest){
					$json = array(
							'success'=>false,
							'content'=>'',
							'message'=>'',
					);

					if (isset($_POST['index'])) {
							$detail = new DetailPerson();

							$json['content'] = $this->renderPartial('_formPerson', array(
									'detail'=>$detail,
									'index'=>$_POST['index'],
							), true);
							$json['success'] = true;
					}
					else{
							$json['message'] = 'The requested page does not exist.';
					}

					echo CJSON::encode($json);
					Yii::app()->end();
			}
	}

	public function actionAddScheme(){
			if(Yii::app()->request->isAjaxRequest){
					$json = array(
							'success'=>false,
							'content'=>'',
							'message'=>'',
					);

					if (isset($_POST['index'])) {
							$detail = new CustomerCommisionSchema();

							$listCommision = array();
							$commision = CommisionSchema::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE));
							foreach ($commision as $key => $value) {
									$listCommision[$value->ComisionCodeSkema] = $value->ComisionNameSkema.' ('.$value->Percentage.'%)';
							}

							$json['content'] = $this->renderPartial('_formScheme', array(
									'detail'=>$detail,
									'listCommision'=>$listCommision,
									'index'=>$_POST['index'],
							), true);
							$json['success'] = true;
					}
					else{
							$json['message'] = 'The requested page does not exist.';
					}

					echo CJSON::encode($json);
					Yii::app()->end();
			}
	}

	public function actionAddState(){
		// print_r($_POST);die;
		$state = Yii::app()->db->createCommand("SELECT * FROM State WHERE CountryCode = '".$_POST['countryID']."'")->queryAll();
		echo '<br><select class="form-control" style="width : 100%" tabindex="-1" id="stateID">';
      	foreach ($state as $key => $value) {
        	echo '<option value="'.$value['StateCode'].'">'.$value['StateName'].'</option>';
      }
  	echo '</select>';
	}

	public function actionAddCity(){
		if(isset($_POST['stateID'])){
			$city = Yii::app()->db->createCommand("SELECT * FROM City WHERE StateCode = '".$_POST['stateID']."' AND Status = '1'")->queryAll();
			echo '<br><select class="form-control" style="width : 100%" tabindex="-1" id="cityID" name="Customer[CityCode]">';
	      	foreach ($city as $key => $value) {
	        	echo '<option value="'.$value['CityCode'].'">'.$value['CityName'].'</option>';
	      }
	  	echo '</select><br>';
		}else{
			echo '<br><select class="form-control" style="width : 100%" tabindex="-1" id="cityID" name="Customer[CityCode]">';

	        	echo '<option value="">-</option>';
	  	echo '</select><br>';
		}

	}
}
