<?php

class DashboardController extends AdminController {

    public function actionIndex(){
        //cari summary order
        $sql = "select count(*) as JumlahOrder, sum(OrderTotal) as TotalOrder, OrderStatusCode as Status from dbo.OrderHeader group by OrderStatusCode";
        $orders =  Yii::app()->db->createCommand($sql)->queryAll();

        //latest order
        $latest = OrderHeader::model()->findAllByAttributes(array(), array('limit'=>5, 'order'=>'OrderDate desc'));

        //top six product best Seller
        // $topProduct = Product::model()->findAllByAttributes(array(), array('limit'=>6));
        $topProduct = Yii::app()->db->createCommand("
          select count(dbo.Product.ProductID) as Total, dbo.Product.ProductID from dbo.OrderDetail
          inner join dbo.Product on dbo.Product.ProductID = dbo.OrderDetail.ProductID
          group by dbo.Product.ProductID
        ")->queryAll();
        // echo'<pre>';print_r($topProduct);die;

        $modelOrders = new OrderHeader('search');

        $this->render('index', array(
            'orders'=>$orders,
            'latest'=>$latest,
            'modelOrders'=>$modelOrders,
            'topProduct' => $topProduct,
        ));
    }
}
