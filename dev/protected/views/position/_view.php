<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('PositionCode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->PositionCode), array('view', 'id'=>$data->PositionCode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PositionName')); ?>:</b>
	<?php echo CHtml::encode($data->PositionName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Note')); ?>:</b>
	<?php echo CHtml::encode($data->Note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />


</div>