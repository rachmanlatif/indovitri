<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">View Incoterm</span>
            </div>
            <div class="widget-body">
                <div class="table-scrollable">
									<a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
                      <i class="btn-label glyphicon glyphicon-th-list"></i>List Incoterm
                  </a>
									<a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-success">
                      <i class="btn-label glyphicon glyphicon-plus"></i>Add Incoterm
                  </a>
									<a href="<?php echo $this->createUrl('update', array('id'=>$model->IncotermID)); ?>" class="btn btn-labeled btn-warning">
                      <i class="btn-label glyphicon glyphicon-edit"></i>Edit Incoterm
                  </a>
                  <a href="<?php echo $this->createUrl('delete', array('id'=>$model->IncotermID)); ?>" class="btn btn-labeled btn-delete btn-danger">
                      <i class="btn-label glyphicon glyphicon-trash"></i>Delete
                  </a>
									<hr>
									<?php $this->widget('zii.widgets.CDetailView', array(
									    'htmlOptions'=>array(
									        'class'=>'detail-view table table-striped table-bordered table-hover'
									    ),
									    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
										'data'=>$model,
										'attributes'=>array(
											'IncotermID',
											'IncotermName',
											'Note',
									        array(
									            'name'=>'Status',
									            'value'=>EnumStatus::getLabel($model->Status),
									        ),
										),
									)); ?>
									<hr>
									<div class="tabbable">
									    <ul class="nav nav-tabs">
									        <li class="active">
									            <a data-toggle="tab" href="#tab1">
									                Order History
									            </a>
									        </li>
									    </ul>

									    <div class="tab-content radius-bordered">
									        <div id="tab1" class="tab-pane in active">
															<div class="table-scrollable">
																	<?php $this->widget('zii.widgets.grid.CGridView', array(
																		'id'=>'order-header-grid',
																	    'itemsCssClass'=>'table table-striped table-bordered table-hover',
																			'pager' => array(
																		    'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
																		    // 'maxButtonCount'=>4,
																		    'header' => '',
																		    'prevPageLabel' => 'Previous',
																		    'nextPageLabel' => 'Next',
																		    'firstPageLabel'=>'First',
																		    'lastPageLabel'=>'Last',
																		    'htmlOptions'=>array('style' => 'float : left'),
																		  ),
																		'dataProvider'=>$order->search(),
																		//'filter'=>$model,
																		'selectableRows'=>2,
																		'columns'=>array(
																					array(
																							'class'=>'CButtonColumn',
																							'template'=>'{view}',
																							'header'=>'View',
																							'htmlOptions' => array('class' => 'col-xs-2 text-center'),
																							'buttons'=>array(
																									'view'=>array(
																											'imageUrl'=>false,
																											'label'=>'<i class="fa fa-search fa-fw"></i>',
																											'options'=>array(
																													'class'=>'btn btn-info btn-xs',
																													'title'=>'View Detail Order',
																											),
																											'url'=>'Yii::app()->createUrl("order/viewOrder", array("id"=>$data->OrderCode))',
																									),
																							),
																					),
																					array(
																							'name'=>'OrderStatusCode',
																							'value'=>'EnumOrder::getLabel($data->OrderStatusCode)',
																					),
																					array(
																							'name'=>'OrderDate',
																							'value'=>'date("Y-m-d", strtotime($data->OrderDate))',
																					),
																					array(
																							'name'=>'CustomerCode',
																							'value'=>'($data->customer != null ? $data->customer->Name : "")',
																					),
																					array(
																							'name'=>'SellerCode',
																							'value'=>'($data->seller != null ? $data->seller->Name : "")',
																					),
																					array(
																							'name'=>'PolCode',
																							'value'=>'($data->pol != null ? $data->pol->CityName : "")',
																					),
																					array(
																							'name'=>'PodCode',
																							'value'=>'($data->pod != null ? $data->pod->CityName : "")',
																					),
																					array(
																							'name'=>'ConsigneeCode',
																							'value'=>'($data->consignee != null ? $data->consignee->Name : "")',
																					),
																					'PORefCode',
																					array(
																							'name'=>'PODate',
																							'value'=>'($data->PODate > "1980-01-01" ? date("Y-m-d", strtotime($data->PODate)) : "")',
																					),
																					'BillCodeSupplier',
																					array(
																							'name'=>'BillSupplierDate',
																							'value'=>'($data->PODate > "1980-01-01" ? date("Y-m-d", strtotime($data->BillSupplierDate)) : "")',
																					),
																					array(
																							'name'=>'OrderTotal',
																							'value'=>'number_format($data->OrderTotal, 2)',
																					),
																					array(
																							'name'=>'BuyerFee',
																							'value'=>'number_format($data->BuyerFee, 2)',
																					),
																					array(
																							'name'=>'BuyerFeeAmount',
																							'value'=>'number_format($data->BuyerFeeAmount, 2)',
																					),
																					array(
																							'name'=>'SupplierFee',
																							'value'=>'number_format($data->SupplierFee, 2)',
																					),
																					array(
																							'name'=>'AgentCodeOrigin',
																							'value'=>'($data->agentOrigin != null ? $data->agentOrigin->Name : "")',
																					),
																					array(
																							'name'=>'IncotermID',
																							'value'=>'($data->incoterm != null ? $data->incoterm->IncotermName : "")',
																					),
																					array(
																							'name'=>'NotifyPartyCode',
																							'value'=>'($data->notifyParty != null ? $data->notifyParty->Name : "")',
																					),
																	    ),
																	)); ?>
															</div>
													</div>
											</div>
									</div>
								</div>
						</div>
				</div>
		</div>
</div>
