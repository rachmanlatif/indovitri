<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('IncotermID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->IncotermID), array('view', 'id'=>$data->IncotermID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IncotermName')); ?>:</b>
	<?php echo CHtml::encode($data->IncotermName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Note')); ?>:</b>
	<?php echo CHtml::encode($data->Note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isFOB')); ?>:</b>
	<?php echo CHtml::encode($data->isFOB); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />


</div>