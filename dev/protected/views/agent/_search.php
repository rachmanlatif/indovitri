<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="widget">
			<div class="form-horizontal">
				<h5 class="row-title before-magenta"><i class="fa fa-search magenta"></i>Form Search</h5>
				<?php $form=$this->beginWidget('CActiveForm', array(
					'action'=>Yii::app()->createUrl($this->route),
					'method'=>'get',
				)); ?>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'AgentCode'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'AgentCode',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'Name'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'Name',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'ShortName'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'ShortName',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'Phone'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'Phone',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'email'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'Address'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'Address',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'PostCode'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'PostCode',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right">Country</label>
							<div class="col-sm-10">
									<select class="select2" style="width : 100%" id="country">
										<?php if($get_country){
					                  foreach ($get_country as $rows) {
					                    echo '<option value="'.$rows['CountryCode'].'">'.$rows['CountryName'].'</option>';
					                  }
					                }
										?>
									</select>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right">State</label>
							<div class="col-sm-10" id="state">

							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right">City</label>
							<div class="col-sm-10" id="city">

							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'Status'); ?></label>
							<div class="col-sm-10">
								<?php echo $form->dropDownList($model,'Status',
										EnumStatus::getList()); ?>
							</div>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Search</button>
					</div>
				</div>
				<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
country();
function country(){
	var countryID = $("#country option:selected").attr('value');
    $.post("<?php echo $this->createUrl('addState'); ?>", {countryID:countryID}, function(data){
			// console.log(data)
        $("#state").show();
        $("#state").html(data);
  			citys();
  			$("#state").change(citys);
    });
}

$('#country').on('change', function() {
  $.post("<?php echo $this->createUrl('addState'); ?>", {countryID:this.value}, function(data){
		$("#state").show();
		$("#state").html(data);
		citys();
		// $("#state").change(citys);
	})
});

function citys(){
	var stateID = $("#stateID option:selected").attr('value');
	var countryID = $("#country").val();
	$.post("<?php echo $this->createUrl('addCity'); ?>", {stateID:stateID, countryID:countryID}, function(data){
		// console.log(data)
			$("#city").show();
			$("#city").html(data);
	});
}
</script>
