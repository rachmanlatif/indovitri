<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Create Profile</span>
            </div>
            <div class="widget-body">
							<a href="<?php echo Yii::app()->baseUrl.'/dashboard'; ?>" class="btn btn-labeled btn-blue">
									<i class="btn-label glyphicon glyphicon-th-list"></i>Dashboard
							</a>
							<hr>
							<div class="row">
							    <div class="col-sm-12 col-xs-12">
							        <div id="status-message">
							            <?php if(Yii::app()->user->hasFlash('success')): ?>
							                <div class="alert alert-success">
							                    <?php echo Yii::app()->user->getFlash('success') ?>
							                </div>
							            <?php endif ?>

							            <?php if(Yii::app()->user->hasFlash('info')): ?>
							                <div class="alert alert-info">
							                    <?php echo Yii::app()->user->getFlash('info') ?>
							                </div>
							            <?php endif ?>

							            <?php if(Yii::app()->user->hasFlash('danger')): ?>
							                <div class="alert alert-danger">
							                    <?php echo Yii::app()->user->getFlash('danger') ?>
							                </div>
							            <?php endif ?>
							        </div>
							    </div>
							</div>

							<div class="form">

							<?php $form=$this->beginWidget('CActiveForm', array(
								'id'=>'profile-form',
								'enableAjaxValidation'=>false,
							)); ?>

								<?php echo $form->errorSummary($model); ?>

								<div class="row form-row form-group">
							        <div class="col-xs-2">
							            <?php echo $form->labelEx($model,'Username'); ?>
							        </div>
									<div class="col-xs-10">
							            <div class="row">
							                <div class="col-xs-6">
							                    <?php echo $form->textField($model,'Username',array('size'=>60,'maxlength'=>255, 'readonly'=>true,'class'=>'form-control')); ?>
							                </div>
							                <div class="col-xs-6">
							                    <?php echo $form->error($model,'Username'); ?>
							                </div>
							            </div>

									</div>
								</div>

							  	<div class="row form-row form-group">
							          <div class="col-xs-2">
							              <?php echo $form->labelEx($model,'Name'); ?>
							          </div>
							  		<div class="col-xs-10">
							              <div class="row">
							                  <div class="col-xs-6">
							                      <?php echo $form->textField($model,'Name',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
							                  </div>
							                  <div class="col-xs-6">
							                      <?php echo $form->error($model,'Name'); ?>
							                  </div>
							              </div>

							  		</div>
							  	</div>

							  <div class="row form-row form-group">
							        <div class="col-xs-2">
							            <?php echo $form->labelEx($model,'Phone'); ?>
							        </div>
									<div class="col-xs-10">
							            <div class="row">
							                <div class="col-xs-6">
							                    <?php echo $form->textField($model,'Phone',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
							                </div>
							                <div class="col-xs-6">
							                    <?php echo $form->error($model,'Phone'); ?>
							                </div>
							            </div>

									</div>
								</div>

								<div class="row form-row form-group">
							        <div class="col-xs-2">
							            <?php echo $form->labelEx($model,'email'); ?>
							        </div>
									<div class="col-xs-10">
							            <div class="row">
							                <div class="col-xs-6">
							                    <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
							                </div>
							                <div class="col-xs-6">
							                    <?php echo $form->error($model,'email'); ?>
							                </div>
							            </div>

									</div>
								</div>

								<div class="row form-row form-group">
							        <div class="col-xs-2">
							            <?php echo $form->labelEx($model,'Address'); ?>
							        </div>
									<div class="col-xs-10">
							            <div class="row">
							                <div class="col-xs-6">
							                    <?php echo $form->textField($model,'Address',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
							                </div>
							                <div class="col-xs-6">
							                    <?php echo $form->error($model,'Address'); ?>
							                </div>
							            </div>

									</div>
								</div>

								<div class="row form-row form-group">
										<label for="inputEmail3" class="col-xs-2 control-label no-padding-right">Country</label>
										<div class="col-xs-10">
												<div class="row">
														<div class="col-xs-6">
															<select class="select2" style="width : 100%" id="country">
																<?php if($get_country){
																				foreach ($get_country as $rows) {
																					echo '<option value="'.$rows['CountryCode'].'">'.$rows['CountryName'].'</option>';
																				}
																			}
																?>
															</select>
														</div>
														<div class="col-xs-6">

														</div>
												</div>

										</div>
								</div>

								<div class="row form-row form-group">
										<label for="inputEmail3" class="col-xs-2 control-label no-padding-right">State</label>
										<div class="col-xs-10">
												<div class="row">
														<div class="col-xs-6" id="state">
														</div>
														<div class="col-xs-6">
														</div>
												</div>

										</div>
								</div>

								<div class="row form-row form-group">
										<label for="inputEmail3" class="col-xs-2 control-label no-padding-right">City</label>
										<div class="col-xs-10">
												<div class="row">
														<div class="col-xs-6" id="city">
														</div>
														<div class="col-xs-6">
														</div>
												</div>

										</div>
								</div>

								

							    <div class="row form-row form-group">
							        <div class="col-xs-2">
							            <?php echo $form->labelEx($model,'PostCode'); ?>
							        </div>
									<div class="col-xs-10">
							            <div class="row">
							                <div class="col-xs-6">
							                    <?php echo $form->textField($model,'PostCode',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
							                </div>
							                <div class="col-xs-6">
							                    <?php echo $form->error($model,'PostCode'); ?>
							                </div>
							            </div>

									</div>
								</div>

							    <hr>

								<div class="row buttons">
							        <div class="col-xs-12">
							            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>        </div>
								</div>

							<?php $this->endWidget(); ?>

							</div><!-- form -->
						</div>
				</div>
		</div>
</div>

<script type="text/javascript">
country();
function country(){
	var countryID = $("#country option:selected").attr('value');
    $.post("<?php echo $this->createUrl('addState'); ?>", {countryID:countryID}, function(data){
			// console.log(data)
        $("#state").show();
        $("#state").html(data);
  			citys();
  			$("#state").change(citys);
    });
}

$('#country').on('change', function() {
  $.post("<?php echo $this->createUrl('addState'); ?>", {countryID:this.value}, function(data){
		$("#state").show();
		$("#state").html(data);
		citys();
		// $("#state").change(citys);
	})
});

function citys(){
	var stateID = $("#stateID option:selected").attr('value');
	var countryID = $("#country").val();
	$.post("<?php echo $this->createUrl('addCity'); ?>", {stateID:stateID, countryID:countryID}, function(data){
		// console.log(data)
			$("#city").show();
			$("#city").html(data);
	});
}
</script>
