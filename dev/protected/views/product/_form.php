<?php
$cs = Yii::app()->clientScript;

$cs->registerScript('form-specific', "
	$(document).ready(function(){
		$('#image-container').on('click','.delete-image', function(){
			if (confirm('Are you sure?')) {
				$.ajax({
					type: 'post',
					url: '". Yii::app()->request->baseUrl ."/product/deleteFile',
					data: {filename: $('.Product_delete').val()},
					success: function(response) {
						$('#image-container').html('');
					}
				});
			}
			return false;
		});

	});
");

$cs2 = Yii::app()->clientScript;

$cs2->registerScript('form-specific', "
	$(document).ready(function(){
		$('#image-barcode').on('click','.delete-barcode', function(){
			if (confirm('Are you sure?')) {
				$.ajax({
					type: 'post',
					url: '". Yii::app()->request->baseUrl ."/product/deleteFile',
					data: {filename: $('.Product_deleteBarcode').val()},
					success: function(response) {
						$('#image-barcode').html('');
					}
				});
			}
			return false;
		});

	});
");
?>

<div class="form-horizontal">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'product-form',
		'enableAjaxValidation'=>false,
		'htmlOptions' => array(
				'enctype' => 'multipart/form-data',
		),
	)); ?>

	<?php echo $form->errorSummary($model); ?>
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Barcode'); ?></label>
				<div class="col-sm-10">
					<div id="image-barcode"></div>
					<div class="clear"></div>
					<?php $this->widget('common.extensions.widgets.qqFileUploader.QQFileUploader', array(
							'id'=>'qq-file-uploader2',
							'action'=>Yii::app()->request->baseUrl . '/product/uploadBarcode',
							'options'=>array(
									'allowedExtensions'=>Yii::app()->params['imageExtension'],
							),
							'onComplete'=>"
									var content = '<input type=\"hidden\" class=\"Product_deleteBarcode\" id=\"Product_Barcode\" name=\"Product[Barcode]\" value=\"'+ responseJSON.newFileName +'\" />';
									content += '<img src=\"".Product::getFileUrl()."'+responseJSON.newFileName +' \" width=\"50%\"><br><br>';
									content += '<a href=\"javascript:void(0)\" class=\"btn btn-sm dtn-danger delete-barcode\" id=\"delete-barcode\"><i class=\"fa fa-file-image-o \"/> Delete file</a><br><br>';

									$('#image-barcode').append(content);
							",
							'onSubmit' => "$('.qq-upload-list').html('');",
					));?>
					<b>Choose or drop an image</b>
					<br>
					<?php echo $form->error($model,'Barcode'); ?>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'EANCode'); ?></label>
				<div class="col-sm-10">
						<?php echo $form->textField($model,'EANCode',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
						<br>
						<?php echo $form->error($model,'EANCode'); ?>
				</div>
		</div>

		<div class="form-group">
				<div class="col-sm-4">
						<div class="checkbox">
		            <label>
		                <?php echo $form->checkbox($model,'isStock',array('class'=>'form-control')); ?>
		                <span class="text"><?php echo $form->labelEx($model,'isStock'); ?></span>
		            </label>
		        </div>
						<br>
						<?php echo $form->error($model,'isStock'); ?>
				</div>
				<div class="col-sm-4">
						<div class="checkbox">
								<label>
										<?php echo $form->checkbox($model,'isProduction',array('class'=>'form-control')); ?>
										<span class="text"><?php echo $form->labelEx($model,'isProduction'); ?></span>
								</label>
						</div>
						<br>
						<?php echo $form->error($model,'isProduction'); ?>
				</div>
				<div class="col-sm-4">
						<div class="checkbox">
								<label>
										<?php echo $form->checkbox($model,'isCatalogue',array('class'=>'form-control')); ?>
										<span class="text"><?php echo $form->labelEx($model,'isCatalogue'); ?></span>
								</label>
						</div>
						<br>
						<?php echo $form->error($model,'isCatalogue'); ?>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'SellerCode'); ?></label>
				<div class="col-sm-10">
						<?php echo $form->dropDownList($model,'SellerCode',
							CHtml::listData(Seller::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'SellerCode', 'Name'),
							array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 100%')); ?>
						<br>
						<?php echo $form->error($model,'SellerCode'); ?>
						<a href="<?php echo Yii::app()->baseUrl.'/seller/add'; ?>" class="btn btn-labeled btn-blue" target="_blank">
								<i class="btn-label glyphicon glyphicon-plus"></i>Add Seller
						</a>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'SellerSKU'); ?></label>
				<div class="col-sm-10">
						<?php echo $form->textField($model,'SellerSKU',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
						<br>
						<?php echo $form->error($model,'SellerSKU'); ?>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'CategoryCode'); ?></label>
				<div class="col-sm-10">
						<?php echo $form->dropDownList($model,'CategoryCode',
							CHtml::listData(CategoryProduct::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'CategoryCode', 'CategoryName'),
							array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 100%')); ?>
						<br>
						<?php echo $form->error($model,'CategoryCode'); ?>
						<a href="<?php echo Yii::app()->baseUrl.'/categoryProduct/add'; ?>" class="btn btn-labeled btn-blue" target="_blank">
								<i class="btn-label glyphicon glyphicon-plus"></i>Add Category
						</a>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'ProductName'); ?></label>
				<div class="col-sm-10">
						<?php echo $form->textField($model,'ProductName',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
						<br>
						<?php echo $form->error($model,'ProductName'); ?>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Price'); ?></label>
				<div class="col-sm-10">
						<?php echo $form->textField($model,'Price',array('size'=>19,'maxlength'=>19, 'class'=>'form-control')); ?>
						<br>
						<?php echo $form->error($model,'Price'); ?>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'StokQTY'); ?></label>
				<div class="col-sm-4">
						<?php echo $form->textField($model,'StokQTY',array('size'=>19,'maxlength'=>19, 'class'=>'form-control')); ?>
						<br>
						<?php echo $form->error($model,'StokQTY'); ?>
				</div>
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'MinQTY'); ?></label>
				<div class="col-sm-4">
						<?php echo $form->textField($model,'MinQTY',array('size'=>19,'maxlength'=>19, 'class'=>'form-control')); ?>
						<br>
						<?php echo $form->error($model,'MinQTY'); ?>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'UOMCode'); ?></label>
				<div class="col-sm-10">
						<?php echo $form->dropDownList($model,'UOMCode',
								$listUOM,
								array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 100%')); ?>
						<br>
						<?php echo $form->error($model,'UOMCode'); ?>
						<a href="<?php echo Yii::app()->baseUrl.'/uom/add'; ?>" class="btn btn-labeled btn-blue" target="_blank">
								<i class="btn-label glyphicon glyphicon-plus"></i>Add UOM
						</a>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'InnerCarton'); ?></label>
				<div class="col-sm-10">
						<?php echo $form->textField($model,'InnerCarton',array('size'=>19,'maxlength'=>19, 'class'=>'form-control')); ?>
						<br>
						<?php echo $form->error($model,'InnerCarton'); ?>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-4 "><?php echo $form->labelEx($model,'NettWeight'); ?></label>
				<label for="inputEmail3" class="col-sm-4 "><?php echo $form->labelEx($model,'GrossWeight'); ?></label>
				<label for="inputEmail3" class="col-sm-4 "><?php echo $form->labelEx($model,'VolumeGross'); ?></label>
				<div class="col-sm-4">
						<?php echo $form->textField($model,'NettWeight',array('size'=>19,'maxlength'=>19, 'class'=>'form-control')); ?>
						<?php echo $form->error($model,'NettWeight'); ?>
				</div>
				<div class="col-sm-4">
						<?php echo $form->textField($model,'GrossWeight',array('size'=>20,'maxlength'=>20, 'class'=>'form-control')); ?>
						<?php echo $form->error($model,'GrossWeight'); ?>
				</div>
				<div class="col-sm-4">
						<?php echo $form->textField($model,'VolumeGross',array('size'=>20,'maxlength'=>20, 'class'=>'form-control')); ?>
						<?php echo $form->error($model,'VolumeGross'); ?>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-4 "><?php echo $form->labelEx($model,'CartonLenght'); ?></label>
				<label for="inputEmail3" class="col-sm-4 "><?php echo $form->labelEx($model,'CartonHeight'); ?></label>
				<label for="inputEmail3" class="col-sm-4 "><?php echo $form->labelEx($model,'CartonDepth'); ?></label>
				<div class="col-sm-4">
						<?php echo $form->textField($model,'CartonLenght',array('size'=>19,'maxlength'=>19, 'class'=>'form-control')); ?>
						<?php echo $form->error($model,'CartonLenght'); ?>
				</div>
				<div class="col-sm-4">
						<?php echo $form->textField($model,'CartonHeight',array('size'=>20,'maxlength'=>20, 'class'=>'form-control')); ?>
						<?php echo $form->error($model,'CartonHeight'); ?>
				</div>
				<div class="col-sm-4">
						<?php echo $form->textField($model,'CartonDepth',array('size'=>20,'maxlength'=>20, 'class'=>'form-control')); ?>
						<?php echo $form->error($model,'CartonDepth'); ?>
				</div>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'ThubnailsImage'); ?></label>
				<div class="col-sm-10">
					<div id="image-container"></div>
					<div class="clear"></div>
					<?php $this->widget('common.extensions.widgets.qqFileUploader.QQFileUploader', array(
							'id'=>'qq-file-uploader',
							'action'=>Yii::app()->request->baseUrl . '/product/upload',
							'options'=>array(
									'allowedExtensions'=>Yii::app()->params['imageExtension'],
							),
							'onComplete'=>"
									var content = '<input type=\"hidden\" class=\"Product_delete\" id=\"Product_ThubnailsImage\" name=\"Product[ThubnailsImage]\" value=\"'+ responseJSON.newFileName +'\" />';
									content += '<img src=\"".Product::getFileUrl()."'+responseJSON.newFileName +' \" width=\"50%\" ><br><br>';
									content += '<a href=\"javascript:void(0)\" class=\"btn btn-sm btn-danger delete-image\" id=\"delete-image\"><i class=\"fa fa-file-image-o \"/> Delete file</a><br><br>';

									$('#image-container').append(content);
							",
							'onSubmit' => "$('.qq-upload-list').html('');",
					));?>
					<b>Choose or drop an image</b>
					<br>
					<?php echo $form->error($model,'ThubnailsImage'); ?>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'ImagesProduct'); ?></label>
				<div class="col-sm-10">
					<table class="table">
						<thead>
								<tr>
										<td>
												<button type="button" id="addImageDetail" class="btn btn-primary">Add</button>
										</td>
										<td></td>
								</tr>
						</thead>
						<tbody id="detailImages">

						</tbody>
					</table>
					<br>
					<?php echo $form->error($model,'ImagesProduct'); ?>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'StartProduction'); ?></label>
				<div class="col-sm-4">
						<?php echo $form->textField($model,'StartProduction',array('class'=>'date-picker form-control')); ?>
						<br>
						<?php echo $form->error($model,'StartProduction'); ?>
				</div>
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'EndProduction'); ?></label>
				<div class="col-sm-4">
						<?php echo $form->textField($model,'EndProduction',array('class'=>'date-picker form-control')); ?>
						<br>
						<?php echo $form->error($model,'EndProduction'); ?>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'DeadlineProduction'); ?></label>
				<div class="col-sm-10">
						<?php echo $form->textField($model,'DeadlineProduction',array('class'=>'date-picker form-control')); ?>
						<br>
						<?php echo $form->error($model,'DeadlineProduction'); ?>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Status'); ?></label>
				<div class="col-sm-10">
					<?php echo $form->dropDownList($model,'Status',
							EnumStatus::getList()); ?>
						<br>
						<?php echo $form->error($model,'Status'); ?>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Note'); ?></label>
				<div class="col-sm-10">
						<?php echo $form->textarea($model,'Note',array('class'=>'form-control')); ?>
						<br>
						<?php echo $form->error($model,'Note'); ?>
				</div>
		</div>
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<table class="table table-bordered table-striped table-hover">
        <thead>
	        <tr>
	            <th>Atribute Name</th>
	            <th class="col-xs-8">Value</th>
	            <th>
	                <button type="button" id="addAtribute" class="btn btn-primary btn-sm">Add</button>
	                <span id="tree-loading"></span>
	            </th>
	        </tr>
        </thead>
        <tbody id="detailAtribute">
        <?php
        if($model->atributes != null){
            $index = 0;
            foreach($model->atributes as $att){ ?>
                <tr>
                    <td>
                        <?php echo $form->hiddenField($att,"[$index]IDProductAtribute"); ?>
                        <?php echo $form->dropDownList($att,"[$index]AtributeCode",
                            CHtml::listData(AtributeMaster::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'AtributeCode', 'AtributeName'),
                            array('empty'=>'- Choose -', 'class'=>'form-control', 'required'=>true)); ?>
                    </td>
                    <td>
                        <?php echo $form->textArea($att,"[$index]AtributeValue", array('class'=>'form-control', 'required'=>true)); ?>
                    </td>
                    <td align="center">
                        <a href="<?php echo Yii::app()->baseUrl.'/product/deleteAtribute/id/'.$att->IDProductAtribute; ?>"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
                    </td>
                </tr>
            <?php $index++; }
        } ?>
        </tbody>
    </table>
		<br>
		<hr>
	</div>





	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
        </div>
	</div>

	<?php $this->endWidget(); ?>

	<script type="text/javascript">
	    $('#addAtribute').click(function(){
	        var index = $('#detailAtribute tr').length;

	        $.ajax({
	            type: 'post',
	            url: '<?php echo $this->createUrl('addAtribute'); ?>',
	            dataType: 'json',
	            data: {index: index},
	            beforeSend: function(){
	                $('#tree-loading').html('<i class="fa fa-rotate-right fa-spin">');
	            },
	            success: function(responseJSON) {
	                if (responseJSON.success) {
	                    $('#detailAtribute').append(responseJSON.content);
	                } else {
	                    alert(responseJSON.message);
	                }
	            },
	            complete: function(){
	                $('#tree-loading').html('');
	            }
	        });
	    });

			$('#addImageDetail').click(function(){
	        var index = $('#detailImages tr').length;

	        $.ajax({
	            type: 'post',
	            url: '<?php echo $this->createUrl('addImages'); ?>',
	            dataType: 'json',
	            data: {index: index},
	            beforeSend: function(){
	                $('#tree-loading').html('<i class="fa fa-rotate-right fa-spin">');
	            },
	            success: function(responseJSON) {
	                if (responseJSON.success) {
	                    $('#detailImages').append(responseJSON.content);
	                } else {
	                    alert(responseJSON.message);
	                }
	            },
	            complete: function(){
	                $('#tree-loading').html('');
	            }
	        });
	    });
	</script>
</div><!-- form -->
