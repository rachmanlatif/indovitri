<?php

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('product-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Manage Products</span>
                <!-- <div class="widget-buttons buttons-bordered" style="padding : 5px 5px">
                    <label>
                        <input class="checkbox-slider toggle colored-blue" type="checkbox" class="show_hide" data-toggle="showImages" id="showHide">
                        <span class="text">Show Image</span>
                    </label>
                </div> -->
            </div>
            <div class="widget-body">
                <div class="table-scrollable">
                  <a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-blue">
                      <i class="btn-label glyphicon glyphicon-plus"></i>Add Product
                  </a>
                  <?php if(file_exists(UploadForm::getFilePath().'product.xls') || file_exists(UploadForm::getFilePath().'product.xlsx')){ ?>
                      <a href="#" class="btn btn-labeled btn-palegreen" id="confirmDelete">
                          <i class="btn-label glyphicon glyphicon-import"></i>Import Product
                      </a>
                  <?php } else { ?>
                      <a href="<?php echo $this->createUrl('import'); ?>" class="btn btn-labeled btn-palegreen">
                          <i class="btn-label glyphicon glyphicon-import"></i>Import Product
                      </a>
                  <?php } ?>
                  <?php if($showImage == 'true') { ?>
                      <a href="<?php echo $this->createUrl("index"); ?>" class="btn btn-labeled btn-darkorange">
                          <i class="btn-label glyphicon glyphicon-resize-small"></i>Hide Image
                      </a>

                  <?php }else{ ?>
                    <a href="<?php echo $this->createUrl("index"); ?>?showImage=true" class="btn btn-labeled btn-darkorange">
                        <i class="btn-label glyphicon glyphicon-resize-full"></i>Show Image
                    </a>
                  <?php } ?>

                  <?php echo CHtml::link('<i class="btn-label glyphicon glyphicon-search"></i> Advanced Search','#',array('class'=>'search-button btn btn-labeled btn-darkorange pull-right')); ?>


                  <div class="search-form" style="display:none">
                    <br>
                    <?php $this->renderPartial('_search',array(
                    	'model'=>$model,
                    )); ?>
                  </div><!-- search-form -->
                  <br>
                  <br>
                  <div id="status-message">
                      <?php if(Yii::app()->user->hasFlash('success')): ?>
                          <div class="alert alert-success">
                              <?php echo Yii::app()->user->getFlash('success') ?>
                          </div>
                      <?php endif ?>

                      <?php if(Yii::app()->user->hasFlash('info')): ?>
                          <div class="alert alert-info">
                              <?php echo Yii::app()->user->getFlash('info') ?>
                          </div>
                      <?php endif ?>

                      <?php if(Yii::app()->user->hasFlash('danger')): ?>
                          <div class="alert alert-danger">
                              <?php echo Yii::app()->user->getFlash('danger') ?>
                          </div>
                      <?php endif ?>
                  </div>
                  <hr>
                  <?php $this->widget('zii.widgets.grid.CGridView', array(
                  	'id'=>'product-grid',
                      'itemsCssClass'=>'table table-bordered table-hover',
                  	'dataProvider'=>$model->search(),
                  	//'filter'=>$model,
                    //'htmlOptions' => array('class' => 'grid-view rounded'),
                    'pager' => array(
                      'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
                      // 'maxButtonCount'=>4,
                      'header' => '',
                      'prevPageLabel' => 'Previous',
                      'nextPageLabel' => 'Next',
                      'firstPageLabel'=>'First',
                      'lastPageLabel'=>'Last',
                      'htmlOptions'=>array('style' => 'float : left'),
                    ),
                  	'selectableRows'=>2,
                  	'columns'=>array(
                      array(
                          'name'=>'Seller Name',
                          'value'=>'($data->seller != null ? ($data->seller->ShortName != null ? $data->seller->ShortName : $data->seller->Name) : "")',
                      ),
                  		'ProductCode',
                  		'ProductName',
                          array(
                              'name'=>'ThubnailsImage',
                              'header'=>'Image',
                              'filter'=>false,
                              'sortable'=>false,
                              'visible'=>$showImage,
                              'value'=>'"<img id=\"images\"  height=\"75\" src=\"".Product::getFileUrl().$data->ThubnailsImage."\">"',
                              // 'value'=>function($data){
                              //     echo $data->ProductID;
                              // },
                              'type'=>'raw',
                          ),
                          array(
                              'name'=>'Price',
                              'value'=>'"$".number_format($data->Price, 2)',
                          ),
                          array(
                              'name'=>'UOM',
                              'value'=>'($data->uom != null ? $data->uom->UOMName : "")',
                          ),
                          array(
                              'name'=>'QTY Carton',
                              'value'=>'($data->uom != null ? ($data->uom->define != null ? number_format($data->uom->define->QTYUOMSmall) : 0) : "")',
                          ),
                          'StokQTY',
                          array(
                              'name'=>'NettWeight',
                              'value'=>'number_format($data->NettWeight, 2)',
                          ),
                          array(
                              'name'=>'GrossWeight',
                              'value'=>'number_format($data->GrossWeight, 2)',
                          ),
                          array(
                              'name'=>'VolumeGross',
                              'value'=>'number_format($data->VolumeGross, 2)',
                          ),
                          array(
                              'name'=>'isStock',
                              'value'=>'($data->isStock > 0 ? "Yes" : "No")',
                          ),
                          array(
                              'name'=>'isProduction',
                              'value'=>'($data->isProduction > 0 ? "Yes" : "No")',
                          ),
                          array(
                              'name'=>'isCatalogue',
                              'value'=>'($data->isCatalogue > 0 ? "Yes" : "No")',
                          ),
                  		array(
                              'class'=>'CButtonColumn',
                              'template'=>'{view} {update} {delete}',
                              'header'=>'Action',
                              'htmlOptions' => array('class' => 'col-xs-2 text-center'),
                              'buttons'=>array(
                                  'view'=>array(
                                      'imageUrl'=>false,
                                      'label'=>'<i class="fa fa-search fa-fw"></i>',
                                      'options'=>array(
                                          'class'=>'btn btn-info btn-sm',
                                          'title'=>'View Detail',
                                      ),
                                  ),
                                  'update'=>array(
                                      'imageUrl'=>false,
                                      'label'=>'<i class="fa fa-edit fa-fw"></i>',
                                      'options'=>array(
                                          'class'=>'btn btn-warning btn-sm',
                                          'title'=>'Update',
                                      ),
                                  ),
                                  'delete'=>array(
                                      'imageUrl'=>false,
                                      'label'=>'<i class="fa fa-trash-o fa-fw"></i>',
                                      'options'=>array(
                                          'class'=>'btn btn-danger btn-sm',
                                          'title'=>'Delete',
                                      ),
                                  ),
                              ),
                          ),
                  	),
                  )); ?>
                </div>
            </div>
        </div>
    </div>
</div>





<div class="row">
    <div class="col-sm-12 col-xs-12">

    </div>
</div>

<script type="text/javascript">
$('#confirmDelete').click(function(){
    if (confirm('Pending import product is already exist. Renew ?')) {
        window.location.href="<?php echo $this->createUrl('import'); ?>";
    } else {
        window.location.href="<?php echo $this->createUrl('reviewUpload'); ?>";
    }
});

$('#showHide').change(function () {
  if(this.checked == true){
    window.location.href = "<?php echo $this->createUrl('index'); ?>?showImage=true";
    // $('#images').show();
    // $('#images2').hide();
  }else{
    window.location.href = "<?php echo $this->createUrl('index'); ?>?showImage=false";
    // $('#images').hide();
    // $('#images2').show();
  }

});
</script>
