<style>

  .tile {
    position: relative;
    float: left;
    width: 100%;
    height: 100%;
    overflow: hidden;
    border-radius: 3px;
    vertical-align: middle;
    padding: 10px;
  }

  .photo {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
    transition: transform .5s ease-out;
  }

  .txt {
    position: absolute;
    z-index: 2;
    right: 0;
    bottom: 10%;
    left: 0;
    font-family: 'Roboto Slab', serif;
    font-size: 9px;
    line-height: 12px;
    text-align: center;
    cursor: default;
  }

  .x {
    font-size: 32px;
    line-height: 32px;
  }

</style>

<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">View Product</span>
            </div>
            <div class="widget-body">
                <!-- <div class="flip-scroll"> -->
									<div class="form-button-container">
										<a class="form-button btn btn-labeled btn-primary" href="<?php echo $this->createUrl('index'); ?>"><i class="btn-label glyphicon glyphicon-th-list"></i> List</a>
										<a class="form-button btn btn-labeled btn-success" href="<?php echo $this->createUrl('add'); ?>"><i class="btn-label glyphicon glyphicon-plus"></i> Add</a>
										<a class="form-button btn btn-labeled btn-warning" href="<?php echo $this->createUrl('update', array('id'=>$model->ProductID)); ?>"><i class="btn-label glyphicon glyphicon-edit"></i> Update</a>
                    <a href="<?php echo $this->createUrl('delete', array('id'=>$model->ProductID)); ?>" class="btn btn-labeled btn-delete btn-danger">
                        <i class="btn-label glyphicon glyphicon-trash"></i>Delete
                    </a>
									</div>
									<hr />
									<div class="row">
									    <div class="col-sm-12 col-xs-12">
									        <div id="status-message">
									            <?php if(Yii::app()->user->hasFlash('success')): ?>
									                <div class="alert alert-success">
									                    <?php echo Yii::app()->user->getFlash('success') ?>
									                </div>
									            <?php endif ?>

									            <?php if(Yii::app()->user->hasFlash('info')): ?>
									                <div class="alert alert-info">
									                    <?php echo Yii::app()->user->getFlash('info') ?>
									                </div>
									            <?php endif ?>

									            <?php if(Yii::app()->user->hasFlash('danger')): ?>
									                <div class="alert alert-danger">
									                    <?php echo Yii::app()->user->getFlash('danger') ?>
									                </div>
									            <?php endif ?>
									        </div>
									    </div>
									</div>
                  <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <img class="tile" data-scale="2.4" id="image-0" src="<?php echo Product::getFileUrl().$model->ThubnailsImage; ?>">
                        <?php
                        if($model->images != null){
                            echo '<div id="imageProduct" style="border:1px solid #e0e0e0; border-radius: 3px; vertical-align: middle; overflow: hidden;  width: 300px; height: 300px; display: absolute;">';
                            $no = 1;
                            foreach($model->images as $image){ ?>

                                <!-- <div> -->
                                  <img class="tile" data-scale="2.4" id="image-<?=$no?>" src="<?php echo Product::getFileUrl().$image->ProductImageName; ?>">
                                <!-- </div> -->

                            <?php $no++; }
                            echo '</div><br>';
                            echo '<div id="slider-nav" style="display:flex; align-items:left; justify-content:left;">';
                            ?>
                            <img style="max-width: 52px; max-height: 52px; float: left; vertical-align: middle; padding: 10px" src="<?php echo Product::getFileUrl().$model->ThubnailsImage; ?>">
                            <?php
                            $no1 = 1;
                            foreach($model->images as $image){ ?>
                              <div>
                                <img style="max-width: 52px; max-height: 52px; float: left; vertical-align: middle; padding: 10px" src="<?php echo Product::getFileUrl().$image->ProductImageName; ?>">
                              </div>
                            <?php $no1++; }
                            echo'</div>';
                        }
                        echo'</div>';?>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <!-- <div class="container"> -->
                          <h2><strong><?=$model->ProductCode?> | <?=$model->ProductName?></strong></h2>
                          <h2 style="color : red">$<?=number_format($model->Price,3)?></h5>
                          <hr>
                          <p>Category : <strong><?=($model->category != null ? $model->category->CategoryName : '')?></strong></p>
                          <p>Inner Carton : <strong><?=($model->InnerCarton != null ? $model->InnerCarton : '-')?></strong></p>
                          <table class="table table-bordered table-striped table-hover">
                              <thead>
                                <th>Stock Qty</th>
                                <th>Minimum Qty</th>
                              </thead>
                              <tbody>
                                <td><?=number_format($model->StokQTY)?></td>
                                <td><?=number_format($model->MinQTY)?></td>
                              </tbody>
                          </table>
                          <br>
                          <div class="panel-group accordion" id="accordions">
                              <div class="panel panel-default">
                                  <div class="panel-heading">
                                      <h4 class="panel-title">
                                          <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordions" href="#collapseOnes">
                                              Detail Info
                                          </a>
                                      </h4>
                                  </div>
                                  <div id="collapseOnes" class="panel-collapse collapse" style="">
                                        <?php $this->widget('zii.widgets.CDetailView', array(
                      									    'htmlOptions'=>array(
                      									        'class'=>'detail-view table table-striped table-bordered table-hover'
                      									    ),
                      									    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
                      										'data'=>$model,
                      										'attributes'=>array(
                                            array(
                                                'name'=>'NettWeight',
                                                'value'=>number_format($model->NettWeight, 2),
                                            ),
                                            array(
                                                'name'=>'GrossWeight',
                                                'value'=>number_format($model->GrossWeight, 2),
                                            ),
                                            array(
                                                'name'=>'VolumeGross',
                                                'value'=>number_format($model->VolumeGross, 2),
                                            ),
                      											'CartonLenght',
                      											'CartonHeight',
                      											'CartonDepth',
                      											array(
                      													'name'=>'UOMName',
                      													'label'=> 'UOM Name',
                      													'value'=>($model->uom != null ? $model->uom->UOMName : ''),
                      											),
                      									        array(
                      									            'name'=>'Status',
                      									            'value'=>EnumStatus::getLabel($model->Status),
                      									        ),
                                                array(
                                                    'name'=>'isStock',
                                                    'value'=>($model->isStock > 0 ? "Yes" : "No"),
                                                ),
                                                array(
                                                    'name'=>'isProduction',
                                                    'value'=>($model->isProduction > 0 ? "Yes" : "No"),
                                                ),
                                                array(
                                                    'name'=>'isCatalogue',
                                                    'value'=>($model->isCatalogue > 0 ? "Yes" : "No"),
                                                ),
                      										),
                      									)); ?>
                                  </div>
                              </div>
                          </div>
                          <table class="table table-bordered table-striped table-hover">
                              <thead>
                                <tr>
                                    <th>Note</th>
                                </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><?php echo $model->Note; ?></td>
                                  </tr>
                              </tbody>
                          </table>
                        <!-- </div> -->

                      </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                          <h4>Seller Info</h4>
                          <table class="table table-bordered table-striped table-hover">
                              <tbody>
                                  <tr>
                                      <th>Seller Name</th>
                                  </tr>
                                  <tr>
                                      <td><?=($model->seller != null ? $model->seller->Name : '-')?></td>
                                  </tr>
                                  <tr>
                                      <th>Seller SKU</th>
                                  </tr>
                                  <tr>
                                      <td><?=($model->SellerSKU != null ? $model->SellerSKU : '-')?></td>
                                  </tr>
                              </tbody>
                          </table>
                    </div>
                  </div>


									<hr>

									<div class="tabbable">
									    <ul class="nav nav-tabs">
									        <li class="active">
									            <a data-toggle="tab" href="#tab1">
									                Product Info
									            </a>
									        </li>
													<li>
															<a data-toggle="tab" href="#tab2">
																	Product Atributes
															</a>
													</li>
									        <li>
									            <a data-toggle="tab" href="#tab3">
									                Price History
									            </a>
									        </li>
									        <li>
									            <a data-toggle="tab" href="#tab4">
									                Stock History
									            </a>
									        </li>
									    </ul>

									    <div class="tab-content radius-bordered">
									        <div id="tab1" class="tab-pane in active">
															<div class="table-responsive">
																	<?php if($info != null){ ?>
																			<table class="table table-bordered table-striped table-hover">
																					<tr>
																							<th>Start Production</th>
																							<th>End Production</th>
																							<th>Deadline Order</th>
																					</tr>
																					<tr>
																							<td><?php echo $info->StartProduction; ?></td>
																							<td><?php echo $info->EndProduction; ?></td>
																							<td><?php echo $info->DeadlineOrder; ?></td>
																					</tr>
																			</table>
																	<?php } ?>
															</div>
													</div>
													<div id="tab2" class="tab-pane">
									            <div class="table-responsive">
									                <table class="table table-bordered table-striped table-hover">
									                    <tr>
									                        <th>Name</th>
									                        <th>Value</th>
									                        <th>Note</th>
									                    </tr>
									                    <?php
									                    if($model->atributes != null){
									                        foreach($model->atributes as $att){ ?>
									                            <tr>
									                                <td><?php echo $att->atribute->AtributeName; ?></td>
									                                <td><?php echo $att->AtributeValue; ?></td>
									                                <td><?php echo $att->Note; ?></td>
									                            </tr>
									                        <?php }
									                    }?>
									                </table>
									            </div>
									        </div>
									        <div id="tab3" class="tab-pane">
									            <div class="table-responsive">
									                <table class="table table-bordered table-striped table-hover">
									                    <tr>
									                        <th>Date</th>
									                        <th>Begin Price</th>
									                        <th>End Price</th>
									                    </tr>
									                    <?php
									                    if($model->prices != null){
									                        foreach($model->prices as $price){ ?>
									                            <tr>
									                                <td><?php echo $price->Date; ?></td>
									                                <td><?php echo number_format($price->BeginPrice,3); ?></td>
									                                <td><?php echo number_format($price->ENDPrice,3); ?></td>
									                            </tr>
									                        <?php }
									                    } ?>
									                </table>
									            </div>
									        </div>
									        <div id="tab4" class="tab-pane">
									            <div class="table-responsive">
									                <table class="table table-bordered table-striped table-hover">
									                    <tr>
									                        <th>Date</th>
									                        <th>Begin QTY</th>
									                        <th>End QTY</th>
									                    </tr>
									                    <?php
									                    if($model->stocks != null){
									                        foreach($model->stocks as $stock){ ?>
									                            <tr>
									                                <td><?php echo $stock->Date; ?></td>
									                                <td><?php echo number_format($stock->BeginQTY); ?></td>
									                                <td><?php echo number_format($stock->ENDQTY); ?></td>
									                            </tr>
									                        <?php }
									                    }?>
									                </table>
									            </div>
									        </div>

									    </div>
									</div>
								<!-- </div> -->
						</div>
				</div>
		</div>
</div>
<div class="clear"></div>
<script>
$(document).ready(function(){
    $('#imageProduct').slick({
     slidesToShow: 1,
     slidesToScroll: 1,
     arrows: false,
     fade: true,
     asNavFor: '#slider-nav'
    });
    $('#slider-nav').slick({
      infinite: true,
     slidesToShow: 2,
     slidesToScroll: 1,
     asNavFor: '#imageProduct',
     dots: false,
     centerMode: true,
     focusOnSelect: true
    });

});

$('.tile')
  // tile mouse actions
  .on('mouseover', function(){
    // alert($(this).attr('data-scale'))
    $(this).children('.photo').css({'transform': 'scale('+ $(this).attr('data-scale') +')'});
  })
  .on('mouseout', function(){
    $(this).children('.photo').css({'transform': 'scale(1)'});
  })
  .on('mousemove', function(e){
    // alert(((e.pageY - $(this).offset().top) / $(this).height()) * 100 + '%')
    $(this).children('.photo').css({'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 100 + '%' + ((e.pageY - $(this).offset().top) / $(this).height()) * 100 + '%'});
  })
  // tiles set up
  .each(function(){
    $(this)
      // add a photo container
      .append('<div class="photo"></div>')

      // set up a background image for each tile based on data-image attribute
      .children('.photo').css({'background-image': 'url('+ $(this).attr('src') +')'});
  })
</script>
