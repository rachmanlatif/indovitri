<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProductID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ProductID), array('view', 'id'=>$data->ProductID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Barcode')); ?>:</b>
	<?php echo CHtml::encode($data->Barcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProductName')); ?>:</b>
	<?php echo CHtml::encode($data->ProductName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ThubnailsImage')); ?>:</b>
	<?php echo CHtml::encode($data->ThubnailsImage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Price')); ?>:</b>
	<?php echo CHtml::encode($data->Price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('StokQTY')); ?>:</b>
	<?php echo CHtml::encode($data->StokQTY); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('InnerCarton')); ?>:</b>
	<?php echo CHtml::encode($data->InnerCarton); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('NettWeight')); ?>:</b>
	<?php echo CHtml::encode($data->NettWeight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('GrossWeight')); ?>:</b>
	<?php echo CHtml::encode($data->GrossWeight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('VolumeNett')); ?>:</b>
	<?php echo CHtml::encode($data->VolumeNett); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('VolumeGross')); ?>:</b>
	<?php echo CHtml::encode($data->VolumeGross); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SellerCode')); ?>:</b>
	<?php echo CHtml::encode($data->SellerCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CartonWeight')); ?>:</b>
	<?php echo CHtml::encode($data->CartonWeight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CartonHeight')); ?>:</b>
	<?php echo CHtml::encode($data->CartonHeight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CartonDepth')); ?>:</b>
	<?php echo CHtml::encode($data->CartonDepth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('EANCode')); ?>:</b>
	<?php echo CHtml::encode($data->EANCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CategoryCode')); ?>:</b>
	<?php echo CHtml::encode($data->CategoryCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SellerSKU')); ?>:</b>
	<?php echo CHtml::encode($data->SellerSKU); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UOMCode')); ?>:</b>
	<?php echo CHtml::encode($data->UOMCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />

	*/ ?>

</div>