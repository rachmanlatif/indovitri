<?php $form = new CActiveForm(); ?>

<tr>
    <td>
        <?php echo $form->hiddenField($atribute,"[$index]IDProductAtribute"); ?>
        <?php echo $form->dropDownList($atribute,"[$index]AtributeCode",
            CHtml::listData(AtributeMaster::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'AtributeCode', 'AtributeName'),
            array('empty'=>'- Choose -', 'class'=>'form-control', 'required'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textArea($atribute,"[$index]AtributeValue", array('class'=>'form-control', 'required'=>true)); ?>
    </td>
    <td>
        <?php echo CHtml::link('Delete', 'javascript:void(0)',
            array('class'=>'btn btn-danger btn-sm delete')) ?>
    </td>
</tr>

<script>
    $('.delete').click(function() {
        $(this).parent().parent().remove();
    });
</script>