<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Import Product</span>
            </div>
            <div class="widget-body">
              <a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
                  <i class="btn-label glyphicon glyphicon-th-list"></i>Product List
              </a>
              <a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-success">
                  <i class="btn-label glyphicon glyphicon-plus"></i>Add  New Product
              </a>
              <hr>

              <?php $form=$this->beginWidget('CActiveForm', array(
                  'id'=>'product-form',
                  'enableAjaxValidation'=>false,
              )); ?>

              <?php echo $form->errorSummary($model); ?>

              <div class="row form-row form-group">
                  <div class="col-xs-2">
                      <?php echo $form->labelEx($model,'filename'); ?>
                  </div>
                  <div class="col-xs-10">
                      <div class="row">
                          <div class="col-xs-6">
                              <div id="image-container" class="uploaded-image">

                              </div>
                              <div class="clear"></div>
                              <?php $this->widget('common.extensions.widgets.qqFileUploader.QQFileUploader', array(
                                  'id'=>'qq-file-uploader',
                                  'action'=>Yii::app()->request->baseUrl . '/product/uploadExcel',
                                  'options'=>array(
                                      'allowedExtensions'=>Yii::app()->params['excelExtension'],
                                  ),
                                  'onComplete'=>"
                                      var content = '<input type=\"hidden\" id=\"UploadForm_filename\" name=\"UploadForm[filename]\" value=\"'+ responseJSON.newFileName +'\" />';
                                      $('#image-container').html(content);
                                  ",
                                  'onSubmit' => "$('.qq-upload-list').html('');",
                              ));?>
                          </div>
                          <div class="col-xs-6">
                              <?php echo $form->error($model,'filename'); ?>
                          </div>
                      </div>

                  </div>
              </div>

              <hr>

              <div class="row buttons">
                  <div class="col-xs-12">
                      <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Mapping Data</button>
                  </div>
              </div>

              <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
      
