<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="widget">
			<div class="form-horizontal">
				<h5 class="row-title before-magenta"><i class="fa fa-search magenta"></i>Form Search</h5>
				<?php $form=$this->beginWidget('CActiveForm', array(
					'action'=>Yii::app()->createUrl($this->route),
					'method'=>'get',
				)); ?>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'ProductCode'); ?></label>
                <div class="col-sm-10">
                    <?php echo $form->textField($model,'ProductCode',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
                </div>
            </div>

						<div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'ProductName'); ?></label>
                <div class="col-sm-10">
                    <?php echo $form->textField($model,'ProductName',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
                </div>
            </div>

						<div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'Price'); ?></label>
                <div class="col-sm-4">
                    <?php echo $form->textField($model,'Price',array('size'=>19,'maxlength'=>19, 'class'=>'form-control')); ?>
                </div>
								<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'StokQTY'); ?></label>
                <div class="col-sm-4">
                    <?php echo $form->textField($model,'StokQTY',array('size'=>20,'maxlength'=>20, 'class'=>'form-control')); ?>
                </div>
            </div>

						<div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'InnerCarton'); ?></label>
                <div class="col-sm-4">
                    <?php echo $form->textField($model,'InnerCarton',array('size'=>19,'maxlength'=>19, 'class'=>'form-control')); ?>
                </div>
								<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'NettWeight'); ?></label>
                <div class="col-sm-4">
                    <?php echo $form->textField($model,'NettWeight',array('size'=>20,'maxlength'=>20, 'class'=>'form-control')); ?>
                </div>
            </div>

						<div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'GrossWeight'); ?></label>
                <div class="col-sm-4">
                    <?php echo $form->textField($model,'GrossWeight',array('size'=>19,'maxlength'=>19, 'class'=>'form-control')); ?>
                </div>
								<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'VolumeGross'); ?></label>
                <div class="col-sm-4">
                    <?php echo $form->textField($model,'VolumeGross',array('size'=>20,'maxlength'=>20, 'class'=>'form-control')); ?>
                </div>
            </div>

						<div class="form-group">
                <div class="col-sm-4">
									<div class="checkbox">
                      <label>
                          <?php echo $form->checkBox($model,'isStock',array('size'=>19,'maxlength'=>19, 'class'=>'form-control')); ?>
                          <span class="text"><?php echo $form->label($model,'isStock'); ?></span>
                      </label>
                  </div>
                </div>
								<div class="col-sm-4">
									<div class="checkbox">
                      <label>
                          <?php echo $form->checkBox($model,'isProduction',array('size'=>19,'maxlength'=>19, 'class'=>'form-control')); ?>
                          <span class="text"><?php echo $form->label($model,'isProduction'); ?></span>
                      </label>
                  </div>
                </div>
								<div class="col-sm-4">
									<div class="checkbox">
                      <label>
                          <?php echo $form->checkBox($model,'isCatalogue',array('size'=>19,'maxlength'=>19, 'class'=>'form-control')); ?>
                          <span class="text"><?php echo $form->label($model,'isCatalogue'); ?></span>
                      </label>
                  </div>
                </div>
            </div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'SellerCode'); ?></label>
								<div class="col-sm-10">
									<?php echo $form->dropDownList($model,'SellerCode',
											CHtml::listData(Seller::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'SellerCode', 'Name'),
											array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 100%')); ?>
								</div>
						</div>

						<div class="form-group">
                <label for="inputEmail3" class="col-sm-4 "><?php echo $form->label($model,'CartonLenght'); ?></label>
                <label for="inputEmail3" class="col-sm-4 "><?php echo $form->label($model,'CartonHeight'); ?></label>
                <label for="inputEmail3" class="col-sm-4 "><?php echo $form->label($model,'CartonDepth'); ?></label>
                <div class="col-sm-4">
                    <?php echo $form->textField($model,'CartonLenght',array('size'=>19,'maxlength'=>19, 'class'=>'form-control')); ?>
                </div>
								<div class="col-sm-4">
                    <?php echo $form->textField($model,'CartonHeight',array('size'=>20,'maxlength'=>20, 'class'=>'form-control')); ?>
                </div>
								<div class="col-sm-4">
                    <?php echo $form->textField($model,'CartonDepth',array('size'=>20,'maxlength'=>20, 'class'=>'form-control')); ?>
                </div>
            </div>

						<div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'EANCode'); ?></label>
                <div class="col-sm-10">
                    <?php echo $form->textField($model,'EANCode',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
                </div>
            </div>

						<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'CategoryCode'); ?></label>
								<div class="col-sm-10">
									<?php echo $form->dropDownList($model,'CategoryCode',
											CHtml::listData(CategoryProduct::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'CategoryCode', 'CategoryName'),
											array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 100%')); ?>
								</div>
						</div>

						<div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'SellerSKU'); ?></label>
                <div class="col-sm-10">
                    <?php echo $form->textField($model,'SellerSKU',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
                </div>
            </div>

						<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'UOMCode'); ?></label>
								<div class="col-sm-10">
									<?php echo $form->dropDownList($model,'UOMCode',
											CHtml::listData(UOM::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'UOMCode', 'UOMName'),
											array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 100%')); ?>
								</div>
						</div>

						<div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'Status'); ?></label>
                <div class="col-sm-10">
									<?php echo $form->dropDownList($model,'Status',
											EnumStatus::getList()); ?>
                </div>
            </div>
					</div>

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Search</button>
					</div>


				<?php $this->endWidget(); ?>

			</div><!-- search-form -->
		</div>
	</div>
</div>
