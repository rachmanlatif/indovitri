<div class="row">
    <div class="col-sm-12">

        <h3 class="left">Import Review</h3>
        <div class="form-button-container">
            <a href="<?php echo $this->createUrl('index'); ?>"><input type="button" value="Product List" class="btn btn-primary"></a>
            <a href="<?php echo $this->createUrl('add'); ?>"><input type="button" value="Add New Product" class="btn btn-primary"></a>
            <a href="<?php echo $this->createUrl('deleteImport'); ?>"><input type="button" value="Delete Data" class="btn btn-primary"></a>
            <div class="clear"></div>
        </div>
        <hr>

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'product-form',
            'enableAjaxValidation'=>false,
        )); ?>

        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div id="status-message">
                    <?php if(Yii::app()->user->hasFlash('success')): ?>
                        <div class="alert alert-success">
                            <?php echo Yii::app()->user->getFlash('success') ?>
                        </div>
                    <?php endif ?>

                    <?php if(Yii::app()->user->hasFlash('info')): ?>
                        <div class="alert alert-info">
                            <?php echo Yii::app()->user->getFlash('info') ?>
                        </div>
                    <?php endif ?>

                    <?php if(Yii::app()->user->hasFlash('danger')): ?>
                        <div class="alert alert-danger">
                            <?php echo Yii::app()->user->getFlash('danger') ?>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>

        <div class="alert alert-info fade in radius-bordered alert-shadowed">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <strong>Require Column Mapping Information!</strong><br>
            - Seller<br>
            - Product Category<br>
            <br>
            - Product Code (<i>If product code already exist, data will be update</i>)<br>
            - Price<br>
            - UOM<br>
        </div>

        <div class="row form-row form-group">
            <div class="col-xs-2">
                <?php echo $form->labelEx($model,'status'); ?>
            </div>
            <div class="col-xs-10">
                <div class="row">
                    <div class="col-xs-6">
                        <?php echo $form->dropDownList($model,'status',
                            EnumStatus::getList(), array('class'=>'form-control')); ?>
                    </div>
                    <div class="col-xs-6">
                        <?php echo $form->error($model,'status'); ?>
                    </div>
                </div>

            </div>
        </div>

        <div class="row form-row form-group">
            <div class="col-xs-2">
                <?php echo $form->labelEx($model,'isSellerFromExcel'); ?>
            </div>
            <div class="col-xs-10">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="radio">
                            <label>
                                <input name="UploadForm[isSellerFromExcel]" class="isSellerFromExcel1" type="radio" checked="checked" value="1">
                                <span class="text">Yes </span>
                            </label>
                            <label>
                                <input name="UploadForm[isSellerFromExcel]" class="isSellerFromExcel0" type="radio" value="0">
                                <span class="text">No </span>
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <?php echo $form->error($model,'isSellerFromExcel'); ?>
                    </div>
                </div>

            </div>
        </div>

        <div class="row form-row form-group sellerID hide">
            <div class="col-xs-2">
                <?php echo $form->labelEx($model,'sellerID'); ?>
            </div>
            <div class="col-xs-10">
                <div class="row">
                    <div class="col-xs-6">
                        <?php echo $form->dropDownList($model,'sellerID',
                            CHtml::listData(Seller::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'SellerCode', 'Name'),
                            array('empty'=>'- Choose -', 'class'=>'form-control')); ?>
                    </div>
                    <div class="col-xs-6">
                        <div class="button1">
                            <a href="<?php echo Yii::app()->homeUrl.'seller/add'; ?>" target="_blank"><input type="button" value="Add Seller" class="btn"></a>
                        </div>
                        <?php echo $form->error($model,'sellerID'); ?>
                    </div>
                </div>

            </div>
        </div>

        <div class="row form-row form-group">
            <div class="col-xs-2">
                <?php echo $form->labelEx($model,'isFromExcel'); ?>
            </div>
            <div class="col-xs-10">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="radio">
                            <label>
                                <input name="UploadForm[isFromExcel]" class="isFromExcel1" type="radio" checked="checked" value="1">
                                <span class="text">Yes </span>
                            </label>
                            <label>
                                <input name="UploadForm[isFromExcel]" class="isFromExcel0" type="radio" value="0">
                                <span class="text">No </span>
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <?php echo $form->error($model,'isFromExcel'); ?>
                    </div>
                </div>

            </div>
        </div>

        <div class="row form-row form-group kodeProductCategory hide">
            <div class="col-xs-2">
                <?php echo $form->labelEx($model,'kodeProductCategory'); ?>
            </div>
            <div class="col-xs-10">
                <div class="row">
                    <div class="col-xs-6">
                        <?php echo $form->dropDownList($model,'kodeProductCategory',
                            CHtml::listData(CategoryProduct::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'CategoryCode', 'CategoryName'),
                            array('empty'=>'- Choose -', 'class'=>'form-control', 'disabled'=>true)); ?>
                    </div>
                    <div class="col-xs-6">
                        <div class="button1">
                            <a href="<?php echo Yii::app()->homeUrl.'categoryProduct/add'; ?>" target="_blank"><input type="button" value="Add Product Category" class="btn"></a>
                        </div>
                        <?php echo $form->error($model,'kodeProductCategory'); ?>
                    </div>
                </div>

            </div>
        </div>

        <div class="row form-row form-group">
            <div class="col-xs-2">

            </div>
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="checkbox">
                            <label>
                                <?php echo $form->checkbox($model,'isStock',array('class'=>'form-control')); ?>
                                <span class="text"><?php echo $form->labelEx($model,'isStock'); ?></span>
                            </label>
                        </div>
                        <br>
                        <?php echo $form->error($model,'isStock'); ?>
                    </div>
                    <div class="col-xs-4">
                        <div class="checkbox">
                            <label>
                                <?php echo $form->checkbox($model,'isProduction',array('class'=>'form-control')); ?>
                                <span class="text"><?php echo $form->labelEx($model,'isProduction'); ?></span>
                            </label>
                        </div>
                        <br>
                        <?php echo $form->error($model,'isProduction'); ?>
                    </div>
                    <div class="col-xs-4">
                        <div class="checkbox">
                            <label>
                                <?php echo $form->checkbox($model,'isCatalogue',array('class'=>'form-control isProduction')); ?>
                                <span class="text"><?php echo $form->labelEx($model,'isCatalogue'); ?></span>
                            </label>
                        </div>
                        <br>
                        <?php echo $form->error($model,'isCatalogue'); ?>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <b>Mapping your data</b>
        <div class="table-scrollable">
            <?php
            if(file_exists(UploadForm::getFilePath().'product.xls')){
                $filename = 'product.xls';
            }
            else{
                $filename = 'product.xlsx';
            }
            ?>
            <?php echo $form->hiddenField($model,"filename",array('value'=>$filename)); ?>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tbody>
                    <?php
                    $phpExcelPath = Yii::getPathOfAlias('site.vendors.phpexcel.Classes');
                    include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
                    require_once $phpExcelPath. '/PHPExcel/IOFactory.php';

                    if(file_exists(UploadForm::getFilePath().'product.xlsx')){
                        $path = UploadForm::getFilePath().'product.xlsx';
                    }
                    else{
                        $path = UploadForm::getFilePath().'product.xls';
                    }

                    try{
                        $inputFileType = PHPExcel_IOFactory::identify($path);
                        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                        $objPHPExcel = $objReader->load($path);
                        $objPHPExcel->setActiveSheetIndex(0);
                        $obj = $objPHPExcel->getActiveSheet();
                    } catch(Exception $e) {
                        throw new CHttpException(404,'Error read excel: '.$e->getMessage());
                    }

                    $highestRow = $obj->getHighestRow();
                    $highestColumn = $obj->getHighestColumn();
                    if(!($highestRow -1) >0)
                    {
                        throw new CHttpException('No data found in Sheet 0 or Sheet not available.');
                    }

                    function toNumber($dest)
                    {
                        if ($dest)
                            return ord(strtolower($dest)) - 96;
                        else
                            return 0;
                    }

                    $arr = array();
                    for($row=1; $row<=$highestRow; $row++) {
                        $rowData = $obj->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);
                        $hide = '';
                        if($rowData[0][0] == ''){
                            $hide = 'hide';
                        }
                        ?>
                        <tr class="<?php echo $hide; ?>">
                            <td>
                              <?php if($row == 1){ ?>
                                Thumbnail
                              <?php } else {  ?>
                                  <img src="<?php echo UploadForm::getFileUrl().'Image_'.$row.'.png'; ?>" width="100" height="80">
                              <?php } ?>
                            </td>
                            <?php for($aa=0;$aa<tonumber($highestColumn);$aa++){
                                ?>
                                <td>
                                    <span class="col<?php echo $aa; ?>"><?php echo $rowData[0][$aa]; ?></span>
                                    <?php if($row == 1){ ?>
                                    <br>
                                    <p>
                                        <b>Mapping to:</b>
                                        <br>
                                        <?php echo $form->dropDownList($model,"column[$aa]", $list,
                                            array('class'=>'form-control mapping', 'empty'=>'- Choose -')); ?>
                                            <input type="hidden" id="tampung<?php echo $aa; ?>" readonly>
                                    </p>
                                    <script type="text/javascript">
                                    var tampung = [];

                                    $('#UploadForm_column_<?php echo $aa; ?>').change(function(){
                                        var data = $('#UploadForm_column_<?php echo $aa; ?>').val();
                                        var text = $('#UploadForm_column_<?php echo $aa; ?> option:selected').text();
                                        var a = $('#tampung<?php echo $aa; ?>').val();

                                        if(data != ""){
                                            if(jQuery.inArray(data, tampung) !== -1){
                                                alert('Mapping '+text+' already choose');
                                                $('#UploadForm_column_<?php echo $aa; ?>').val("");

                                                return false;
                                            }
                                            else{
                                                tampung = jQuery.grep(tampung, function(value) {
                                                    return value != a;
                                                });

                                                tampung.push(data);
                                                $('#tampung<?php echo $aa; ?>').val(data);
                                            }
                                        }
                                        else{
                                            tampung = jQuery.grep(tampung, function(value) {
                                                return value != a;
                                            });
                                            $('#tampung<?php echo $aa; ?>').val("");
                                        }
                                    });
                                    </script>
                                    <?php } ?>
                                    <script type="text/javascript">
                                    var arr = [];
                                    $(".col<?php echo $aa; ?>").each(function(){
                                        var value = $(this).html();
                                        if (arr.indexOf(value) == -1)
                                            arr.push(value);
                                        else
                                            $(this).addClass("label label-warning");
                                    });
                                    </script>
                                </td>
                            <?php } ?>
                            <?php if($row > 1){ ?>
                                <!-- <td><button type="button" class="delete<?php echo $row; ?> btn btn-danger btn-sm">Hapus</button></td>-->
                            <?php } ?>
                        </tr>
                        <script type="text/javascript">
                            $('.delete<?php echo $row; ?>').click(function(){
                                $(this).parent().parent().remove();
                            });
                        </script>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>

        <hr>

        <div class="row buttons">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div>
</div>

<script type="text/javascript">
    $('#UploadForm_isProduction').click(function(){
      if($("#UploadForm_isProduction").is(':checked')){
          var newOption = $('<option value="StartProduction">Start Production</option><option value="EndProduction">End Production</option><option value="DeadlineProduction">Deadline Production</option>');
          $('.mapping').append(newOption);
      }
      else{
          $(".mapping option[value='StartProduction']").remove();
          $(".mapping option[value='EndProduction']").remove();
          $(".mapping option[value='DeadlineProduction']").remove();
      }
    });

    $('.isSellerFromExcel1').click(function(){
        $('#UploadForm_sellerID').prop('disabled', true);
        $('.sellerID').addClass('hide');

        var newOption = $('<option value="SellerCode">Seller</option>');
        $('.mapping').append(newOption);
    });

    $('.isSellerFromExcel0').click(function(){
        $('#UploadForm_sellerID').prop('disabled', false);
        $('.sellerID').removeClass('hide');

        $(".mapping option[value='SellerCode']").remove();
    });

    $('.isFromExcel1').click(function(){
        $('#UploadForm_kodeProductCategory').prop('disabled', true);
        $('.kodeProductCategory').addClass('hide');

        var newOption = $('<option value="CategoryCode">Product Category</option>');
        $('.mapping').append(newOption);
    });

    $('.isFromExcel0').click(function(){
        $('#UploadForm_kodeProductCategory').prop('disabled', false);
        $('.kodeProductCategory').removeClass('hide');

        $(".mapping option[value='CategoryCode']").remove();
    });
</script>
