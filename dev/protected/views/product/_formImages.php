<?php $form = new CActiveForm(); ?>

<tr>
    <td>
			<?php echo CHtml::activeFileField($model, "ImageProduct[$index]"); ?>
    </td>
    <td>
        <?php echo CHtml::link('Delete', 'javascript:void(0)',
            array('class'=>'btn btn-danger btn-sm delete'.$index)) ?>
    </td>
</tr>

<script>
    $('.delete<?php echo $index; ?>').click(function() {
        $(this).parent().parent().remove();
    });
</script>
