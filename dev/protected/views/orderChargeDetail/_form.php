<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'order-charge-detail-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th></th>
            <th>Charge Name</th>
            <th>Type Value</th>
            <th>Value</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $index = 0;
        foreach($master as $master){ ?>
            <tr>
                <td class="col-sm-1">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="OrderChargeDetail[<?php echo $index; ?>][ChargeID]" value="<?php echo $master->ID; ?>">
                            <span class="text"></span>
                        </label>
                    </div>
                </td>
                <td><?php echo $master->Name; ?></td>
                <td><?php echo $master->TypeValue; ?></td>
                <td><?php echo $master->Value; ?></td>
            </tr>
        <?php $index++; } ?>
        </tbody>
    </table>

    <hr>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>        </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->