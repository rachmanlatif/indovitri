<?php

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('order-charge-detail-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<div>
	<h1 class="left">Manage Order Charge Details</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<?php echo CHtml::ajaxLink("Delete",
				$this->createUrl('deleteSelected'),
				array(
					'type' => 'post',
					'data' => 'js:{ajax:true, ids:$.fn.yiiGridView.getSelection(\'order-charge-detail-grid\')}',
					'success' => 'function(data) {
						$.fn.yiiGridView.update(\'order-charge-detail-grid\');
					}',
				),
				array(
					'class' => 'form-button btn btn-primary',
					'confirm' => 'Are you sure?',
				)
			);
		?>	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'order-charge-detail-grid',
    'itemsCssClass'=>'table table-striped table-bordered table-hover',
    'pager' => array(
      'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
      // 'maxButtonCount'=>4,
      'header' => '',
      'prevPageLabel' => 'Previous',
      'nextPageLabel' => 'Next',
      'firstPageLabel'=>'First',
      'lastPageLabel'=>'Last',
      'htmlOptions'=>array('style' => 'float : left'),
    ),
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'selectableRows'=>2,
	'columns'=>array(
		array(
			'class'=>'CCheckBoxColumn',
			'id'=>'ids',
		),
		'OrderID',
		'ChargeID',
		'Value',
		//CGridViewHelper::getCreatedBy(),
		//CGridViewHelper::getCreatedDate(),
		//CGridViewHelper::getModifiedBy(),
		//CGridViewHelper::getModifiedDate(),
		array(
			'class'=>'CButtonColumn',
            'template'=>'{view} {update} {delete}',
            'buttons'=>array(
                'view'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-search fa-fw"></i> View',
                    'options'=>array(
                        'class'=>'btn btn-info btn-xs',
                        'title'=>'View Detail',
                    ),
                ),
                'update'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-edit fa-fw"></i> Update',
                    'options'=>array(
                        'class'=>'btn btn-warning btn-xs',
                        'title'=>'Update',
                    ),
                ),
                'delete'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-trash-o fa-fw"></i> Delete',
                    'options'=>array(
                        'class'=>'btn btn-danger btn-xs',
                        'title'=>'Delete',
                    ),
                ),
            ),
		),
	),
)); ?>
