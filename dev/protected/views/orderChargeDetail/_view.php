<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ChargeID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ChargeID), array('view', 'id'=>$data->ChargeID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('OrderID')); ?>:</b>
	<?php echo CHtml::encode($data->OrderID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Value')); ?>:</b>
	<?php echo CHtml::encode($data->Value); ?>
	<br />


</div>