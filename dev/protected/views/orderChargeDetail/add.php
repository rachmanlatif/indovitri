
<div>
	<h1 class="left">Create OrderChargeDetail</h1>
	<div class="form-button-container">
        <a class="form-button btn btn-info" href="<?php echo Yii::app()->baseUrl.'/order/view/id/'.$order->OrderCode; ?>">Back To Order</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array(
    'model'=>$model,
    'master'=>$master,
    'order'=>$order,
)); ?>