<div class="error-header"> </div>
<div class="container ">
    <section class="error-container text-center">
        <h1><?php echo $code; ?></h1>
        <div class="error-divider">
            <?php if($code == '404'){ ?>
                <h2>page not found</h2>
                <p class="description">We Couldn’t Find This Page</p>
            <?php } else if($code == '500'){ ?>
                <h2>ooops!!</h2>
                <p class="description">SOMETHING WENT WRONG.</p>
            <?php } else{ ?>
                <h2>failed</h2>
                <p class="description"><?php echo CHtml::encode($message); ?></p>
            <?php } ?>
        </div>
        <button onclick="window.history.back()" class="return-btn"><i class="fa fa-reply"></i> Back</button>
    </section>
</div>
