
<?php

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('order-header-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">View Seller</span>
            </div>
            <div class="widget-body">
                <div class="table-scrollable">
									<a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
                      <i class="btn-label glyphicon glyphicon-th-list"></i>List
                  </a>
									<a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-success">
                      <i class="btn-label glyphicon glyphicon-plus"></i>Add
                  </a>
									<a href="<?php echo $this->createUrl('update', array('id'=>$model->SellerCode)); ?>" class="btn btn-labeled btn-warning">
                      <i class="btn-label glyphicon glyphicon-edit"></i>Edit
                  </a>
                  <a href="<?php echo $this->createUrl('delete', array('id'=>$model->SellerCode)); ?>" class="btn btn-labeled btn-delete btn-danger">
                      <i class="btn-label glyphicon glyphicon-trash"></i>Delete
                  </a>
                  <br>
									<div class="row">
									    <div class="col-sm-12 col-xs-12">
									        <div id="status-message">
									            <?php if(Yii::app()->user->hasFlash('success')): ?>
									                <div class="alert alert-success">
									                    <?php echo Yii::app()->user->getFlash('success') ?>
									                </div>
									            <?php endif ?>

									            <?php if(Yii::app()->user->hasFlash('info')): ?>
									                <div class="alert alert-info">
									                    <?php echo Yii::app()->user->getFlash('info') ?>
									                </div>
									            <?php endif ?>

									            <?php if(Yii::app()->user->hasFlash('danger')): ?>
									                <div class="alert alert-danger">
									                    <?php echo Yii::app()->user->getFlash('danger') ?>
									                </div>
									            <?php endif ?>
									        </div>
									    </div>
									</div>
                  <br>
									<?php $this->widget('zii.widgets.CDetailView', array(
									    'htmlOptions'=>array(
									        'class'=>'detail-view table table-striped table-bordered table-hover'
									    ),
									    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
										'data'=>$model,
										'attributes'=>array(
											'SellerCode',
											'Name',
											'ShortName',
											'Phone',
											'email',
											'Address',
											'PostCode',
											array(
													'name'=>'CityCode',
													'value'=>($model->city != null ? $model->city->CityName : ''),
											),
											array(
													'name'=>'BankCode',
													'value'=>($model->bank != null ? $model->bank->BankName : ''),
											),
											'AccountNo',
											'AccountName',
                      'SwiftCode',
                      'IbanCode',
                      'Branch',
                      'BankAddress',
											'Website',
											'EmployeeCount',
											'Furnase',
											'CapacityTon',
											'FactoryCount',
									        array(
									            'name'=>'Status',
									            'value'=>EnumStatus::getLabel($model->Status),
									        ),
										),
									)); ?>

									<hr>

									<div class="tabbable">
									    <ul class="nav nav-tabs">
									        <li class="active">
									            <a data-toggle="tab" href="#tab1">
									                Factories
									            </a>
									        </li>
									        <li>
									            <a data-toggle="tab" href="#tab2">
									                Contact Person
									            </a>
									        </li>
													<li>
															<a data-toggle="tab" href="#tab3">
																	Product List
															</a>
													</li>
													<li>
															<a data-toggle="tab" href="#tab4">
																	Order History
															</a>
													</li>
									    </ul>

									    <div class="tab-content radius-bordered">
									        <div id="tab1" class="tab-pane in active">
															<div class="table-scrollable">
																	<table class="table table-bordered table-striped table-hover">
																			<tr>
																					<th>Factory Name</th>
																					<th>Address</th>
																					<th>Phone</th>
																					<th>Note</th>
																			</tr>

																			<?php
																			if($factories != null){
																					foreach($factories as $detail){ ?>
																							<tr>
																									<td><?php echo $detail->FactoryName; ?></td>
																									<td><?php echo $detail->Address; ?></td>
																									<td><?php echo $detail->Phone; ?></td>
																									<td><?php echo $detail->Note; ?></td>
																							</tr>
																					<?php }
																			} ?>
																	</table>
															</div>
													</div>
													<div id="tab2" class="tab-pane">
															<div class="table-scrollable">
																	<table class="table table-bordered table-striped table-hover">
																			<tr>
																					<th>Position Code</th>
																					<th>Name</th>
																					<th>Phone</th>
																					<th>Address</th>
																					<th>Email</th>
																					<th>Note</th>
																			</tr>

																			<?php if($detailPerson != null){
																					foreach($detailPerson as $detail){ ?>
																							<tr>
																									<td><?php echo ($detail->position != null ? $detail->position->PositionName : ''); ?></td>
																									<td><?php echo $detail->Name; ?></td>
																									<td><?php echo $detail->Phone; ?></td>
																									<td><?php echo $detail->Address; ?></td>
																									<td><?php echo $detail->Email; ?></td>
																									<td><?php echo $detail->Note; ?></td>
																							</tr>
																					<?php }
																			} ?>
																	</table>
															</div>
													</div>
													<div id="tab3" class="tab-pane">
                              <?php if($showImage == 'true') { ?>
                                  <a href="<?php echo $this->createUrl('view', array('id'=>$model->SellerCode)); ?>" class="btn btn-labeled btn-darkorange">
                                      <i class="btn-label glyphicon glyphicon-resize-small"></i>Hide Image
                                  </a>

                              <?php }else{ ?>
                                <a href="<?php echo $this->createUrl('view', array('id'=>$model->SellerCode)); ?>?showImage=true" class="btn btn-labeled btn-darkorange">
                                    <i class="btn-label glyphicon glyphicon-resize-full"></i>Show Image
                                </a>
                              <?php } ?>

															<div class="table-scrollable">
																	<?php $this->widget('zii.widgets.grid.CGridView', array(
																		'id'=>'product-grid',
																			'itemsCssClass'=>'table table-striped table-bordered table-hover',
																		'dataProvider'=>$product->search(),
                                    'pager' => array(
                                      'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
                                      // 'maxButtonCount'=>4,
                                      'header' => '',
                                      'prevPageLabel' => 'Previous',
                                      'nextPageLabel' => 'Next',
                                      'firstPageLabel'=>'First',
                                      'lastPageLabel'=>'Last',
                                      'htmlOptions'=>array('style' => 'float : left'),
                                    ),
																		//'filter'=>$model,
																		'selectableRows'=>2,
																		'columns'=>array(
																			'ProductID',
																			'ProductName',
																					array(
																							'name'=>'ThubnailsImage',
																							'header'=>'Image',
																							'filter'=>false,
																							'sortable'=>false,
                                              'visible'=>$showImage,
																							'value'=>'"<img height=\"75\" src=\"".Product::getFileUrl().$data->ThubnailsImage."\">"',
																							'type'=>'raw',
																					),
                                          array(
                                              'name'=>'CategoryName',
                                              'value'=>'($data->category != null ? $data->category->CategoryName : "")',
                                          ),
																					array(
																							'name'=>'Price',
																							'value'=>'number_format($data->Price, 2)',
																					),
																					array(
																							'name'=>'StokQTY',
																							'value'=>'number_format($data->StokQTY)',
																					),
																			array(
																							'class'=>'CButtonColumn',
																							'template'=>'{view}',
																							'header'=>'Action',
																							'htmlOptions' => array('class' => 'col-xs-2 text-center'),
																							'buttons'=>array(
																									'view'=>array(
																											'imageUrl'=>false,
																											'label'=>'<i class="fa fa-search fa-fw"></i>',
																											'options'=>array(
																													'class'=>'btn btn-info btn-xs',
																													'title'=>'View Detail',
																											),
																											'url'=>'Yii::app()->createUrl("seller/viewProduct", array("id"=>$data->ProductID))',
																									),
																							),
																					),
																		),
																	)); ?>
															</div>
													</div>
													<div id="tab4" class="tab-pane">
                              <?php echo CHtml::link('<i class="btn-label glyphicon glyphicon-search"></i> Advanced Search','#',array('class'=>'search-button btn btn-labeled btn-darkorange pull-right')); ?>

                              <div class="search-form" style="display:none">
                                <br>
                                <?php $this->renderPartial('_searchOrder',array(
                                  'model'=>$order,
                                  'seller'=>$model,
                                )); ?>
                              </div><!-- search-form -->

															<div class="table-scrollable">
																	<?php $this->widget('zii.widgets.grid.CGridView', array(
																		'id'=>'order-header-grid',
																			'itemsCssClass'=>'table table-striped table-bordered table-hover',
																			'pager' => array(
																		    'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
																		    // 'maxButtonCount'=>4,
																		    'header' => '',
																		    'prevPageLabel' => 'Previous',
																		    'nextPageLabel' => 'Next',
																		    'firstPageLabel'=>'First',
																		    'lastPageLabel'=>'Last',
																		    'htmlOptions'=>array('style' => 'float : left'),
																		  ),
																		'dataProvider'=>$order->search(),
																		//'filter'=>$model,
																		'selectableRows'=>2,
																		'columns'=>array(
																					array(
																							'class'=>'CButtonColumn',
																							'template'=>'{view}',
																							'header'=>'View',
																							'htmlOptions' => array('class' => 'col-xs-2 text-center'),
																							'buttons'=>array(
																									'view'=>array(
																											'imageUrl'=>false,
																											'label'=>'<i class="fa fa-search fa-fw"></i>',
																											'options'=>array(
																													'class'=>'btn btn-info btn-xs',
																													'title'=>'View Detail Order',
																											),
																											'url'=>'Yii::app()->createUrl("order/viewOrder", array("id"=>$data->OrderCode))',
																									),
																							),
																					),
																					array(
																							'name'=>'OrderStatusCode',
																							'value'=>'EnumOrder::getLabel($data->OrderStatusCode)',
																					),
																					array(
																							'name'=>'OrderDate',
																							'value'=>'date("Y-m-d", strtotime($data->OrderDate))',
																					),
																					array(
																							'name'=>'CustomerCode',
																							'value'=>'($data->customer != null ? $data->customer->Name : "")',
																					),
																					array(
																							'name'=>'PolCode',
																							'value'=>'($data->pol != null ? $data->pol->CityName : "")',
																					),
																					array(
																							'name'=>'PodCode',
																							'value'=>'($data->pod != null ? $data->pod->CityName : "")',
																					),
																					array(
																							'name'=>'ConsigneeCode',
																							'value'=>'($data->consignee != null ? $data->consignee->Name : "")',
																					),
																					'PORefCode',
																					array(
																							'name'=>'PODate',
																							'value'=>'($data->PODate > "1980-01-01" ? date("Y-m-d", strtotime($data->PODate)) : "")',
																					),
																					'BillCodeSupplier',
																					array(
																							'name'=>'BillSupplierDate',
																							'value'=>'($data->PODate > "1980-01-01" ? date("Y-m-d", strtotime($data->BillSupplierDate)) : "")',
																					),
																					array(
																							'name'=>'OrderTotal',
																							'value'=>'number_format($data->OrderTotal, 2)',
																					),
																					array(
																							'name'=>'SupplierFee',
																							'value'=>'number_format($data->SupplierFee, 2)',
																					),
																					array(
																							'name'=>'AgentCodeOrigin',
																							'value'=>'($data->agentOrigin != null ? $data->agentOrigin->Name : "")',
																					),
																					array(
																							'name'=>'IncotermID',
																							'value'=>'($data->incoterm != null ? $data->incoterm->IncotermName : "")',
																					),
																			),
																	)); ?>
															</div>
													</div>
											</div>
									</div>
								</div>
						</div>
				</div>
		</div>
</div>
