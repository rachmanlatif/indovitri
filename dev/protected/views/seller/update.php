<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Update Seller</span>
            </div>
            <div class="widget-body">
							<a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
									<i class="btn-label glyphicon glyphicon-th-list"></i>List Seller
							</a>
							<a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-success">
									<i class="btn-label glyphicon glyphicon-plus"></i>Add Seller
							</a>
							<a href="<?php echo $this->createUrl('view', array('id'=>$model->SellerCode)); ?>" class="btn btn-labeled btn-warning">
									<i class="btn-label glyphicon glyphicon-search"></i>View Seller
							</a>
							<hr>
							<?php echo $this->renderPartial('_form', array(
							    'model'=>$model,
									'get_country' => $get_country,
							    'factories'=>$factories,
							    'detailPerson'=>$detailPerson,
							)); ?>
						</div>
				</div>
		</div>
</div>
