
<div>
	<h1 class="left">View Order</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
    </div>
</div>
<div class="clear"></div>
<hr />

<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div id="status-message">
            <?php if(Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success">
                    <?php echo Yii::app()->user->getFlash('success') ?>
                </div>
            <?php endif ?>

            <?php if(Yii::app()->user->hasFlash('info')): ?>
                <div class="alert alert-info">
                    <?php echo Yii::app()->user->getFlash('info') ?>
                </div>
            <?php endif ?>

            <?php if(Yii::app()->user->hasFlash('danger')): ?>
                <div class="alert alert-danger">
                    <?php echo Yii::app()->user->getFlash('danger') ?>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'OrderCode',
		'OrderDate',
		'CustomerCode',
		'SellerCode',
		'ConsigneeCode',
		'NotifyPartyCode',
        'IncontermID',
        'ComisionCodeSkema',
        'AgentCodeOrigin',
        'AgentCodeDestination',
        array(
            'name'=>'OrderStatusCode',
            'value'=>EnumOrder::getLabel($model->OrderStatusCode),
        ),
        'POLCode',
        'PODCode',
        array(
            'name'=>'BuyerFee',
            'value'=>number_format($model->BuyerFee),
        ),
        array(
            'name'=>'BuyerFeeAmount',
            'value'=>number_format($model->BuyerFeeAmount),
        ),
        array(
            'name'=>'SupplierFee',
            'value'=>number_format($model->SupplierFee),
        ),
        array(
            'name'=>'FreightFee',
            'value'=>number_format($model->FreightFee),
        ),
        array(
            'name'=>'OrderTotal',
            'value'=>number_format($model->OrderTotal),
        ),
	),
)); ?>

<hr>

<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active">
            <a data-toggle="tab" href="#tab1">
                <b>Detail Order</b>
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#tab2">
                <b>PO</b>
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#tab3">
                <b>PI</b>
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#tab4">
                <b>Customer Payment List</b>
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#tab5">
                <b>Seller Payment List</b>
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#tab6">
                <b>BL</b>
            </a>
        </li>
    </ul>

    <div class="tab-content radius-bordered">
        <div id="tab1" class="tab-pane in active">
            <?php
            $details = array();
            if($model->OrderStatusCode == EnumOrder::PROSPECT){
                $details = $model->detailsTemp;
            }
            else{
                $details = $model->details;
            }
            ?>

            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Image</th>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total Order</th>
                        <th>Total Volume</th>
                        <th>Container</th>
                        <th>Supplier Fee</th>
                    </tr>
                    <?php foreach($details as $detail){ ?>
                        <tr>
                            <td>
                                <img height="50" src="<?php echo Product::getFileUrl().$detail->product->ThubnailsImage; ?>" />
                            </td>
                            <td><?php echo $detail->product->ProductName; ?></td>
                            <td><?php echo number_format($detail->product->Price); ?></td>
                            <td><?php echo number_format($detail->QTY); ?></td>
                            <td><?php echo number_format($detail->TotalOrder); ?></td>
                            <td><?php echo number_format($detail->TotalVolume); ?></td>
                            <td>
                                <?php echo $detail->container->ContainerName; ?><br>
                                <?php echo number_format($detail->container->UsableVolume - $detail->TotalVolume); ?> volume remains
                            </td>
                            <td><?php echo number_format($detail->SupplierFee); ?></td>
                        </tr>
                    <?php } ?>
                    </thead>
                </table>
            </div>
        </div>
        <div id="tab2" class="tab-pane">
            <?php $this->widget('zii.widgets.CDetailView', array(
                'htmlOptions'=>array(
                    'class'=>'detail-view table table-striped table-bordered table-hover'
                ),
                'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
                'data'=>$model,
                'attributes'=>array(
                    'PORefCode',
                    array(
                        'name'=>'POStatusCode',
                        'value'=>EnumStatus::getLabel($model->POStatusCode),
                    ),
                    'PODate',
                ),
            )); ?>
        </div>
        <div id="tab3" class="tab-pane">
            <?php $this->widget('zii.widgets.CDetailView', array(
                'htmlOptions'=>array(
                    'class'=>'detail-view table table-striped table-bordered table-hover'
                ),
                'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
                'data'=>$model,
                'attributes'=>array(
                    'BillCodeSupplier',
                    'BillSupplierDate',
                    'BillSupplierTermofPayment',
                ),
            )); ?>
        </div>
        <div id="tab4" class="tab-pane">
            <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'customer-payment-grid',
                'itemsCssClass'=>'table table-striped table-bordered table-hover',
								'pager' => array(
							    'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
							    // 'maxButtonCount'=>4,
							    'header' => '',
							    'prevPageLabel' => 'Previous',
							    'nextPageLabel' => 'Next',
							    'firstPageLabel'=>'First',
							    'lastPageLabel'=>'Last',
							    'htmlOptions'=>array('style' => 'float : left'),
							  ),
                'dataProvider'=>$customerPayment->search(),
                //'filter'=>$model,
                'selectableRows'=>2,
                'columns'=>array(
                    'PaymentCode',
                    'CustomerCode',
                    'DatePayment',
                    'CustomerComercial',
                    array(
                        'name'=>'Ammount',
                        'value'=>'number_format($data->Ammount)',
                    ),
                    'BankCode',
                    'Note',
                    array(
                        'class'=>'CButtonColumn',
                        'template'=>'{view}',
                        'buttons'=>array(
                            'view'=>array(
                                'imageUrl'=>false,
                                'label'=>'<i class="fa fa-search fa-fw"></i>',
                                'options'=>array(
                                    'class'=>'btn btn-info btn-xs',
                                    'title'=>'View Detail',
                                ),
                                'url'=>'Yii::app()->createUrl("customerPayment/view", array("id"=>$data->IDCustomerPayment))',
                            ),
                        ),
                    ),
                ),
            )); ?>
        </div>
        <div id="tab5" class="tab-pane">
            <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'seller-bill-payment-grid',
                'itemsCssClass'=>'table table-striped table-bordered table-hover',
								'pager' => array(
							    'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
							    // 'maxButtonCount'=>4,
							    'header' => '',
							    'prevPageLabel' => 'Previous',
							    'nextPageLabel' => 'Next',
							    'firstPageLabel'=>'First',
							    'lastPageLabel'=>'Last',
							    'htmlOptions'=>array('style' => 'float : left'),
							  ),
                'dataProvider'=>$sellerPayment->search(),
                //'filter'=>$model,
                'selectableRows'=>2,
                'columns'=>array(
                    'PaymentCode',
                    'DatePayment',
                    'SellerComercial',
                    array(
                        'name'=>'Ammount',
                        'value'=>'number_format($data->Ammount)',
                    ),
                    'BankCode',
                    'Note',
                ),
            )); ?>
        </div>
        <div id="tab6" class="tab-pane">
            <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'order-bl-grid',
                'itemsCssClass'=>'table table-striped table-bordered table-hover',
								'pager' => array(
							    'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
							    // 'maxButtonCount'=>4,
							    'header' => '',
							    'prevPageLabel' => 'Previous',
							    'nextPageLabel' => 'Next',
							    'firstPageLabel'=>'First',
							    'lastPageLabel'=>'Last',
							    'htmlOptions'=>array('style' => 'float : left'),
							  ),
                'dataProvider'=>$orderBl->search(),
                //'filter'=>$model,
                'selectableRows'=>2,
                'columns'=>array(
                    'BLNumber',
                    'ContainerCode',
                    'DocStatus',
                    'CourierTrackNo',
                    'LoadingDate',
                    'EstimatedTimeDelivery',
                    'EstimatedTimeArive',
                    'BLReleasePayment',
                    'CBDocs',
                ),
            )); ?>
        </div>
    </div>
</div>
