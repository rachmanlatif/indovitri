<?php $form = new CActiveForm(); ?>

<tr>
    <td>
        <?php echo $form->hiddenField($detail,"[$index]FactoryCode"); ?>
        <?php echo $form->textField($detail,"[$index]FactoryName", array('class'=>'form-control', 'required'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textField($detail,"[$index]Address", array('class'=>'form-control', 'required'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textField($detail,"[$index]Phone", array('class'=>'form-control')); ?>
    </td>
    <td>
        <?php echo $form->textField($detail,"[$index]Note", array('class'=>'form-control')); ?>
    </td>
    <td>
        <?php echo CHtml::link('Delete', 'javascript:void(0)',
            array('class'=>'btn btn-danger btn-sm delete')) ?>
    </td>
</tr>

<script>
    $('.delete').click(function() {
        $(this).parent().parent().remove();
    });
</script>