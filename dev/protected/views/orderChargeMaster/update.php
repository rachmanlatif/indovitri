<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Update Charges</span>
            </div>
            <div class="widget-body">
							<a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
									<i class="btn-label glyphicon glyphicon-th-list"></i>List
							</a>
							<a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-success">
									<i class="btn-label glyphicon glyphicon-plus"></i>Add
							</a>
							<a href="<?php echo $this->createUrl('view', array('id'=>$model->ID)); ?>" class="btn btn-labeled btn-warning">
									<i class="btn-label glyphicon glyphicon-search"></i>View
							</a>
							<hr>
							<?php echo $this->renderPartial('_form', array(
							    'model'=>$model,
							)); ?>
						</div>
				</div>
		</div>
</div>
