<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ContainerCode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ContainerCode), array('view', 'id'=>$data->ContainerCode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ContainerName')); ?>:</b>
	<?php echo CHtml::encode($data->ContainerName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ContainerMaxVolume')); ?>:</b>
	<?php echo CHtml::encode($data->ContainerMaxVolume); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UsableVolume')); ?>:</b>
	<?php echo CHtml::encode($data->UsableVolume); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Note')); ?>:</b>
	<?php echo CHtml::encode($data->Note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />


</div>