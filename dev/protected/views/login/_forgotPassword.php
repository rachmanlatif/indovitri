<?php if(Yii::app()->user->getState('user_id') == null): ?>


    <?php if(Yii::app()->user->hasFlash('forgetPassword')): ?>
        <div class="logobox">
            <?php echo Yii::app()->user->getFlash('forgetPassword'); ?>
        </div>
    <?php endif; ?>

    <div class="loginbox bg-white" style="background : #FFF; height: 350px;">
        <div class="loginbox-title">Input Email</div>
        <div class="loginbox-or">
            <div class="or-line"></div>
        </div>
        <div class="loginbox-textbox">
            <input type="text" name="email" id="email" class="form-control" placeholder="Input Email">

        </div>
        <div class="loginbox-submit">
            <input type="button" class="btn btn-primary btn-block" value="Process" onclick="prosesEmail();" id="btnEmail">
        </div>
        <div id="setPass" style="display : none">
          <div class="loginbox-title">Set New Password</div>
          <div class="loginbox-or">
              <div class="or-line"></div>
          </div>
          <div class="loginbox-textbox">
              <input type="password" name="newPass" id="newPass" class="form-control" placeholder="New Password">
              <br>
              <input type="password" name="confirmPass" id="confirmPass" class="form-control" placeholder="Confirm New Password">

          </div>
          <div class="loginbox-submit">
              <input type="button" class="btn btn-primary btn-block" value="Process" onclick="prosesChange();">
          </div>
        </div>


        <div class="loginbox-or">
            <div class="or-line"></div>
        </div>
        <br>
    </div>


<?php endif; ?>

<script>
function prosesEmail(){
  if($('#email').val() == ''){
    alert('Input Email');
  }else{
    $('#setPass').show();
    $('#btnEmail').hide();
  }
}
</script>
