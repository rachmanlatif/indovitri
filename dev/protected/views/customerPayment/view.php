
<div>
	<h1 class="left">View CustomerPayment</h1>
	<div class="form-button-container">
		<?php if($customer != null){ ?>
			<a class="form-button btn btn-info" href="<?php echo Yii::app()->baseUrl.'/customer/view/id/'.$customer->CustomerCode; ?>">Back To Customer Detail</a>
			<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add', array('id'=>$customer->CustomerCode)); ?>">Add</a>
		<?php } ?>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('update', array('id'=>$model->IDCustomerPayment)); ?>">Update</a>
		<?php echo CHtml::linkButton('Delete',
				array(
					'class' => 'form-button btn btn-primary',
					'submit' => array('delete','id'=>$model->IDCustomerPayment),
					'confirm' => 'Are you sure you want to delete this item?',
				)
			);
		?>	</div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'IDCustomerPayment',
		'CustomerCode',
		'PaymentCode',
		'DatePayment',
		'CustomerComercial',
		array(
				'name'=>'Ammount',
				'value'=>number_format($model->Ammount),
		),
		'BankCode',
		'Note',
		'Status',
		//CDetailViewHelper::getCreatedBy($model),
		//CDetailViewHelper::getCreatedDate($model),
		//CDetailViewHelper::getModifiedBy($model),
		//CDetailViewHelper::getModifiedDate($model),
	),
)); ?>
