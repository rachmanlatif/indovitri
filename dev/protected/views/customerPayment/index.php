<?php

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('customer-payment-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<div>
	<h1 class="left">Manage Customer Payments</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-payment-grid',
    'itemsCssClass'=>'table table-striped table-bordered table-hover',
    'pager' => array(
      'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
      // 'maxButtonCount'=>4,
      'header' => '',
      'prevPageLabel' => 'Previous',
      'nextPageLabel' => 'Next',
      'firstPageLabel'=>'First',
      'lastPageLabel'=>'Last',
      'htmlOptions'=>array('style' => 'float : left'),
    ),
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'selectableRows'=>2,
	'columns'=>array(
		'CustomerCode',
		'PaymentCode',
		'DatePayment',
		'CustomerComercial',
        'BankCode',
		'Ammount',
		'Note',
		array(
            'class'=>'CButtonColumn',
            'template'=>'{view} {update} {delete}',
            'header'=>'Action',
            'htmlOptions' => array('class' => 'col-xs-2 text-center'),
            'buttons'=>array(
                'view'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-search fa-fw"></i>',
                    'options'=>array(
                        'class'=>'btn btn-info btn-xs',
                        'title'=>'View Detail',
                    ),
                ),
                'update'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-edit fa-fw"></i>',
                    'options'=>array(
                        'class'=>'btn btn-warning btn-xs',
                        'title'=>'Update',
                    ),
                ),
                'delete'=>array(
                    'imageUrl'=>false,
                    'label'=>'<i class="fa fa-trash-o fa-fw"></i>',
                    'options'=>array(
                        'class'=>'btn btn-danger btn-xs',
                        'title'=>'Delete',
                    ),
                ),
            ),
        ),
	),
)); ?>
