<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Create Customer Payment</span>
            </div>
            <div class="widget-body">
							<?php if($customer != null){ ?>
								<a href="<?php echo Yii::app()->baseUrl.'/customer/view/id/'.$customer->CustomerCode; ?>" class="btn btn-labeled btn-blue">
										<i class="btn-label glyphicon glyphicon-chevron-left"></i>Back To Customer Detail
								</a>
							<?php } ?>
							<hr>
							<?php echo $this->renderPartial('_form', array(
							    'model'=>$model,
							    'customer'=>$customer,
							    'totalPayment'=>$totalPayment,
							    'totalOrder'=>$totalOrder,
							)); ?>
						</div>
				</div>
		</div>
</div>
