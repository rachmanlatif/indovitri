
<div>
	<h1 class="left">Update Customer Payment</h1>
	<div class="form-button-container">
		<?php if($customer != null){ ?>
			<a class="form-button btn btn-info" href="<?php echo Yii::app()->baseUrl.'/customer/view/id/'.$customer->CustomerCode; ?>">Back To Customer Detail</a>
			<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add', array('id'=>$customer->CustomerCode)); ?>">Add</a>
		<?php } ?>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->IDCustomerPayment)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array(
    'model'=>$model,
    'customer'=>$customer,
    'totalPayment'=>$totalPayment,
    'totalOrder'=>$totalOrder,
)); ?>
