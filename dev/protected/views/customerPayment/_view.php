<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('IDCustomerPayment')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->IDCustomerPayment), array('view', 'id'=>$data->IDCustomerPayment)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CustomerCode')); ?>:</b>
	<?php echo CHtml::encode($data->CustomerCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PaymentCode')); ?>:</b>
	<?php echo CHtml::encode($data->PaymentCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DatePayment')); ?>:</b>
	<?php echo CHtml::encode($data->DatePayment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CustomerComercial')); ?>:</b>
	<?php echo CHtml::encode($data->CustomerComercial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Ammount')); ?>:</b>
	<?php echo CHtml::encode($data->Ammount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BankCode')); ?>:</b>
	<?php echo CHtml::encode($data->BankCode); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('Note')); ?>:</b>
	<?php echo CHtml::encode($data->Note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />

	*/ ?>

</div>