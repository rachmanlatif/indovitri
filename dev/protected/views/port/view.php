<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">View Port</span>
            </div>
            <div class="widget-body">
                <div class="table-scrollable">
									<a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
                      <i class="btn-label glyphicon glyphicon-th-list"></i>List Port
                  </a>
									<a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-success">
                      <i class="btn-label glyphicon glyphicon-plus"></i>Add Port
                  </a>
									<a href="<?php echo $this->createUrl('update', array('id'=>$model->PortID)); ?>" class="btn btn-labeled btn-warning">
                      <i class="btn-label glyphicon glyphicon-edit"></i>Edit Port
                  </a>
                  <a href="<?php echo $this->createUrl('delete', array('id'=>$model->PortID)); ?>" class="btn btn-labeled btn-delete btn-danger">
                      <i class="btn-label glyphicon glyphicon-trash"></i>Delete
                  </a>
									<hr>

									<div class="row">
									    <div class="col-sm-12 col-xs-12">
									        <div id="status-message">
									            <?php if(Yii::app()->user->hasFlash('success')): ?>
									                <div class="alert alert-success">
									                    <?php echo Yii::app()->user->getFlash('success') ?>
									                </div>
									            <?php endif ?>

									            <?php if(Yii::app()->user->hasFlash('info')): ?>
									                <div class="alert alert-info">
									                    <?php echo Yii::app()->user->getFlash('info') ?>
									                </div>
									            <?php endif ?>

									            <?php if(Yii::app()->user->hasFlash('danger')): ?>
									                <div class="alert alert-danger">
									                    <?php echo Yii::app()->user->getFlash('danger') ?>
									                </div>
									            <?php endif ?>
									        </div>
									    </div>
									</div>

									<hr />

									<?php $this->widget('zii.widgets.CDetailView', array(
									    'htmlOptions'=>array(
									        'class'=>'detail-view table table-striped table-bordered table-hover'
									    ),
									    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
										'data'=>$model,
										'attributes'=>array(
											'PortID',
											'PortName',
											'ShortName',
											'Note',
											array(
													'name'=>'CityCode',
													'value'=>($model->city != null ? $model->city->CityName : ""),
											),
										),
									)); ?>
								</div>
							</div>
					</div>
			</div>
	</div>
