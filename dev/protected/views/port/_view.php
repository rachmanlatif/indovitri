<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('PortID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->PortID), array('view', 'id'=>$data->PortID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PortName')); ?>:</b>
	<?php echo CHtml::encode($data->PortName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Note')); ?>:</b>
	<?php echo CHtml::encode($data->Note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CityCode')); ?>:</b>
	<?php echo CHtml::encode($data->CityCode); ?>
	<br />


</div>