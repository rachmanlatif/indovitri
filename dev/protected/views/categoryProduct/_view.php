<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('CategoryCode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->CategoryCode), array('view', 'id'=>$data->CategoryCode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CategoryName')); ?>:</b>
	<?php echo CHtml::encode($data->CategoryName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Note')); ?>:</b>
	<?php echo CHtml::encode($data->Note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />


</div>