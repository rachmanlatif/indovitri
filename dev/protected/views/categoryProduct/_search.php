<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="widget">
			<div class="form-horizontal">
				<h5 class="row-title before-magenta"><i class="fa fa-search magenta"></i>Form Search</h5>
				<?php $form=$this->beginWidget('CActiveForm', array(
					'action'=>Yii::app()->createUrl($this->route),
					'method'=>'get',
				)); ?>

				<div class="row form-row form-group">
		        <div class="col-xs-2">
		            <?php echo $form->label($model,'CategoryCode'); ?>
		        </div>
		        <div class="col-xs-10">
		            <div class="row">
		                <div class="col-xs-6">
		                    <?php echo $form->textField($model,'CategoryCode',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		                </div>
		            </div>

		        </div>
		    </div>

		    <div class="row form-row form-group">
		        <div class="col-xs-2">
		            <?php echo $form->label($model,'CategoryName'); ?>
		        </div>
		        <div class="col-xs-10">
		            <div class="row">
		                <div class="col-xs-6">
		                    <?php echo $form->textField($model,'CategoryName',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		                </div>
		            </div>

		        </div>
		    </div>

		    <div class="row form-row form-group">
		        <div class="col-xs-2">
		            <?php echo $form->label($model,'Note'); ?>
		        </div>
		        <div class="col-xs-10">
		            <div class="row">
		                <div class="col-xs-6">
		                    <?php echo $form->textField($model,'Note',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		                </div>
		            </div>

		        </div>
		    </div>

				<div class="row form-row form-group">
						<div class="col-xs-2">
								<?php echo $form->label($model,'Status'); ?>
						</div>
						<div class="col-xs-10">
								<div class="row">
										<div class="col-xs-6">
											<?php echo $form->dropDownList($model,'Status',
													EnumStatus::getList()); ?>
										</div>
								</div>

						</div>
				</div>

				<div class="row form-row buttons">
							<div class="col-xs-7">
									<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Search</button>    	</div>
				</div>
				<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</div>
