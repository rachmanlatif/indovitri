<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('UOMCode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->UOMCode), array('view', 'id'=>$data->UOMCode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UOMName')); ?>:</b>
	<?php echo CHtml::encode($data->UOMName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Note')); ?>:</b>
	<?php echo CHtml::encode($data->Note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />


</div>