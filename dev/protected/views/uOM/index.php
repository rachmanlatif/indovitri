<?php

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('uom-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Manage Unit Of Measurement</span>
            </div>
            <div class="widget-body">
                <div class="table-scrollable">
                  <a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-blue">
                      <i class="btn-label glyphicon glyphicon-plus"></i>Add
                  </a>

                  <?php echo CHtml::link('<i class="btn-label glyphicon glyphicon-search"></i> Advanced Search','#',array('class'=>'search-button btn btn-labeled btn-darkorange pull-right')); ?>

                  <div class="search-form" style="display:none">
                    <br>
                    <?php $this->renderPartial('_search',array(
                    	'model'=>$model,
                    )); ?>
                  </div><!-- search-form -->

                  <hr>
                  <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id'=>'uom-grid',
                      'itemsCssClass'=>'table table-striped table-bordered table-hover',
                      'pager' => array(
                        'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
                        // 'maxButtonCount'=>4,
                        'header' => '',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel'=>'First',
                        'lastPageLabel'=>'Last',
                        'htmlOptions'=>array('style' => 'float : left'),
                      ),
                  	'dataProvider'=>$model->search(),
                  	//'filter'=>$model,
                  	'selectableRows'=>2,
                  	'columns'=>array(
                  		'UOMCode',
                  		'UOMName',
                  		'ShortName',
                      array(
                          'name'=>'QTYSmaller',
                          'value'=>'($data->define != null ? number_format($data->define->QTYUOMSmall) : "")',
                      ),
                  		'Note',
                          array(
                              'name'=>'Status',
                              'value'=>'EnumStatus::getLabel($data->Status)',
                          ),
                      array(
                          'class'=>'CButtonColumn',
                          'template'=>'{view} {update} {delete}',
                          'header'=>'Action',
                          'htmlOptions' => array('class' => 'col-xs-3 text-center'),
                          'buttons'=>array(
                              'view'=>array(
                                  'imageUrl'=>false,
                                  'label'=>'<i class="fa fa-search fa-fw"></i>',
                                  'options'=>array(
                                      'class'=>'btn btn-info btn-sm',
                                      'title'=>'View Detail',
                                  ),
                              ),
                              'update'=>array(
                                  'imageUrl'=>false,
                                  'label'=>'<i class="fa fa-edit fa-fw"></i>',
                                  'options'=>array(
                                      'class'=>'btn btn-warning btn-sm',
                                      'title'=>'Update',
                                  ),
                              ),
                              'delete'=>array(
                                  'imageUrl'=>false,
                                  'label'=>'<i class="fa fa-trash-o fa-fw"></i>',
                                  'options'=>array(
                                      'class'=>'btn btn-danger btn-sm',
                                      'title'=>'Delete',
                                  ),
                              ),
                          ),
                      ),
                  	),
                  )); ?>
                </div>
            </div>
        </div>
    </div>
</div>
