<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Create Seller Bill Payment</span>
            </div>
            <div class="widget-body">
							<a href="<?php echo Yii::app()->baseUrl.'/order/view/id/'.$model->OrderID; ?>" class="btn btn-labeled btn-blue">
									<i class="btn-label glyphicon glyphicon-chevron-left"></i>Back To Order
							</a>
							<hr>
							<?php echo $this->renderPartial('_form', array(
							    'model'=>$model,
							    'order'=>$order,
							    'totalPaid'=>$totalPaid,
							)); ?>
						</div>
				</div>
		</div>
</div>
