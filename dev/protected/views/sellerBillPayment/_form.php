<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'seller-bill-payment-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<table class="table table-bordered table-striped table-hover">
			<tr>
					<th>Order Total</th>
					<td><?php echo number_format($order->OrderTotal); ?></td>
			</tr>
			<tr>
					<th>Seller Discount %</th>
					<td><?php echo number_format($order->SupplierFee); ?></td>
			</tr>
			<tr>
					<th colspan="2"></th>
			</tr>
			<tr>
					<th>Summary Order</th>
					<td>
							<?php
							$diskon = ($order->OrderTotal * $order->SupplierFee)/100;
							$total = $order->OrderTotal - $diskon;
							echo number_format($total);
							?>
					</td>
			</tr>
			<tr>
					<th>Total Paid</th>
					<td><?php echo number_format($totalPaid); ?></td>
			</tr>
			<tr>
					<th>Remain Payment</th>
					<td>
						<?php
						$remain = $total - $totalPaid;
						echo number_format($remain);
						?>
					</td>
			</tr>
	</table>
	<hr>
	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'PaymentCode'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'PaymentCode',array('size'=>60,'maxlength'=>255, 'readonly'=>true,'class'=>'form-control')); ?>
										(Automatic Payment Code Generate)
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'PaymentCode'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'DatePayment'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'DatePayment', array('class'=>'date-picker', 'autocomplete'=>'off','class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'DatePayment'); ?>
                </div>
            </div>

		</div>
	</div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'BankCode'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'BankCode',
                        CHtml::listData(BankList::model()->findAll(), 'BankCode', 'BankName'),
                        array('empty'=>'- Choose -', 'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'BankCode'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Ammount'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'Ammount',array('size'=>19,'maxlength'=>19,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Ammount'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Note'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'Note',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Note'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>        </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
