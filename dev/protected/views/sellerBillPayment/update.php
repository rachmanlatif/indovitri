
<div>
	<h1 class="left">Update Seller Bill Payment</h1>
	<div class="form-button-container">
        <a class="form-button btn btn-info" href="<?php echo Yii::app()->baseUrl.'/order/view/id/'.$model->OrderID; ?>">Back To Order</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add', array('id'=>$model->OrderID)); ?>">Add</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array(
    'model'=>$model,
    'order'=>$order,
    'totalPaid'=>$totalPaid,
)); ?>
