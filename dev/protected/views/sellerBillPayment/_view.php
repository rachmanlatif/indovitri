<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('IDSellerBillPayment')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->IDSellerBillPayment), array('view', 'id'=>$data->IDSellerBillPayment)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('OrderID')); ?>:</b>
	<?php echo CHtml::encode($data->OrderID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PaymentCode')); ?>:</b>
	<?php echo CHtml::encode($data->PaymentCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DatePayment')); ?>:</b>
	<?php echo CHtml::encode($data->DatePayment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SellerComercial')); ?>:</b>
	<?php echo CHtml::encode($data->SellerComercial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Ammount')); ?>:</b>
	<?php echo CHtml::encode($data->Ammount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BankCode')); ?>:</b>
	<?php echo CHtml::encode($data->BankCode); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('Note')); ?>:</b>
	<?php echo CHtml::encode($data->Note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />

	*/ ?>

</div>