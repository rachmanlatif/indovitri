
<div>
	<h1 class="left">View SellerBillPayment</h1>	
	<div class="form-button-container">
        <a class="form-button btn btn-info" href="<?php echo Yii::app()->baseUrl.'/order/view/id/'.$model->OrderID; ?>">Back To Order</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add', array('id'=>$model->OrderID)); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('update', array('id'=>$model->IDSellerBillPayment)); ?>">Update</a>
		<a class="form-button btn btn-danger" href="<?php echo $this->createUrl('delete', array('id'=>$model->IDSellerBillPayment)); ?>">Delete</a>
</div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'OrderID',
		'PaymentCode',
		'DatePayment',
		'SellerComercial',
		'Ammount',
		'BankCode',
		'Note',
		'Status',
		'IDSellerBillPayment',
		//CDetailViewHelper::getCreatedBy($model),
		//CDetailViewHelper::getCreatedDate($model),
		//CDetailViewHelper::getModifiedBy($model),
		//CDetailViewHelper::getModifiedDate($model),
	),
)); ?>
