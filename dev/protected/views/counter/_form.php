<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'counter-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'keyword'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'keyword',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'keyword'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'value'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'value',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'value'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>        </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
