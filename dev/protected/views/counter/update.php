<?php
$this->breadcrumbs=array(
	'Counters'=>array('index'),
	$model->keyword=>array('view','id'=>$model->keyword),
	'Update',
);

$this->menu=array(
	array('label'=>'List Counter', 'url'=>array('index')),
	array('label'=>'Create Counter', 'url'=>array('create')),
	array('label'=>'View Counter', 'url'=>array('view', 'id'=>$model->keyword)),
	array('label'=>'Manage Counter', 'url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Update Counter</span>
            </div>
            <div class="widget-body">
							<a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
									<i class="btn-label glyphicon glyphicon-th-list"></i>List Counter
							</a>
							<a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-success">
									<i class="btn-label glyphicon glyphicon-plus"></i>Add Counter
							</a>
							<a href="<?php echo $this->createUrl('view', array('id'=>$model->keyword)); ?>" class="btn btn-labeled btn-warning">
									<i class="btn-label glyphicon glyphicon-search"></i>View Consignee
							</a>
							<hr>
							<?php echo $this->renderPartial('_form', array(
							    'model'=>$model,
							)); ?>
						</div>
				</div>
		</div>
</div>
