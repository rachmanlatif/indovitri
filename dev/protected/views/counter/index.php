<?php
$this->breadcrumbs=array(
	'Counters'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Counter', 'url'=>array('index')),
	array('label'=>'Create Counter', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('counter-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Manage Counters</span>
            </div>
            <div class="widget-body">
                <div class="table-scrollable">
                  <a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-blue">
                      <i class="btn-label glyphicon glyphicon-plus"></i>Add Counter
                  </a>


                  <hr>
                  <?php $this->widget('zii.widgets.grid.CGridView', array(
										'id'=>'counter-grid',
									    'itemsCssClass'=>'table table-striped table-bordered table-hover',
											'pager' => array(
										    'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
										    // 'maxButtonCount'=>4,
										    'header' => '',
										    'prevPageLabel' => 'Previous',
										    'nextPageLabel' => 'Next',
										    'firstPageLabel'=>'First',
										    'lastPageLabel'=>'Last',
										    'htmlOptions'=>array('style' => 'float : left'),
										  ),
										'dataProvider'=>$model->search(),
										'selectableRows'=>2,
										'columns'=>array(
											'keyword',
											'value',
                      array(
                          'class'=>'CButtonColumn',
                          'template'=>'{view} {update} {delete}',
                          'header'=>'Action',
                          'htmlOptions' => array('class' => 'col-xs-3 text-center'),
                          'buttons'=>array(
                              'view'=>array(
                                  'imageUrl'=>false,
                                  'label'=>'<i class="fa fa-search fa-fw"></i> Detail',
                                  'options'=>array(
                                      'class'=>'btn btn-info btn-sm',
                                      'title'=>'View Detail',
                                  ),
                              ),
                              'update'=>array(
                                  'imageUrl'=>false,
                                  'label'=>'<i class="fa fa-edit fa-fw"></i> Update',
                                  'options'=>array(
                                      'class'=>'btn btn-warning btn-sm',
                                      'title'=>'Update',
                                  ),
                              ),
                              'delete'=>array(
                                  'imageUrl'=>false,
                                  'label'=>'<i class="fa fa-trash-o fa-fw"></i> Delete',
                                  'options'=>array(
                                      'class'=>'btn btn-danger btn-sm',
                                      'title'=>'Delete',
                                  ),
                              ),
                          ),
                      ),
                  	),
                  )); ?>
                </div>
            </div>
        </div>
    </div>
</div>
