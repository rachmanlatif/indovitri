<?php
$this->breadcrumbs=array(
	'Counters'=>array('index'),
	$model->keyword,
);

$this->menu=array(
	array('label'=>'List Counter', 'url'=>array('index')),
	array('label'=>'Create Counter', 'url'=>array('create')),
	array('label'=>'Update Counter', 'url'=>array('update', 'id'=>$model->keyword)),
	array('label'=>'Delete Counter', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->keyword),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Counter', 'url'=>array('admin')),
);
?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">View Counter</span>
            </div>
            <div class="widget-body">
                <div class="table-scrollable">
									<a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
                      <i class="btn-label glyphicon glyphicon-th-list"></i>List Counter
                  </a>
									<a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-success">
                      <i class="btn-label glyphicon glyphicon-plus"></i>Add Counter
                  </a>
									<a href="<?php echo $this->createUrl('update', array('id'=>$model->keyword)); ?>" class="btn btn-labeled btn-warning">
                      <i class="btn-label glyphicon glyphicon-edit"></i>Edit Counter
                  </a>
									<?php echo CHtml::linkButton('<i class="btn-label glyphicon glyphicon-trash"></i> Delete',
											array(
												'class' => 'form-button btn-labeled btn btn-danger',
												'submit' => array('delete','id'=>$model->keyword),
												'confirm' => 'Are you sure you want to delete this item?',
											)
										);
									?>
									<hr>
									<?php $this->widget('zii.widgets.CDetailView', array(
									    'htmlOptions'=>array(
									        'class'=>'detail-view table table-striped table-bordered table-hover'
									    ),
									    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
										'data'=>$model,
										'attributes'=>array(
											'keyword',
											'value',
										),
									)); ?>
								</div>
						</div>
				</div>
		</div>
</div>
