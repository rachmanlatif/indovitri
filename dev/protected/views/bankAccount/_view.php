<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('BankAccountCode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->BankCode), array('view', 'id'=>$data->BankAccountCode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BankCode')); ?>:</b>
	<?php echo CHtml::encode($data->BankCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Branch')); ?>:</b>
	<?php echo CHtml::encode($data->Branch); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Address')); ?>:</b>
	<?php echo CHtml::encode($data->Address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('AccountNumber')); ?>:</b>
	<?php echo CHtml::encode($data->AccountNumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('AccountName')); ?>:</b>
	<?php echo CHtml::encode($data->AccountName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SwiftCode')); ?>:</b>
	<?php echo CHtml::encode($data->SwiftCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IbanCode')); ?>:</b>
	<?php echo CHtml::encode($data->IbanCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Note')); ?>:</b>
	<?php echo CHtml::encode($data->Note); ?>
	<br />
</div>
