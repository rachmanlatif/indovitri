<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="widget">
			<div class="form-horizontal">
				<h5 class="row-title before-magenta"><i class="fa fa-search magenta"></i>Form Search</h5>
				<?php $form=$this->beginWidget('CActiveForm', array(
					'action'=>Yii::app()->createUrl($this->route),
					'method'=>'get',
				)); ?>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'BankAccountCode'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'BankAccountCode',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="row form-row form-group">
				        <div class="col-xs-2">
				            <?php echo $form->labelEx($model,'BankCode'); ?>
				        </div>
						<div class="col-xs-10">
				            <div class="row">
				                <div class="col-xs-6">
				                    <?php echo $form->dropDownList($model,'BankCode',
				                        CHtml::listData(BankList::model()->findAll(), 'BankCode', 'BankName'),
				                        array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width:100%')); ?>
				                </div>
				                <div class="col-xs-6">
				                    <?php echo $form->error($model,'BankCode'); ?>
				                </div>
				            </div>

						</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'Branch'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'Branch',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'Address'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'Address',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'AccountNumber'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'AccountNumber',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'AccountName'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'AccountName',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'SwiftCode'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'SwiftCode',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'IbanCode'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'IbanCode',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'Status'); ?></label>
							<div class="col-sm-10">
								<?php echo $form->dropDownList($model,'Status',
										EnumStatus::getList()); ?>
							</div>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Search</button>
					</div>
				</div>

				<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</div>
