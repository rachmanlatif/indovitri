<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bank-list-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'BankCode'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'BankCode',
                        CHtml::listData(BankList::model()->findAll(), 'BankCode', 'BankName'),
                        array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width:100%')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'BankCode'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Branch'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'Branch',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Branch'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Address'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textarea($model,'Address',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Address'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'AccountNumber'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'AccountNumber',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'AccountNumber'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
				<div class="col-xs-2">
						<?php echo $form->labelEx($model,'AccountName'); ?>
				</div>
		<div class="col-xs-10">
						<div class="row">
								<div class="col-xs-6">
										<?php echo $form->textField($model,'AccountName',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
								</div>
								<div class="col-xs-6">
										<?php echo $form->error($model,'AccountName'); ?>
								</div>
						</div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'SwiftCode'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'SwiftCode',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'SwiftCode'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
				<div class="col-xs-2">
						<?php echo $form->labelEx($model,'IbanCode'); ?>
				</div>
		<div class="col-xs-10">
						<div class="row">
								<div class="col-xs-6">
										<?php echo $form->textField($model,'IbanCode',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
								</div>
								<div class="col-xs-6">
										<?php echo $form->error($model,'IbanCode'); ?>
								</div>
						</div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Status'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'Status',
                        EnumStatus::getList()); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Status'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Note'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textarea($model,'Note',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Note'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>        </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
