<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">View Bank Account</span>
            </div>
            <div class="widget-body">
                <div class="table-scrollable">
									<a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
                      <i class="btn-label glyphicon glyphicon-th-list"></i>List Bank Account
                  </a>
									<a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-success">
                      <i class="btn-label glyphicon glyphicon-plus"></i>Add Account
                  </a>
									<a href="<?php echo $this->createUrl('update', array('id'=>$model->BankAccountCode)); ?>" class="btn btn-labeled btn-warning">
                      <i class="btn-label glyphicon glyphicon-edit"></i>Edit Account
                  </a>
									<?php echo CHtml::linkButton('<i class="btn-label glyphicon glyphicon-trash"></i> Delete',
											array(
												'class' => 'form-button btn-labeled btn btn-danger',
												'submit' => array('delete','id'=>$model->BankAccountCode),
												'confirm' => 'Are you sure you want to delete this item?',
											)
										);
									?>
									<hr>
									<?php $this->widget('zii.widgets.CDetailView', array(
									    'htmlOptions'=>array(
									        'class'=>'detail-view table table-striped table-bordered table-hover'
									    ),
									    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
										'data'=>$model,
										'attributes'=>array(
											'BankAccountCode',
                      array(
                          'name'=>'BankCode',
                          'value'=>($model->bank != null ? $model->bank->BankName : ""),
                      ),
											'Branch',
											'Address',
											'AccountNumber',
											'AccountName',
											'SwiftCode',
											'IbanCode',
									        array(
									            'name'=>'Status',
									            'value'=>EnumStatus::getLabel($model->Status),
									        ),
											'Note',
											//CDetailViewHelper::getCreatedBy($model),
											//CDetailViewHelper::getCreatedDate($model),
											//CDetailViewHelper::getModifiedBy($model),
											//CDetailViewHelper::getModifiedDate($model),
										),
									)); ?>
								</div>
						</div>
				</div>
		</div>
</div>
