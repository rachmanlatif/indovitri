<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('CityCode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->CityCode), array('view', 'id'=>$data->CityCode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CityName')); ?>:</b>
	<?php echo CHtml::encode($data->CityName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Note')); ?>:</b>
	<?php echo CHtml::encode($data->Note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('StateCode')); ?>:</b>
	<?php echo CHtml::encode($data->StateCode); ?>
	<br />


</div>