<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'city-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right">Country</label>
			<div class="col-sm-10">
					<select class="select2" style="width : 100%" id="country">
						<?php if($get_country){
										foreach ($get_country as $rows) {
											echo '<option value="'.$rows['CountryCode'].'">'.$rows['CountryName'].'</option>';
										}
									}
						?>
					</select>
			</div>
			<br>
	</div>
	<br>
	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right">State</label>
			<div class="col-sm-10" id="state">

			</div>
			<br>
	</div>
	<br>
    <!-- <div class="row form-row form-group">
        <div class="col-xs-2">
            <?//php echo $form->labelEx($model,'StateCode'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?/*php echo $form->dropDownList($model,'StateCode',
                        CHtml::listData(State::model()->findAll(), 'StateCode', 'StateName'),
                        array('empty'=>'- Choose -', 'class'=>'form-control')); */?>
                </div>
                <div class="col-xs-6">
                    <?//php echo $form->error($model,'StateCode'); ?>
                </div>
            </div>

        </div>
    </div> -->

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'CityName'); ?></label>
				<div class="col-sm-10">
						<?php echo $form->textField($model,'CityName',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
						<br>
						<?php echo $form->error($model,'CityName'); ?>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Note'); ?></label>
				<div class="col-sm-10">
						<?php echo $form->textField($model,'Note',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
						<br>
						<?php echo $form->error($model,'Note'); ?>
				</div>
		</div>

		<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Status'); ?></label>
				<div class="col-sm-10">
					<?php echo $form->dropDownList($model,'Status',
							EnumStatus::getList()); ?>
						<br>
						<?php echo $form->error($model,'Status'); ?>
				</div>
		</div>


	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>        </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
country();
function country(){
	var countryID = $("#country option:selected").attr('value');
    $.post("<?php echo $this->createUrl('addState'); ?>", {countryID:countryID}, function(data){
			// console.log(data)
        $("#state").show();
        $("#state").html(data);
  			citys();
  			$("#state").change(citys);
    });
}

$('#country').on('change', function() {
  $.post("<?php echo $this->createUrl('addState'); ?>", {countryID:this.value}, function(data){
		$("#state").show();
		$("#state").html(data);
		citys();
		// $("#state").change(citys);
	})
});

function citys(){
	var stateID = $("#stateID option:selected").attr('value');
	var countryID = $("#country").val();
	$.post("<?php echo $this->createUrl('addCity'); ?>", {stateID:stateID, countryID:countryID}, function(data){
		// console.log(data)
			$("#city").show();
			$("#city").html(data);
	});
}
</script>
