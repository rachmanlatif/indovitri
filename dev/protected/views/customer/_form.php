<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Name'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'Name',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'Name'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Phone'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'Phone',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'Phone'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'email'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'email'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Address'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'Address',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'Address'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right">Country</label>
			<div class="col-sm-10">
					<select class="select2" style="width : 100%" id="country">
						<?php if($get_country){
	                  foreach ($get_country as $rows) {
	                    echo '<option value="'.$rows['CountryCode'].'">'.$rows['CountryName'].'</option>';
	                  }
	                }
						?>
					</select>
			</div>
	</div>
	<br>
	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><br>State</label>
			<div class="col-sm-10" id="state">

			</div>
	</div>
	<br>
	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><br>City</label>
			<div class="col-sm-10" id="city">

			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'PostCode'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'PostCode',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'PostCode'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'BankCode'); ?></label>
			<div class="col-sm-10">
				<?php echo $form->dropDownList($model,'BankCode',
						CHtml::listData(BankList::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'BankCode', 'BankName'),
						array('empty'=>'- Choose -', 'class'=>'select2', 'style' => 'width : 100%')); ?><br>
					<br>
					<?php echo $form->error($model,'BankCode'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'AccountNo'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'AccountNo',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'AccountNo'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'AccountName'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'AccountName',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'AccountName'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'SwiftCode'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'SwiftCode',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'SwiftCode'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'IbanCode'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'IbanCode',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'IbanCode'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Branch'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'Branch',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'Branch'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'BankAddress'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textarea($model,'BankAddress',array('maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'BankAddress'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Website'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'Website',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'Website'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'EmployeeCount'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'EmployeeCount',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'EmployeeCount'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Status'); ?></label>
			<div class="col-sm-10">
				<?php echo $form->dropDownList($model,'Status',
						EnumStatus::getList()); ?>
					<br>
					<?php echo $form->error($model,'Status'); ?>
			</div>
	</div>



	<h3>Detail Person</h3>
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
				<thead>
				<tr>
						<th>Position</th>
						<th>Name</th>
						<th>Phone</th>
						<th>Address</th>
						<th>Email</th>
						<th>Note</th>
						<td>
								<button type="button" id="btn-person" class="btn btn-sm btn-primary">Add</button>
								<span id="tree-loading2"></span>
						</td>
				</tr>
				</thead>
				<tbody id="detailPerson">
				<?php if($detailPerson != null){
						$index = 0;
						foreach($detailPerson as $detail){ ?>
								<tr>
										<td>
												<?php echo $form->hiddenField($detail,"[$index]IDDetailPerson"); ?>
												<?php echo $form->dropDownList($detail,"[$index]PositionCode",
														CHtml::listData(Position::model()->findAll(), 'PositionCode', 'PositionName'),
														array('empty'=>'- Choose -', 'class'=>'form-control', 'required'=>true)); ?>
										</td>
										<td>
												<?php echo $form->textField($detail,"[$index]Name", array('class'=>'form-control', 'required'=>true)); ?>
										</td>
										<td>
												<?php echo $form->textField($detail,"[$index]Phone", array('class'=>'form-control', 'required'=>true)); ?>
										</td>
										<td>
												<?php echo $form->textField($detail,"[$index]Address", array('class'=>'form-control')); ?>
										</td>
										<td>
												<?php echo $form->textField($detail,"[$index]Email", array('class'=>'form-control')); ?>
										</td>
										<td>
												<?php echo $form->textField($detail,"[$index]Note", array('class'=>'form-control')); ?>
										</td>
										<td align="center">
												<a href="<?php echo Yii::app()->baseUrl.'/customer/deletePerson/id/'.$detail->IDDetailPerson; ?>"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
										</td>
								</tr>
						<?php $index++; }
				} ?>
				</tbody>
		</table>
	</div>

	<h3>Commision Scheme</h3>
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
				<thead>
				<tr>
						<th>Percentage</th>
						<th>Name</th>
						<th>Note</th>
						<td>
								<button type="button" id="btn-scheme" class="btn btn-sm btn-primary">Add</button>
								<span id="tree-loading3"></span>
						</td>
				</tr>
				</thead>
				<tbody id="detailScheme">
				<?php if($model->commisionDetails != null){
						$index = 0;
						foreach($model->commisionDetails as $detail){ ?>
								<tr>
										<td>
												<?php echo $form->hiddenField($detail,"[$index]IDCustomerCommisionSkema"); ?>
												<?php echo CHtml::dropDownList("CustomerCommisionSchema[$index][ComisionCodeSkema]", $detail->ComisionCodeSkema,
						                $listCommision,
						                array('class'=>'form-control', 'empty'=>'- Choose -', 'required'=>true)); ?>
										</td>
										<td>
								        <?php echo $detail->commision->ComisionNameSkema; ?>
								    </td>
								    <td>
								        <?php echo $detail->commision->Note; ?>
								    </td>
										<td align="center">
												<a href="<?php echo Yii::app()->baseUrl.'/customer/deleteScheme/id/'.$detail->IDCustomerCommisionSkema; ?>"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
										</td>
								</tr>
						<?php $index++; }
				} ?>
				</tbody>
		</table>
	</div>

	<hr>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>        </div>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
$('#btn-person').click(function(){
		var index = $('#detailPerson tr').length;

		$.ajax({
				type: 'post',
				url: '<?php echo $this->createUrl('addPerson'); ?>',
				dataType: 'json',
				data: {index: index},
				beforeSend: function(){
						$('#tree-loading2').html('<i class="fa fa-rotate-right fa-spin">');
				},
				success: function(responseJSON) {
						if (responseJSON.success) {
								$('#detailPerson').append(responseJSON.content);
						} else {
								alert(responseJSON.message);
						}
				},
				complete: function(){
						$('#tree-loading2').html('');
				}
		});
});

$('#btn-scheme').click(function(){
		var index = $('#detailScheme tr').length;

		$.ajax({
				type: 'post',
				url: '<?php echo $this->createUrl('addScheme'); ?>',
				dataType: 'json',
				data: {index: index},
				beforeSend: function(){
						$('#tree-loading3').html('<i class="fa fa-rotate-right fa-spin">');
				},
				success: function(responseJSON) {
						if (responseJSON.success) {
								$('#detailScheme').append(responseJSON.content);
						} else {
								alert(responseJSON.message);
						}
				},
				complete: function(){
						$('#tree-loading3').html('');
				}
		});
});

country();
function country(){
	var countryID = $("#country option:selected").attr('value');
    $.post("<?php echo $this->createUrl('addState'); ?>", {countryID:countryID}, function(data){
			// console.log(data)
        $("#state").show();
        $("#state").html(data);
  			citys();
  			$("#state").change(citys);
    });
}

$('#country').on('change', function() {
  $.post("<?php echo $this->createUrl('addState'); ?>", {countryID:this.value}, function(data){
		$("#state").show();
		$("#state").html(data);
		citys();
		// $("#state").change(citys);
	})
});

function citys(){
	var stateID = $("#stateID option:selected").attr('value');
	var countryID = $("#country").val();
	$.post("<?php echo $this->createUrl('addCity'); ?>", {stateID:stateID, countryID:countryID}, function(data){
		// console.log(data)
			$("#city").show();
			$("#city").html(data);
	});
}

</script>
</div><!-- form -->
