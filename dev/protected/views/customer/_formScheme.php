<?php $form = new CActiveForm(); ?>

<tr>
    <td>
        <?php echo $form->hiddenField($detail,"[$index]IDCustomerCommisionSkema"); ?>
        <?php echo CHtml::dropDownList("CustomerCommisionSchema[$index][ComisionCodeSkema]", '',
            $listCommision,
            array('class'=>'form-control', 'empty'=>'- Choose -', 'required'=>true)); ?>
    </td>
    <td></td>
    <td></td>
    <td>
        <?php echo CHtml::link('Delete', 'javascript:void(0)',
            array('class'=>'btn btn-danger btn-sm delete')) ?>
    </td>
</tr>

<script>
    $('.delete').click(function() {
        $(this).parent().parent().remove();
    });
</script>
