
<div>
	<h1 class="left">Update Customer</h1>
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->CustomerCode)); ?>">View</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array(
    'model'=>$model,
		'get_country' => $get_country,
    'detailPerson'=>$detailPerson,
    'listCommision'=>$listCommision,
)); ?>
