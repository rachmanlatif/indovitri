<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">View Customer</span>
            </div>
            <div class="widget-body">
              <div class="widget-main no-padding">
								<a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
										<i class="btn-label glyphicon glyphicon-th-list"></i>List Customer
								</a>
								<a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-success">
										<i class="btn-label glyphicon glyphicon-plus"></i>Add Customer
								</a>
								<a href="<?php echo $this->createUrl('update', array('id'=>$model->CustomerCode)); ?>" class="btn btn-labeled btn-warning">
										<i class="btn-label glyphicon glyphicon-edit"></i>Update Customer
								</a>
                <a href="<?php echo $this->createUrl('delete', array('id'=>$model->CustomerCode)); ?>" class="btn btn-labeled btn-delete btn-danger">
                    <i class="btn-label glyphicon glyphicon-trash"></i>Delete
                </a>
								<a class="form-button btn btn-labeled btn-primary" target="_blank" href="<?php echo Yii::app()->baseUrl.'/download/arcpdf/id/'.$model->CustomerCode; ?>"><i class="btn-label glyphicon glyphicon-download"></i> Download AR Report</a>

								<div class="col-sm-12 col-xs-12">
						        <div id="status-message">
						            <?php if(Yii::app()->user->hasFlash('success')): ?>
						                <div class="alert alert-success">
						                    <?php echo Yii::app()->user->getFlash('success') ?>
						                </div>
						            <?php endif ?>

						            <?php if(Yii::app()->user->hasFlash('info')): ?>
						                <div class="alert alert-info">
						                    <?php echo Yii::app()->user->getFlash('info') ?>
						                </div>
						            <?php endif ?>

						            <?php if(Yii::app()->user->hasFlash('danger')): ?>
						                <div class="alert alert-danger">
						                    <?php echo Yii::app()->user->getFlash('danger') ?>
						                </div>
						            <?php endif ?>
						        </div>
						    </div>
								<hr>
								<?php $this->widget('zii.widgets.CDetailView', array(
								    'htmlOptions'=>array(
								        'class'=>'detail-view table table-striped table-bordered table-hover'
								    ),
								    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
									'data'=>$model,
									'attributes'=>array(
										'CustomerCode',
										'Name',
										'Phone',
										'email',
										'Address',
										'PostCode',
										array(
												'name'=>'CityCode',
												'value'=>($model->city != null ? $model->city->CityName : ''),
										),
										array(
												'name'=>'BankCode',
												'value'=>($model->bank != null ? $model->bank->BankName : ''),
										),
										'AccountNo',
										'AccountName',
                    'SwiftCode',
                    'IbanCode',
                    'Branch',
                    'BankAddress',
										'Website',
										'EmployeeCount',
								        array(
								            'name'=>'Status',
								            'value'=>EnumStatus::getLabel($model->Status),
								        ),
									),
								)); ?>

								<hr>

								<div class="tabbable">
								    <ul class="nav nav-tabs">
								        <li class="active">
								            <a data-toggle="tab" href="#tab1">
																<b>Customer Payment List</b>
														</a>
												</li>
												<li>
								            <a data-toggle="tab" href="#tab2">
								                <b>Customer Fee</b>
								            </a>
								        </li>
												<li>
														<a data-toggle="tab" href="#tab3">
																<b>Contact Person</b>
														</a>
												</li>
												<li>
														<a data-toggle="tab" href="#tab4">
																<b>Order History</b>
														</a>
												</li>
								    </ul>

								    <div class="tab-content radius-bordered">
												<div id="tab1" class="tab-pane in active">
								            <a class="form-button btn btn-primary" href="<?php echo Yii::app()->baseUrl.'/customerPayment/add/id/'.$model->CustomerCode; ?>">Add</a>
								            <hr>
														<table class="table table-bordered table-striped table-hover">
																<tr>
																		<th>Total Order</th>
																		<td><?php echo number_format($totalOrder); ?></td>
																</tr>
																<tr>
																		<th>Total Paid</th>
																		<td><?php echo number_format($totalPayment); ?></td>
																</tr>
																<tr>
																		<th>Remains</th>
																		<td><?php echo number_format($totalOrder - $totalPayment); ?></td>
																</tr>
														</table>
								            <?php $this->widget('zii.widgets.grid.CGridView', array(
								                'id'=>'customer-payment-grid',
								                'itemsCssClass'=>'table table-striped table-bordered table-hover',
								                'dataProvider'=>$customerPayment->search(),
																'htmlOptions' => array('class' => 'grid-view rounded'),
                                'pager' => array(
                                  'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
                                  // 'maxButtonCount'=>4,
                                  'header' => '',
                                  'prevPageLabel' => 'Previous',
                                  'nextPageLabel' => 'Next',
                                  'firstPageLabel'=>'First',
                                  'lastPageLabel'=>'Last',
                                  'htmlOptions'=>array('style' => 'float : left'),
                                ),
								                //'filter'=>$model,
								                'selectableRows'=>2,
								                'columns'=>array(
								                    'PaymentCode',
								                    'CustomerCode',
																		array(
																				'name'=>'DatePayment',
																				'value'=>'date("Y-m-d", strtotime($data->DatePayment))',
																		),
								                    'CustomerComercial',
								                    array(
								                        'name'=>'Ammount',
								                        'value'=>'number_format($data->Ammount)',
								                    ),
								                    'BankCode',
								                    'Note',
								                    array(
								                        'class'=>'CButtonColumn',
								                        'template'=>'{view} {update} {delete}',
																				'header'=>'Action',
																				'htmlOptions' => array('class' => 'col-xs-2 text-center'),
								                        'buttons'=>array(
								                            'view'=>array(
								                                'imageUrl'=>false,
								                                'label'=>'<i class="fa fa-search fa-fw"></i>',
								                                'options'=>array(
								                                    'class'=>'btn btn-info btn-xs',
								                                    'title'=>'View Detail',
								                                ),
								                                'url'=>'Yii::app()->createUrl("customerPayment/view", array("id"=>$data->IDCustomerPayment))',
								                            ),
								                            'update'=>array(
								                                'imageUrl'=>false,
								                                'label'=>'<i class="fa fa-edit fa-fw"></i>',
								                                'options'=>array(
								                                    'class'=>'btn btn-warning btn-xs',
								                                    'title'=>'Update',
								                                ),
								                                'url'=>'Yii::app()->createUrl("customerPayment/update", array("id"=>$data->IDCustomerPayment))',
								                            ),
								                            'delete'=>array(
								                                'imageUrl'=>false,
								                                'label'=>'<i class="fa fa-trash-o fa-fw"></i>',
								                                'options'=>array(
								                                    'class'=>'btn btn-danger btn-xs',
								                                    'title'=>'Delete',
								                                ),
								                                'url'=>'Yii::app()->createUrl("customerPayment/delete", array("id"=>$data->IDCustomerPayment))',
								                            ),
								                        ),
								                    ),
								                ),
								            )); ?>
								        </div>
								        <div id="tab2" class="tab-pane">

								            <table class="table table-bordered table-hover table-striped">
								                <tr>
								                    <th>Commision Name</th>
								                    <th>Percentage</th>
								                    <th>Note</th>
								                </tr>
								                <?php if($model->commisionDetails != null){
								                    foreach($model->commisionDetails as $detail){
								                        if($detail->commision != null){ ?>
								                        <tr>
								                            <td><?php echo $detail->commision->ComisionNameSkema; ?></td>
								                            <td><?php echo $detail->commision->Percentage; ?></td>
								                            <td><?php echo $detail->commision->Note; ?></td>
								                        </tr>
								                    <?php }
								                    }
								                } ?>
								            </table>
								        </div>
												<div id="tab3" class="tab-pane">
														<div class="table-responsive">
																<table class="table table-bordered table-striped table-hover">
																		<tr>
																				<th>Position Code</th>
																				<th>Name</th>
																				<th>Phone</th>
																				<th>Address</th>
																				<th>Email</th>
																				<th>Note</th>
																		</tr>

																		<?php if($detailPerson != null){
																				foreach($detailPerson as $detail){ ?>
																						<tr>
																								<td><?php echo ($detail->position != null ? $detail->position->PositionName : ''); ?></td>
																								<td><?php echo $detail->Name; ?></td>
																								<td><?php echo $detail->Phone; ?></td>
																								<td><?php echo $detail->Address; ?></td>
																								<td><?php echo $detail->Email; ?></td>
																								<td><?php echo $detail->Note; ?></td>
																						</tr>
																				<?php }
																		} ?>
																</table>
														</div>
												</div>
												<div id="tab4" class="tab-pane">
														<div class="table-scrollable">
																<?php $this->widget('zii.widgets.grid.CGridView', array(
																	'id'=>'order-header-grid',
																    'itemsCssClass'=>'table table-striped table-bordered table-hover',
                                    'pager' => array(
                                      'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
                                      // 'maxButtonCount'=>4,
                                      'header' => '',
                                      'prevPageLabel' => 'Previous',
                                      'nextPageLabel' => 'Next',
                                      'firstPageLabel'=>'First',
                                      'lastPageLabel'=>'Last',
                                      'htmlOptions'=>array('style' => 'float : left'),
                                    ),
																	'dataProvider'=>$order->search(),
																	'htmlOptions' => array('class' => 'grid-view rounded'),
																	'pager' => array(
																		'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
																		// 'maxButtonCount'=>4,
																		'header' => '',
																		'prevPageLabel' => 'Previous',
																		'nextPageLabel' => 'Next',
																		'firstPageLabel'=>'First',
																		'lastPageLabel'=>'Last',
                                    'htmlOptions'=>array('style' => 'float : left'),
																	),
																	//'filter'=>$model,
																	'selectableRows'=>2,
																	'columns'=>array(
																				array(
																						'class'=>'CButtonColumn',
																						'template'=>'{view}',
																						'header'=>'View',
																						'htmlOptions' => array('class' => 'text-center'),
																						'buttons'=>array(
																								'view'=>array(
																										'imageUrl'=>false,
																										'label'=>'<i class="fa fa-search fa-fw"></i>',
																										'options'=>array(
																												'class'=>'btn btn-info btn-xs',
																												'title'=>'View Detail Order',
																										),
																										'url'=>'Yii::app()->createUrl("order/viewOrder", array("id"=>$data->OrderCode))',
																								),
																						),
																				),
																				array(
																						'name'=>'OrderStatusCode',
																						'value'=>'EnumOrder::getLabel($data->OrderStatusCode)',
																				),
																				array(
																						'name'=>'OrderDate',
																						'value'=>'date("Y-m-d", strtotime($data->OrderDate))',
																				),
																				array(
																						'name'=>'CustomerCode',
																						'value'=>'($data->customer != null ? $data->customer->Name : "")',
																				),
																				array(
																						'name'=>'SellerCode',
																						'value'=>'($data->seller != null ? $data->seller->Name : "")',
																				),
																				array(
																						'name'=>'PolCode',
																						'value'=>'($data->pol != null ? $data->pol->CityName : "")',
																				),
																				array(
																						'name'=>'PodCode',
																						'value'=>'($data->pod != null ? $data->pod->CityName : "")',
																				),
																				array(
																						'name'=>'ConsigneeCode',
																						'value'=>'($data->consignee != null ? $data->consignee->Name : "")',
																				),
																				'PORefCode',
																				array(
																						'name'=>'PODate',
																						'value'=>'($data->PODate > "1980-01-01" ? date("Y-m-d", strtotime($data->PODate)) : "")',
																				),
																				'BillCodeSupplier',
																				array(
																						'name'=>'BillSupplierDate',
																						'value'=>'($data->PODate > "1980-01-01" ? date("Y-m-d", strtotime($data->BillSupplierDate)) : "")',
																				),
																				array(
																						'name'=>'OrderTotal',
																						'value'=>'number_format($data->OrderTotal, 2)',
																				),
																				array(
																						'name'=>'BuyerFee',
																						'value'=>'number_format($data->BuyerFee, 2)',
																				),
																				array(
																						'name'=>'BuyerFeeAmount',
																						'value'=>'number_format($data->BuyerFeeAmount, 2)',
																				),
																				array(
																						'name'=>'SupplierFee',
																						'value'=>'number_format($data->SupplierFee, 2)',
																				),
																				array(
																						'name'=>'AgentCodeOrigin',
																						'value'=>'($data->agentOrigin != null ? $data->agentOrigin->Name : "")',
																				),
																				array(
																						'name'=>'IncotermID',
																						'value'=>'($data->incoterm != null ? $data->incoterm->IncotermName : "")',
																				),
																				array(
																						'name'=>'NotifyPartyCode',
																						'value'=>'($data->notifyParty != null ? $data->notifyParty->Name : "")',
																				),
																    ),
																)); ?>
														</div>
												</div>
								    </div>
								</div>

							</div>
						</div>
				</div>
		</div>
</div>
