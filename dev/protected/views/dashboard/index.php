<div class="page-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <?php
                foreach($orders as $order){
                    if($order['Status'] == 1){
                        $url = 'order/index?OrderHeader[OrderStatusCode]=1';
                        $color = 'bg-success';
                    }
                    else if($order['Status'] == 2){
                        $url = 'order/index?OrderHeader[OrderStatusCode]=2';
                        $color = 'bg-themeprimary';
                    }
                    else if($order['Status'] == 9){
                        $url = 'order/index?OrderHeader[OrderStatusCode]=9';
                        $color = 'bg-themethirdcolor';
                    }
                    else{
                        $url = 'order/index?OrderHeader[OrderStatusCode]=0';
                        $color = 'bg-themesecondary';
                    }
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <a href="<?=$url?>">
                        <div class="databox databox-lg radius-bordered databox-shadowed">
                              <div class="databox-left <?php echo $color; ?>">
                                  <div class="databox-sparkline">
                                      <?php echo $order['JumlahOrder']; ?>
                                  </div>
                              </div>
                              <div class="databox-right bordered-thick bordered-sky bg-white">
                                  <span class="databox-number sky">$ <?php echo number_format($order['TotalOrder']); ?></span>
                                  <div class="databox-text darkgray"><?php echo EnumOrder::getLabel($order['Status']); ?></div>
                                  <div class="databox-stat bg-sky radius-bordered">
                                  </div>
                              </div>
                          </div>
                      </a>

                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="orders-container">
                <div class="orders-header">
                    <h6>Latest Orders</h6>
                </div>
                <ul class="orders-list">
                    <?php foreach($latest as $late){ ?>
                    <li class="order-item" title="Click arrow for view detail order">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 item-left">
                                <div class="item-booker"><?php echo ($late->customer != null ? $late->customer->Name : ''); ?></div>
                                <div class="item-time">
                                    <i class="fa fa-calendar"></i>
                                    <span><?php echo date('d F Y', strtotime($late->OrderDate)); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 item-right">
                                <div class="item-price">
                                    <span class="currency">$</span>
                                    <span class="price"><?php echo number_format($late->OrderTotal); ?></span>
                                </div>
                            </div>
                        </div>
                        <a class="item-more" title="Click for view detail order" href="<?php echo Yii::app()->baseUrl.'/order/view/id/'.$late->OrderCode; ?>">
                            <i></i>
                        </a>
                    </li>
                  <?php } ?>
                </ul>
                <div class="orders-footer">
                    <a class="show-all" href="<?php echo Yii::app()->baseUrl.'/order'; ?>"><i class="fa fa-angle-down"></i> Show All</a>
                </div>
            </div>


        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <h6 class="row-title before-orange">Top <?=count($topProduct)?> Products Best Seller</h6>
          <br>
          <?php foreach($topProduct as $topProduct){ ?>
            <div class="col-md-4 col-sm-12 col-xs-12">
              <a href="<?php echo Yii::app()->baseUrl.'/product/id/'.$topProduct['ProductID']; ?>">
                <div class="databox databox-lg databox-inverted radius-bordered databox-shadowed databox-graded databox-vertical">
                    <div class="databox-top no-padding">
                        <img src="<?= Product::getFileUrl().$topProduct['ProductID'] ?>" alt="" style="height:100%; width:100%;">
                    </div>
                    <div class="databox-bottom no-padding bordered-thick bordered-orange">
                        <div class="databox-row">
                            <div class="databox-cell cell-12 no-padding text-align-center bordered-right bordered-platinum">
                                <span class="databox-number lightcarbon no-margin"><?= $topProduct['ProductID'] ?></span>
                                <span class="databox-text sonic-silver  no-margin"><?= $topProduct['Total'] ?> Total Sale</span>
                            </div>
                        </div>
                    </div>
                </div>
              </a>

            </div>
          <?php } ?>

        </div>
    </div>

    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-sx-12">
        <div class="widget">
            <div class="widget-header bordered-bottom header bordered-sky">
                <i class="widget-icon fa fa-tags"></i>
                <span class="widget-caption">Order Board</span>
            </div><!--Widget Header-->
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="table-scrollable">
                        <?php $this->widget('zii.widgets.grid.CGridView', array(
                            'id'=>'order-header-grid',
                            'itemsCssClass'=>'table table-bordered table-hover',
                            'dataProvider'=>$modelOrders->search(),
                            'selectableRows'=>2,
                            'htmlOptions' => array('class' => 'grid-view rounded'),
                            'pager' => array(
                              'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
                              // 'maxButtonCount'=>4,
                              'header' => '',
                              'prevPageLabel' => 'Previous',
                              'nextPageLabel' => 'Next',
                              'firstPageLabel'=>'First',
                              'lastPageLabel'=>'Last',
                              'htmlOptions'=>array('style' => 'float : left'),
                            ),
                            'columns'=>array(
                                array(
                                    'class'=>'CButtonColumn',
                                    'template'=>'{view}',
                                    'header'=>'#',
                                    'htmlOptions' => array('class' => 'text-center'),
                                    'buttons'=>array(
                                        'view'=>array(
                                            'imageUrl'=>false,
                                            'label'=>'<i class="fa fa-search fa-fw"></i>',
                                            'options'=>array(
                                                'class'=>'btn btn-info btn-sm',
                                                'title'=>'View Detail',
                                            ),
                                            'url'=>'Yii::app()->createUrl("order/view", array("id"=>$data->OrderCode))',
                                        ),
                                    ),
                                ),
                                array(
                                    'name'=>'OrderStatusCode',
                                    'value'=>'EnumOrder::getLabel($data->OrderStatusCode)',
                                ),
                                array(
                                    'name'=>'OrderDate',
                                    'value'=>'date("Y-m-d", strtotime($data->OrderDate))',
                                ),
                                array(
                                    'name'=>'CustomerCode',
                                    'header'=>'Customer Name',
                                    'value'=>'($data->customer != null ? $data->customer->Name : "")',
                                    'headerHtmlOptions' => array('style'=>'width:250px;'),

                                ),
                                array(
                                    'name'=>'SellerCode',
                                    'header'=>'Seller Name',
                                    'value'=>'($data->seller != null ? $data->seller->Name : "")',
                                    'headerHtmlOptions' => array('style'=>'width:250px;'),
                                ),
                                array(
                                    'name'=>'PolCode',
                                    'header' => 'City Name Loading',
                                    'value'=>'($data->pol != null ? $data->pol->CityName : "")',
                                    'headerHtmlOptions' => array('style'=>'width:250px;'),
                                ),
                                array(
                                    'name'=>'PodCode',
                                    'header' => 'City Name Destination',
                                    'value'=>'($data->pod != null ? $data->pod->CityName : "")',
                                    'headerHtmlOptions' => array('style'=>'width:250px;'),
                                ),
                                array(
                                    'name'=>'ConsigneeCode',
                                    'header' => 'Consignee Name',
                                    'value'=>'($data->consignee != null ? $data->consignee->Name : "")',
                                    'headerHtmlOptions' => array('style'=>'width:250px;'),
                                ),
                                array(
                                  'name'=>'PORefCode',
                                  'headerHtmlOptions' => array('style'=>'width:250px;'),
                                ),
                                // 'PORefCode',
                                array(
                                    'name'=>'PODate',
                                    'value'=>'($data->PODate > "1980-01-01" ? date("Y-m-d", strtotime($data->PODate)) : "")',
                                ),
                                'BillCodeSupplier',
                                array(
                                    'name'=>'BillSupplierDate',
                                    'value'=>'($data->PODate > "1980-01-01" ? date("Y-m-d", strtotime($data->BillSupplierDate)) : "")',
                                ),
                                array(
                                    'name'=>'OrderTotal',
                                    'value'=>'number_format($data->OrderTotal, 2)',
                                ),
                                array(
                                    'name'=>'BuyerFee',
                                    'value'=>'number_format($data->BuyerFee, 2)',
                                ),
                                array(
                                    'name'=>'BuyerFeeAmount',
                                    'value'=>'number_format($data->BuyerFeeAmount, 2)',
                                ),
                                array(
                                    'name'=>'SupplierFee',
                                    'value'=>'number_format($data->SupplierFee, 2)',
                                ),
                                array(
                                    'name'=>'AgentCodeOrigin',
                                    'value'=>'($data->agentOrigin != null ? $data->agentOrigin->Name : "")',
                                ),
                                array(
                                    'name'=>'IncotermID',
                                    'header'=> 'Incoterm Name',
                                    'value'=>'($data->incoterm != null ? $data->incoterm->IncotermName : "")',
                                ),
                                array(
                                    'name'=>'NotifyPartyCode',
                                    'value'=>'($data->notifyParty != null ? $data->notifyParty->Name : "")',
                                ),
                            ),
                        )); ?>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

        </div>
        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

        </div>
    </div>
</div>
