<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('CountryCode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->CountryCode), array('view', 'id'=>$data->CountryCode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CountryName')); ?>:</b>
	<?php echo CHtml::encode($data->CountryName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Note')); ?>:</b>
	<?php echo CHtml::encode($data->Note); ?>
	<br />


</div>