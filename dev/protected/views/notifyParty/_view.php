<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ConsigneeCode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ConsigneeCode), array('view', 'id'=>$data->ConsigneeCode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Name')); ?>:</b>
	<?php echo CHtml::encode($data->Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Phone')); ?>:</b>
	<?php echo CHtml::encode($data->Phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Address')); ?>:</b>
	<?php echo CHtml::encode($data->Address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PostCode')); ?>:</b>
	<?php echo CHtml::encode($data->PostCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CityCode')); ?>:</b>
	<?php echo CHtml::encode($data->CityCode); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />

	*/ ?>

</div>