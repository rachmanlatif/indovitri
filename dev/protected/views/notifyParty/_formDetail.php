<?php $form = new CActiveForm(); ?>

<tr>
    <td>
        <?php echo $form->hiddenField($detail,"[$index]IDConsigneeDetail"); ?>
        <?php echo $form->dropDownList($detail,"[$index]CountryCode",
            CHtml::listData(Country::model()->findAll(), 'CountryCode', 'CountryName'),
            array('empty'=>'- Choose -', 'class'=>'form-control', 'required'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textField($detail,"[$index]RegistrationNumberName", array('class'=>'form-control', 'required'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textField($detail,"[$index]RegistrationNumberValue", array('class'=>'form-control', 'required'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textField($detail,"[$index]TaxCodeName", array('class'=>'form-control', 'required'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textField($detail,"[$index]TaxCodeValue", array('class'=>'form-control', 'required'=>true)); ?>
    </td>
    <td>
        <?php echo CHtml::link('Delete', 'javascript:void(0)',
            array('class'=>'btn btn-danger btn-sm delete')) ?>
    </td>
</tr>

<script>
    $('.delete').click(function() {
        $(this).parent().parent().remove();
    });
</script>