<?php $form = new CActiveForm(); ?>

<tr>
    <td>
        <?php echo $form->hiddenField($detail,"[$index]IDDetailPerson"); ?>
        <?php echo $form->dropDownList($detail,"[$index]PositionCode",
            CHtml::listData(Position::model()->findAll(), 'PositionCode', 'PositionName'),
            array('empty'=>'- Choose -', 'class'=>'form-control', 'required'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textField($detail,"[$index]Name", array('class'=>'form-control', 'required'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textField($detail,"[$index]Phone", array('class'=>'form-control', 'required'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textArea($detail,"[$index]Address", array('class'=>'form-control')); ?>
    </td>
    <td>
        <?php echo $form->textField($detail,"[$index]Email", array('class'=>'form-control')); ?>
    </td>
    <td>
        <?php echo $form->textField($detail,"[$index]Note", array('class'=>'form-control')); ?>
    </td>
    <td>
        <?php echo CHtml::link('Delete', 'javascript:void(0)',
            array('class'=>'btn btn-danger btn-sm delete')) ?>
    </td>
</tr>

<script>
    $('.delete').click(function() {
        $(this).parent().parent().remove();
    });
</script>
