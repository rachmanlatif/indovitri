<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('IDOrderBL')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->IDOrderBL), array('view', 'id'=>$data->IDOrderBL)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('OrderID')); ?>:</b>
	<?php echo CHtml::encode($data->OrderID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BLNumber')); ?>:</b>
	<?php echo CHtml::encode($data->BLNumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DocStatus')); ?>:</b>
	<?php echo CHtml::encode($data->DocStatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CourierTrackNo')); ?>:</b>
	<?php echo CHtml::encode($data->CourierTrackNo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LoadingDate')); ?>:</b>
	<?php echo CHtml::encode($data->LoadingDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('EstimatedTimeDelivery')); ?>:</b>
	<?php echo CHtml::encode($data->EstimatedTimeDelivery); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('EstimatedTimeArive')); ?>:</b>
	<?php echo CHtml::encode($data->EstimatedTimeArive); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BLReleasePayment')); ?>:</b>
	<?php echo CHtml::encode($data->BLReleasePayment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CBDocs')); ?>:</b>
	<?php echo CHtml::encode($data->CBDocs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ContainerCode')); ?>:</b>
	<?php echo CHtml::encode($data->ContainerCode); ?>
	<br />

	*/ ?>

</div>