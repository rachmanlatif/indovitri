<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'OrderID'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'OrderID',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'BLNumber'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'BLNumber',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'DocStatus'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'DocStatus'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'CourierTrackNo'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'CourierTrackNo',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'LoadingDate'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'LoadingDate'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'EstimatedTimeDelivery'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'EstimatedTimeDelivery'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'EstimatedTimeArive'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'EstimatedTimeArive'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'BLReleasePayment'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'BLReleasePayment'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'CBDocs'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'CBDocs',array('size'=>19,'maxlength'=>19)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'ContainerCode'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'ContainerCode',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'IDOrderBL'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'IDOrderBL'); ?>
                </div>
            </div>

        </div>
    </div>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>    	</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->