
<div>
	<h1 class="left">Update Order BL</h1>
	<div class="form-button-container">
        <a class="form-button btn btn-info" href="<?php echo Yii::app()->baseUrl.'/order/view/id/'.$model->OrderID; ?>">Back To Order</a>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo $this->renderPartial('_form', array(
    'model'=>$model,
    'order'=>$order,
    'listContainer'=>$listContainer,
)); ?>
