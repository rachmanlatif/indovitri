<div class="table-scrollable">
    <table class="table table-bordered table-hover table-striped">
        <thead>
            <?php
            $totalQTY = 0;
            $totalUnit = 0;
            $totalOrder = 0;
            $totalGrossWeight = 0;
            $totalNettWeight = 0;
            $totalVolume = 0;
            if($details != null){
                foreach($details as $detail){
                    $totalQTY+=$detail->QTY;
                    $totalUnit+=$detail->QtyUnit;
                    $totalOrder+=$detail->TotalOrder;
                    $totalGrossWeight+=$detail->TotalGrossWeight;
                    $totalNettWeight+=$detail->TotalNettWeight;
                    $totalVolume+=$detail->TotalVolume;
                }
            }
            ?>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <th colspan="2">WORKING TOTALS:</th>
                <td></td>
                <td>
                    <?php echo number_format($totalQTY); ?>
                </td>
                <td></td>
                <td></td>
                <td>
                    <?php echo number_format($totalUnit); ?>
                </td>
                <td>
                    <?php echo number_format($totalOrder, 2); ?>
                </td>
                <td></td>
                <td>
                    <?php echo number_format($totalGrossWeight, 2); ?>
                </td>
                <td></td>
                <td>
                    <?php echo number_format($totalNettWeight, 2); ?>
                </td>
                <td></td>
                <td>
                      <?php echo number_format($totalVolume, 2); ?>
                </td>
            </tr>
            <tr>
                <th>Seq</th>
                <th>Image</th>
                <th>Description</th>
                <th>Product Code</th>
                <th>Price /Unit (USD)</th>
                <th>Unit</th>
                <th>Master Carton Unit</th>
                <th>Order Qty Carton</th>
                <th>Container Seq</th>
                <th>Stocks</th>
                <th>Qty Units</th>
                <th>Total Cost (USD)</th>
                <th>G.W (Kgs)</th>
                <th>G.W Total</th>
                <th>N.W (Kgs)</th>
                <th>N.W Total</th>
                <th>CBM</th>
                <th>CBM Total</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $index = 1;
        foreach($details as $detail){
            $volume = number_format($detail->TotalVolume, 2);
            $order = number_format($detail->TotalOrder, 2);
            $grossweight = number_format($detail->TotalGrossWeight, 2);
            $nettweight = number_format($detail->TotalNettWeight, 2);
            $qtyUnit = number_format($detail->QtyUnit, 0);
            $qty = number_format($detail->QTY, 0);
            ?>
            <tr>
                <td><?php echo $index; ?></td>
                <td>
                    <img height="50" src="<?php echo Product::getFileUrl().$detail->product->ThubnailsImage; ?>" />
                </td>
                <td>
                    <?php echo $detail->product->ProductName; ?>
                </td>
                <td>
                    <?php echo $detail->product->ProductID; ?>
                </td>
                <td>
                    <?php echo number_format($detail->product->Price, 2); ?>
                </td>
                <td>
                    <?php
                    $uom = '';
                    if($detail->product->uom !== null){
                        $uom = $detail->product->uom->UOMName;
                    }

                    echo $uom;
                    ?>
                </td>
                <td>
                    <?php
                    $small = 0;
                    if($detail->product->uom->define !== null){
                        $small = $detail->product->uom->define->QTYUOMSmall;
                    }

                    echo number_format($small);
                    ?>
                </td>
                <td>
                    <?php echo $qty; ?>
                </td>
                <td>
                    <?php echo $detail->container->ContainerName; ?>
                </td>
                <td>
                    <?php echo number_format($detail->product->StokQTY); ?>
                </td>
                <td>
                    <?php echo $qtyUnit; ?>
                </td>
                <td>
                    <?php echo $order; ?>
                </td>
                <td>
                    <?php echo number_format($detail->product->GrossWeight, 2); ?>
                </td>
                <td>
                    <?php echo $grossweight; ?>
                </td>
                <td>
                    <?php echo number_format($detail->product->NettWeight, 2); ?>
                </td>
                <td>
                    <?php echo $nettweight; ?>
                </td>
                <td>
                    <?php echo number_format($detail->product->VolumeGross, 2); ?>
                </td>
                <td>
                    <?php echo $volume; ?>
                </td>
            </tr>
        <?php $index++; } ?>
      </tbody>
    </table>
</div>
