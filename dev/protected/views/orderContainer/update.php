<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Update Order BL Container</span>
            </div>
            <div class="widget-body">
							<a href="<?php echo Yii::app()->baseUrl.'/order/view/id/'.$order->OrderCode; ?>" class="btn btn-labeled btn-blue">
									<i class="btn-label glyphicon glyphicon-chevron-left"></i>Back To Order
							</a>

							<hr />

							<?php echo $this->renderPartial('_form', array(
							    'models'=>$models,
							    'order'=>$order,
							)); ?>
						</div>
				</div>
		</div>
</div>
