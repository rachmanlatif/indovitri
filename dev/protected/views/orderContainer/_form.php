<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'order-bl-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($models); ?>

	<div class="row">
	    <div class="col-sm-12 col-xs-12">
	        <div id="status-message">
	            <?php if(Yii::app()->user->hasFlash('success')): ?>
	                <div class="alert alert-success">
	                    <?php echo Yii::app()->user->getFlash('success') ?>
	                </div>
	            <?php endif ?>

	            <?php if(Yii::app()->user->hasFlash('info')): ?>
	                <div class="alert alert-info">
	                    <?php echo Yii::app()->user->getFlash('info') ?>
	                </div>
	            <?php endif ?>

	            <?php if(Yii::app()->user->hasFlash('danger')): ?>
	                <div class="alert alert-danger">
	                    <?php echo Yii::app()->user->getFlash('danger') ?>
	                </div>
	            <?php endif ?>
	        </div>
	    </div>
	</div>

    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th>Container</th>
            <th>Container Number</th>
            <th>Seal Code</th>
            <th>Max Volume</th>
            <th>Usable Volume</th>
            <th>Gross Wight</th>
            <th>Nett Wight</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $index = 0;
        foreach($models as $model){ ?>
            <tr>
                <td>
                    <?php echo $form->hiddenField($model,"[$index]IDOrderBLContainer"); ?>
                    <?php echo $form->hiddenField($model,"[$index]ContainerCode"); ?>
                    <?php echo $model->container->ContainerName; ?>
                </td>
                <td>
                    <?php echo $form->textField($model,"[$index]NumberContainer",array('class'=>'form-control','maxlength'=>255)); ?>
                </td>
                <td>
                    <?php echo $form->textField($model,"[$index]SealCode",array('class'=>'form-control','maxlength'=>255)); ?>
                </td>
								<td>
										<?php echo number_format($model->container->ContainerMaxVolume,2); ?>
								</td>
								<td>
										<?php echo number_format($model->container->UsableVolume,2); ?>
								</td>
								<td>
										<?php echo number_format($model->container->GrossWeight,2); ?>
								</td>
								<td>
										<?php echo number_format($model->container->NettWeight,2); ?>
								</td>
            </tr>
        <?php $index++; } ?>
        </tbody>
    </table>

    <hr>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
        </div>
	</div>

<?php $this->endWidget(); ?>

<hr>
<?php
$this->renderPartial('viewProduct', array(
		'details'=>$order->details,
));
?>

</div><!-- form -->
