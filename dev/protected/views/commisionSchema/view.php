<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">View Contract Scheme</span>
            </div>
            <div class="widget-body">
                <div class="table-scrollable">
									<a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
                      <i class="btn-label glyphicon glyphicon-th-list"></i>List Contract Scheme
                  </a>
									<a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-success">
                      <i class="btn-label glyphicon glyphicon-plus"></i>Add Contract Scheme
                  </a>
									<a href="<?php echo $this->createUrl('update', array('id'=>$model->ComisionCodeSkema)); ?>" class="btn btn-labeled btn-warning">
                      <i class="btn-label glyphicon glyphicon-edit"></i>Edit Contract Scheme
                  </a>
                  <a href="<?php echo $this->createUrl('delete', array('id'=>$model->ComisionCodeSkema)); ?>" class="btn btn-labeled btn-delete btn-danger">
                      <i class="btn-label glyphicon glyphicon-trash"></i>Delete
                  </a>
									<hr>
									<?php $this->widget('zii.widgets.CDetailView', array(
									    'htmlOptions'=>array(
									        'class'=>'detail-view table table-striped table-bordered table-hover'
									    ),
									    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
										'data'=>$model,
										'attributes'=>array(
											'ComisionCodeSkema',
											'ComisionNameSkema',
											'Note',
											'Percentage',
									        array(
									            'name'=>'Status',
									            'value'=>EnumStatus::getLabel($model->Status),
									        ),
											//CDetailViewHelper::getCreatedBy($model),
											//CDetailViewHelper::getCreatedDate($model),
											//CDetailViewHelper::getModifiedBy($model),
											//CDetailViewHelper::getModifiedDate($model),
										),
									)); ?>
								</div>
						</div>
				</div>
		</div>
</div>
