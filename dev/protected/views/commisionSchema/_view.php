<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ComisionCodeSkema')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ComisionCodeSkema), array('view', 'id'=>$data->ComisionCodeSkema)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ComisionNameSkema')); ?>:</b>
	<?php echo CHtml::encode($data->ComisionNameSkema); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Note')); ?>:</b>
	<?php echo CHtml::encode($data->Note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />


</div>