<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'commision-schema-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'ComisionNameSkema'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'ComisionNameSkema',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'ComisionNameSkema'); ?>
                </div>
            </div>

		</div>
	</div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Percentage'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-5">
                    <?php echo $form->textField($model,'Percentage',array('class'=>'form-control')); ?>
                </div>
								<div class="col-xs-1">
                    %
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Percentage'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Note'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'Note',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Note'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Status'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'Status',
                        EnumStatus::getList()); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Status'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>        </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
