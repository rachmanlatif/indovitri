<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">View Shipper</span>
            </div>
            <div class="widget-body">
								<div class="row">
										<div class="col-sm-12 col-xs-12">
												<div id="status-message">
														<?php if(Yii::app()->user->hasFlash('success')): ?>
																<div class="alert alert-success">
																		<?php echo Yii::app()->user->getFlash('success') ?>
																</div>
														<?php endif ?>

														<?php if(Yii::app()->user->hasFlash('info')): ?>
																<div class="alert alert-info">
																		<?php echo Yii::app()->user->getFlash('info') ?>
																</div>
														<?php endif ?>

														<?php if(Yii::app()->user->hasFlash('danger')): ?>
																<div class="alert alert-danger">
																		<?php echo Yii::app()->user->getFlash('danger') ?>
																</div>
														<?php endif ?>
												</div>
										</div>
								</div>

                <div class="table-scrollable">
									<a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
                      <i class="btn-label glyphicon glyphicon-th-list"></i>List Shipper
                  </a>
									<a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-success">
                      <i class="btn-label glyphicon glyphicon-plus"></i>Add Shipper
                  </a>
									<a href="<?php echo $this->createUrl('update', array('id'=>$model->ShipperCode)); ?>" class="btn btn-labeled btn-warning">
                      <i class="btn-label glyphicon glyphicon-edit"></i>Edit Shipper
                  </a>
									<?php echo CHtml::linkButton('<i class="btn-label glyphicon glyphicon-trash"></i> Delete',
											array(
												'class' => 'form-button btn-labeled btn btn-danger',
												'submit' => array('delete','id'=>$model->ShipperCode),
												'confirm' => 'Are you sure you want to delete this item?',
											)
										);
									?>
									<hr>

									<?php $this->widget('zii.widgets.CDetailView', array(
									    'htmlOptions'=>array(
									        'class'=>'detail-view table table-striped table-bordered table-hover'
									    ),
									    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
										'data'=>$model,
										'attributes'=>array(
											'ShipperCode',
											'Reference',
											'Name',
											'Phone',
											'email',
											'Address',
											'PostCode',
											array(
													'name'=>'CityCode',
													'value'=>($model->city != null ? $model->city->CityName : ''),
											),
											array(
													'name'=>'BankCode',
													'value'=>($model->bank != null ? $model->bank->BankName : ''),
											),
									        'AccountNo',
									        'AccountName',
									        'Website',
									        array(
									            'name'=>'Status',
									            'value'=>EnumStatus::getLabel($model->Status),
									        ),
										),
									)); ?>
								</div>
						</div>
				</div>
		</div>
</div>

<div class="tabbable">
    <ul class="nav nav-tabs">
			<li class="active">
					<a data-toggle="tab" href="#tab1">
							<b>Detail</b>
					</a>
			</li>
        <li>
						<a data-toggle="tab" href="#tab2">
								<b>Contact Person</b>
						</a>
				</li>
    </ul>

    <div class="tab-content radius-bordered">
        <div id="tab1" class="tab-pane in active">
						<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover">
								    <tr>
								        <th>Country Code</th>
								        <th>Registration Number Name</th>
								        <th>Registration Number Value</th>
								        <th>Tax Code Name</th>
								        <th>Tax Code Value</th>
								    </tr>

								    <?php if($model->details != null){
								        foreach($model->details as $detail){ ?>
								            <tr>
																<td><?php echo ($detail->country != null ? $detail->country->CountryName : ''); ?></td>
								                <td><?php echo $detail->RegistrationNumberName; ?></td>
								                <td><?php echo $detail->RegistrationNumberValue; ?></td>
								                <td><?php echo $detail->TaxCodeName; ?></td>
								                <td><?php echo $detail->TaxCodeValue; ?></td>
								            </tr>
								        <?php }
								    } ?>
								</table>
						</div>
				</div>
				<div id="tab2" class="tab-pane">
						<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover">
										<tr>
												<th>Position Code</th>
												<th>Name</th>
												<th>Phone</th>
												<th>Address</th>
												<th>Email</th>
												<th>Note</th>
										</tr>

										<?php if($detailPerson != null){
												foreach($detailPerson as $detail){ ?>
														<tr>
																<td><?php echo ($detail->position != null ? $detail->position->PositionName : ''); ?></td>
																<td><?php echo $detail->PositionCode; ?></td>
																<td><?php echo $detail->Name; ?></td>
																<td><?php echo $detail->Phone; ?></td>
																<td><?php echo $detail->Address; ?></td>
																<td><?php echo $detail->Email; ?></td>
																<td><?php echo $detail->Note; ?></td>
														</tr>
												<?php }
										} ?>
								</table>
						</div>
				</div>
		</div>
</div>
