<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Create Shipper</span>
            </div>
            <div class="widget-body">
							<a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
									<i class="btn-label glyphicon glyphicon-th-list"></i>List Shipper
							</a>
							<hr>
							<?php echo $this->renderPartial('_form', array(
							    'model'=>$model,
									'detailPerson'=>$detailPerson,
							)); ?>
						</div>
				</div>
		</div>
</div>
