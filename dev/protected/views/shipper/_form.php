<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'shipper-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Name'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'Name',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Name'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Reference'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'Reference',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Reference'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Phone'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'Phone',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Phone'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'email'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'email'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Address'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'Address',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Address'); ?>
                </div>
            </div>

		</div>
	</div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'CityCode'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'CityCode',
                        CHtml::listData(City::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'CityCode', 'CityName'),
                        array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width:100%')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'CityCode'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'PostCode'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'PostCode',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'PostCode'); ?>
                </div>
            </div>

		</div>
	</div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'BankCode'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'BankCode',
                        CHtml::listData(BankList::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'BankCode', 'BankName'),
                        array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width:100%')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'BankCode'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'AccountNo'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'AccountNo',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'AccountNo'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'AccountName'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'AccountName',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'AccountName'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Website'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'Website',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Website'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Status'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->dropDownList($model,'Status',
                        EnumStatus::getList()); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Status'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>Country</th>
            <th>Registration Number Name</th>
            <th>Registration Value</th>
            <th>Tax Code Name</th>
            <th>Tax Code Value</th>
            <td>
                <button type="button" id="btn-detail" class="btn btn-sm btn-primary">Add</button>
                <span id="tree-loading"></span>
            </td>
        </tr>
        </thead>
        <tbody id="detailShip">
        <?php if($model->details != null){
            $index = 0;
            foreach($model->details as $detail){ ?>
                <tr>
                    <td>
                        <?php echo $form->hiddenField($detail,"[$index]IDShipperDetail"); ?>
                        <?php echo $form->dropDownList($detail,"[$index]CountryCode",
                            CHtml::listData(Country::model()->findAll(), 'CountryCode', 'CountryName'),
                            array('empty'=>'- Choose -', 'class'=>'form-control', 'required'=>true)); ?>
                    </td>
                    <td>
                        <?php echo $form->textField($detail,"[$index]RegistrationNumberName", array('class'=>'form-control', 'required'=>true)); ?>
                    </td>
                    <td>
                        <?php echo $form->textField($detail,"[$index]RegistrationNumberValue", array('class'=>'form-control', 'required'=>true)); ?>
                    </td>
                    <td>
                        <?php echo $form->textField($detail,"[$index]TaxCodeName", array('class'=>'form-control', 'required'=>true)); ?>
                    </td>
                    <td>
                        <?php echo $form->textField($detail,"[$index]TaxCodeValue", array('class'=>'form-control', 'required'=>true)); ?>
                    </td>
                    <td align="center">
                        <a href="<?php echo Yii::app()->baseUrl.'/shipper/deleteDetail/id/'.$detail->IDShipperDetail; ?>"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
                    </td>
                </tr>
                <?php $index++; }
        } ?>
        </tbody>
    </table>
	</div>

	<h3>Detail Person</h3>
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
				<thead>
				<tr>
						<th>Position</th>
						<th>Name</th>
						<th>Phone</th>
						<th>Address</th>
						<th>Email</th>
						<th>Note</th>
						<td>
								<button type="button" id="btn-person" class="btn btn-sm btn-primary">Add</button>
								<span id="tree-loading2"></span>
						</td>
				</tr>
				</thead>
				<tbody id="detailPerson">
				<?php if($detailPerson != null){
						$index = 0;
						foreach($detailPerson as $detail){ ?>
								<tr>
										<td>
												<?php echo $form->hiddenField($detail,"[$index]IDDetailPerson"); ?>
												<?php echo $form->dropDownList($detail,"[$index]PositionCode",
														CHtml::listData(Position::model()->findAll(), 'PositionCode', 'PositionName'),
														array('empty'=>'- Choose -', 'class'=>'form-control', 'required'=>true)); ?>
										</td>
										<td>
												<?php echo $form->textField($detail,"[$index]Name", array('class'=>'form-control', 'required'=>true)); ?>
										</td>
										<td>
												<?php echo $form->textField($detail,"[$index]Phone", array('class'=>'form-control', 'required'=>true)); ?>
										</td>
										<td>
												<?php echo $form->textField($detail,"[$index]Address", array('class'=>'form-control')); ?>
										</td>
										<td>
												<?php echo $form->textField($detail,"[$index]Email", array('class'=>'form-control')); ?>
										</td>
										<td>
												<?php echo $form->textField($detail,"[$index]Note", array('class'=>'form-control')); ?>
										</td>
										<td align="center">
												<a href="<?php echo Yii::app()->baseUrl.'/shipper/deletePerson/id/'.$detail->IDDetailPerson; ?>"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
										</td>
								</tr>
						<?php $index++; }
				} ?>
				</tbody>
		</table>
	</div>

    <hr>

    <div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
        </div>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    $('#btn-detail').click(function(){
        var index = $('#detailShip tr').length;

        $.ajax({
            type: 'post',
            url: '<?php echo $this->createUrl('addDetail'); ?>',
            dataType: 'json',
            data: {index: index},
            beforeSend: function(){
                $('#tree-loading').html('<i class="fa fa-rotate-right fa-spin">');
            },
            success: function(responseJSON) {
                if (responseJSON.success) {
                    $('#detailShip').append(responseJSON.content);
                } else {
                    alert(responseJSON.message);
                }
            },
            complete: function(){
                $('#tree-loading').html('');
            }
        });
    });

		$('#btn-person').click(function(){
				var index = $('#detailPerson tr').length;

				$.ajax({
						type: 'post',
						url: '<?php echo $this->createUrl('addPerson'); ?>',
						dataType: 'json',
						data: {index: index},
						beforeSend: function(){
								$('#tree-loading2').html('<i class="fa fa-rotate-right fa-spin">');
						},
						success: function(responseJSON) {
								if (responseJSON.success) {
										$('#detailPerson').append(responseJSON.content);
								} else {
										alert(responseJSON.message);
								}
						},
						complete: function(){
								$('#tree-loading2').html('');
						}
				});
		});
</script>
</div><!-- form -->
