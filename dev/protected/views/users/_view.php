<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('IDUser')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->IDUser), array('view', 'id'=>$data->IDUser)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Username')); ?>:</b>
	<?php echo CHtml::encode($data->Username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FirstName')); ?>:</b>
	<?php echo CHtml::encode($data->FirstName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MiddleName')); ?>:</b>
	<?php echo CHtml::encode($data->MiddleName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LastName')); ?>:</b>
	<?php echo CHtml::encode($data->LastName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PositionCode')); ?>:</b>
	<?php echo CHtml::encode($data->PositionCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Phone')); ?>:</b>
	<?php echo CHtml::encode($data->Phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Address')); ?>:</b>
	<?php echo CHtml::encode($data->Address); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('PostCode')); ?>:</b>
	<?php echo CHtml::encode($data->PostCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CityCode')); ?>:</b>
	<?php echo CHtml::encode($data->CityCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Password')); ?>:</b>
	<?php echo CHtml::encode($data->Password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LastLogin')); ?>:</b>
	<?php echo CHtml::encode($data->LastLogin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SessionKey')); ?>:</b>
	<?php echo CHtml::encode($data->SessionKey); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SessionExpired')); ?>:</b>
	<?php echo CHtml::encode($data->SessionExpired); ?>
	<br />

	*/ ?>

</div>
