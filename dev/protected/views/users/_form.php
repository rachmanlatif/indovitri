<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Username'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'Username',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'Username'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'FirstName'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'FirstName',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'FirstName'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'MiddleName'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'MiddleName',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'MiddleName'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'LastName'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'LastName',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'LastName'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'PositionCode'); ?></label>
			<div class="col-sm-10">
				<?php echo $form->dropDownList($model,'PositionCode',
						CHtml::listData(Position::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'PositionCode', 'PositionName'),
						array('empty'=>'- Choose -', 'class'=>'select2','style'=>'width:100%')); ?>
					<br>
					<?php echo $form->error($model,'PositionCode'); ?>
			</div>
			<br>
	</div>
	<br>
	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><br><?php echo $form->labelEx($model,'Phone'); ?></label>
			<div class="col-sm-10"><br>
					<?php echo $form->textField($model,'Phone',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'Phone'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'email'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'email'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Address'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'Address',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'Address'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right">Country</label>
			<div class="col-sm-2">
					<select class="form-control"  id="country">
						<?php if($get_country){
	                  foreach ($get_country as $rows) {
	                    echo '<option value="'.$rows['CountryCode'].'">'.$rows['CountryName'].'</option>';
	                  }
	                }
						?>
					</select>
			</div>
			<label for="inputEmail3" class="col-sm-1 control-label no-padding-right">State</label>
			<div class="col-sm-3" id="state">

			</div>

			<label for="inputEmail3" class="col-sm-1 control-label no-padding-right">City</label>
			<div class="col-sm-3" id="city">

			</div>

	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><br><?php echo $form->labelEx($model,'PostCode'); ?></label>
			<div class="col-sm-10"><br>
					<?php echo $form->textField($model,'PostCode',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'PostCode'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Password'); ?></label>
			<div class="col-sm-10">
					<?php echo $form->textField($model,'Password',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
					<br>
					<?php echo $form->error($model,'Password'); ?>
			</div>
	</div>

	<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->labelEx($model,'Status'); ?></label>
			<div class="col-sm-10">
				<?php echo $form->dropDownList($model,'Status',
						EnumStatus::getList()); ?>
					<br>
					<?php echo $form->error($model,'Status'); ?>
			</div>
	</div>


    <div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>        </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
country();
function country(){
	var countryID = $("#country option:selected").attr('value');
    $.post("<?php echo $this->createUrl('addState'); ?>", {countryID:countryID}, function(data){
			// console.log(data)
        $("#state").show();
        $("#state").html(data);
  			citys();
  			$("#state").change(citys);
    });
}

$('#country').on('change', function() {
  $.post("<?php echo $this->createUrl('addState'); ?>", {countryID:this.value}, function(data){
		$("#state").show();
		$("#state").html(data);
		citys();
		// $("#state").change(citys);
	})
});

function citys(){
	var stateID = $("#stateID option:selected").attr('value');
	var countryID = $("#country").val();
	$.post("<?php echo $this->createUrl('addCity'); ?>", {stateID:stateID, countryID:countryID}, function(data){
		// console.log(data)
			$("#city").show();
			$("#city").html(data);
	});
}
</script>
