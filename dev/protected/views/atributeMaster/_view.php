<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('AtributeCode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->AtributeCode), array('view', 'id'=>$data->AtributeCode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('AtributeName')); ?>:</b>
	<?php echo CHtml::encode($data->AtributeName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Note')); ?>:</b>
	<?php echo CHtml::encode($data->Note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />


</div>