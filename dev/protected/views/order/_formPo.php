<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'order-header-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'PORefCode'); ?>
        </div>
        <div class="col-xs-4">
            <div class="row">
                <div class="col-xs-12">
                    <?php echo $form->textField($model,'PORefCode', array('class'=>'form-control', 'readonly'=>true)); ?>
                    *Auto generate code<br>
                    <?php echo $form->error($model,'PORefCode'); ?>
                </div>
            </div>
        </div>
    </div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'PODate'); ?>
        </div>
		<div class="col-xs-4">
            <div class="row">
                <div class="col-xs-12">
                    <?php echo $form->textField($model,'PODate', array('class'=>'form-control date-picker','value'=>date('Y-m-d'))); ?>
                    <?php echo $form->error($model,'PODate'); ?>
                </div>
            </div>
		</div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'POStatusCode'); ?>
        </div>
        <div class="col-xs-4">
            <div class="row">
                <div class="col-xs-12">
                    <?php echo $form->dropDownList($model,'POStatusCode',
                        EnumStatus::getList()); ?>
                    <?php echo $form->error($model,'POStatusCode'); ?>
                </div>
            </div>
        </div>
    </div>

    <hr>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
        </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->