<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">View Order #<?php echo $model->OrderCode; ?></span>
            </div>
            <div class="widget-body">
              <div>
                  <!-- <h1 class="left">View Order #<?//php echo $model->OrderCode; ?></h1> -->
                  <div class="form-button-container">
                      <a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
                          <i class="btn-label glyphicon glyphicon-th-list"></i>List
                      </a>
                      <a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-success">
                          <i class="btn-label glyphicon glyphicon-plus"></i>Add
                      </a>
                      <?php if($model->OrderStatusCode == 2 && $step < 7){ ?>
                          <a href="<?php echo $this->createUrl('finish', array('id'=>$model->OrderCode)); ?>" class="btn btn-labeled btn-primary">
                              <i class="btn-label glyphicon glyphicon-th-list"></i>Finish Order
                          </a>
                          <a href="<?php echo $this->createUrl('delete', array('id'=>$model->OrderCode)); ?>" class="btn btn-labeled btn-danger pull-right">
                              <i class="btn-label glyphicon glyphicon-plus"></i>Close Order
                          </a>
                          <!-- <a class="form-button btn btn-success" href="<?//php echo $this->createUrl('finish', array('id'=>$model->OrderCode)); ?>">Finish Order</a>
                          <a class="form-button btn btn-danger pull-right" href="<?//php echo $this->createUrl('delete', array('id'=>$model->OrderCode)); ?>">Close Order</a> -->
                      <?php } ?>
                  </div>
              </div>
              <div class="clear"></div>
              <hr />

              <div class="row">
                  <div class="col-sm-12 col-xs-12">
                      <div id="status-message">
                          <?php if(Yii::app()->user->hasFlash('success')): ?>
                              <div class="alert alert-success">
                                  <?php echo Yii::app()->user->getFlash('success') ?>
                              </div>
                          <?php endif ?>

                          <?php if(Yii::app()->user->hasFlash('info')): ?>
                              <div class="alert alert-info">
                                  <?php echo Yii::app()->user->getFlash('info') ?>
                              </div>
                          <?php endif ?>

                          <?php if(Yii::app()->user->hasFlash('danger')): ?>
                              <div class="alert alert-danger">
                                  <?php echo Yii::app()->user->getFlash('danger') ?>
                              </div>
                          <?php endif ?>
                      </div>
                  </div>
              </div>

              <?php
              $s1 = '';
              $s2 = '';
              $s3 = '';
              $s4 = '';
              $s5 = '';
              $s6 = '';
              $s7 = '';
              if($step == 1){
                  $s1 = 'complete';
                  $s2 = 'active';
              }
              else if($step == 2){
                  $s1 = 'complete';
                  $s2 = 'complete';
                  $s3 = 'active';
              }
              else if($step == 3){
                  $s1 = 'complete';
                  $s2 = 'complete';
                  $s3 = 'complete';
                  $s4 = 'active';
              }
              else if($step == 4){
                  $s1 = 'complete';
                  $s2 = 'complete';
                  $s3 = 'complete';
                  $s4 = 'complete';
                  $s5 = 'active';
              }
              else if($step == 5){
                  $s1 = 'complete';
                  $s2 = 'complete';
                  $s3 = 'complete';
                  $s4 = 'complete';
                  $s5 = 'complete';
                  $s6 = 'active';
              }
              else if($step == 6){
                  $s1 = 'complete';
                  $s2 = 'complete';
                  $s3 = 'complete';
                  $s4 = 'complete';
                  $s5 = 'complete';
                  $s6 = 'complete';
                  $s7 = 'active';
              }
              else if($step == 7){
                  $s1 = 'complete';
                  $s2 = 'complete';
                  $s3 = 'complete';
                  $s4 = 'complete';
                  $s5 = 'complete';
                  $s6 = 'complete';
                  $s7 = 'complete';
              }
              ?>
              <div id="simplewizard" class="wizard" data-target="#simplewizard-steps">
                  <ul class="steps">
                      <li data-target="#simplewizardstep1" class="<?php echo $s1; ?>" style="font-size: 12px;"><span class="step">1</span>Create Order<span class="chevron"></span></li>
                      <li data-target="#simplewizardstep2" class="<?php echo $s2; ?>" style="font-size: 12px;"><span class="step">2</span>Choose Container<span class="chevron"></span></li>
                      <li data-target="#simplewizardstep3" class="<?php echo $s3; ?>" style="font-size: 12px;"><span class="step">3</span>PO<span class="chevron"></span></li>
                      <li data-target="#simplewizardstep4" class="<?php echo $s4; ?>" style="font-size: 12px;"><span class="step">4</span>Receive Bill<span class="chevron"></span></li>
                      <li data-target="#simplewizardstep5" class="<?php echo $s5; ?>" style="font-size: 12px;"><span class="step">5</span>PI<span class="chevron"></span></li>
                      <li data-target="#simplewizardstep6" class="<?php echo $s6; ?>" style="font-size: 12px;"><span class="step">6</span>Settlement<span class="chevron"></span></li>
                      <li data-target="#simplewizardstep7" class="<?php echo $s7; ?>" style="font-size: 12px;"><span class="step">7</span>Shipment<span class="chevron"></span></li>
                  </ul>
              </div>
              <div class="actions actions-footer" id="simplewizard-actions" style="margin-top: 10px;">
                  <div class="btn-group" style="padding: 10px;">
                      <button type="button" class="btn btn-danger btn-prev"> <i class="fa fa-angle-left"></i>Prev</button>
                      <button type="button" class="btn btn-success btn-next" data-last="Finish">Next<i class="fa fa-angle-right"></i></button>
                  </div>
              </div>
              <div class="step-content" id="simplewizard-steps">
              <div class="step-pane <?php echo $s1; ?>" id="simplewizardstep1">
                  <br>
                  <?php if($model->OrderStatusCode == 1){ ?>
                      <a class="form-button btn btn-warning" href="<?php echo $this->createUrl('update', array('id'=>$model->OrderCode)); ?>">Update</a>
                  <?php } ?>
                  <hr>
                  <?php $this->widget('zii.widgets.CDetailView', array(
                      'htmlOptions'=>array(
                          'class'=>'detail-view table table-striped table-bordered table-hover'
                      ),
                      'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
                      /*'pager' => array(
                        'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
                        // 'maxButtonCount'=>4,
                        'header' => '',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel'=>'First',
                        'lastPageLabel'=>'Last',
                        'htmlOptions'=>array('style' => 'float : left'),
                      ),*/
                      'data'=>$model,
                      'attributes'=>array(
                          'OrderCode',
                          'OrderDate',
                          array(
                              'name'=>'CustomerCode',
                              'value'=>($model->customer != null ? $model->customer->Name : ''),
                          ),
                          array(
                              'name'=>'SellerCode',
                              'value'=>($model->seller != null ? $model->seller->Name : ''),
                          ),
                          array(
                              'name'=>'ConsigneeCode',
                              'value'=>($model->consignee != null ? $model->consignee->Name : ''),
                          ),
                          array(
                              'name'=>'NotifyPartyCode',
                              'value'=>($model->notifyParty != null ? $model->notifyParty->Name : ''),
                          ),
                          array(
                              'name'=>'IncontermID',
                              'value'=>($model->incoterm != null ? $model->incoterm->IncotermName : ''),
                          ),
                          array(
                              'name'=>'AgentCodeOrigin',
                              'value'=>($model->agentOrigin != null ? $model->agentOrigin->Name : ''),
                          ),
                          array(
                              'name'=>'AgentCodeDestination',
                              'value'=>($model->agentDestination != null ? $model->agentDestination->Name : ''),
                          ),
                          array(
                              'name'=>'OrderStatusCode',
                              'value'=>EnumOrder::getLabel($model->OrderStatusCode),
                          ),
                          array(
                              'name'=>'POLCode',
                              'value'=>($model->pol != null ? $model->pol->CityName : ''),
                          ),
                          array(
                              'name'=>'PODCode',
                              'value'=>($model->pod != null ? $model->pod->CityName : ''),
                          ),
                          array(
                              'name'=>'OrderTotal',
                              'value'=>number_format($model->OrderTotal),
                          ),
                      ),
                  )); ?>
                  <hr>
                  <?php
                  $this->renderPartial('viewProduct', array(
                      'details'=>$model->detailsTemp,
                  ));
                  ?>
              </div>
              <div class="step-pane <?php echo $s2; ?>" id="simplewizardstep2">
                  <br>
                  <?php if($model->OrderStatusCode == 1){ ?>
                      <a class="form-button btn btn-warning" href="<?php echo $this->createUrl('addContainer', array('id'=>$model->OrderCode)); ?>">Update</a>
                  <?php } ?>
                  <hr>
                  <?php
                  $this->renderPartial('viewProduct', array(
                      'details'=>$model->details,
                  ));
                  ?>
              </div>
              <div class="step-pane <?php echo $s3; ?>" id="simplewizardstep3">
                  <br>
                  <?php
                  if($model->PORefCode === null and $model->OrderStatusCode == EnumOrder::ORDER){ ?>
                      <a class="form-button btn btn-primary" href="<?php echo $this->createUrl('createPo', array('id'=>$model->OrderCode)); ?>">Create PO</a>
                  <?php } ?>
                  <?php if($model->PORefCode != ''){ ?>
                      <!-- <a class="form-button btn btn-warning" href="<?php echo $this->createUrl('updatePo', array('id'=>$model->OrderCode)); ?>">Update PO</a> -->
                      <a class="form-button btn btn-success" target="_blank" href="<?php echo Yii::app()->baseUrl.'/download/popdf/id/'.$model->OrderCode; ?>">Download PO</a>
                  <?php } ?>
                  <hr>
                  <?php $this->widget('zii.widgets.CDetailView', array(
                      'htmlOptions'=>array(
                          'class'=>'detail-view table table-striped table-bordered table-hover'
                      ),
                      'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
                      'data'=>$model,
                      'attributes'=>array(
                          'OrderCode',
                          array(
                              'name'=>'OrderDate',
                              'value'=>date("Y-m-d", strtotime($model->OrderDate)),
                          ),
                          array(
                              'name'=>'CustomerCode',
                              'value'=>($model->customer != null ? $model->customer->Name : ''),
                          ),
                          array(
                              'name'=>'SellerCode',
                              'value'=>($model->seller != null ? $model->seller->Name : ''),
                          ),
                          array(
                              'name'=>'ConsigneeCode',
                              'value'=>($model->consignee != null ? $model->consignee->Name : ''),
                          ),
                          array(
                              'name'=>'NotifyPartyCode',
                              'value'=>($model->notifyParty != null ? $model->notifyParty->Name : ''),
                          ),
                          array(
                              'name'=>'IncontermID',
                              'value'=>($model->incoterm != null ? $model->incoterm->IncotermName : ''),
                          ),
                          array(
                              'name'=>'AgentCodeOrigin',
                              'value'=>($model->agentOrigin != null ? $model->agentOrigin->Name : ''),
                          ),
                          array(
                              'name'=>'AgentCodeDestination',
                              'value'=>($model->agentDestination != null ? $model->agentDestination->Name : ''),
                          ),
                          array(
                              'name'=>'OrderStatusCode',
                              'value'=>EnumOrder::getLabel($model->OrderStatusCode),
                          ),
                          array(
                              'name'=>'POLCode',
                              'value'=>($model->pol != null ? $model->pol->CityName : ''),
                          ),
                          array(
                              'name'=>'PODCode',
                              'value'=>($model->pod != null ? $model->pod->CityName : ''),
                          ),
                          'PORefCode',
                          array(
                              'name'=>'POStatusCode',
                              'value'=>EnumStatus::getLabel($model->POStatusCode),
                          ),
                          array(
                              'name'=>'PODate',
                              'value'=>date("Y-m-d", strtotime($model->PODate)),
                          ),
                          array(
                              'name'=>'OrderTotal',
                              'value'=>number_format($model->OrderTotal),
                          ),
                      ),
                  )); ?>
                  <hr>
                  <?php
                  $this->renderPartial('viewProduct', array(
                      'details'=>$model->details,
                  ));
                  ?>
              </div>
              <div class="step-pane <?php echo $s4; ?>" id="simplewizardstep4">
                  <br>
                  <?php if($model->PORefCode != ''){ ?>
                      <a class="form-button btn btn-primary" href="<?php echo $this->createUrl('receiveBill', array('id'=>$model->OrderCode)); ?>">Receive</a>
                  <?php } ?>
                  <hr>
                  <?php $this->widget('zii.widgets.CDetailView', array(
                      'htmlOptions'=>array(
                          'class'=>'detail-view table table-striped table-bordered table-hover'
                      ),
                      'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
                      'data'=>$model,
                      'attributes'=>array(
                          'PORefCode',
                          array(
                              'name'=>'POStatusCode',
                              'value'=>EnumStatus::getLabel($model->POStatusCode),
                          ),
                          array(
                              'name'=>'PODate',
                              'value'=>date("Y-m-d", strtotime($model->PODate)),
                          ),
                      ),
                  )); ?>
                  <hr>
                  <table class="table table-bordered table-striped table-hover">
                      <tr>
                          <th>Order Total</th>
                          <td><?php echo number_format($model->OrderTotal); ?></td>
                      </tr>
                      <tr>
                          <th>Seller Fee Amount (%)</th>
                          <td>
                              <?php
                              $diskon = ($model->OrderTotal * $model->SupplierFee)/100;
                              echo number_format($diskon).' ('.round($model->SupplierFee, 0).'%)';
                              ?>
                          </td>
                      </tr>
                      <tr>
                          <th colspan="2"></th>
                      </tr>
                      <tr>
                          <th>Summary Order</th>
                          <td>
                              <?php
                              $total = $model->OrderTotal - $diskon;
                              echo number_format($total);
                              ?>
                          </td>
                      </tr>
                  </table>

                  <h3>Payment Terms</h3>
                  <table class="table table-bordered table-striped table-hover">
                      <thead>
                      <tr>
                          <th>No</th>
                          <th>Percentage</th>
                          <th>Value</th>
                          <th>Issue Date</th>
                          <th>Expired Date</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php if($model->terms != null){
                          $index = 0;
                          foreach($model->terms as $term){ ?>
                              <tr>
                                  <td><?php echo $term->NumberTermPayment; ?></td>
                                  <td><?php echo round($term->PercentageofPayment, 0); ?></td>
                                  <td><?php echo round($term->ValuePercentageOfPayment, 0); ?></td>
                                  <td><?php echo date('Y-m-d', strtotime($term->IssueDate)); ?></td>
                                  <td><?php echo date('Y-m-d', strtotime($term->ExpiredDate)); ?></td>
                              </tr>
                              <?php $index++; }
                      } ?>
                      </tbody>
                  </table>
              </div>
              <div class="step-pane <?php echo $s5; ?>" id="simplewizardstep5">
                  <br>
                  <?php if($model->BillCodeSupplier == '' and $model->OrderStatusCode == EnumOrder::ORDER){ ?>
                      <a class="form-button btn btn-primary" href="<?php echo $this->createUrl('createPi', array('id'=>$model->OrderCode)); ?>">Create PI</a>
                  <?php } ?>

                  <?php if($model->PORefCode != '' and $model->BillCodeSupplier != ''){ ?>
                      <a class="form-button btn btn-warning" href="<?php echo $this->createUrl('updatePi', array('id'=>$model->OrderCode)); ?>">Update PI</a>
                      <a class="form-button btn btn-success" target="_blank" href="<?php echo Yii::app()->baseUrl.'/download/pipdf/id/'.$model->OrderCode; ?>">Download PI</a>
                      <a class="form-button btn btn-success" target="_blank" href="<?php echo Yii::app()->baseUrl.'/download/ocipdf/id/'.$model->OrderCode; ?>">Download Order Confirmation Images</a>
                      <a class="form-button btn btn-success" target="_blank" href="<?php echo Yii::app()->baseUrl.'/download/ocspdf/id/'.$model->OrderCode; ?>">Download Summary</a>
                  <?php } ?>
                  <hr>
                  <?php $this->widget('zii.widgets.CDetailView', array(
                      'htmlOptions'=>array(
                          'class'=>'detail-view table table-striped table-bordered table-hover'
                      ),
                      'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
                      'data'=>$model,
                      'attributes'=>array(
                          'BillCodeSupplier',
                          array(
                              'name'=>'BillSupplierDate',
                              'value'=>date("Y-m-d", strtotime($model->BillSupplierDate)),
                          ),
                          'BillSupplierTermofPayment',
                      ),
                  )); ?>
                  <hr>
                  <table class="table table-bordered table-striped table-hover">
                      <tr>
                          <th>Order Total</th>
                          <td><?php echo number_format($model->OrderTotal); ?></td>
                      </tr>
                      <tr>
                          <th>Buyer Fee</th>
                          <td><?php echo number_format($model->BuyerFeeAmount).' ('.$model->BuyerFee.'%)'; ?></td>
                      </tr>
                      <tr>
                          <th>Freight Fee</th>
                          <td><?php echo number_format($model->FreightFee); ?></td>
                      </tr>
                      <tr>
                          <th>Summary Charge</th>
                          <td><?php echo number_format($totalSummary); ?></td>
                      </tr>
                      <tr>
                          <th colspan="2"></th>
                      </tr>
                      <tr>
                          <th>Summary Order</th>
                          <td>
                              <?php
                              $total = ($model->OrderTotal - $model->BuyerFeeAmount) + $model->FreightFee + $totalSummary;
                              echo number_format($total);
                              ?>
                          </td>
                      </tr>
                  </table>

                  <h3>Charge Detail</h3>
                  <table class="table table-bordered table-hover table-striped">
                      <tr>
                          <th>Name</th>
                          <th>Value</th>
                      </tr>
                      <?php foreach($chargeDetail as $detail){ ?>
                          <tr>
                              <td><?php echo ($detail->master != null ? $detail->master->Name : ''); ?></td>
                              <td><?php echo round($detail->Value, 0); ?></td>
                          </tr>
                      <?php } ?>
                  </table>
              </div>
              <div class="step-pane <?php echo $s6; ?>" id="simplewizardstep6">
                  <br>
                  <a class="form-button btn btn-primary" href="<?php echo Yii::app()->baseUrl.'/sellerBillPayment/add/id/'.$model->OrderCode; ?>">Add</a>
                  <hr>
                  <table class="table table-bordered table-striped table-hover">
                      <tr>
                          <th>Order Total</th>
                          <td><?php echo number_format($model->OrderTotal); ?></td>
                      </tr>
                      <tr>
                          <th>Seller Fee %</th>
                          <td><?php echo number_format($model->SupplierFee); ?></td>
                      </tr>
                      <tr>
                          <th colspan="2"></th>
                      </tr>
                      <tr>
                          <th>Summary Order</th>
                          <td>
                              <?php
                              $diskon = ($model->OrderTotal * $model->SupplierFee)/100;
                              $total = $model->OrderTotal - $diskon;
                              echo number_format($total);
                              ?>
                          </td>
                      </tr>
                      <tr>
                          <th>Total Paid</th>
                          <td><?php echo number_format($totalPaid); ?></td>
                      </tr>
                      <tr>
                          <th>Remain Payment</th>
                          <td><?php echo number_format($total - $totalPaid); ?></td>
                      </tr>
                  </table>
                  <br>
                  <?php $this->widget('zii.widgets.grid.CGridView', array(
                      'id'=>'seller-bill-payment-grid',
                      'itemsCssClass'=>'table table-striped table-bordered table-hover',
                      'pager' => array(
                        'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
                        // 'maxButtonCount'=>4,
                        'header' => '',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel'=>'First',
                        'lastPageLabel'=>'Last',
                        'htmlOptions'=>array('style' => 'float : left'),
                      ),
                      'dataProvider'=>$sellerPayment->search(),
                      //'filter'=>$model,
                      'selectableRows'=>2,
                      'columns'=>array(
                          'PaymentCode',
                          'DatePayment',
                          'SellerComercial',
                          array(
                              'name'=>'Ammount',
                              'value'=>'number_format($data->Ammount)',
                          ),
                          array(
                              'name'=>'BankCode',
                              'value'=>'($data->bank != null ? $data->bank->BankName : "")',
                          ),
                          'Note',
                          array(
                              'class'=>'CButtonColumn',
                              'template'=>'{update} {delete}',
                              'header'=>'Action',
                              'htmlOptions' => array('class' => 'col-xs-2 text-center'),
                              'buttons'=>array(
                                  'update'=>array(
                                      'imageUrl'=>false,
                                      'label'=>'<i class="fa fa-edit fa-fw"></i>',
                                      'options'=>array(
                                          'class'=>'btn btn-warning btn-xs',
                                          'title'=>'Update',
                                      ),
                                      'url'=>'Yii::app()->createUrl("sellerBillPayment/update", array("id"=>$data->IDSellerBillPayment))',
                                  ),
                                  'delete'=>array(
                                      'imageUrl'=>false,
                                      'label'=>'<i class="fa fa-trash-o fa-fw"></i>',
                                      'options'=>array(
                                          'class'=>'btn btn-danger btn-xs',
                                          'title'=>'Delete',
                                      ),
                                      'url'=>'Yii::app()->createUrl("sellerBillPayment/delete", array("id"=>$data->IDSellerBillPayment))',
                                  ),
                              ),
                          ),
                      ),
                  )); ?>
              </div>
              <div class="step-pane <?php echo $s7; ?>" id="simplewizardstep7">
                  <br>
                  <a class="form-button btn btn-warning" href="<?php echo Yii::app()->baseUrl.'/orderContainer/update/id/'.$model->OrderCode; ?>">Update Container</a>
                  <a class="form-button btn btn-success" target="_blank" href="<?php echo Yii::app()->baseUrl.'/download/plpdf/id/'.$model->OrderCode; ?>">Download Shipment List</a>
                  <hr>
                  <table class="table table-bordered table-striped table-hover">
                      <thead>
                      <tr>
                          <th>Container</th>
                          <th>Container Number</th>
                          <th>Seal Code</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php foreach($model->blContainer as $bl){ ?>
                          <tr>
                              <td><?php echo $bl->container->ContainerName; ?></td>
                              <td><?php echo $bl->NumberContainer; ?></td>
                              <td><?php echo $bl->SealCode; ?></td>
                          </tr>
                      <?php } ?>
                      </tbody>
                  </table>
                  <br>
                  <a class="form-button btn btn-primary" href="<?php echo Yii::app()->baseUrl.'/orderBl/add/id/'.$model->OrderCode; ?>">Add</a>
                  <hr>
                  <?php $this->widget('zii.widgets.grid.CGridView', array(
                      'id'=>'order-bl-grid',
                      'itemsCssClass'=>'table table-striped table-bordered table-hover',
                      'pager' => array(
                        'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
                        // 'maxButtonCount'=>4,
                        'header' => '',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel'=>'First',
                        'lastPageLabel'=>'Last',
                        'htmlOptions'=>array('style' => 'float : left'),
                      ),
                      'dataProvider'=>$orderBl->search(),
                      //'filter'=>$model,
                      'selectableRows'=>2,
                      'columns'=>array(
                          'BLNumber',
                          array(
                              'name'=>'ContainerCode',
                              'value'=>'($data->container != null ? $data->container->ContainerName : "")',
                          ),
                          'DocStatus',
                          'CourierTrackNo',
                          array(
                              'name'=>'LoadingDate',
                              'value'=>'date("Y-m-d", strtotime($data->LoadingDate))',
                          ),
                          array(
                              'name'=>'EstimatedTimeDelivery',
                              'value'=>'date("Y-m-d", strtotime($data->EstimatedTimeDelivery))',
                          ),
                          array(
                              'name'=>'EstimatedTimeArive',
                              'value'=>'date("Y-m-d", strtotime($data->EstimatedTimeArive))',
                          ),
                          array(
                              'name'=>'BLReleasePayment',
                              'value'=>'date("Y-m-d", strtotime($data->BLReleasePayment))',
                          ),
                          array(
                              'class'=>'CButtonColumn',
                              'template'=>'{update} {delete}',
                              'header'=>'Action',
                              'htmlOptions' => array('class' => 'col-xs-2 text-center'),
                              'buttons'=>array(
                                  'update'=>array(
                                      'imageUrl'=>false,
                                      'label'=>'<i class="fa fa-edit fa-fw"></i>',
                                      'options'=>array(
                                          'class'=>'btn btn-warning btn-xs',
                                          'title'=>'Update',
                                      ),
                                      'url'=>'Yii::app()->createUrl("orderBl/update", array("id"=>$data->IDOrderBL))',
                                  ),
                                  'delete'=>array(
                                      'imageUrl'=>false,
                                      'label'=>'<i class="fa fa-trash-o fa-fw"></i>',
                                      'options'=>array(
                                          'class'=>'btn btn-danger btn-xs',
                                          'title'=>'Delete',
                                      ),
                                      'url'=>'Yii::app()->createUrl("oderBl/delete", array("id"=>$data->IDOrderBL))',
                                  ),
                              ),
                          ),
                      ),
                  )); ?>
              </div>
              </div>
            </div>
        </div>
    </div>
</div>
