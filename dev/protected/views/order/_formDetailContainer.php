<?php $form = new CActiveForm(); ?>

<tr>
    <td>
        <?php echo $container->ContainerName; ?>
    </td>
    <td>
        <?php echo $form->textField($detail, "RemainVolume", array('value'=>round($detail->container->UsableVolume, 0), 'class'=>'form-control', 'id'=>$detail->IDOrderBLContainer.'_Volume', 'readonly'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textField($detail, "RemainWeight", array('value'=>round($detail->container->NettWeight, 0), 'class'=>'form-control', 'id'=>$detail->IDOrderBLContainer.'_Weight', 'readonly'=>true)); ?>
    </td>
    <td>

    </td>
</tr>