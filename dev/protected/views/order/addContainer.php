
<div>
    <h1 class="left">Add Container #<?php echo $model->OrderCode; ?></h1>
    <div class="form-button-container">
        <a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
        <a class="form-button btn btn-primary" href="<?php echo $this->createUrl('view', array('id'=>$model->OrderCode)); ?>">View</a>
    </div>
</div>
<div class="clear"></div>
<hr />

<div id="simplewizard" class="wizard" data-target="#simplewizard-steps">
    <ul class="steps">
        <li data-target="#simplewizardstep1" class="complete" style="font-size: 12px;"><span class="step">1</span>Create Order<span class="chevron"></span></li>
        <li data-target="#simplewizardstep2" class="active" style="font-size: 12px;"><span class="step">2</span>Choose Container<span class="chevron"></span></li>
        <li data-target="#simplewizardstep3" class="" style="font-size: 12px;"><span class="step">3</span>PO<span class="chevron"></span></li>
        <li data-target="#simplewizardstep4" class="" style="font-size: 12px;"><span class="step">4</span>Receive Bill<span class="chevron"></span></li>
        <li data-target="#simplewizardstep5" class="" style="font-size: 12px;"><span class="step">5</span>PI<span class="chevron"></span></li>
        <li data-target="#simplewizardstep6" class="" style="font-size: 12px;"><span class="step">6</span>Settlement<span class="chevron"></span></li>
        <li data-target="#simplewizardstep6" class="" style="font-size: 12px;"><span class="step">7</span>Shipment<span class="chevron"></span></li>
    </ul>
</div>
<div class="step-content" id="simplewizard-steps">
    <div class="step-pane active" id="simplewizardstep2">
        <br>
        <?php echo $this->renderPartial('_formContainer', array(
            'model'=>$model,
            'detail'=>$detail,
            'listContainer'=>$listContainer,
            'seal'=>$seal,
        )); ?>
    </div>
</div>
