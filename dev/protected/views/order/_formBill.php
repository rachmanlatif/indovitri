<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'order-header-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-6">
            <div class="row form-row form-group">
                <div class="col-xs-4">
                    <?php echo $form->labelEx($model,'PORefCode'); ?>
                </div>
                <div class="col-xs-8">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php echo $form->textField($model,'PORefCode', array('class'=>'form-control', 'readonly'=>true)); ?>
                            *Auto generate code<br>
                            <?php echo $form->error($model,'PORefCode'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-row form-group">
                <div class="col-xs-4">
                    <?php echo $form->labelEx($model,'PODate'); ?>
                </div>
                <div class="col-xs-8">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php echo $form->textField($model,'PODate', array('class'=>'form-control date-picker','value'=>date('Y-m-d'))); ?>
                            <?php echo $form->error($model,'PODate'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-row form-group">
                <div class="col-xs-4">
                    <?php echo $form->labelEx($model,'POStatusCode'); ?>
                </div>
                <div class="col-xs-8">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php echo $form->dropDownList($model,'POStatusCode',
                                EnumStatus::getList()); ?>
                            <?php echo $form->error($model,'POStatusCode'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-row form-group">
                <div class="col-xs-4">
                    <?php echo $form->labelEx($model,'SupplierFee'); ?>
                </div>
                <div class="col-xs-8">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php echo $form->textField($model,'SupplierFee', array('class'=>'form-control')); ?>
                            <?php echo $form->error($model,'SupplierFee'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <table class="table table-bordered table-striped table-hover">
                <tr>
                    <th>Order Total</th>
                    <td>
												<?php echo $form->textField($model,'OrderTotal', array('class'=>'form-control', 'readonly'=>true, 'value'=>round($model->OrderTotal, 0))); ?>
										</td>
                </tr>
                <tr>
                    <th>Seller Fee %</th>
                    <td>
											<input type="text" class="form-control" id="SupplierFee" value="<?php echo $model->SupplierFee ?>" readonly>
										</td>
                </tr>
								<tr>
                    <th>Seller Fee Amount</th>
                    <td>
											<?php
											$diskon = ($model->OrderTotal*$model->SupplierFee)/100;
											?>
											<input type="text" class="form-control" id="SupplierFeeAmount" value="<?php echo $diskon ?>" readonly>
										</td>
                </tr>
                <tr>
                    <th colspan="2"></th>
                </tr>
                <tr>
                    <th>Summary Order</th>
                    <td>
                        <?php
                        $diskon = ($model->OrderTotal * $model->SupplierFee)/100;
                        $total = $model->OrderTotal - $diskon;
                        ?>
												<input type="text" class="form-control" id="SummaryOrder" value="<?php echo $total ?>" readonly>
                    </td>
                </tr>
            </table>
        </div>

        <div class="col-sm-12">
            <h3>Payment Terms</h3>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Percentage</th>
                    <th>Value</th>
                    <th>Issue Date</th>
                    <th>Expired Date</th>
                    <th>
                        <button type="button" class="btn btn-sm btn-primary" id="addDetail">Add Term</button>
                        <span id="tree-loading"></span>
                    </th>
                </tr>
                </thead>
                <tbody id="detailTerm">
                <?php if($model->terms != null){
                    $index = 0;
                    foreach($model->terms as $term){ ?>
                        <tr>
                            <td>
                                <?php echo $form->hiddenField($term, "[$index]IDOrderTermPayment"); ?>
                                <?php echo $form->hiddenField($term, "[$index]NumberTermPayment", array('value'=>$index+1)); ?>
                                <?php echo $index+1; ?>
                            </td>
                            <td>
                                <?php echo $form->textField($term, "[$index]PercentageofPayment", array('class'=>'form-control', 'value'=>round($term->PercentageofPayment))); ?>
                            </td>
                            <td>
                                <?php echo $form->textField($term, "[$index]ValuePercentageOfPayment", array('class'=>'form-control', 'value'=>round($term->ValuePercentageOfPayment))); ?>
                            </td>
                            <td>
                                <?php echo $form->textField($term, "[$index]IssueDate", array('class'=>'form-control date-picker', 'value'=>date('Y-m-d'))); ?>
                            </td>
                            <td>
                                <?php echo $form->textField($term, "[$index]ExpiredDate", array('class'=>'form-control date-picker')); ?>
                            </td>
                            <td align="center">
                                <a href="<?php echo Yii::app()->baseUrl.'/order/deleteTerm/id/'.$term->IDOrderTermPayment; ?>"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
                            </td>
                        </tr>
												<script type="text/javascript">
												$('#OrderTermPayment_<?php echo $index; ?>_PercentageofPayment').change(function(){
													var fee = $('#OrderHeader_SupplierFee').val();
													var total = $('#OrderHeader_OrderTotal').val();
													var percentage = $('#OrderTermPayment_<?php echo $index; ?>_PercentageofPayment').val();
													var value = $('#OrderTermPayment_<?php echo $index; ?>_ValuePercentageOfPayment').val();

													disc = (parseFloat(total) * parseFloat(percentage))/100;
													value = $('#OrderTermPayment_<?php echo $index; ?>_ValuePercentageOfPayment').val(disc);
												});
												</script>
                        <?php $index++; }
                } ?>
                </tbody>
								<tfoot>
										<tr>
												<td><b>Total</b></td>
												<td></td>
												<td>
														<input type="text" class="form-control" id="TotalAmount" value="0" readonly>
												</td>
												<td></td>
												<td></td>
												<td></td>
										</tr>
								</tfoot>
            </table>
        </div>

    </div>

    <hr>

	<div class="row buttons" style="margin-bottom: 200px;">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
        </div>
	</div>

<?php $this->endWidget(); ?>

    <script type="text/javascript">
				$('#OrderHeader_SupplierFee').change(function(){
						var fee = $('#OrderHeader_SupplierFee').val();
						var total = $('#OrderHeader_OrderTotal').val();

						discount = (parseFloat(total) * parseFloat(fee)) / 100;
						order = parseFloat(total) - parseFloat(discount);

						$('#SupplierFee').val(fee);
						$('#SupplierFeeAmount').val(discount);
						$('#SummaryOrder').val(order);
				});

        var index = <?php echo count($model->terms) ?>;

        $('#addDetail').click(function(){
            $.ajax({
                type: 'post',
                url: '<?php echo $this->createUrl('addDetailTerm'); ?>',
                dataType: 'json',
                data: {index: index},
                beforeSend: function(){
                    $('#tree-loading').html('<i class="fa fa-rotate-right fa-spin">');
                },
                success: function(responseJSON) {
                    if (responseJSON.success) {
                        $('#detailTerm').append(responseJSON.content);

                        index++;
                    } else {
                        alert(responseJSON.message);
                    }
                },
                complete: function(){
                    $('#tree-loading').html('');
                }
            });
        });
    </script>

</div><!-- form -->
