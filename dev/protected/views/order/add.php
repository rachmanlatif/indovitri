<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Create Order</span>
            </div>
            <div class="widget-body">
              <div>
                  <!-- <h1 class="left">Create Order</h1> -->
                  <div class="form-button-container">
                    <a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
                        <i class="btn-label glyphicon glyphicon-th-list"></i>List
                    </a>
                  </div>
              </div>
              <div class="clear"></div>
              <hr />

              <div id="simplewizard" class="wizard" data-target="#simplewizard-steps">
                  <ul class="steps">
                      <li data-target="#simplewizardstep1" class="" style="font-size: 12px;" style="border-bottom: none;"><span class="step">1</span>Create Order<span class="chevron"></span></li>
                      <li data-target="#simplewizardstep2" class="" style="font-size: 12px;"><span class="step">2</span>Choose Container<span class="chevron"></span></li>
                      <li data-target="#simplewizardstep3" class="" style="font-size: 12px;"><span class="step">3</span>PO<span class="chevron"></span></li>
                      <li data-target="#simplewizardstep4" class="" style="font-size: 12px;"><span class="step">4</span>Receive Bill<span class="chevron"></span></li>
                      <li data-target="#simplewizardstep5" class="" style="font-size: 12px;"><span class="step">5</span>PI<span class="chevron"></span></li>
                      <li data-target="#simplewizardstep6" class="" style="font-size: 12px;"><span class="step">6</span>Settlement<span class="chevron"></span></li>
                      <li data-target="#simplewizardstep6" class="" style="font-size: 12px;"><span class="step">7</span>Shipment<span class="chevron"></span></li>
                  </ul>
              </div>
              <div class="step-content" id="simplewizard-steps">
                  <div class="step-pane active" id="simplewizardstep1">
                      <br>
                      <?php echo $this->renderPartial('_form', array(
                          'model'=>$model,
                          'detail'=>$detail,
                          'container'=>$container,
                          'seal'=>$seal,
                      )); ?>
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
