
<?php

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('order-header-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Manage Orders</span>
            </div>
            <div class="widget-body">
              <div class="widget-main no-padding">
                <div class="table-scrollable">
                  <a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-blue">
                      <i class="btn-label glyphicon glyphicon-plus"></i>Add Order
                  </a>

                  <?php echo CHtml::link('<i class="btn-label glyphicon glyphicon-search"></i> Advanced Search','#',array('class'=>'search-button btn btn-labeled btn-darkorange pull-right')); ?>


                  <div class="search-form" style="display:none">
                    <br>
                    <?php $this->renderPartial('_search',array(
                    	'model'=>$model,
                    )); ?>
                  </div><!-- search-form -->

                  <div class="row">
                      <div class="col-sm-12 col-xs-12">
                          <div id="status-message">
                              <?php if(Yii::app()->user->hasFlash('success')): ?>
                                  <div class="alert alert-success">
                                      <?php echo Yii::app()->user->getFlash('success') ?>
                                  </div>
                              <?php endif ?>

                              <?php if(Yii::app()->user->hasFlash('info')): ?>
                                  <div class="alert alert-info">
                                      <?php echo Yii::app()->user->getFlash('info') ?>
                                  </div>
                              <?php endif ?>

                              <?php if(Yii::app()->user->hasFlash('danger')): ?>
                                  <div class="alert alert-danger">
                                      <?php echo Yii::app()->user->getFlash('danger') ?>
                                  </div>
                              <?php endif ?>
                          </div>
                      </div>
                  </div>

                  <!-- <div class="table-scrollable"> -->
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'order-header-grid',
                        'itemsCssClass'=>'table table-striped table-bordered table-hover',


                        'dataProvider'=>$model->search(),
                        'htmlOptions' => array('class' => 'grid-view rounded'),
                        'pager' => array(
                          'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
                          // 'maxButtonCount'=>4,
                          'header' => '',
                          'prevPageLabel' => 'Previous',
                          'nextPageLabel' => 'Next',
                          'firstPageLabel'=>'First',
                          'lastPageLabel'=>'Last',
                          'htmlOptions'=>array('style' => 'float : left'),
                        ),
                        //'filter'=>$model,
                        'selectableRows'=>2,
                        'columns'=>array(
                            // array(
                            //     'class'=>'CButtonColumn',
                            //     'template'=>'{view} {update} {delete}',
                            //     'header'=>'Action',
                            //     'htmlOptions' => array('class' => 'col-xs-3 text-center'),
                            //     'buttons'=>array(
                            //         'view'=>array(
                            //             'imageUrl'=>false,
                            //             'label'=>'<i class="fa fa-search fa-fw"></i>',
                            //             'options'=>array(
                            //                 'class'=>'btn btn-info btn-sm',
                            //                 'title'=>'View Detail',
                            //             ),
                            //         ),
                            //         'update'=>array(
                            //             'imageUrl'=>false,
                            //             'label'=>'<i class="fa fa-edit fa-fw"></i>',
                            //             'options'=>array(
                            //                 'class'=>'btn btn-warning btn-sm',
                            //                 'title'=>'Update',
                            //             ),
                            //         ),
                            //         'delete'=>array(
                            //             'imageUrl'=>false,
                            //             'label'=>'<i class="fa fa-trash-o fa-fw"></i>',
                            //             'options'=>array(
                            //                 'class'=>'btn btn-danger btn-sm',
                            //                 'title'=>'Delete',
                            //             ),
                            //         ),
                            //     ),
                            // ),
                            array(
                                'class'=>'CButtonColumn',
                                'template'=>'{view}',
                                'header'=>'View',
                                'buttons'=>array(
                                    'view'=>array(
                                        'imageUrl'=>false,
                                        'label'=>'<i class="fa fa-search fa-fw"></i>',
                                        'options'=>array(
                                            'class'=>'btn btn-info btn-sm',
                                            'title'=>'View Detail',
                                        ),
                                    ),
                                ),
                            ),
                            array(
                                'class'=>'CButtonColumn',
                                'template'=>'{update}',
                                'header'=>'Edit',
                                'buttons'=>array(
                                    'update'=>array(
                                        'imageUrl'=>false,
                                        'label'=>'<i class="fa fa-edit fa-fw"></i>',
                                        'options'=>array(
                                            'class'=>'btn btn-warning btn-sm',
                                            'title'=>'Update',
                                        ),
                                    ),
                                ),
                            ),
                            array(
                                'class'=>'CButtonColumn',
                                'template'=>'{delete}',
                                'header'=>'Delete',
                                'buttons'=>array(
                                    'delete'=>array(
                                        'imageUrl'=>false,
                                        'label'=>'<i class="fa fa-trash-o fa-fw"></i>',
                                        'options'=>array(
                                            'class'=>'btn btn-danger btn-sm',
                                            'title'=>'Delete',
                                        ),
                                    ),
                                ),
                            ),
                            array(
                                'name'=>'OrderStatusCode',
                                'value'=>'EnumOrder::getLabel($data->OrderStatusCode)',
                            ),
                            array(
                                'name'=>'OrderDate',
                                'value'=>'date("Y-m-d", strtotime($data->OrderDate))',
                                'type'=>'raw',
                            ),
                            array(
                                'name'=>'CustomerCode',
                                'header'=>'Customer Name',
                                'value'=>'($data->customer != null ? $data->customer->Name : "")',
                                'headerHtmlOptions' => array('style'=>'width:250px;'),

                            ),
                            array(
                                'name'=>'SellerCode',
                                'header'=>'Seller Name',
                                'value'=>'($data->seller != null ? $data->seller->Name : "")',
                                'headerHtmlOptions' => array('style'=>'width:250px;'),
                            ),
                            array(
                                'name'=>'PolCode',
                                'header' => 'City Name Loading',
                                'value'=>'($data->pol != null ? $data->pol->CityName : "")',
                                'headerHtmlOptions' => array('style'=>'width:250px;'),
                            ),
                            array(
                                'name'=>'PodCode',
                                'header' => 'City Name Destination',
                                'value'=>'($data->pod != null ? $data->pod->CityName : "")',
                                'headerHtmlOptions' => array('style'=>'width:250px;'),
                            ),
                            array(
                                'name'=>'ConsigneeCode',
                                'header' => 'Consignee Name',
                                'value'=>'($data->consignee != null ? $data->consignee->Name : "")',
                                'headerHtmlOptions' => array('style'=>'width:250px;'),
                            ),
                            array(
                              'name'=>'PORefCode',
                              'headerHtmlOptions' => array('style'=>'width:250px;'),
                            ),
                            // 'PORefCode',
                            array(
                                'name'=>'PODate',
                                'value'=>'($data->PODate > "1980-01-01" ? date("Y-m-d", strtotime($data->PODate)) : "")',
                            ),
                            'BillCodeSupplier',
                            array(
                                'name'=>'BillSupplierDate',
                                'value'=>'($data->PODate > "1980-01-01" ? date("Y-m-d", strtotime($data->BillSupplierDate)) : "")',
                            ),
                            array(
                                'name'=>'OrderTotal',
                                'value'=>'number_format($data->OrderTotal, 2)',
                            ),
                            array(
                                'name'=>'BuyerFee',
                                'value'=>'number_format($data->BuyerFee, 2)',
                            ),
                            array(
                                'name'=>'BuyerFeeAmount',
                                'value'=>'number_format($data->BuyerFeeAmount, 2)',
                            ),
                            array(
                                'name'=>'SupplierFee',
                                'value'=>'number_format($data->SupplierFee, 2)',
                            ),
                            array(
                                'name'=>'AgentCodeOrigin',
                                'value'=>'($data->agentOrigin != null ? $data->agentOrigin->Name : "")',
                            ),
                            array(
                                'name'=>'IncotermID',
                                'header'=> 'Incoterm Name',
                                'value'=>'($data->incoterm != null ? $data->incoterm->IncotermName : "")',
                            ),
                            array(
                                'name'=>'NotifyPartyCode',
                                'value'=>'($data->notifyParty != null ? $data->notifyParty->Name : "")',
                            ),
                        ),
                    ));
                    ?>
                  <!-- </div> -->
                </div>
              </div>

            </div>
        </div>
    </div>
</div>
