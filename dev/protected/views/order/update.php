<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Update Order #<?php echo $model->OrderCode; ?></span>
            </div>
            <div class="widget-body">
							<div>
								<!-- <h1 class="left">Update Order #<?//php echo $model->OrderCode; ?></h1> -->
								<div class="form-button-container">
									<a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-labeled btn-blue">
											<i class="btn-label glyphicon glyphicon-th-list"></i>List
									</a>
									<a href="<?php echo $this->createUrl('add'); ?>" class="btn btn-labeled btn-success">
											<i class="btn-label glyphicon glyphicon-plus"></i>Add
									</a>
									<a href="<?php echo $this->createUrl('view', array('id'=>$model->OrderCode)); ?>" class="btn btn-labeled btn-warning">
											<i class="btn-label glyphicon glyphicon-search"></i>View
									</a>

								</div>
							</div>
							<div class="clear"></div>
							<hr />

							<div class="row">
							    <div class="col-sm-12 col-xs-12">
							        <div id="status-message">
							            <?php if(Yii::app()->user->hasFlash('success')): ?>
							                <div class="alert alert-success">
							                    <?php echo Yii::app()->user->getFlash('success') ?>
							                </div>
							            <?php endif ?>

							            <?php if(Yii::app()->user->hasFlash('info')): ?>
							                <div class="alert alert-info">
							                    <?php echo Yii::app()->user->getFlash('info') ?>
							                </div>
							            <?php endif ?>

							            <?php if(Yii::app()->user->hasFlash('danger')): ?>
							                <div class="alert alert-danger">
							                    <?php echo Yii::app()->user->getFlash('danger') ?>
							                </div>
							            <?php endif ?>
							        </div>
							    </div>
							</div>

							<?php echo $this->renderPartial('_form', array(
							    'model'=>$model,
							    'seal'=>$seal,
							    'container'=>$container,
							)); ?>
						</div>
				</div>
		</div>
</div>
