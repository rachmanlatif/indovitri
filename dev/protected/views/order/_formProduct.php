<?php $form = new CActiveForm(); ?>

<tr>
    <td><?php echo $index+1; ?></td>
    <td>
        <?php echo $form->hiddenField($detail, "[$index]IDOrderDetailTemp"); ?>
        <?php echo $form->hiddenField($detail, "[$index]ProductID"); ?>
        <img height="50" src="<?php echo Product::getFileUrl().$detail->product->ThubnailsImage; ?>" />
    </td>
    <td>
        <?php echo CHtml::encode($detail->product->ProductName) ?>
    </td>
    <td>
        <?php echo CHtml::encode($detail->product->ProductID) ?>
    </td>
    <td>
        <?php echo number_format($detail->product->Price, 2); ?>
    </td>
    <td>
        <?php
        $uom = '';
        if($detail->product->uom !== null){
            $uom = $detail->product->uom->UOMName;
        }

        echo $uom;
        ?>
    </td>
    <td>
        <?php
        $small = 0;
        if($detail->product->uom->define !== null){
            $small = $detail->product->uom->define->QTYUOMSmall;
        }

        echo number_format($small);
        ?>
        <?php echo $form->hiddenField($detail, "[$index]MasterUnit", array('value'=>$small, 'readonly'=>true)); ?>
    </td>
    <td>
        <input type="hidden" id="QtyOld_<?php echo $index; ?>" value="<?php echo $detail->QTY; ?>" readonly>
        <?php echo $form->textField($detail, "[$index]QTY", array('class'=>'form-control')); ?>
    </td>
    <?php if($action == 'update'){ ?>
        <td></td>
    <?php } ?>
    <td>
        <?php echo number_format($detail->product->StokQTY); ?>
    </td>
    <td>
        <?php $totalUnit = $detail->QTY * $small; ?>
        <?php echo $form->textField($detail, "[$index]QtyUnit", array('class'=>'form-control', 'value'=>$totalUnit,'readonly'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textField($detail, "[$index]TotalOrder", array('class'=>'form-control', 'readonly'=>true)); ?>
    </td>
    <td>
        <?php echo number_format($detail->product->GrossWeight, 2); ?>
    </td>
    <td>
        <?php echo $form->textField($detail, "[$index]TotalGrossWeight", array('class'=>'form-control', 'readonly'=>true)); ?>
    </td>
    <td>
        <?php echo number_format($detail->product->NettWeight, 2); ?>
    </td>
    <td>
        <?php echo $form->textField($detail, "[$index]TotalNettWeight", array('class'=>'form-control', 'readonly'=>true)); ?>
    </td>
    <td>
        <?php echo number_format($detail->product->VolumeGross, 2); ?>
    </td>
    <td>
        <?php echo $form->textField($detail, "[$index]TotalVolume", array('class'=>'form-control', 'readonly'=>true)); ?>
    </td>
    <td style="vertical-align: middle;">
        <?php echo CHtml::link('Delete', 'javascript:void(0)',
            array('class'=>'btn btn-danger btn-sm delete-'.$index)) ?>
    </td>
</tr>

<script type="text/javascript">
    $("#OrderDetailTemp_<?php echo $index; ?>_QTY").change(function(){
        var product = $("#OrderDetailTemp_<?php echo $index; ?>_ProductID").val();
        var qty = $("#OrderDetailTemp_<?php echo $index; ?>_QTY").val();
        var qtyOld = $("#QtyOld_<?php echo $index; ?>").val();

        $.ajax({
            type: 'post',
            url: '<?php echo $this->createUrl('updateOrder'); ?>',
            dataType: 'json',
            data: {product: product, qty: qty, qtyOld: qtyOld},
            beforeSend: function(){
                $('#tree-loading').html('<i class="fa fa-rotate-right fa-spin">');
            },
            success: function(responseJSON) {
                if (responseJSON.success) {
                    $("#QtyOld_<?php echo $index; ?>").val(responseJSON.qty);

                    $("#OrderDetailTemp_<?php echo $index; ?>_TotalVolume").val(responseJSON.totalVolume);
                    $("#OrderDetailTemp_<?php echo $index; ?>_TotalGrossWeight").val(responseJSON.totalGrossWeight);
                    $("#OrderDetailTemp_<?php echo $index; ?>_TotalNettWeight").val(responseJSON.totalNettWeight);
                    $("#OrderDetailTemp_<?php echo $index; ?>_TotalOrder").val(responseJSON.totalOrder);
                    $("#OrderDetailTemp_<?php echo $index; ?>_TotalUnit").val(responseJSON.totalUnit);
                    $("#OrderDetailTemp_<?php echo $index; ?>_QtyUnit").val(responseJSON.qtyUnit);

                    var v = $('#OrderHeader_TotalVolume').val();
                    var p = $('#OrderHeader_OrderTotal').val();
                    var w = $('#OrderHeader_TotalGrossWeight').val();
                    var n = $('#OrderHeader_TotalNettWeight').val();
                    var q = $('#OrderHeader_TotalQty').val();
                    var u = $('#OrderHeader_TotalUnit').val();

                    totalV = (parseFloat(v) - parseFloat(responseJSON.totalVolumeOld)) + parseFloat(responseJSON.totalVolume);
                    totalP = (parseFloat(p) - parseFloat(responseJSON.totalOrderOld)) + parseFloat(responseJSON.totalOrder);
                    totalW = (parseFloat(w) - parseFloat(responseJSON.totalGrossWeightOld)) + parseFloat(responseJSON.totalGrossWeight);
                    totalN = (parseFloat(n) - parseFloat(responseJSON.totalNettWeightOld)) + parseFloat(responseJSON.totalNettWeight);
                    totalQ = (parseFloat(q) - parseFloat(responseJSON.qtyOld)) + parseFloat(responseJSON.qty);
                    totalU = (parseFloat(u) - parseFloat(responseJSON.qtyUnitOld)) + parseFloat(responseJSON.qtyUnit);

                    $('#OrderHeader_OrderTotal').val(totalP);
                    $('#OrderHeader_TotalVolume').val(totalV);
                    $('#OrderHeader_TotalGrossWeight').val(totalW);
                    $('#OrderHeader_TotalNettWeight').val(totalN);
                    $('#OrderHeader_TotalQty').val(totalQ);
                    $('#OrderHeader_TotalUnit').val(totalU);
                } else {
                    $("#OrderDetailTemp_<?php echo $index; ?>_QTY").val(responseJSON.qty);
                    alert(responseJSON.message);
                }
            },
            complete: function(){
                $('#tree-loading').html('');
            }
        });
    });

    $('.delete-<?php echo $index; ?>').click(function() {
        var vv = $("#OrderDetailTemp_<?php echo $index; ?>_TotalVolume").val();
        var ww = $("#OrderDetailTemp_<?php echo $index; ?>_TotalGrossWeight").val();
        var nn = $("#OrderDetailTemp_<?php echo $index; ?>_TotalNettWeight").val();
        var pp = $("#OrderDetailTemp_<?php echo $index; ?>_TotalOrder").val();
        var qq = $("#QtyOld_<?php echo $index; ?>").val();
        var uu = $("#OrderDetailTemp_<?php echo $index; ?>_QtyUnit").val();


        var v = $('#OrderHeader_TotalVolume').val();
        var w = $('#OrderHeader_TotalGrossWeight').val();
        var n = $('#OrderHeader_TotalNettWeight').val();
        var p = $('#OrderHeader_OrderTotal').val();
        var q = $('#OrderHeader_TotalQty').val();
        var u = $('#OrderHeader_TotalUnit').val();

        totalV = parseFloat(v) - parseFloat(vv);
        totalW = parseFloat(w) - parseFloat(ww);
        totalN = parseFloat(n) - parseFloat(nn);
        totalP = parseFloat(p) - parseFloat(pp);
        totalQ = parseFloat(q) - parseFloat(qq);
        totalU = parseFloat(u) - parseFloat(uu);

        $('#OrderHeader_TotalVolume').val(totalV);
        $('#OrderHeader_TotalGrossWeight').val(totalW);
        $('#OrderHeader_TotalNettWeight').val(totalN);
        $('#OrderHeader_OrderTotal').val(totalP);
        $('#OrderHeader_TotalQty').val(totalQ);
        $('#OrderHeader_TotalUnit').val(totalU);

        $(this).parent().parent().remove();
    });
</script>
