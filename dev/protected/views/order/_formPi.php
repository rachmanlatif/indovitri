<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'order-header-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'BillCodeSupplier'); ?>
        </div>
        <div class="col-xs-4">
            <div class="row">
                <div class="col-xs-12">
                    <?php echo $form->textField($model,'BillCodeSupplier', array('class'=>'form-control', 'readonly'=>true)); ?>
                    *Auto generate code<br>
                    <?php echo $form->error($model,'BillCodeSupplier'); ?>
                </div>
            </div>
        </div>
    </div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'BillSupplierDate'); ?>
        </div>
		<div class="col-xs-4">
            <div class="row">
                <div class="col-xs-12">
                    <?php echo $form->textField($model,'BillSupplierDate', array('class'=>'form-control date-picker','value'=>date('Y-m-d'))); ?>
                    <?php echo $form->error($model,'BillSupplierDate'); ?>
                </div>
            </div>
		</div>
    </div>

		<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'OrderTotal'); ?>
        </div>
        <div class="col-xs-4">
            <div class="row">
                <div class="col-xs-12">
                    <?php echo $form->textField($model,'OrderTotal', array('class'=>'form-control', 'readonly'=>true)); ?>
                    <?php echo $form->error($model,'OrderTotal'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'BuyerFee'); ?>
        </div>
        <div class="col-xs-4">
            <div class="row">
                <div class="col-xs-12">
										<?php echo CHtml::dropDownList("OrderHeader[BuyerFee]", $model->BuyerFee,
												$listCommision,
												array('class'=>'form-control', 'empty'=>'- Choose -')); ?>
										*Lists from customer comission scheme
                    <?php echo $form->error($model,'BuyerFee'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'BuyerFeeAmount'); ?>
        </div>
        <div class="col-xs-4">
            <div class="row">
                <div class="col-xs-12">
                    <?php echo $form->textField($model,'BuyerFeeAmount', array('class'=>'form-control', 'readonly'=>true)); ?>
                    <?php echo $form->error($model,'BuyerFeeAmount'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'FreightFee'); ?>
        </div>
        <div class="col-xs-4">
            <div class="row">
                <div class="col-xs-12">
                    <?php echo $form->textField($model,'FreightFee', array('class'=>'form-control')); ?>
                    <?php echo $form->error($model,'FreightFee'); ?>
                </div>
            </div>
        </div>
    </div>

    <h3>Other Charge</h3>

    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th>No</th>
            <th>Charge Name</th>
            <th>Value</th>
						<th>
								<button type="button" id="addCharge" class="btn btn-primary btn-sm">Add</button>
								<span id="tree-loading"></span>
						</th>
        </tr>
        </thead>
        <tbody id="detailCharge">
        <?php
        $index = 0;
        foreach($model->charges as $charge){
						$value = round($charge->Value, 0);
					?>
            <tr>
								<td><?php echo $index+1; ?></td>
                <td>
										<?php echo $form->hiddenField($charge,"[$index]IDOrderChargeDetail"); ?>
										<?php echo $form->dropDownList($charge,"[$index]ChargeID",
												CHtml::listData(OrderChargeMaster::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'ID', 'Name'),
												array('empty'=>'- Choose -', 'class'=>'form-control', 'required'=>true)); ?>
                </td>
                <td>
										<?php echo $form->textField($charge,"[$index]Value", array('class'=>'form-control', 'value'=>$value, 'required'=>true)); ?>
								</td>
                <td>
										<a href="<?php echo Yii::app()->baseUrl.'/order/deleteCharge/id/'.$charge->IDOrderChargeDetail; ?>"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
								</td>
            </tr>
            <?php $index++; } ?>
        </tbody>
    </table>

    <hr>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
        </div>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
		$('#OrderHeader_BuyerFee').change(function(){
				var total = $('#OrderHeader_OrderTotal').val();
				var persen = $('#OrderHeader_BuyerFee').val();

				sum = (parseFloat(total) * parseFloat(persen)) / 100;
				$('#OrderHeader_BuyerFeeAmount').val(sum);
		});

    $('#addCharge').click(function(){
        var index = $('#detailCharge tr').length;

        $.ajax({
            type: 'post',
            url: '<?php echo $this->createUrl('addCharge'); ?>',
            dataType: 'json',
            data: {index: index},
            beforeSend: function(){
                $('#tree-loading').html('<i class="fa fa-rotate-right fa-spin">');
            },
            success: function(responseJSON) {
                if (responseJSON.success) {
                    $('#detailCharge').append(responseJSON.content);
                } else {
                    alert(responseJSON.message);
                }
            },
            complete: function(){
                $('#tree-loading').html('');
            }
        });
    });
</script>
</div><!-- form -->
