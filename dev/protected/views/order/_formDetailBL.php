<?php $form = new CActiveForm(); ?>

<tr>
    <td>
        <?php echo $form->hiddenField($detail, "[$index]IDOrderDetailTemp"); ?>
        <div class="col-xs-4">
            <img height="50" src="<?php echo Product::getFileUrl().$detail->product->ThubnailsImage; ?>" />
        </div>
        <div class="col-xs-8">
            <?php echo $form->hiddenField($detail, "[$index]ProductID"); ?>
            <b><?php echo CHtml::encode($detail->product->ProductName) ?></b>
            <br>
            Price: <?php echo number_format($detail->product->Price); ?>
            <br>
            Volume Gross: <?php echo number_format($detail->product->VolumeGross); ?>
        </div>
    </td>
    <td>
        <input type="hidden" id="QtyOld_<?php echo $index; ?>" value="<?php echo $detail->QTY; ?>" readonly>
        <?php echo $form->textField($detail, "[$index]QTY", array('class'=>'form-control')); ?>
    </td>
    <td>
        <?php echo $form->textArea($detail, "[$index]Note"); ?>
    </td>
    <td>
        <?php echo $form->textField($detail, "[$index]TotalOrder", array('class'=>'form-control', 'readonly'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textField($detail, "[$index]TotalVolume", array('class'=>'form-control', 'readonly'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textField($detail, "[$index]TotalWeight", array('class'=>'form-control', 'readonly'=>true)); ?>
    </td>
    <td>
        <input type="hidden" id="ContainerOld_<?php echo $index; ?>" readonly>
        <input type="hidden" id="BLOld_<?php echo $index; ?>" readonly>
        <?php if($listContainer != null){ ?>
            <?php echo CHtml::dropDownList("OrderDetailTemp[$index][BLContainer]", '',
                $listContainer,
                array('class'=>'form-control', 'empty'=>'- Choose -')) ?>
        <?php } else { ?>
            <?php echo 'Add container above'; ?>
        <?php } ?>
        <span id="tree-loading3"></span>
    </td>
    <td>
        <?php echo CHtml::link('Delete', 'javascript:void(0)',
            array('class'=>'btn btn-danger btn-sm delete-'.$index)) ?>
    </td>
</tr>

<script type="text/javascript">
    $("#OrderDetailTemp_<?php echo $index; ?>_QTY").change(function(){
        var product = $("#OrderDetailTemp_<?php echo $index; ?>_ProductID").val();
        var qty = $("#OrderDetailTemp_<?php echo $index; ?>_QTY").val();
        var container = $("#OrderDetailTemp_<?php echo $index; ?>_BLContainer option:selected").val();
        var qtyOld = $("#QtyOld_<?php echo $index; ?>").val();
        var containerOld = $("#ContainerOld_<?php echo $index; ?>").val();
        var totalVolume = $("#OrderDetailTemp_<?php echo $index; ?>_TotalVolume").val();
        var totalOrder = $("#OrderDetailTemp_<?php echo $index; ?>_TotalOrder").val();

        if(container == ''){
            alert('Please choose container');
            $("#OrderDetailTemp_<?php echo $index; ?>_QTY").val(qtyOld);
            return false;
        }
        else{
            $.ajax({
                type: 'post',
                url: '<?php echo $this->createUrl('checkOrder'); ?>',
                dataType: 'json',
                data: {product: product, qty: qty, container: container, qtyOld: qtyOld, containerOld: containerOld},
                beforeSend: function(){
                    $('#tree-loading').html('<i class="fa fa-rotate-right fa-spin">');
                },
                success: function(responseJSON) {
                    if (responseJSON.success) {
                        $("#Remains_<?php echo $index; ?>").html(responseJSON.remains);
                        $("#OrderDetailTemp_<?php echo $index; ?>_TotalVolume").val(responseJSON.totalVolume);
                        $("#OrderDetailTemp_<?php echo $index; ?>_TotalOrder").val(responseJSON.totalOrder);

                        var v = $('#OrderHeader_TotalVolume').val();
                        var p = $('#OrderHeader_OrderTotal').val();

                        totalV = (parseFloat(v) - parseFloat(totalVolume)) + parseFloat(responseJSON.totalVolume);
                        totalP = (parseFloat(p) - parseFloat(totalOrder)) + parseFloat(responseJSON.totalOrder);
                        $('#OrderHeader_OrderTotal').val(totalP);
                        $('#OrderHeader_TotalVolume').val(totalV);
                    } else {
                        $("#OrderDetailTemp_<?php echo $index; ?>_QTY").val(responseJSON.qty);
                        $("#OrderDetailTemp_<?php echo $index; ?>_ContainerID").val(responseJSON.container);
                        alert(responseJSON.message);
                    }
                },
                complete: function(){
                    $('#tree-loading').html('');
                }
            });
        }
    });

    $("#OrderDetailTemp_<?php echo $index; ?>_BLContainer").change(function(){
        var product = $("#OrderDetailTemp_<?php echo $index; ?>_ProductID").val();
        var qty = $("#OrderDetailTemp_<?php echo $index; ?>_QTY").val();
        var container = $("#OrderDetailTemp_<?php echo $index; ?>_BLContainer option:selected").val();
        var qtyOld = $("#QtyOld_<?php echo $index; ?>").val();
        var containerOld = $("#ContainerOld_<?php echo $index; ?>").val();
        var BLOld = $("#BLOld_<?php echo $index; ?>").val();
        var totalVolume = $("#OrderDetailTemp_<?php echo $index; ?>_TotalVolume").val();
        var totalOrder = $("#OrderDetailTemp_<?php echo $index; ?>_TotalOrder").val();

        if(container != ''){
            $.ajax({
                type: 'post',
                url: '<?php echo $this->createUrl('checkOrder'); ?>',
                dataType: 'json',
                data: {product: product, qty: qty, container: container, qtyOld: qtyOld, containerOld: containerOld},
                beforeSend: function(){
                    $('#tree-loading').html('<i class="fa fa-rotate-right fa-spin">');
                },
                success: function(responseJSON) {
                    if (responseJSON.success) {
                        $("#Remains_<?php echo $index; ?>").html(responseJSON.remains);
                        $("#OrderDetailTemp_<?php echo $index; ?>_TotalVolume").val(responseJSON.totalVolume);
                        $("#OrderDetailTemp_<?php echo $index; ?>_TotalOrder").val(responseJSON.totalOrder);

                        var v = $('#OrderHeader_TotalVolume').val();
                        var p = $('#OrderHeader_OrderTotal').val();

                        totalV = (parseFloat(v) - parseFloat(totalVolume)) + parseFloat(responseJSON.totalVolume);
                        totalP = (parseFloat(p) - parseFloat(totalOrder)) + parseFloat(responseJSON.totalOrder);
                        $('#OrderHeader_OrderTotal').val(totalP);
                        $('#OrderHeader_TotalVolume').val(totalV);

                        var tVolume = $("#"+BLOld+"_Volume").val();
                        var tWeight = $("#"+BLOld+"_Weight").val();
                        var tVol = $("#OrderDetailTemp_<?php echo $index; ?>_TotalVolume").val();
                        var tWei = $("#OrderDetailTemp_<?php echo $index; ?>_TotalWeight").val();
                        tv = parseFloat(tVolume) + parseFloat(tVol);
                        tw = parseFloat(tWeight) + parseFloat(tWei);
                        $("#"+BLOld+"_Volume").val(tv);
                        $("#"+BLOld+"_Weight").val(tw);

                        $("#BLOld_<?php echo $index; ?>").val(responseJSON.containerBL);
                        var tVolume = $("#"+responseJSON.containerBL+"_Volume").val();
                        var tWeight = $("#"+responseJSON.containerBL+"_Weight").val();
                        tv = parseFloat(tVolume) - parseFloat(responseJSON.totalVolume);
                        tw = parseFloat(tWeight) - parseFloat(responseJSON.totalWeight);
                        $("#"+responseJSON.containerBL+"_Volume").val(tv);
                        $("#"+responseJSON.containerBL+"_Weight").val(tw);

                        if(tv < 0){
                            alert('Container over limit. Please change to other container.');
                            return false;
                        }
                    } else {
                        $("#OrderDetailTemp_<?php echo $index; ?>_QTY").val(responseJSON.qty);
                        $("#OrderDetailTemp_<?php echo $index; ?>_ContainerID").val(responseJSON.container);
                        alert(responseJSON.message);
                    }
                },
                complete: function(){
                    $('#tree-loading').html('');
                }
            });
        }
        else{
            var tVolume = $("#"+BLOld+"_Volume").val();
            var tWeight = $("#"+BLOld+"_Weight").val();
            var tVol = $("#OrderDetailTemp_<?php echo $index; ?>_TotalVolume").val();
            var tWei = $("#OrderDetailTemp_<?php echo $index; ?>_TotalWeight").val();
            tv = parseFloat(tVolume) + parseFloat(tVol);
            tw = parseFloat(tWeight) + parseFloat(tWei);
            $("#"+BLOld+"_Volume").val(tv);
            $("#"+BLOld+"_Weight").val(tw);
        }
    });

    $('.delete-<?php echo $index; ?>').click(function() {
        var vv = $("#OrderDetailTemp_<?php echo $index; ?>_TotalVolume").val();
        var ww = $("#OrderDetailTemp_<?php echo $index; ?>_TotalWeight").val();
        var pp = $("#OrderDetailTemp_<?php echo $index; ?>_TotalOrder").val();

        var v = $('#OrderHeader_TotalVolume').val();
        var w = $('#OrderHeader_TotalWeight').val();
        var p = $('#OrderHeader_OrderTotal').val();

        totalV = parseFloat(v) - parseFloat(vv);
        totalW = parseFloat(w) - parseFloat(ww);
        totalP = parseFloat(p) - parseFloat(pp);

        $('#OrderHeader_TotalVolume').val(totalV);
        $('#OrderHeader_TotalWeight').val(totalW);
        $('#OrderHeader_OrderTotal').val(totalP);

        $(this).parent().parent().remove();
    });
</script>
