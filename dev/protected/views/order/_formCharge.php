<?php $form = new CActiveForm(); ?>

<tr>
    <td><?php echo $index+1; ?></td>
    <td>
        <?php echo $form->hiddenField($detail,"[$index]IDOrderChargeDetail"); ?>
        <?php echo $form->dropDownList($detail,"[$index]ChargeID",
            CHtml::listData(OrderChargeMaster::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'ID', 'Name'),
            array('empty'=>'- Choose -', 'class'=>'form-control', 'required'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textField($detail,"[$index]Value", array('class'=>'form-control', 'required'=>true)); ?>
    </td>
    <td>
        <?php echo CHtml::link('Delete', 'javascript:void(0)',
            array('class'=>'btn btn-danger btn-sm delete'.$index)) ?>
    </td>
</tr>

<script>
    $('.delete<?php echo $index; ?>').click(function() {
        $(this).parent().parent().remove();
    });
</script>
