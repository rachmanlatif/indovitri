<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">Create Order BL</span>
            </div>
            <div class="widget-body">
							<a href="<?php echo Yii::app()->baseUrl.'/order/view/id/'.$model->OrderCode; ?>" class="btn btn-labeled btn-blue">
									<i class="btn-label glyphicon glyphicon-chevron-left"></i>Back To Order
							</a>

							<hr />

							<?php echo $this->renderPartial('_formBill', array(
							    'model'=>$model,
							)); ?>
						</div>
				</div>
		</div>
</div>
