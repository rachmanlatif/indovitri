<!--Datatables-->
<link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/dataTables.bootstrap.css" rel="stylesheet" />

<div class="col-sm-12">
    <div class="table-scrollable">
        <table class="table table-bordered table-hover table-striped" id="searchable">
            <thead>
            <tr>
                <th></th>
                <th>Image</th>
                <th>Product Name</th>
                <th>QTY</th>
                <th>QTY Stock</th>
                <th>Price</th>
                <th>Volume</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $no = 1;
            foreach($products as $product){ ?>
                <tr>
                    <td>
                        <?php echo $no; ?>
                    </td>
                    <td>
                        <img height="50" src="<?php echo Product::getFileUrl().$product->ThubnailsImage; ?>" />
                    </td>
                    <td><?php echo $product->ProductName; ?></td>
                    <td>
                        <input type="text" class="form-control" id="qty<?php echo $no; ?>" value="0">
                    </td>
                    <td><?php echo number_format($product->StokQTY); ?></td>
                    <td><?php echo number_format($product->Price); ?></td>
                    <td><?php echo number_format($product->VolumeGross); ?></td>
                    <td>
                        <a href="<?php echo Yii::app()->baseUrl.'/product/view/id/'.$product->ProductID; ?>" target="_blank">View Product</a>
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary" id="btn-choose<?php echo $no; ?>">Choose</button>
                        <span id="tree-loading<?php echo $no; ?>"></span>
                    </td>
                </tr>

                <script type="text/javascript">
                    $('#btn-choose<?php echo $no; ?>').click(function(){
                        var index = $('#detail tr').length;
                        var qty = $('#qty<?php echo $no; ?>').val();
                        var note = $('#note<?php echo $no; ?>').val();

                        $.ajax({
                            type: 'post',
                            url: '<?php echo $this->createUrl('addProduct'); ?>',
                            dataType: 'json',
                            data: {index: index, action: '<?php echo $action; ?>', product: '<?php echo $product->ProductID; ?>', qty: qty},
                            beforeSend: function(){
                                $('#tree-loading<?php echo $no; ?>').html('<i class="fa fa-rotate-right fa-spin">');
                            },
                            success: function(responseJSON) {
                                $('#detail').append(responseJSON.content);

                                var v = $('#OrderHeader_TotalVolume').val();
                                var w = $('#OrderHeader_TotalGrossWeight').val();
                                var n = $('#OrderHeader_TotalNettWeight').val();
                                var p = $('#OrderHeader_OrderTotal').val();
                                var q = $('#OrderHeader_TotalQty').val();
                                var u = $('#OrderHeader_TotalUnit').val();

                                totalV = parseFloat(v) + parseFloat(responseJSON.volume);
                                totalW = parseFloat(w) + parseFloat(responseJSON.grossWeight);
                                totalN = parseFloat(n) + parseFloat(responseJSON.nettWeight);
                                totalP = parseFloat(p) + parseFloat(responseJSON.price);
                                totalQ = parseFloat(q) + parseFloat(responseJSON.qty);
                                totalU = parseFloat(u) + parseFloat(responseJSON.qtyUnit);

                                $('#OrderHeader_OrderTotal').val(totalP);
                                $('#OrderHeader_TotalVolume').val(totalV);
                                $('#OrderHeader_TotalGrossWeight').val(totalW);
                                $('#OrderHeader_TotalNettWeight').val(totalN);
                                $('#OrderHeader_TotalQty').val(totalQ);
                                $('#OrderHeader_TotalUnit').val(totalU);

                                index++;
                            },
                            complete: function(){
                                $('#tree-loading<?php echo $no; ?>').html('');
                            }
                        });
                    });
                </script>
            <?php $no++; } ?>
          </tbody>
        </table>
    </div>
</div>

<div class="modal fade modal-prod" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="width: 80%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Product Detail</h4>
            </div>
            <div class="modal-product">

            </div>
            <div class="modal-footer">
                <button data-bb-handler="cancel" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!--Datatables-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/datatable/ZeroClipboard.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/datatable/dataTables.tableTools.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/datatable/dataTables.bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/datatable/datatables-init.js"></script>
<script>
    InitiateSimpleDataTable.init();
    InitiateEditableDataTable.init();
    InitiateExpandableDataTable.init();
    InitiateSearchableDataTable.init();
</script>
