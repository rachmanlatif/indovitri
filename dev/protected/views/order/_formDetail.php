<?php $form = new CActiveForm(); ?>

<tr>
    <td>
        <?php echo $form->hiddenField($detail, "[$index]IDOrderDetailTemp"); ?>
        <div class="col-xs-4">
            <img height="50" src="<?php echo Product::getFileUrl().$detail->product->ThubnailsImage; ?>" />
        </div>
        <div class="col-xs-8">
            <?php echo $form->hiddenField($detail, "[$index]ProductID"); ?>
            <b><?php echo CHtml::encode($detail->product->ProductName) ?></b>
            <br>
            Price: <?php echo number_format($detail->product->Price); ?>
            <br>
            Volume Gross: <?php echo number_format($detail->product->VolumeGross); ?>
        </div>
    </td>
    <td>
        <input type="hidden" id="QtyOld_<?php echo $index; ?>" value="<?php echo $detail->QTY; ?>" readonly>
        <?php echo $form->textField($detail, "[$index]QTY", array('class'=>'form-control')); ?>
    </td>
    <td>
        <?php echo $form->textArea($detail, "[$index]Note"); ?>
    </td>
    <td>
        <?php echo $form->textField($detail, "[$index]TotalOrder", array('class'=>'form-control', 'readonly'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textField($detail, "[$index]TotalVolume", array('class'=>'form-control', 'readonly'=>true)); ?>
    </td>
    <td>
        <?php echo $form->textField($detail, "[$index]TotalWeight", array('class'=>'form-control', 'readonly'=>true)); ?>
    </td>
    <td align="center" style="vertical-align: middle;">
        <?php echo CHtml::link('Delete', 'javascript:void(0)',
            array('class'=>'btn btn-danger btn-sm delete-'.$index)) ?>
    </td>
</tr>

<script type="text/javascript">
    $("#OrderDetailTemp_<?php echo $index; ?>_QTY").change(function(){
        var product = $("#OrderDetailTemp_<?php echo $index; ?>_ProductID").val();
        var qty = $("#OrderDetailTemp_<?php echo $index; ?>_QTY").val();
        var qtyOld = $("#QtyOld_<?php echo $index; ?>").val();

        $.ajax({
            type: 'post',
            url: '<?php echo $this->createUrl('updateOrder'); ?>',
            dataType: 'json',
            data: {product: product, qty: qty, qtyOld: qtyOld},
            beforeSend: function(){
                $('#tree-loading').html('<i class="fa fa-rotate-right fa-spin">');
            },
            success: function(responseJSON) {
                if (responseJSON.success) {
                    $("#QtyOld_<?php echo $index; ?>").val(responseJSON.qty);

                    $("#OrderDetailTemp_<?php echo $index; ?>_TotalVolume").val(responseJSON.totalVolume);
                    $("#OrderDetailTemp_<?php echo $index; ?>_TotalWeight").val(responseJSON.totalWeight);
                    $("#OrderDetailTemp_<?php echo $index; ?>_TotalOrder").val(responseJSON.totalOrder);

                    var vv = $('#OrderHeader_TotalVolume').val();
                    var ww = $('#OrderHeader_TotalWeight').val();
                    var pp = $('#OrderHeader_OrderTotal').val();
                    totalVV = (parseFloat(vv) - parseFloat(responseJSON.totalVolumeOld)) + parseFloat(responseJSON.totalVolume);
                    totalWW = (parseFloat(ww) - parseFloat(responseJSON.totalWeightOld)) + parseFloat(responseJSON.totalWeight);
                    totalPP = parseFloat(pp) + parseFloat(responseJSON.totalOrder);
                    $('#OrderHeader_OrderTotal').val(totalPP);
                    $('#OrderHeader_TotalVolume').val(totalVV);
                    $('#OrderHeader_TotalWeight').val(totalWW);

                } else {
                    $("#OrderDetailTemp_<?php echo $index; ?>_QTY").val(responseJSON.qty);
                    alert(responseJSON.message);
                }
            },
            complete: function(){
                $('#tree-loading').html('');
            }
        });
    });

    $('.delete-<?php echo $index; ?>').click(function() {
        var vv = $("#OrderDetailTemp_<?php echo $index; ?>_TotalVolume").val();
        var ww = $("#OrderDetailTemp_<?php echo $index; ?>_TotalWeight").val();
        var pp = $("#OrderDetailTemp_<?php echo $index; ?>_TotalOrder").val();

        var v = $('#OrderHeader_TotalVolume').val();
        var w = $('#OrderHeader_TotalWeight').val();
        var p = $('#OrderHeader_OrderTotal').val();

        totalV = parseFloat(v) - parseFloat(vv);
        totalW = parseFloat(w) - parseFloat(ww);
        totalP = parseFloat(p) - parseFloat(pp);

        $('#OrderHeader_TotalVolume').val(totalV);
        $('#OrderHeader_TotalWeight').val(totalW);
        $('#OrderHeader_OrderTotal').val(totalP);

        $(this).parent().parent().remove();
    });
</script>