<?php $form = new CActiveForm(); ?>

<?php
if($model->detailsTemp != null){
    $index2 = 0;
    $volume = 0;
    $order = 0;
    $grossweight = 0;
    $nettweight = 0;
    $qtyunit = 0;
    $qty = 0;
    foreach($model->detailsTemp as $detail){
        $volume = round($detail->TotalVolume, 0);
        $order = round($detail->TotalOrder, 0);
        $grossweight = round($detail->TotalGrossWeight, 0);
        $nettweight = round($detail->TotalNettWeight, 0);
        $qtyUnit = round($detail->QtyUnit, 0);
        $qty = round($detail->QTY, 0);
        ?>
        <tr>
            <td><?php echo $index2+1; ?></td>
            <td>
                <?php echo $form->hiddenField($detail, "[$index2]IDOrderDetailTemp"); ?>
                <?php echo $form->hiddenField($detail, "[$index2]ProductID"); ?>
                <img height="50" src="<?php echo Product::getFileUrl().$detail->product->ThubnailsImage; ?>" />
            </td>
            <td>
                <?php echo CHtml::encode($detail->product->ProductName) ?>
            </td>
            <td>
                <?php echo CHtml::encode($detail->product->ProductID) ?>
            </td>
            <td>
                <?php echo number_format($detail->product->Price, 2); ?>
            </td>
            <td>
                <?php
                $uom = '';
                if($detail->product->uom !== null){
                    $uom = $detail->product->uom->UOMName;
                }

                echo $uom;
                ?>
            </td>
            <td>
                <?php
                $small = 0;
                if($detail->product->uom->define !== null){
                    $small = $detail->product->uom->define->QTYUOMSmall;
                }

                echo number_format($small);
                ?>
                <?php echo $form->hiddenField($detail, "[$index2]MasterUnit", array('value'=>$small, 'readonly'=>true)); ?>
            </td>
            <td>
                <input type="hidden" id="QtyOld_<?php echo $index2; ?>" value="<?php echo $detail->QTY; ?>" readonly>
                <?php echo $form->textField($detail, "[$index2]QTY", array('class'=>'form-control', 'value'=>$qty)); ?>
            </td>
            <td>
                <input type="hidden" id="ContainerOld_<?php echo $index2; ?>" readonly>
                <?php
                $cont = 0;
                if($listContainer != null){ ?>
                    <?php echo CHtml::dropDownList("OrderDetailTemp[$index2][BLContainer]", $detail->ContainerID,
                        $listContainer,
                        array('class'=>'form-control', 'empty'=>'- Choose -', 'required'=>true)) ?>
                <?php } else { ?>
                    <?php
                    $cont = 1;
                    echo 'Add container above'; ?>
                <?php } ?>
                <span id="tree-loading3"></span>
            </td>
            <td>
                <?php echo number_format($detail->product->StokQTY); ?>
            </td>
            <td>
                <?php echo $form->textField($detail, "[$index2]QtyUnit", array('class'=>'form-control', 'value'=>$qtyUnit, 'readonly'=>true)); ?>
            </td>
            <td>
                <?php echo $form->textField($detail, "[$index2]TotalOrder", array('class'=>'form-control', 'value'=>$order, 'readonly'=>true)); ?>
            </td>
            <td>
                <?php echo number_format($detail->product->GrossWeight, 2); ?>
            </td>
            <td>
                <?php echo $form->textField($detail, "[$index2]TotalGrossWeight", array('class'=>'form-control', 'value'=>$grossweight, 'readonly'=>true)); ?>
            </td>
            <td>
                <?php echo number_format($detail->product->NettWeight, 2); ?>
            </td>
            <td>
                <?php echo $form->textField($detail, "[$index2]TotalNettWeight", array('class'=>'form-control', 'value'=>$nettweight, 'readonly'=>true)); ?>
            </td>
            <td>
                <?php echo number_format($detail->product->VolumeGross, 2); ?>
            </td>
            <td>
                <?php echo $form->textField($detail, "[$index2]TotalVolume", array('class'=>'form-control', 'value'=>$volume, 'readonly'=>true)); ?>
            </td>
            <td>
                <a href="<?php echo Yii::app()->baseUrl.'/order/deleteItems/id/'.$detail->IDOrderDetailTemp; ?>"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
            </td>
        </tr>

        <script type="text/javascript">
            $("#OrderDetailTemp_<?php echo $index2; ?>_QTY").change(function(){
                var product = $("#OrderDetailTemp_<?php echo $index2; ?>_ProductID").val();
                var qty = $("#OrderDetailTemp_<?php echo $index2; ?>_QTY").val();
                var qtyOld = $("#QtyOld_<?php echo $index2; ?>").val();
                var container = $("#OrderDetailTemp_<?php echo $index2; ?>_BLContainer option:selected").val();
                var empty = '<?php echo $cont; ?>';

                if(container == '' || empty == 1){
                    $.ajax({
                        type: 'post',
                        url: '<?php echo $this->createUrl('updateOrder'); ?>',
                        dataType: 'json',
                        data: {product: product, qty: qty, qtyOld: qtyOld},
                        beforeSend: function(){
                            $('#tree-loading').html('<i class="fa fa-rotate-right fa-spin">');
                        },
                        success: function(responseJSON) {
                            if (responseJSON.success) {
                                $("#QtyOld_<?php echo $index2; ?>").val(responseJSON.qty);

                                $("#OrderDetailTemp_<?php echo $index2; ?>_TotalVolume").val(responseJSON.totalVolume);
                                $("#OrderDetailTemp_<?php echo $index2; ?>_TotalGrossWeight").val(responseJSON.totalGrossWeight);
                                $("#OrderDetailTemp_<?php echo $index2; ?>_TotalNettWeight").val(responseJSON.totalNettWeight);
                                $("#OrderDetailTemp_<?php echo $index2; ?>_TotalOrder").val(responseJSON.totalOrder);
                                $("#OrderDetailTemp_<?php echo $index2; ?>_TotalUnit").val(responseJSON.totalUnit);
                                $("#OrderDetailTemp_<?php echo $index2; ?>_QtyUnit").val(responseJSON.qtyUnit);

                                var v = $('#OrderHeader_TotalVolume').val();
                                var p = $('#OrderHeader_OrderTotal').val();
                                var w = $('#OrderHeader_TotalGrossWeight').val();
                                var n = $('#OrderHeader_TotalNettWeight').val();
                                var q = $('#OrderHeader_TotalQty').val();
                                var u = $('#OrderHeader_TotalUnit').val();

                                totalV = (parseFloat(v) - parseFloat(responseJSON.totalVolumeOld)) + parseFloat(responseJSON.totalVolume);
                                totalP = (parseFloat(p) - parseFloat(responseJSON.totalOrderOld)) + parseFloat(responseJSON.totalOrder);
                                totalW = (parseFloat(w) - parseFloat(responseJSON.totalGrossWeightOld)) + parseFloat(responseJSON.totalGrossWeight);
                                totalN = (parseFloat(n) - parseFloat(responseJSON.totalNettWeightOld)) + parseFloat(responseJSON.totalNettWeight);
                                totalQ = (parseFloat(q) - parseFloat(responseJSON.qtyOld)) + parseFloat(responseJSON.qty);
                                totalU = (parseFloat(u) - parseFloat(responseJSON.qtyUnitOld)) + parseFloat(responseJSON.qtyUnit);

                                $('#OrderHeader_OrderTotal').val(totalP);
                                $('#OrderHeader_TotalVolume').val(totalV);
                                $('#OrderHeader_TotalGrossWeight').val(totalW);
                                $('#OrderHeader_TotalNettWeight').val(totalN);
                                $('#OrderHeader_TotalQty').val(totalQ);
                                $('#OrderHeader_TotalUnit').val(totalU);
                            } else {
                                $("#OrderDetailTemp_<?php echo $index2; ?>_QTY").val(responseJSON.qty);
                                alert(responseJSON.message);
                            }
                        },
                        complete: function(){
                            $('#tree-loading').html('');
                        }
                    });
                }
                else{
                  alert('Please empty container before change quantity');
                  $("#OrderDetailTemp_<?php echo $index2; ?>_QTY").val(parseInt(qtyOld));
                  return false;
                }
            });

            $("#OrderDetailTemp_<?php echo $index2; ?>_BLContainer").change(function(){
                var container = $("#OrderDetailTemp_<?php echo $index2; ?>_BLContainer option:selected").val();

                var totalVolume = $('#OrderDetailTemp_<?php echo $index2; ?>_TotalVolume').val();
                var totalOrder = $('#OrderDetailTemp_<?php echo $index2; ?>_TotalOrder').val();
                var totalWeight = $('#OrderDetailTemp_<?php echo $index2; ?>_TotalNettWeight').val();

                if(container != ''){
                    $.ajax({
                        type: 'post',
                        url: '<?php echo $this->createUrl('checkOrder'); ?>',
                        dataType: 'json',
                        data: {container: container, totalOrder: totalOrder, totalWeight: totalWeight, totalVolume: totalVolume},
                        beforeSend: function(){
                            $('#tree-loading').html('<i class="fa fa-rotate-right fa-spin">');
                        },
                        success: function(responseJSON) {
                            if (responseJSON.success) {
                                var tVolume = $("#"+responseJSON.containerBL+"_Volume").html();
                                var tWeight = $("#"+responseJSON.containerBL+"_Weight").html();
                                var tVolumeU = $("#"+responseJSON.containerBL+"_VolumeUsage").html();
                                var tWeightU = $("#"+responseJSON.containerBL+"_WeightUsage").html();

                                var tvp = 0;
                                var tvu = 0;
                                if(responseJSON.totalVolume > 0){
                                    tvu = parseFloat(tVolumeU) + parseFloat(responseJSON.totalVolume);
                                    tvp = (parseFloat(tvu) / parseFloat(tVolume))*100;
                                }

                                var twp = 0;
                                var twu = 0;
                                if(responseJSON.totalWeight > 0){
                                    twu = parseFloat(tWeight) + parseFloat(responseJSON.totalWeight);
                                    twp = (parseFloat(twu) / parseFloat(tWeight))*100;
                                }

                                $("#ContainerOld_<?php echo $index2; ?>").val(responseJSON.containerBL);

                                $("#"+responseJSON.containerBL+"_VolumeUsage").html(tvu);
                                $("#"+responseJSON.containerBL+"_WeightUsage").html(twu);

                                $("#"+responseJSON.containerBL+"_VolumePercentage").html(tvp.toFixed(2));
                                $("#"+responseJSON.containerBL+"_WeightPercentage").html(twp.toFixed(2));

                                if(tvp > 100 || twp > 100){
                                    alert('Container over limit. Please change to other container.');
                                    return false;
                                }

                            } else {
                                $("#OrderDetailTemp_<?php echo $index2; ?>_BLContainer").val('');
                                alert(responseJSON.message);
                            }
                        },
                        complete: function(){
                            $('#tree-loading').html('');
                        }
                    });
                }
                else{
                    var old = $("#ContainerOld_<?php echo $index2; ?>").val();

                    var tVolume = $("#"+old+"_Volume").html();
                    var tWeight = $("#"+old+"_Weight").html();
                    var tVolumeU = $("#"+old+"_VolumeUsage").html();
                    var tWeightU = $("#"+old+"_WeightUsage").html();

                    var tvu = 0;
                    var tvp = 0;
                    if(parseFloat(totalVolume) > 0){
                        tvu = parseFloat(tVolumeU) - parseFloat(totalVolume);

                        if(tvu > 0){
                            tvp = (parseFloat(tvu) / parseFloat(tVolume))*100;
                        }
                    }

                    var twu = 0;
                    var twp = 0;
                    if(parseFloat(totalWeight) > 0){
                        twu = parseFloat(tWeightU) - parseFloat(totalWeight);

                        if(twu > 0){
                            twp = (parseFloat(twu) - parseFloat(tWeight))*100;
                        }
                    }

                    $("#"+old+"_VolumeUsage").html(tvu);
                    $("#"+old+"_WeightUsage").html(twu);
                    $("#"+old+"_VolumePercentage").html(tvp.toFixed(2));
                    $("#"+old+"_WeightPercentage").html(twp.toFixed(2));
                }
            });

        </script>
    <?php $index2++; }
} ?>
