<?php $form = new CActiveForm(); ?>

<?php
if($model->details != null){
    $index2 = 0;
    $totalVolume = 0;
    $totalOrder = 0;
    $totalWeight = 0;
    foreach($model->details as $detail){
        $totalVolume+=$detail->TotalVolume;
        $totalOrder+=$detail->TotalOrder;
        $totalWeight+=$detail->TotalOrder;
        ?>
        <tr>
            <td>
                <?php echo $form->hiddenField($detail, "[$index2]IDOrderDetail"); ?>
                <div class="col-xs-4">
                    <img height="50" src="<?php echo Product::getFileUrl().$detail->product->ThubnailsImage; ?>" />
                </div>
                <div class="col-xs-8">
                    <?php echo $form->hiddenField($detail, "[$index2]ProductID"); ?>
                    <b><?php echo CHtml::encode($detail->product->ProductName) ?></b>
                    <br>
                    Price: <?php echo number_format($detail->product->Price); ?>
                    <br>
                    Volume Gross: <?php echo number_format($detail->product->VolumeGross); ?>
                </div>
            </td>
            <td>
                <input type="hidden" id="QtyOld_<?php echo $index2; ?>" value="<?php echo $detail->QTY; ?>" readonly>
                <?php echo $form->textField($detail, "[$index2]QTY", array('class'=>'form-control')); ?>
            </td>
            <td>
                <?php echo $form->textArea($detail, "[$index2]Note"); ?>
            </td>
            <td>
                <?php echo $form->textField($detail, "[$index2]TotalOrder", array('class'=>'form-control', 'readonly'=>true)); ?>
            </td>
            <td>
                <?php echo $form->textField($detail, "[$index2]TotalVolume", array('class'=>'form-control', 'readonly'=>true)); ?>
            </td>
            <td>
                <?php echo $form->textField($detail, "[$index2]TotalWeight", array('class'=>'form-control', 'readonly'=>true)); ?>
            </td>
            <td>
                <input type="hidden" id="ContainerOld_<?php echo $index2; ?>" value="<?php echo $detail->ContainerID; ?>" readonly>
                <?php echo $form->dropDownList($detail,"[$index2]ContainerID",
                    CHtml::listData(Container::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'ContainerCode', 'ContainerName'),
                    array('class'=>'form-control')); ?>
                <span id="Remains_<?php echo $index2; ?>"><?php echo number_format($detail->container->UsableVolume - $detail->TotalVolume); ?></span> volume remains
            </td>
            <td align="center" style="vertical-align: middle;">
                <a href="<?php echo Yii::app()->baseUrl.'/order/deleteItems/id/'.$detail->IDOrderDetail; ?>"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
            </td>
        </tr>

        <script>
            $("#OrderDetailTemp_<?php echo $index2; ?>_QTY").change(function(){
                checkOrder();
            });

            $("#OrderDetailTemp_<?php echo $index2; ?>_ContainerID").change(function(){
                checkOrder();
            });

            function checkOrder(){
                var product = $("#OrderDetailTemp_<?php echo $index2; ?>_ProductID").val();
                var qty = $("#OrderDetailTemp_<?php echo $index2; ?>_QTY").val();
                var container = $("#OrderDetailTemp_<?php echo $index2; ?>_ContainerID option:selected").val();
                var qtyOld = $("#QtyOld_<?php echo $index2; ?>").val();
                var containerOld = $("#ContainerOld_<?php echo $index2; ?>").val();
                var totalVolume = $("#OrderDetailTemp_<?php echo $index2; ?>_TotalVolume").val();
                var totalOrder = $("#OrderDetailTemp_<?php echo $index2; ?>_TotalOrder").val();

                $.ajax({
                    type: 'post',
                    url: '<?php echo $this->createUrl('checkOrder'); ?>',
                    dataType: 'json',
                    data: {product: product, qty: qty, container: container, qtyOld: qtyOld, containerOld: containerOld},
                    beforeSend: function(){
                        $('#tree-loading').html('<i class="fa fa-rotate-right fa-spin">');
                    },
                    success: function(responseJSON) {
                        if (responseJSON.success) {
                            $("#Remains_<?php echo $index2; ?>").html(responseJSON.remains);
                            $("#OrderDetailTemp_<?php echo $index2; ?>_TotalVolume").val(responseJSON.totalVolume);
                            $("#OrderDetailTemp_<?php echo $index2; ?>_TotalOrder").val(responseJSON.totalOrder);

                            var v = $('#OrderHeader_TotalVolume').val();
                            var p = $('#OrderHeader_OrderTotal').val();

                            totalV = (parseFloat(v) - parseFloat(totalVolume)) + parseFloat(responseJSON.totalVolume);
                            totalP = (parseFloat(p) - parseFloat(totalOrder)) + parseFloat(responseJSON.totalOrder);
                            $('#OrderHeader_OrderTotal').val(totalP);
                            $('#OrderHeader_TotalVolume').val(totalV);
                        } else {
                            $("#OrderDetailTemp_<?php echo $index2; ?>_QTY").val(responseJSON.qty);
                            $("#OrderDetailTemp_<?php echo $index2; ?>_ContainerID").val(responseJSON.container);
                            alert(responseJSON.message);
                        }
                    },
                    complete: function(){
                        $('#tree-loading').html('');
                    }
                });
            }
        </script>
    <?php $index2++; }
} ?>
