<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('OrderCode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->OrderCode), array('view', 'id'=>$data->OrderCode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('OrderDate')); ?>:</b>
	<?php echo CHtml::encode($data->OrderDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SellerCode')); ?>:</b>
	<?php echo CHtml::encode($data->SellerCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('POLCode')); ?>:</b>
	<?php echo CHtml::encode($data->POLCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PODCode')); ?>:</b>
	<?php echo CHtml::encode($data->PODCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ConsigneeCode')); ?>:</b>
	<?php echo CHtml::encode($data->ConsigneeCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NotifyPartyCode')); ?>:</b>
	<?php echo CHtml::encode($data->NotifyPartyCode); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('OrderStatusCode')); ?>:</b>
	<?php echo CHtml::encode($data->OrderStatusCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IncontermID')); ?>:</b>
	<?php echo CHtml::encode($data->IncontermID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PORefCode')); ?>:</b>
	<?php echo CHtml::encode($data->PORefCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('POStatusCode')); ?>:</b>
	<?php echo CHtml::encode($data->POStatusCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PODate')); ?>:</b>
	<?php echo CHtml::encode($data->PODate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BillCodeSupplier')); ?>:</b>
	<?php echo CHtml::encode($data->BillCodeSupplier); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BillSupplierDate')); ?>:</b>
	<?php echo CHtml::encode($data->BillSupplierDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BuyerFee')); ?>:</b>
	<?php echo CHtml::encode($data->BuyerFee); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BuyerFeeAmount')); ?>:</b>
	<?php echo CHtml::encode($data->BuyerFeeAmount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BillSupplierTermofPayment')); ?>:</b>
	<?php echo CHtml::encode($data->BillSupplierTermofPayment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SupplierFee')); ?>:</b>
	<?php echo CHtml::encode($data->SupplierFee); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FreightFee')); ?>:</b>
	<?php echo CHtml::encode($data->FreightFee); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ComisionCodeSkema')); ?>:</b>
	<?php echo CHtml::encode($data->ComisionCodeSkema); ?>
	<br />

	*/ ?>

</div>