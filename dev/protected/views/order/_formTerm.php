<?php $form = new CActiveForm(); ?>

<tr>
    <td>
        <?php echo $form->hiddenField($model, "[$index]IDOrderTermPayment"); ?>
        <?php echo $form->hiddenField($model, "[$index]NumberTermPayment", array('value'=>$index+1)); ?>
        <?php echo $index+1; ?>
    </td>
    <td>
        <?php echo $form->textField($model, "[$index]PercentageofPayment", array('class'=>'form-control')); ?>
    </td>
    <td>
        <?php echo $form->textField($model, "[$index]ValuePercentageOfPayment", array('class'=>'form-control', 'readonly'=>true, 'value'=>'0')); ?>
    </td>
    <td>
        <?php echo $form->textField($model, "[$index]IssueDate", array('class'=>'form-control date-picker', 'value'=>date('Y-m-d'))); ?>
    </td>
    <td>
        <?php echo $form->textField($model, "[$index]ExpiredDate", array('class'=>'form-control date-picker', 'autocomplete'=>'off')); ?>
    </td>
    <td align="center" style="vertical-align: middle;">
        <?php echo CHtml::link('Delete', 'javascript:void(0)',
            array('class'=>'btn btn-danger btn-sm delete')) ?>
    </td>
</tr>

<!--Bootstrap Date Picker-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/backend/js/datetime/bootstrap-datepicker.js"></script>

<script>
$('#OrderTermPayment_<?php echo $index; ?>_PercentageofPayment').change(function(){
  var fee = $('#OrderHeader_SupplierFee').val();
  var total = $('#SummaryOrder').val();
  var percentage = $('#OrderTermPayment_<?php echo $index; ?>_PercentageofPayment').val();
  var value = $('#OrderTermPayment_<?php echo $index; ?>_ValuePercentageOfPayment').val();

  var totalAmount = $('#TotalAmount').val();

  var summaryOrder = $('#SummaryOrder').val();

  disc = (parseFloat(total) * parseFloat(percentage))/100;
  rA = (parseFloat(totalAmount) - parseFloat(value)) + parseFloat(disc);

  if(rA > parseFloat(summaryOrder)){
      $('#OrderTermPayment_<?php echo $index; ?>_PercentageofPayment').val(0);
      alert('Over Total Summary '+summaryOrder);
      return false;
  }
  else{
      $('#TotalAmount').val(rA);
      $('#OrderTermPayment_<?php echo $index; ?>_ValuePercentageOfPayment').val(disc);
  }
});

//--Bootstrap Date Picker--
$('.date-picker').datepicker();

$('.delete').click(function() {
    var value = $('#OrderTermPayment_<?php echo $index; ?>_ValuePercentageOfPayment').val();
    var totalAmount = $('#TotalAmount').val();

    rA = parseFloat(totalAmount) - parseFloat(value);
    $('#TotalAmount').val(rA);

    $(this).parent().parent().remove();
});
</script>
