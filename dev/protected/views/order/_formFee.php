<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'order-header-form',
    'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'BuyerFee'); ?>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-12">
                <?php echo $form->textField($model,'BuyerFee', array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'BuyerFee'); ?>
            </div>
        </div>
    </div>
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'SupplierFee'); ?>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-12">
                <?php echo $form->textField($model,'SupplierFee', array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'SupplierFee'); ?>
            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'BuyerFeeAmount'); ?>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-12">
                <?php echo $form->textField($model,'BuyerFeeAmount', array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'BuyerFeeAmount'); ?>
            </div>
        </div>
    </div>
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'FreightFee'); ?>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-12">
                <?php echo $form->textField($model,'FreightFee', array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'FreightFee'); ?>
            </div>
        </div>

    </div>
</div>

<hr>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th class="col-xs-2">Product Name</th>
            <th class="col-xs-1">QTY</th>
            <th class="col-xs-2">Note</th>
            <th class="col-xs-1">Total Price</th>
            <th class="col-xs-1">Total Volume</th>
            <th class="col-xs-1">Total Weight</th>
            <th class="col-xs-2">Fee</th>
        </tr>
        </thead>
        <tbody>
        <?php $form = new CActiveForm(); ?>

        <?php
        if($model->detailsTemp != null){
            $index = 0;
            foreach($model->detailsTemp as $detail){
                ?>
                <tr>
                    <td>
                        <?php echo $form->hiddenField($detail, "[$index]IDOrderDetailTemp"); ?>
                        <div class="col-xs-4">
                            <img height="50" src="<?php echo Product::getFileUrl().$detail->product->ThubnailsImage; ?>" />
                        </div>
                        <div class="col-xs-8">
                            <b><?php echo CHtml::encode($detail->product->ProductName) ?></b>
                            <br>
                            Price: <?php echo number_format($detail->product->Price); ?>
                            <br>
                            Volume Gross: <?php echo number_format($detail->product->VolumeGross); ?>
                        </div>
                    </td>
                    <td>
                        <?php echo number_format($detail->QTY); ?>
                    </td>
                    <td>
                        <?php echo $detail->Note; ?>
                    </td>
                    <td>
                        <?php echo number_format($detail->TotalOrder); ?>
                    </td>
                    <td>
                        <?php echo number_format($detail->TotalVolume); ?>
                    </td>
                    <td>
                        <?php echo number_format($detail->TotalWeight); ?>
                    </td>
                    <td>
                        <?php echo $form->textField($detail, "[$index]SupplierFee", array('class'=>'form-control')); ?>
                    </td>
                </tr>
                <?php $index++; }
        } ?>
        </tbody>
    </table>
</div>

<hr>

<div class="row buttons">
    <div class="col-xs-12">
        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
    </div>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
