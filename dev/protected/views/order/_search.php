<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="widget">
			<div class="form-horizontal">
				<h5 class="row-title before-magenta"><i class="fa fa-search magenta"></i>Form Search</h5>
				<?php $form=$this->beginWidget('CActiveForm', array(
					'action'=>Yii::app()->createUrl($this->route),
					'method'=>'get',
				)); ?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'OrderCode'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'OrderCode',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'OrderDate'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'OrderDate',array('class'=>'date-picker form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'CustomerCode'); ?></label>
							<div class="col-sm-10">
								<?php echo $form->dropDownList($model,'CustomerCode',
										CHtml::listData(Customer::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'CustomerCode', 'Name'),
										array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 100%')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'SellerCode'); ?></label>
							<div class="col-sm-10">
								<?php echo $form->dropDownList($model,'SellerCode',
										CHtml::listData(Seller::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'SellerCode', 'Name'),
										array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 100%')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'ConsigneeCode'); ?></label>
							<div class="col-sm-10">
								<?php echo $form->dropDownList($model,'ConsigneeCode',
										CHtml::listData(Consignee::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE, 'isConsign'=>0)), 'ConsigneeCode', 'Name'),
										array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 100%')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'NotifyPartyCode'); ?></label>
							<div class="col-sm-10">
								<?php echo $form->dropDownList($model,'NotifyPartyCode',
										CHtml::listData(Consignee::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE, 'isConsign'=>1)), 'ConsigneeCode', 'Name'),
										array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 100%')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'OrderStatusCode'); ?></label>
							<div class="col-sm-10">
								<?php echo $form->dropDownList($model,'OrderStatusCode',
										EnumOrder::getList()); ?>
							</div>
					</div>

					<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
				</div>

				<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</div>
