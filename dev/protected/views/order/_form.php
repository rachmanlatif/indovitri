<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'order-header-form',
    'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'OrderDate'); ?>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-12">
                <?php echo $form->textField($model,'OrderDate', array('class'=>'form-control date-picker','value'=>date('Y-m-d'))); ?>
                <?php echo $form->error($model,'OrderDate'); ?>
            </div>
        </div>
    </div>
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'OrderStatusCode'); ?>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-12">
                <?php echo $form->dropDownList($model,'OrderStatusCode',
                    EnumOrder::getList()); ?>
                <?php echo $form->error($model,'OrderStatusCode'); ?>
            </div>
        </div>
    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'CustomerCode'); ?>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-12">
                <div class="input-group input-group-sm">
                    <?php echo $form->dropDownList($model,'CustomerCode',
                        CHtml::listData(Customer::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'CustomerCode', 'Name'),
                        array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 300px;')); ?>
                    <?php echo $form->error($model,'CustomerCode'); ?>
                    <span class="input-group-btn">
                        <a href="<?php echo Yii::app()->baseUrl.'/customer/add'; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                    </span>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'SellerCode'); ?>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-12">
              <div class="input-group input-group-sm">
                <?php echo $form->dropDownList($model,'SellerCode',
                    CHtml::listData(Seller::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'SellerCode', 'Name'),
                    array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 300px;')); ?>
                <?php echo $form->error($model,'SellerCode'); ?>
                  <span class="input-group-btn">
                      <a href="<?php echo Yii::app()->baseUrl.'/seller/add'; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                  </span>
              </div>
            </div>
        </div>
    </div>
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'IncontermID'); ?>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-12">
              <div class="input-group input-group-sm">
                <?php echo $form->dropDownList($model,'IncontermID',
                    CHtml::listData(Incoterm::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'IncotermID', 'IncotermName'),
                    array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 300px;')); ?>
                <?php echo $form->error($model,'IncontermID'); ?>
                  <span class="input-group-btn">
                      <a href="<?php echo Yii::app()->baseUrl.'/incoterm/add'; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                  </span>
              </div>

            </div>
        </div>
    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'ConsigneeCode'); ?>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-12">
              <div class="input-group input-group-sm">
                <?php echo $form->dropDownList($model,'ConsigneeCode',
                    CHtml::listData(Consignee::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE, 'isConsign'=>0)), 'ConsigneeCode', 'Name'),
                    array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 300px;')); ?>
                <?php echo $form->error($model,'ConsigneeCode'); ?>
                  <span class="input-group-btn">
                      <a href="<?php echo Yii::app()->baseUrl.'/consignee/add'; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                  </span>
              </div>

            </div>
        </div>
    </div>
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'POLCode'); ?>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-12">
              <div class="input-group input-group-sm">
                <?php echo $form->dropDownList($model,'POLCode',
                    CHtml::listData(Port::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'PortID', 'PortName'),
                    array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 300px;')); ?>
                <?php echo $form->error($model,'POLCode'); ?>
                  <span class="input-group-btn">
                      <a href="<?php echo Yii::app()->baseUrl.'/port/add'; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                  </span>
              </div>

            </div>
        </div>
    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'NotifyPartyCode'); ?>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-12">
              <div class="input-group input-group-sm">
                <?php echo $form->dropDownList($model,'NotifyPartyCode',
                    CHtml::listData(Consignee::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE, 'isConsign'=>1)), 'ConsigneeCode', 'Name'),
                    array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 300px;')); ?>
                <?php echo $form->error($model,'NotifyPartyCode'); ?>
                  <span class="input-group-btn">
                      <a href="<?php echo Yii::app()->baseUrl.'/notifyParty/add'; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                  </span>
              </div>

            </div>
        </div>
    </div>
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'PODCode'); ?>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-12">
              <div class="input-group input-group-sm">
                <?php echo $form->dropDownList($model,'PODCode',
                    CHtml::listData(Port::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'PortID', 'PortName'),
                    array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 300px;')); ?>
                <?php echo $form->error($model,'PODCode'); ?>
                  <span class="input-group-btn">
                      <a href="<?php echo Yii::app()->baseUrl.'/port/add'; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                  </span>
              </div>

            </div>
        </div>

    </div>
</div>

<div class="row form-row form-group">
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'AgentCodeOrigin'); ?>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-12">
              <div class="input-group input-group-sm">
                <?php echo $form->dropDownList($model,'AgentCodeOrigin',
                    CHtml::listData(Agent::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'AgentCode', 'Name'),
                    array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 300px;')); ?>
                <?php echo $form->error($model,'AgentCodeOrigin'); ?>
                  <span class="input-group-btn">
                      <a href="<?php echo Yii::app()->baseUrl.'/agent/add'; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                  </span>
              </div>

            </div>
        </div>
    </div>
    <div class="col-xs-2">
        <?php echo $form->labelEx($model,'AgentCodeDestination'); ?>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-12">
              <div class="input-group input-group-sm">
                <?php echo $form->dropDownList($model,'AgentCodeDestination',
                    CHtml::listData(Agent::model()->findAllByAttributes(array('Status'=>EnumStatus::ACTIVE)), 'AgentCode', 'Name'),
                    array('empty'=>'- Choose -', 'class'=>'select2', 'style'=>'width: 300px;')); ?>
                <?php echo $form->error($model,'AgentCodeDestination'); ?>
                  <span class="input-group-btn">
                      <a href="<?php echo Yii::app()->baseUrl.'/agent/add'; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                  </span>
              </div>

            </div>
        </div>

    </div>
</div>

<hr>

<button type="button" id="addChoose" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-lg">Choose Product</button>

<br>
<span id="tree-loading"></span>
<br>
<div class="table-scrollable">
    <table class="table table-bordered table-striped table-hover">
        <thead>
        <?php
        $totalQTY = 0;
        $totalUnit = 0;
        $totalOrder = 0;
        $totalGrossWeight = 0;
        $totalNettWeight = 0;
        $totalVolume = 0;
        if($model->detailsTemp != null){
            foreach($model->detailsTemp as $detail){
                $totalQTY+=$detail->QTY;
                $totalUnit+=$detail->QtyUnit;
                $totalOrder+=$detail->TotalOrder;
                $totalGrossWeight+=$detail->TotalGrossWeight;
                $totalNettWeight+=$detail->TotalNettWeight;
                $totalVolume+=$detail->TotalVolume;
            }
        }
        ?>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <th colspan="2">WORKING TOTALS:</th>
            <td></td>
            <td>
                <?php echo $form->textField($model,'TotalQty', array('class'=>'form-control', 'value'=>$totalQTY, 'readonly'=>true)); ?>
            </td>
            <td></td>
            <td>
                <?php echo $form->textField($model,'TotalUnit', array('class'=>'form-control', 'value'=>$totalUnit, 'readonly'=>true)); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'OrderTotal', array('class'=>'form-control', 'value'=>$totalOrder, 'readonly'=>true)); ?>
            </td>
            <td></td>
            <td>
                <?php echo $form->textField($model,'TotalGrossWeight', array('class'=>'form-control', 'value'=>$totalGrossWeight, 'readonly'=>true)); ?>
            </td>
            <td></td>
            <td>
                <?php echo $form->textField($model,'TotalNettWeight', array('class'=>'form-control', 'value'=>$totalNettWeight, 'readonly'=>true)); ?>
            </td>
            <td></td>
            <td>
                <?php echo $form->textField($model,'TotalVolume', array('class'=>'form-control', 'value'=>$totalVolume, 'readonly'=>true)); ?>
            </td>
            <td></td>
        </tr>
        <tr>
            <th>Seq</th>
            <th>Image</th>
            <th>Description</th>
            <th>Product Code</th>
            <th>Price /Unit (USD)</th>
            <th>Unit</th>
            <th>Master Carton Unit</th>
            <th>Order Qty Carton</th>
            <th>Stocks</th>
            <th>Qty Units</th>
            <th>Total Cost (USD)</th>
            <th>G.W (Kgs)</th>
            <th>G.W Total</th>
            <th>N.W (Kgs)</th>
            <th>N.W Total</th>
            <th>CBM</th>
            <th>CBM Total</th>
            <th></th>
        </tr>
        </thead>
        <tbody id="detail">
        <?php
            $this->renderPartial('_formDetailTemp', array(
                'model'=>$model,
            ));
        ?>
        </tbody>
    </table>
</div>

<hr>

<div class="row buttons">
    <div class="col-xs-12">
        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
    </div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Product List</h4>
            </div>
            <div class="modal-product">

            </div>
            <div class="modal-footer">
                <button data-bb-handler="cancel" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    var index = <?php echo count($model->detailsTemp) ?>;

    $('#addChoose').click(function(){
      var seller = $('#OrderHeader_SellerCode option:selected').val();

      if(seller == ''){
        alert('Please select seller');
        $('#OrderHeader_SellerCode').focus;
        return false;
      }
      else{
        $('.modal-product').html('');

        $.ajax({
            type: 'post',
            url: '<?php echo $this->createUrl('chooseProduct'); ?>',
            data: {action: 'new', seller: seller},
            beforeSend: function(){
                $('#tree-loading').html('<i class="fa fa-rotate-right fa-spin">');
            },
            success: function(data) {
                $('.modal-product').html(data);
            },
            complete: function(){
                $('#tree-loading').html('');
            }
        });
      }
    });

    $('#addDetail').click(function(){
        var product = $('#productID option:selected').val();
        var qty = $('#qty').val();
        var note = $('#note').val();

        if(product == ''){
            alert('Please select product');
            return false;
        }
        else if(qty == '' || qty <= 0){
            alert('Please input quantity');
            return false;
        }
        else{
            $.ajax({
                type: 'post',
                url: '<?php echo $this->createUrl('addDetail'); ?>',
                dataType: 'json',
                data: {index: index, product: product, qty: qty, note: note},
                beforeSend: function(){
                    $('#tree-loading').html('<i class="fa fa-rotate-right fa-spin">');
                },
                success: function(responseJSON) {
                    if (responseJSON.success) {
                        $('#detail').append(responseJSON.content);

                        var v = $('#OrderHeader_TotalVolume').val();
                        var w = $('#OrderHeader_TotalGrossWeight').val();
                        var n = $('#OrderHeader_TotalNettWeight').val();
                        var p = $('#OrderHeader_OrderTotal').val();

                        totalV = parseFloat(v) + parseFloat(responseJSON.volume);
                        totalW = parseFloat(w) + parseFloat(responseJSON.grossWeight);
                        totalN = parseFloat(w) + parseFloat(responseJSON.nettWeight);
                        totalP = parseFloat(p) + parseFloat(responseJSON.price);

                        $('#OrderHeader_OrderTotal').val(totalP);
                        $('#OrderHeader_TotalVolume').val(totalV);
                        $('#OrderHeader_TotalGrossWeight').val(totalW);
                        $('#OrderHeader_TotalNettWeight').val(totalN);

                        index++;
                    } else {
                        alert(responseJSON.message);
                    }
                },
                complete: function(){
                    $('#tree-loading').html('');
                    $('#qty').val('');
                    $('#note').val('');
                }
            });
        }
    });
</script>
</div><!-- form -->
