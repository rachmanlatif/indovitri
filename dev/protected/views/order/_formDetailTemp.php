<?php $form = new CActiveForm(); ?>

<?php
if($model->detailsTemp != null){
    $index2 = 0;
    $volume = 0;
    $order = 0;
    $grossweight = 0;
    $nettweight = 0;
    $qtyunit = 0;
    $qty = 0;
    foreach($model->detailsTemp as $detail){
        $volume = round($detail->TotalVolume, 0);
        $order = round($detail->TotalOrder, 0);
        $grossweight = round($detail->TotalGrossWeight, 0);
        $nettweight = round($detail->TotalNettWeight, 0);
        $qtyUnit = round($detail->QtyUnit, 0);
        $qty = round($detail->QTY, 0);
        ?>
        <tr>
            <td><?php echo $index2+1; ?></td>
            <td>
                <?php echo $form->hiddenField($detail, "[$index2]IDOrderDetailTemp"); ?>
                <?php echo $form->hiddenField($detail, "[$index2]ProductID"); ?>
                <img height="50" src="<?php echo Product::getFileUrl().$detail->product->ThubnailsImage; ?>" />
            </td>
            <td>
                <?php echo CHtml::encode($detail->product->ProductName) ?>
            </td>
            <td>
                <?php echo CHtml::encode($detail->product->ProductID) ?>
            </td>
            <td>
                <?php echo number_format($detail->product->Price, 2); ?>
            </td>
            <td>
                <?php
                $uom = '';
                if($detail->product->uom !== null){
                    $uom = $detail->product->uom->UOMName;
                }

                echo $uom;
                ?>
            </td>
            <td>
                <?php
                $small = 0;
                if($detail->product->uom->define !== null){
                    $small = $detail->product->uom->define->QTYUOMSmall;
                }

                echo number_format($small);
                ?>
                <?php echo $form->hiddenField($detail, "[$index2]MasterUnit", array('value'=>$small, 'readonly'=>true)); ?>
            </td>
            <td>
                <input type="hidden" id="QtyOld_<?php echo $index2; ?>" value="<?php echo $detail->QTY; ?>" readonly>
                <?php echo $form->textField($detail, "[$index2]QTY", array('class'=>'form-control', 'value'=>$qty)); ?>
            </td>
            <td>
                <?php echo number_format($detail->product->StokQTY); ?>
            </td>
            <td>
                <?php echo $form->textField($detail, "[$index2]QtyUnit", array('class'=>'form-control', 'value'=>$qtyUnit, 'readonly'=>true)); ?>
            </td>
            <td>
                <?php echo $form->textField($detail, "[$index2]TotalOrder", array('class'=>'form-control', 'value'=>$order, 'readonly'=>true)); ?>
            </td>
            <td>
                <?php echo number_format($detail->product->GrossWeight, 2); ?>
            </td>
            <td>
                <?php echo $form->textField($detail, "[$index2]TotalGrossWeight", array('class'=>'form-control', 'value'=>$grossweight, 'readonly'=>true)); ?>
            </td>
            <td>
                <?php echo number_format($detail->product->NettWeight, 2); ?>
            </td>
            <td>
                <?php echo $form->textField($detail, "[$index2]TotalNettWeight", array('class'=>'form-control', 'value'=>$nettweight, 'readonly'=>true)); ?>
            </td>
            <td>
                <?php echo number_format($detail->product->VolumeGross, 2); ?>
            </td>
            <td>
                <?php echo $form->textField($detail, "[$index2]TotalVolume", array('class'=>'form-control', 'value'=>$volume, 'readonly'=>true)); ?>
            </td>
            <td align="center" style="vertical-align: middle;">
                <a href="<?php echo Yii::app()->baseUrl.'/order/deleteItem/id/'.$detail->IDOrderDetailTemp; ?>"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
            </td>
        </tr>

        <script>
            $("#OrderDetailTemp_<?php echo $index2; ?>_QTY").change(function(){
                var product = $("#OrderDetailTemp_<?php echo $index2; ?>_ProductID").val();
                var qty = $("#OrderDetailTemp_<?php echo $index2; ?>_QTY").val();
                var qtyOld = $("#QtyOld_<?php echo $index2; ?>").val();

                $.ajax({
                    type: 'post',
                    url: '<?php echo $this->createUrl('updateOrder'); ?>',
                    dataType: 'json',
                    data: {product: product, qty: qty, qtyOld: qtyOld},
                    beforeSend: function(){
                        $('#tree-loading').html('<i class="fa fa-rotate-right fa-spin">');
                    },
                    success: function(responseJSON) {
                        if (responseJSON.success) {
                            $("#QtyOld_<?php echo $index2; ?>").val(responseJSON.qty);

                            $("#OrderDetailTemp_<?php echo $index2; ?>_TotalVolume").val(responseJSON.totalVolume);
                            $("#OrderDetailTemp_<?php echo $index2; ?>_TotalGrossWeight").val(responseJSON.totalGrossWeight);
                            $("#OrderDetailTemp_<?php echo $index2; ?>_TotalNettWeight").val(responseJSON.totalNettWeight);
                            $("#OrderDetailTemp_<?php echo $index2; ?>_TotalOrder").val(responseJSON.totalOrder);
                            $("#OrderDetailTemp_<?php echo $index2; ?>_TotalUnit").val(responseJSON.totalUnit);
                            $("#OrderDetailTemp_<?php echo $index2; ?>_QtyUnit").val(responseJSON.qtyUnit);

                            var v = $('#OrderHeader_TotalVolume').val();
                            var p = $('#OrderHeader_OrderTotal').val();
                            var w = $('#OrderHeader_TotalGrossWeight').val();
                            var n = $('#OrderHeader_TotalNettWeight').val();
                            var q = $('#OrderHeader_TotalQty').val();
                            var u = $('#OrderHeader_TotalUnit').val();

                            totalV = (parseFloat(v) - parseFloat(responseJSON.totalVolumeOld)) + parseFloat(responseJSON.totalVolume);
                            totalP = (parseFloat(p) - parseFloat(responseJSON.totalOrderOld)) + parseFloat(responseJSON.totalOrder);
                            totalW = (parseFloat(w) - parseFloat(responseJSON.totalGrossWeightOld)) + parseFloat(responseJSON.totalGrossWeight);
                            totalN = (parseFloat(n) - parseFloat(responseJSON.totalNettWeightOld)) + parseFloat(responseJSON.totalNettWeight);
                            totalQ = (parseFloat(q) - parseFloat(responseJSON.qtyOld)) + parseFloat(responseJSON.qty);
                            totalU = (parseFloat(u) - parseFloat(responseJSON.qtyUnitOld)) + parseFloat(responseJSON.qtyUnit);

                            $('#OrderHeader_OrderTotal').val(totalP);
                            $('#OrderHeader_TotalVolume').val(totalV);
                            $('#OrderHeader_TotalGrossWeight').val(totalW);
                            $('#OrderHeader_TotalNettWeight').val(totalN);
                            $('#OrderHeader_TotalQty').val(totalQ);
                            $('#OrderHeader_TotalUnit').val(totalU);
                        } else {
                            $("#OrderDetailTemp_<?php echo $index2; ?>_QTY").val(responseJSON.qty);
                            alert(responseJSON.message);
                        }
                    },
                    complete: function(){
                        $('#tree-loading').html('');
                    }
                });
            });
        </script>
    <?php $index2++; }
} ?>
