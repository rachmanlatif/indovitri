<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header  with-footer">
                <span class="widget-caption" style="font-size: 19px">View Order</span>
            </div>
            <div class="widget-body">
                <div class="table-scrollable">
									<a href="#" class="btn btn-labeled btn-blue" id="btn-back">
                      <i class="btn-label glyphicon glyphicon-chevron-left"></i>Back
                  </a>

									<div class="row">
									    <div class="col-sm-12 col-xs-12">
									        <div id="status-message">
									            <?php if(Yii::app()->user->hasFlash('success')): ?>
									                <div class="alert alert-success">
									                    <?php echo Yii::app()->user->getFlash('success') ?>
									                </div>
									            <?php endif ?>

									            <?php if(Yii::app()->user->hasFlash('info')): ?>
									                <div class="alert alert-info">
									                    <?php echo Yii::app()->user->getFlash('info') ?>
									                </div>
									            <?php endif ?>

									            <?php if(Yii::app()->user->hasFlash('danger')): ?>
									                <div class="alert alert-danger">
									                    <?php echo Yii::app()->user->getFlash('danger') ?>
									                </div>
									            <?php endif ?>
									        </div>
									    </div>
									</div>

									<?php $this->widget('zii.widgets.CDetailView', array(
									    'htmlOptions'=>array(
									        'class'=>'detail-view table table-striped table-bordered table-hover'
									    ),
									    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
									    'data'=>$model,
									    'attributes'=>array(
									        'OrderCode',
									        'OrderDate',
									        array(
									            'name'=>'CustomerCode',
									            'value'=>($model->customer != null ? $model->customer->Name : ''),
									        ),
									        array(
									            'name'=>'SellerCode',
									            'value'=>($model->seller != null ? $model->seller->Name : ''),
									        ),
									        array(
									            'name'=>'ConsigneeCode',
									            'value'=>($model->consignee != null ? $model->consignee->Name : ''),
									        ),
									        array(
									            'name'=>'NotifyPartyCode',
									            'value'=>($model->notifyParty != null ? $model->notifyParty->Name : ''),
									        ),
									        array(
									            'name'=>'IncontermID',
									            'value'=>($model->incoterm != null ? $model->incoterm->IncotermName : ''),
									        ),
									        array(
									            'name'=>'AgentCodeOrigin',
									            'value'=>($model->agentOrigin != null ? $model->agentOrigin->Name : ''),
									        ),
									        array(
									            'name'=>'AgentCodeDestination',
									            'value'=>($model->agentDestination != null ? $model->agentDestination->Name : ''),
									        ),
									        array(
									            'name'=>'OrderStatusCode',
									            'value'=>EnumOrder::getLabel($model->OrderStatusCode),
									        ),
									        array(
									            'name'=>'POLCode',
									            'value'=>($model->pol != null ? $model->pol->CityName : ''),
									        ),
									        array(
									            'name'=>'PODCode',
									            'value'=>($model->pod != null ? $model->pod->CityName : ''),
									        ),
									        array(
									            'name'=>'OrderTotal',
									            'value'=>number_format($model->OrderTotal),
									        ),
									    ),
									)); ?>

									<hr>

									<div class="tabbable">
									    <ul class="nav nav-tabs">
									        <li class="active">
									            <a data-toggle="tab" href="#tab1">
									                <b>Detail Order</b>
									            </a>
									        </li>
									        <li>
									            <a data-toggle="tab" href="#tab2">
									                <b>PO</b>
									            </a>
									        </li>
									        <li>
									            <a data-toggle="tab" href="#tab3">
									                <b>Receive Bill</b>
									            </a>
									        </li>
									        <li>
									            <a data-toggle="tab" href="#tab4">
									                <b>PI</b>
									            </a>
									        </li>
									        <li>
									            <a data-toggle="tab" href="#tab5">
									                <b>Settlement</b>
									            </a>
									        </li>
									        <li>
									            <a data-toggle="tab" href="#tab6">
									                <b>Shipment</b>
									            </a>
									        </li>
									    </ul>

									    <div class="tab-content radius-bordered">
									        <div id="tab1" class="tab-pane in active">
									            <?php
									            $this->renderPartial('viewProduct', array(
									                'details'=>$model->details,
									            ));
									            ?>
									        </div>
									        <div id="tab2" class="tab-pane">
                              <?php if($model->PORefCode != ''){ ?>
                                  <a class="form-button btn btn-success" target="_blank" href="<?php echo Yii::app()->baseUrl.'/download/popdf/id/'.$model->OrderCode; ?>">Download PO</a>
                              <?php } ?>
                              <hr>
									            <?php $this->widget('zii.widgets.CDetailView', array(
									                'htmlOptions'=>array(
									                    'class'=>'detail-view table table-striped table-bordered table-hover'
									                ),
									                'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
									                'data'=>$model,
									                'attributes'=>array(
									                    'PORefCode',
									                    array(
									                        'name'=>'POStatusCode',
									                        'value'=>EnumStatus::getLabel($model->POStatusCode),
									                    ),
									                    array(
									                        'name'=>'PODate',
									                        'value'=>date("Y-m-d", strtotime($model->PODate)),
									                    ),
									                ),
									            )); ?>
									            <hr>
									            <table class="table table-bordered table-striped table-hover">
									                <tr>
									                    <th>Order Total</th>
									                    <td><?php echo number_format($model->OrderTotal); ?></td>
									                </tr>
									                <tr>
									                    <th>Seller Fee Amount (%)</th>
									                    <td>
									                        <?php
									                        $diskon = ($model->OrderTotal * $model->SupplierFee)/100;
									                        echo number_format($diskon).' ('.round($model->SupplierFee, 0).'%)';
									                        ?>
									                    </td>
									                </tr>
									                <tr>
									                    <th colspan="2"></th>
									                </tr>
									                <tr>
									                    <th>Summary Order</th>
									                    <td>
									                        <?php
									                        $total = $model->OrderTotal - $diskon;
									                        echo number_format($total);
									                        ?>
									                    </td>
									                </tr>
									            </table>
									        </div>
									        <div id="tab3" class="tab-pane">
									            <h3>Payment Terms</h3>
									            <table class="table table-bordered table-striped table-hover">
									                <thead>
									                <tr>
									                    <th>No</th>
									                    <th>Percentage</th>
									                    <th>Value</th>
									                    <th>Issue Date</th>
									                    <th>Expired Date</th>
									                </tr>
									                </thead>
									                <tbody>
									                <?php if($model->terms != null){
									                    $index = 0;
									                    foreach($model->terms as $term){ ?>
									                        <tr>
									                            <td><?php echo $term->NumberTermPayment; ?></td>
									                            <td><?php echo round($term->PercentageofPayment, 0); ?></td>
									                            <td><?php echo round($term->ValuePercentageOfPayment, 0); ?></td>
									                            <td><?php echo date('Y-m-d', strtotime($term->IssueDate)); ?></td>
									                            <td><?php echo date('Y-m-d', strtotime($term->ExpiredDate)); ?></td>
									                        </tr>
									                        <?php $index++; }
									                } ?>
									                </tbody>
									            </table>
									        </div>
									        <div id="tab4" class="tab-pane">
                              <?php if($model->PORefCode != '' and $model->BillCodeSupplier != ''){ ?>
                                  <a class="form-button btn btn-success" target="_blank" href="<?php echo Yii::app()->baseUrl.'/download/pipdf/id/'.$model->OrderCode; ?>">Download PI</a>
                                  <a class="form-button btn btn-success" target="_blank" href="<?php echo Yii::app()->baseUrl.'/download/ocipdf/id/'.$model->OrderCode; ?>">Download Order Confirmation Images</a>
                                  <a class="form-button btn btn-success" target="_blank" href="<?php echo Yii::app()->baseUrl.'/download/ocspdf/id/'.$model->OrderCode; ?>">Download Summary</a>
                              <?php } ?>
                              <hr>
									            <?php $this->widget('zii.widgets.CDetailView', array(
									                'htmlOptions'=>array(
									                    'class'=>'detail-view table table-striped table-bordered table-hover'
									                ),
									                'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
									                'data'=>$model,
									                'attributes'=>array(
									                    'BillCodeSupplier',
									                    array(
									                        'name'=>'BillSupplierDate',
									                        'value'=>date("Y-m-d", strtotime($model->BillSupplierDate)),
									                    ),
									                    'BillSupplierTermofPayment',
									                ),
									            )); ?>
									            <hr>
									            <table class="table table-bordered table-striped table-hover">
									                <tr>
									                    <th>Order Total</th>
									                    <td><?php echo number_format($model->OrderTotal); ?></td>
									                </tr>
									                <tr>
									                    <th>Buyer Fee</th>
									                    <td><?php echo number_format($model->BuyerFeeAmount).' ('.$model->BuyerFee.'%)'; ?></td>
									                </tr>
									                <tr>
									                    <th>Freight Fee</th>
									                    <td><?php echo number_format($model->FreightFee); ?></td>
									                </tr>
									                <tr>
									                    <th>Summary Charge</th>
									                    <td><?php echo number_format($totalSummary); ?></td>
									                </tr>
									                <tr>
									                    <th colspan="2"></th>
									                </tr>
									                <tr>
									                    <th>Summary Order</th>
									                    <td>
									                        <?php
									                        $total = ($model->OrderTotal - $model->BuyerFeeAmount) + $model->FreightFee + $totalSummary;
									                        echo number_format($total);
									                        ?>
									                    </td>
									                </tr>
									            </table>

									            <h3>Charge Detail</h3>
									            <table class="table table-bordered table-hover table-striped">
									                <tr>
									                    <th>Name</th>
									                    <th>Value</th>
									                </tr>
									                <?php foreach($chargeDetail as $detail){ ?>
									                    <tr>
									                        <td><?php echo ($detail->master != null ? $detail->master->Name : ''); ?></td>
									                        <td><?php echo round($detail->Value, 0); ?></td>
									                    </tr>
									                <?php } ?>
									            </table>
									        </div>
									        <div id="tab5" class="tab-pane">
									            <table class="table table-bordered table-striped table-hover">
									                <tr>
									                    <th>Order Total</th>
									                    <td><?php echo number_format($model->OrderTotal); ?></td>
									                </tr>
									                <tr>
									                    <th>Seller Fee %</th>
									                    <td><?php echo number_format($model->SupplierFee); ?></td>
									                </tr>
									                <tr>
									                    <th colspan="2"></th>
									                </tr>
									                <tr>
									                    <th>Summary Order</th>
									                    <td>
									                        <?php
									                        $diskon = ($model->OrderTotal * $model->SupplierFee)/100;
									                        $total = $model->OrderTotal - $diskon;
									                        echo number_format($total);
									                        ?>
									                    </td>
									                </tr>
									                <tr>
									                    <th>Total Paid</th>
									                    <td><?php echo number_format($totalPaid); ?></td>
									                </tr>
									                <tr>
									                    <th>Remain Payment</th>
									                    <td><?php echo number_format($total - $totalPaid); ?></td>
									                </tr>
									            </table>
									            <br>
									            <?php $this->widget('zii.widgets.grid.CGridView', array(
									                'id'=>'seller-bill-payment-grid',
									                'itemsCssClass'=>'table table-striped table-bordered table-hover',
																	'pager' => array(
																    'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
																    // 'maxButtonCount'=>4,
																    'header' => '',
																    'prevPageLabel' => 'Previous',
																    'nextPageLabel' => 'Next',
																    'firstPageLabel'=>'First',
																    'lastPageLabel'=>'Last',
																    'htmlOptions'=>array('style' => 'float : left'),
																  ),
									                'dataProvider'=>$sellerPayment->search(),
									                //'filter'=>$model,
									                'selectableRows'=>2,
									                'columns'=>array(
									                    'PaymentCode',
									                    'DatePayment',
									                    'SellerComercial',
									                    array(
									                        'name'=>'Ammount',
									                        'value'=>'number_format($data->Ammount)',
									                    ),
									                    array(
									                        'name'=>'BankCode',
									                        'value'=>'($data->bank != null ? $data->bank->BankName : "")',
									                    ),
									                    'Note',
									                ),
									            )); ?>
									        </div>
									        <div id="tab6" class="tab-pane">
                              <a class="form-button btn btn-success" target="_blank" href="<?php echo Yii::app()->baseUrl.'/download/plpdf/id/'.$model->OrderCode; ?>">Download Shipment List</a>
                              <hr>
									            <table class="table table-bordered table-striped table-hover">
									                <thead>
									                <tr>
									                    <th>Container</th>
									                    <th>Container Number</th>
									                    <th>Seal Code</th>
									                </tr>
									                </thead>
									                <tbody>
									                <?php foreach($model->blContainer as $bl){ ?>
									                    <tr>
									                        <td><?php echo $bl->container->ContainerName; ?></td>
									                        <td><?php echo $bl->NumberContainer; ?></td>
									                        <td><?php echo $bl->SealCode; ?></td>
									                    </tr>
									                <?php } ?>
									                </tbody>
									            </table>
									            <hr>
									            <?php $this->widget('zii.widgets.grid.CGridView', array(
									                'id'=>'order-bl-grid',
									                'itemsCssClass'=>'table table-striped table-bordered table-hover',
																	'pager' => array(
																    'cssFile' => Yii::app()->baseUrl . '/backend/css/bootstrap.css',
																    // 'maxButtonCount'=>4,
																    'header' => '',
																    'prevPageLabel' => 'Previous',
																    'nextPageLabel' => 'Next',
																    'firstPageLabel'=>'First',
																    'lastPageLabel'=>'Last',
																    'htmlOptions'=>array('style' => 'float : left'),
																  ),
									                'dataProvider'=>$orderBl->search(),
									                //'filter'=>$model,
									                'selectableRows'=>2,
									                'columns'=>array(
									                    'BLNumber',
									                    array(
									                        'name'=>'ContainerCode',
									                        'value'=>'($data->container != null ? $data->container->ContainerName : "")',
									                    ),
									                    'DocStatus',
									                    'CourierTrackNo',
									                    array(
									                        'name'=>'LoadingDate',
									                        'value'=>'date("Y-m-d", strtotime($data->LoadingDate))',
									                    ),
									                    array(
									                        'name'=>'EstimatedTimeDelivery',
									                        'value'=>'date("Y-m-d", strtotime($data->EstimatedTimeDelivery))',
									                    ),
									                    array(
									                        'name'=>'EstimatedTimeArive',
									                        'value'=>'date("Y-m-d", strtotime($data->EstimatedTimeArive))',
									                    ),
									                    array(
									                        'name'=>'BLReleasePayment',
									                        'value'=>'date("Y-m-d", strtotime($data->BLReleasePayment))',
									                    ),
									                ),
									            )); ?>
									        </div>
									    </div>
									</div>

								</div>
						</div>
				</div>
		</div>
</div>

<script type="text/javascript">
    $('#btn-back').click(function(){
        history.go(-1);
    });
</script>
