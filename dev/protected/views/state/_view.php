<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('StateCode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->StateCode), array('view', 'id'=>$data->StateCode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CountryCode')); ?>:</b>
	<?php echo CHtml::encode($data->CountryCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('StateName')); ?>:</b>
	<?php echo CHtml::encode($data->StateName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Note')); ?>:</b>
	<?php echo CHtml::encode($data->Note); ?>
	<br />


</div>