<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="widget">
			<div class="form-horizontal">
				<h5 class="row-title before-magenta"><i class="fa fa-search magenta"></i>Form Search</h5>
				<?php $form=$this->beginWidget('CActiveForm', array(
					'action'=>Yii::app()->createUrl($this->route),
					'method'=>'get',
				)); ?>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'StateCode'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'StateCode',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'CountryCode'); ?></label>
							<div class="col-sm-10">
								<?php echo $form->dropDownList($model,'CountryCode',
										CHtml::listData(Country::model()->findAll(), 'CountryCode', 'CountryName'),
										array('empty'=>'- Choose -', 'class'=>'select2','style'=>'width:100%')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'StateName'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'StateName',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'ShortName'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'ShortName',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>

					<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label no-padding-right"><?php echo $form->label($model,'Note'); ?></label>
							<div class="col-sm-10">
									<?php echo $form->textField($model,'Note',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
							</div>
					</div>



					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Search</button>
					</div>
				</div>

				<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</div>
