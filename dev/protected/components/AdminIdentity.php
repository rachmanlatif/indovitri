<?php

class AdminIdentity extends CUserIdentity
{
    public $username;
    public $password;
    private $_id;

    /**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
        $user = Users::model()->find('Username=?', array(
	        $this->username,
        ));

		if($user === null) {
			$this->errorCode=self::ERROR_USERNAME_INVALID;
        } else {
            if($user->Password != md5($this->password)) {
                $this->errorCode=self::ERROR_PASSWORD_INVALID;
            } else {
                $this->errorCode=self::ERROR_NONE;
                $this->_id = $user->IDUser;
            }
        }

		return !$this->errorCode;
	}

    public function getId()
    {
        return $this->_id;
    }
}