<?php

class ForgetPasswordForm extends CFormModel
{
	public $email;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			array('email', 'required'),
			array('email', 'email'),
			array('email', 'isEmailExists'),
		);
	}
	
	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function isEmailExists($attribute,$params)
	{
		// check 
		$connection = Yii::app()->db;
		$result = $connection->createCommand("
			SELECT * 
			FROM tbl_user
			WHERE email='". $this->email ."' AND status=". EnumStatus::ACTIVE ."
		")->queryRow();
		
		if ($result == null) {
			$this->addError('email','Email is not registered.');
		}
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'email'=>'Email',
		);
	}
}
