<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Riko Nagatama
 * Date: 12/8/11
 * Time: 11:20 AM
 * To change this template use File | Settings | File Templates.
 */
 
class REmail
{
	public static function getMailer()
	{
		$mailer = new YiiMail();
		if (SettingsHelper::getValue(EnumSettings::USE_SMTP) == 1)
		{
			$mailer->transportType = 'smtp';

			$mailer->transportOptions['host'] = SettingsHelper::getValue(EnumSettings::SMTP_HOST);
			$mailer->transportOptions['port'] = SettingsHelper::getValue(EnumSettings::SMTP_PORT);
			
			$emailEncryption = SettingsHelper::getValue(EnumSettings::EMAIL_ENCRYPTION);
			if ($emailEncryption != '')
				$mailer->transportOptions['encryption'] = $emailEncryption;
			
			if (SettingsHelper::getValue(EnumSettings::USE_AUTHORIZATION))
			{
				$mailer->transportOptions['username'] = SettingsHelper::getValue(EnumSettings::SMTP_USERNAME);
				$mailer->transportOptions['password'] = Yii::app()->securityManager->decrypt(base64_decode(SettingsHelper::getValue(EnumSettings::SMTP_PASSWORD)), EnumSettings::SMTP_PASSWORD);
			}
			
			/*
			$mailer->transportOptions = array(
				'host'=>'smtp.gmail.com',
				'username'=>'kanata1988@gmail.com',
				'password'=>'',
				'port'=>465,
				'encryption'=>'ssl'
			);*/
		}

		return $mailer;
	}
}