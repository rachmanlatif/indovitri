<?php

Class Redirect extends CWidget{
    
    public $assetsPath;
    public $url='calendar';
    public $param = '';
    public $method = 'POST';
    
    public function init(){
        if($this->assetsPath == null)
            $this->assetsPath = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets');
    }

    public function addConfig($configs)
    {
            foreach ($configs as $key=>$config) {
                    $this->_gridview_config[$key] = $config;
            }
    }

    protected function registerClientScript()
    {
            $cs = Yii::app()->clientScript;
            $cs->registerScriptFile($this->assetsPath .'/js/jquery.redirect.min.js');

            $originalScriptId = $scriptId = 'redirect';
            $num = 0;
            while($cs->isScriptRegistered($scriptId)) {
                    $num++;
                    $scriptId = $originalScriptId . $num;
            }
            if($this->param != ''){
                $cs->registerScript($scriptId, "
                        $().redirect('".$this->url."', {".$this->param."}, '".$this->method."');
                ");
            }
    }   

    public function run()
    {
            $this->registerClientScript();
    }
}
?>
