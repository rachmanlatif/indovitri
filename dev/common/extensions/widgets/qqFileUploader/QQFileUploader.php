<?php

class QQFileUploader extends CWidget
{
	public $id='qq-file-uploader';
	public $cssFile='';
	public $jsFile='';
	public $action;
	public $onSubmit;
	public $onProgress;
	public $onComplete;
	public $onCancel;
	public $options=array();
	
	public function init()
	{
		parent::init();

        $baseAssetUrl = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets');

		if ($this->cssFile == null) {
			$this->cssFile = $baseAssetUrl . '/style.css';
		}

		if ($this->jsFile == null) {
			$this->jsFile = $baseAssetUrl . '/fileuploader.js';
		}

		if (!isset($this->options['params']))
			$this->options['params'] = '{}';
		if (!isset($this->options['allowedExtensions']))
			$this->options['allowedExtensions'] = '[]';
		else
			if (is_array($this->options['allowedExtensions']))
				$this->options['allowedExtensions'] = '["'. implode('","', $this->options['allowedExtensions']) .'"]';

		if (!isset($this->options['sizeLimit']))
			$this->options['sizeLimit'] = '0';
		if (!isset($this->options['minSizeLimit']))
			$this->options['minSizeLimit'] = '0';
		if (!isset($this->options['debug']))
			$this->options['debug'] = 'false';
		if (!isset($this->options['multiple']))
			$this->options['multiple'] = 'false';

		if (!isset($this->onSubmit))
			$this->onSubmit = '';
		if (!isset($this->onProgress))
			$this->onProgress = '';
		if (!isset($this->onComplete))
			$this->onComplete = '';
		if (!isset($this->onCancel))
			$this->onCancel = '';
	}
	
	protected function registerClientScript()
	{
		$cs=Yii::app()->clientScript;
		$cs->registerCssFile($this->cssFile);
		$cs->registerScriptFile($this->jsFile);
		$cs->registerScript("qqFileUploader-". $this->id, "
			// in your app create uploader as soon as the DOM is ready
			// don't wait for the window to load
			$(document).ready(function(){
				var uploader = new qq.FileUploader({
					element: document.getElementById('". $this->id ."'),
					action: '". $this->action ."',
					params: ". $this->options['params'] .",
					// validation
					// ex. ['jpg', 'jpeg', 'png', 'gif'] or []
					allowedExtensions: ". $this->options['allowedExtensions'] .",
					// each file size limit in bytes
					// this option isn't supported in all browsers
					sizeLimit: ". $this->options['sizeLimit'] .", // max size
					minSizeLimit: ". $this->options['minSizeLimit'] .", // min size

					// set to true to output server response to console
					debug: ". $this->options['debug'] .",
					multiple: ". $this->options['multiple'] .",
					onSubmit: function(id, fileName){". $this->onSubmit ."},
					onProgress: function(id, fileName, loaded, total){". $this->onProgress ."},
					onComplete: function(id, fileName, responseJSON) {". $this->onComplete ."},
					onCancel: function(id, fileName){". $this->onCancel ."}
				});
			});
		");
	}

	public function run()
	{
		$this->registerClientScript();
		echo '<div id="'. $this->id .'"></div>';
	}
}