<?php

class TimePicker extends CWidget
{
	public $id='timePicker';
	public $dateTimeFormat = 'yy-mm-dd';
	public $timeFormat = 'hh:mm:ss';
	public $changeMonth = true;
	public $changeYear = true;
	public $baseScriptUrl;
	public $theme = 'smoothness';
	public $jqueryUi = true;
        public $addOn = FALSE;

	public function init()
	{
		if ($this->baseScriptUrl === null)
			$this->baseScriptUrl = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets');
	}
	public function addConfig($configs)
	{
		foreach ($configs as $key=>$config) {
			$this->_gridview_config[$key] = $config;
		}
	}

	protected function registerClientScript()
	{
            $cs = Yii::app()->clientScript;
            if ($this->jqueryUi) {
                    $cs->registerCssFile($this->baseScriptUrl . '/jquery-ui/'. $this->theme .'/jquery-ui-1.8.23.custom.css');
                    $cs->registerCssFile($this->baseScriptUrl . '/jquery-ui/'. $this->theme .'/jquery-ui-timepicker-addon.css');
                    $cs->registerScriptFile($this->baseScriptUrl . '/jquery-ui/jquery-ui-1.8.23.custom.min.js');
                    $cs->registerScriptFile($this->baseScriptUrl .'/jquery-ui/jquery-ui-timepicker-addon.js');
            }

            if($this->addOn == TRUE && $this->jqueryUi == FALSE){
                $cs->registerScriptFile($this->baseScriptUrl .'/jquery-ui/jquery-ui-timepicker-addon.js');
            }
                
		$originalScriptId = $scriptId = 'timePicker';
		$num = 0;
		while($cs->isScriptRegistered($scriptId)) {
			$num++;
			$scriptId = $originalScriptId . $num;
		}

		$cs->registerScript($scriptId, "
			date = $('#". $this->id ."').val();
			$('#". $this->id ."').timepicker({});

			$('#". $this->id ."').val(date);
		");
	}

	public function run()
	{
		$this->registerClientScript();
	}
}