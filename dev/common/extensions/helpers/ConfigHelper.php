<?php

class ConfigHelper
{
    public static function getValue($key)
    {
        $config = Config::model()->findByPk($key);
        if ($config != null)
            return $config->value;

        return '';
    }

    public static function setValue($key, $value)
    {
        $config = Config::model()->findByPk($key);
        if ($config == null) {
            $config = new Config();
        }

        $config->key = $key;
        $config->value = $value;

        $config->save();
    }
}