<?php

class FormButtonHelper
{
    /**
     * Create button List
     *
     * @param string $url
     * @return string
     */
    public static function index($url)
	{
        return CHtml::link(Yii::t('string', 'List'), $url, array('class'=>'btn btn-primary'));
	}

    /**
     * Create button Add
     *
     * @param string $url
     * @return string
     */
    public static function add($url)
	{
        return CHtml::link(Yii::t('string', 'Add'), $url, array('class'=>'btn btn-success'));
	}

	/**
	 * Create button Print
	 *
	 * @param string $url
	 * @return string
	 */
	public static function printRedeem($url)
	{
		return CHtml::link(Yii::t('string', 'Print'), $url,
			array(
				'class'=>'form-button print',
				'confirm' => 'Are you sure ?',
			)
		);
	}

    /**
     * Create button Update
     *
     * @param string $url
     * @return string
     */
    public static function update($url)
	{
        return CHtml::link(Yii::t('string', 'Update'), $url, array('class'=>'btn btn-warning'));
	}

    /**
     * Create button Activate
     *
     * @param string $url
     * @return string
     */
    public static function activate($url)
    {
        return CHtml::link(Yii::t('string', 'Activate'), $url, array('class'=>'form-button update'));
    }

    /**
     * Create button Resend
     *
     * @param string $url
     * @return string
     */
    public static function resend($url)
    {
        return CHtml::link(Yii::t('string', 'Resend Activation'), $url, array('class'=>'form-button update'));
    }

    /**
	 * Create button Update
	 *
	 * @param string $url
	 * @return string
	 */
	public static function changePassword($url)
	{
		return CHtml::link(Yii::t('string', 'Ubah Password'), $url, array('class'=>'btn btn-warning'));
	}

    /**
     * Create button View
     *
     * @param string $url
     * @return string
     */
    public static function view($url, $title='View')
	{
        return CHtml::link(Yii::t('string', $title), $url, array('class'=>'btn btn-info'));
	}

    /**
     * Create button Delete
     *
     * @param string $submit
     * @return string
     */
    public static function delete($submit)
	{
        return CHtml::linkButton(Yii::t('string', 'Delete'),
            array(
                'class' => 'btn btn-danger',
                'submit' => $submit,
                'confirm' => 'Are you sure you want to delete this item?',
            )
        );
	}

    /**
     * Create button Delete Selected
     *
     * @param string $grid_id
     * @param string $url
     * @return string
     */
    public static function deleteSelected($grid_id, $url)
	{
		return CHtml::link(Yii::t('string', 'Delete'), $url, array(
			'class' => 'btn btn-danger',
			'onclick' => "
			    if ($.fn.yiiGridView.getSelection('". $grid_id ."') != '') {
			        if (confirm('Are you sure?')) {
			            $.ajax({
			                type: 'post',
			                url: '". $url ."',
			                data: { ajax: true, ids: $.fn.yiiGridView.getSelection('". $grid_id ."') },
			                success: function(response) {
			                    if (response != '') {
			                        alert(response);
                                }

                                $.fn.yiiGridView.update('". $grid_id ."');
			                }
			            });
			        }
			    } else {
			        alert('No selection has been made.');
			    }

			    return false;
			",
		));
	}

    /**
     * Create button Publish
     *
     * @param string $grid_id
     * @param string $url
     * @return string
     */
    public static function publish($grid_id, $url)
	{
		return CHtml::link("Publish", $url, array(
			'class' => 'form-button publish',
			'onclick' => '
				if($.fn.yiiGridView.getSelection(\''. $grid_id .'\') != \'\') {'.
					CHtml::ajax(array(
						'type' => 'post',
						'url' => $url,
						'data' => 'js:{ajax:true, ids:$.fn.yiiGridView.getSelection(\''. $grid_id .'\')}',
						'success' => 'function(data) {
							$.fn.yiiGridView.update(\''. $grid_id .'\');
						}'
					)) .'
				} else {
					alert(\'No selection has been made.\');
				}

				return false;
			',
		));
	}

    /**
     * Create button Unpublish
     *
     * @param string $grid_id
     * @param string $url
     * @return string
     */
    public static function unpublish($grid_id, $url)
	{
		return CHtml::link("Unpublish", $url, array(
			'class' => 'form-button unpublish',
			'onclick' => '
				if($.fn.yiiGridView.getSelection(\''. $grid_id .'\') != \'\') {'.
					CHtml::ajax(array(
						'type' => 'post',
						'url' => $url,
						'data' => 'js:{ajax:true, ids:$.fn.yiiGridView.getSelection(\''. $grid_id .'\')}',
						'success' => 'function(data) {
							$.fn.yiiGridView.update(\''. $grid_id .'\');
						}'
					)) .'
				} else {
					alert(\'No selection has been made.\');
				}

				return false;
			',
		));
	}
}
