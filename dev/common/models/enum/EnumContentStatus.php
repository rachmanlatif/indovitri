<?php

class EnumContentStatus extends MyEnum
{
    const PENDING = 0;
    const RELEASE = 1;

    public static function getList()
    {
        return array(
            self::PENDING => 'Pending',
            self::RELEASE => 'Release',
        );
    }

    public static function getLabel($id)
    {
        $list = static::getList();
        if (isset($list[$id])) {
            return $list[$id];
        }

        return '-';
    }

}