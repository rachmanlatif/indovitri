<?php

class EnumOrder extends MyEnum
{
    const CANCEL = 0;
    const PROSPECT = 1;
    const ORDER = 2;
    const FINISH = 9;

    public static function getList()
    {
        return array(
            self::CANCEL => 'Cancel Order',
            self::PROSPECT => 'Prospect Order',
            self::ORDER => 'Order',
            self::FINISH => 'Finish Order',
        );
    }

    public static function getLabel($id)
    {
        $list = static::getList();
        if (isset($list[$id])) {
            return $list[$id];
        }

        return '-';
    }
}
