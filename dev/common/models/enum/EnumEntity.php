<?php

class EnumEntity extends MyEnum
{
    const CONSIGNE = 0;
    const NOTIFY = 1;
    const CUSTOMER = 2;
    const SELLER = 3;
    const AGENT = 4;
    const SHIPPER = 5;

    public static function getList()
    {
        return array(
            self::CONSIGNE => 'Consigne',
            self::NOTIFY => 'Notify Party',
            self::CUSTOMER => 'Customer',
            self::SELLER => 'Seller',
            self::AGENT => 'Agent',
            self::SHIPPER => 'Shipper',
        );
    }

    public static function getLabel($id)
    {
        $list = static::getList();
        if (isset($list[$id])) {
            return $list[$id];
        }

        return '-';
    }

}
