<?php

class EnumDecision extends MyEnum
{
    const YES= 1;
    const NO = 0;

    public static function getList()
    {
        return array(
            self::YES => 'Yes',
            self::NO => 'No',
        );
    }

    public static function getLabel($id)
    {
        $list = static::getList();
        if (isset($list[$id])) {
            return $list[$id];
        }

        return '-';
    }
}